// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   host_port.h
//
// PROJECT:     crisp host
//
// PLATFORM:    Crispbread/arm/linux
//
// Abstraction for host port communications for communicating with linux driver
// for cc1310 expansion module.
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __HOST_PORT_H__
#define __HOST_PORT_H__

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>

/////////////////////////////////////////////
// DEFINES
//

//
// Usable protocols
//
#define PROTOCOL_ICEC 1
#define PROTOCOL_ICET 2
#define PROTOCOL_ICEZ 3

//
// Usable regulatory settings
//
#define REGULATORY_EU_2000_299_EC1  1
#define REGULATORY_FCC              2

//
// Uninitialised
//
#define INVALID_FD					-1

#define MAX_CHANNEL( nRegulatory ) ( nRegulatory == REGULATORY_FCC ? 37 : 36 )

//
// Ranking
//
#define RANK_RANDOM_TIME            0xFF
#define RANK_MAX                    6

/////////////////////////////////////////////
// DATA TYPES
//

/////////////////////////////////////////////
// DATA DECLARATIONS
//

/////////////////////////////////////////////
// PROTOTYPES
//

//
// Open/close handle to device
//
int  host_port_open( 
            uint8_t   nCrispSlot,
            uint8_t   nListenChannels,
            uint8_t * pListenChannels,
            uint8_t   nRank,
            uint8_t   nSessionChannels,
            uint8_t * pSessionChannels,
            uint16_t  nProtocol,
            uint8_t * pRegulatory,
            int32_t   nFrequencyOffset
            );

void host_port_close( void );
int  host_port_get_fd( void );

//
// Change radio settings
//
int  host_port_get_regulatory( uint8_t *pRegulatory );
int  host_port_get_listen_channels( uint8_t *pCount, uint8_t *pChannels );
int  host_port_set_listen_channels( uint8_t nChannels, uint8_t *pChannels );
int  host_port_get_session_channels( uint8_t *pCount, uint8_t *pChannels );
int  host_port_set_session_channels( uint8_t nChannels, uint8_t *pChannels );

//
// Read/write
//
int  host_port_read( int * nLen, uint8_t * pBuffer );
int  host_port_write( int nLen, uint8_t * pPacket );
int  host_get_peer( uint32_t * pPeer );
int  host_get_last_rssi( int32_t *pLastRssi );
int  host_disconnect_session( void );

// Module detect
int  host_port_ping( void );

// Keep alive
int  host_port_ping_norsp( void );
int  host_port_refresh_protocol( );

// Frequency Adjustments
int  host_port_set_frequency_offset( int32_t nOffset );
int  host_port_get_frequency_offset( int32_t *pOffset );

/////////////////////////////////////////////
// CLASSES
//

#ifdef __cplusplus
    } // extern "C"
#endif

#endif // !__HOST_PORT_H__

//////////////////////////////////// EOF //////////////////////////////////////

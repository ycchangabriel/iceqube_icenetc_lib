// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   host_comms.h
//
// PROJECT:     crisp host
//
// PLATFORM:    Crispbread/arm/linux
//
// Abstraction for host to iceqube communications.
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __HOST_COMMS_H__
#define __HOST_COMMS_H__

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdbool.h>
#include "types.h"

/////////////////////////////////////////////
// DEFINES
//

//
// Permissible Modes for host_comms_set_mode
//
#define DEVICE_STATE_IDLE               0x00
#define DEVICE_STATE_ACTIVE             0x01
#define DEVICE_STATE_FIRMWARE           0x02

/////////////////////////////////////////////
// DATA TYPES
//

/////////////////////////////////////////////
// DATA DECLARATIONS
//

// Forward declaration
struct icec_net_protocol;

/////////////////////////////////////////////
// PROTOTYPES
//

//
// Initialisation
//
int host_comms_connect(
            uint8_t   nCrispSlot,
            uint8_t * pRegulatory,
            uint8_t   nRank,
            uint8_t   nListenChannels,
            uint8_t * pListenChannels,
            uint8_t   nSessionChannels,
            uint8_t * pSessionChannels,
            int32_t   nFrequencyOffset
            );

void    host_comms_set_internal_retries( uint8_t nRetries );
uint8_t host_comms_get_internal_retries( void );

void host_comms_close( void );

//
// Activity
//
int host_comms_wait_for_connect( uint32_t nTimeout, uint32_t *pPeer );
int host_comms_disconnect( void );

//
// Commands, when connected
//

// Device State Commands
int host_comms_set_mode( uint8_t nNewMode );
int host_comms_get_device_info( struct iceqube_device_state *pInfo );
int host_comms_get_post_info( struct iceqube_system_power_up_state *pInfo );
int host_comms_set_time( uint64_t nNewTime );
int host_comms_set_time_as_offset( int64_t nTimeOffset );
int host_comms_set_mac( uint32_t nNewQubeMac );
int host_comms_set_activation_mode( bool fClearDataOnActivate );
int host_comms_set_regulatory_region( struct iceqube_set_device_region *pRegion );

// Triggering configuration
int host_comms_get_trigger_config( struct trigger_config *pInfo );
int host_comms_set_trigger_config( struct trigger_config *pInfo );
int host_comms_get_timed_ex_mode( struct iceqube_timed_ex_mode *pConfig );
int host_comms_set_timed_ex_mode( struct iceqube_timed_ex_mode *pConfig );

// Data capture and interpretation configuration
int host_comms_get_data_capture_config( struct iceqube_data_capture_config *pConfig );
int host_comms_set_data_capture_config( struct iceqube_data_capture_config *pConfig );
int host_comms_get_data_interpreter_config( struct iceqube_data_interpreter_config *pConfig );
int host_comms_set_data_interpreter_config( struct iceqube_data_interpreter_config *pConfig );

// Data access
int host_comms_get_data( 
            struct iceqube_get_day  *pCount, 
            struct day_data         *pDayData 
            );

// Raw Sample Access
//
int host_comms_get_sample( uint8_t nUnits, struct iceqube_sample *pSample );
 
// Log access
int host_comms_get_log_filter( struct log_config *pConfig );
int host_comms_set_log_filter( struct log_config *pConfig );
int host_comms_get_log_entry_count( struct log_count *pConfig );
int host_comms_get_log( struct log_dump  *pConfig, struct log_entry *pEntries );

// Firmware
int host_comms_start_firmware_upload( struct iceqube_firmware_start *pInfo, struct iceqube_firmware_state *pState );
int host_comms_upload_firmware_chunk( struct iceqube_firmware_block *pBlock );
int host_comms_get_firmware_state( struct iceqube_firmware_state *pState );
int host_comms_get_bootloader_version( uint16_t *pVerMaj, uint16_t *pVerMin );

// Cap array tuning
int host_comms_get_cap_array_delta( int8_t *pDelta );
int host_comms_set_cap_array_delta( int8_t nDelta );

//
// Raw Packet I/O
//
int host_comms_send_command( 
            struct icec_net_protocol *pCmd,
            struct icec_net_protocol **pRsp
            );

int host_comms_send_command_ex(
            struct icec_net_protocol *pCmd,
            struct icec_net_protocol **pRsp,
            uint32_t                  nTimeoutS
            );

/////////////////////////////////////////////
// CLASSES
//


#ifdef __cplusplus
    } // extern "C"
#endif

#endif // !__HOST_PORT_H__

//////////////////////////////////// EOF //////////////////////////////////////

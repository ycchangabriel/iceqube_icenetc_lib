// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   host_port.h
//
// PROJECT:     crisp host
//
// PLATFORM:    Crispbread/arm/linux
//
// Abstraction for host port communications for communicating with linux driver
// for cc1310 expansion module.
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __ICEQUBE_ICENETC_H__
#define __ICEQUBE_ICENETC_H__

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////
// INCLUDE FILES
//

/////////////////////////////////////////////
// DEFINES
//

//
// Lib Version
//
#define ICEQUBE_ICENETC_VERSION_MAJ     1
#define ICEQUBE_ICENETC_VERSION_MIN     13

/////////////////////////////////////////////
// DATA TYPES
//

/////////////////////////////////////////////
// DATA DECLARATIONS
//

/////////////////////////////////////////////
// PROTOTYPES
//

/////////////////////////////////////////////
// CLASSES
//

#ifdef __cplusplus
    } // extern "C"
#endif

#endif // !__ICEQUBE_ICENETC_H__

//////////////////////////////////// EOF //////////////////////////////////////

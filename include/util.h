// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   util.h
//
// PROJECT:     crisp host
//
// PLATFORM:    Crispbread/arm/linux
//
// Utility function to support iceqube_icenetc_lib
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL          Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __UTIL_H__
#define __UTIL_H__

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <time.h>

/////////////////////////////////////////////
// DEFINES
//

/////////////////////////////////////////////
// DATA TYPES
//

/////////////////////////////////////////////
// DATA DECLARATIONS
//

/////////////////////////////////////////////
// PROTOTYPES
//

//
// Lightweight 64-bit time functions
//
uint64_t util_mktime( struct tm * tp );
void     util_localtime( const uint64_t nTime, struct tm *tmbuf );
uint64_t util_time( void );
int      util_tzoffset( void );

/////////////////////////////////////////////
// CLASSES
//

#ifdef __cplusplus  
    }  // Extern "C"
#endif
#endif // ! __UTIL_H__

//////////////////////////////////// EOF //////////////////////////////////////

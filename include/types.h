//FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   types.h
//
// PROJECT:     Ice Qube (Next Gen)
//
// PLATFORM:    Texas Instruments CC1310 (ARM Cortex-M3)
//
// Types and structures used on the IceQubes
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis 
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
// 
// This software is protected by national and international copyright and
// other laws. Unauthorised use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
// 
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef __TYPES_H__
#define __TYPES_H__

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////
// INCLUDE FILES
//

/////////////////////////////////////////////
// DEFINES
//

//
// Structure Versions
//
#define STRUCT_ICEQUBE_SET_DEVICE_MODE_VERSION          1
#define STRUCT_ICEQUBE_DEVICE_STATE_VERSION             1
#define STRUCT_ICEQUBE_SYSTEM_POWER_UP_STATE_VERSION    1
#define STRUCT_ICEQUBE_SET_DEVICE_TIME_VERSION          2
#define STRUCT_ICEQUBE_SET_DEVICE_MAC_VERSION           1
#define STRUCT_ICEQUBE_SET_ACTIVATE_MODE_VERSION        1
#define STRUCT_ICEQUBE_SET_DEVICE_REGION_VERSION        1
#define STRUCT_DAY_DATA_VERSION                         1
#define STRUCT_ICEQUBE_GET_DAY_VERSION                  1
#define STRUCT_LOG_CONFIG_VERSION                       1
#define STRUCT_LOG_COUNT_VERSION                        1
#define STRUCT_LOG_DUMP_VERSION                         1
#define STRUCT_ICEQUBE_TIMED_EX_MODE_VERSION            1
#define STRUCT_ICEQUBE_FIRMWARE_START_VERSION           1
#define STRUCT_ICEQUBE_FIRMWARE_BLOCK_VERSION           1
#define STRUCT_ICEQUBE_FIRMWARE_STATE_VERSION           1  
#define STRUCT_ICEQUBE_BOOTLOADER_VERSION_VERSION       1
#define STRUCT_TRIGGER_CONFIG_VERSION                   1
#define STRUCT_ICEQUBE_DATA_CAPTURE_CONFIG_VERSION      2
#define STRUCT_ICEQUBE_DATA_INTERPRETER_CONFIG_VERSION  2
#define STRUCT_ICEQUBE_GET_SAMPLE_VERSION               1
#define STRUCT_ICEQUBE_SAMPLE_VERSION                   1
#define STRUCT_ICEQUBE_CAPARRAYDELTA_VERSION            1

/////////////////////////////////////////////
// DATA TYPES 
//

struct iceqube_set_device_mode
{
    uint8_t     nStructVersion;
    uint8_t     nNewMode;
}__attribute__(( packed ));

struct iceqube_set_device_time_v1
{
    uint8_t     nStructVersion;
    uint64_t    nNewTime;
}__attribute__(( packed ));

struct iceqube_set_device_time_v2
{
    uint8_t     nStructVersion;
    uint64_t    nNewTime;
    int64_t     nTimeOffset;
}__attribute__(( packed ));

struct iceqube_set_device_mac
{
    uint8_t     nStructVersion;
    uint32_t    nNewMac;
}__attribute__(( packed ));

struct iceqube_set_activate_mode
{
    uint8_t     nStructVersion;
    uint8_t     nActivateMode;
}__attribute__(( packed ));

struct iceqube_set_device_region
{
    uint8_t     nStructVersion;
    uint8_t     nRegion;
}__attribute__(( packed ));

struct iceqube_get_day
{
    uint8_t     nStructVersion;
    int16_t     nDay;
}__attribute__(( packed ));


struct iceqube_get_sample
{
    uint8_t     nStructVersion;
    uint8_t     nSampleUnits;
}__attribute__( (packed) );

struct iceqube_sample
{
    uint8_t     nStructVersion;
    uint8_t     nSampleUnits;
    int16_t     nX;
    int16_t     nY;
    int16_t     nZ;
}__attribute__( (packed) );

//
// Structure containing device state information
// This structure is returned when 
//
struct iceqube_device_state
{ 
    uint8_t  nStructVersion;
    uint32_t nDeviceID;
    uint8_t  nDeviceState;
    uint16_t nFwVersion;
    uint64_t nDeviceTime;
    uint16_t nBatteryLevel; // mV
    uint8_t  nActiveDay;
    uint16_t nDays;
    uint8_t  nClearDataOnActivate;
    uint8_t  nRegulatory;
    uint8_t  nFlags;
}__attribute__(( packed ));

//
// Structure holding power-up self test states.
// This structure is stored in persistenet memeory after
// power-up at address PM_ADDR_POSTERR_RTC.
//
struct iceqube_system_power_up_state
{
    uint8_t  nStructVersion;
    int8_t   nRtc;              // RTC success / error code
    int8_t   nAccelerometer;    // Accelerometer success / error code
    int8_t   nExternflash;      // External Flash IC success / error code
    int8_t   nCircularBuffer;   // Circular buffer init success / error code

    uint8_t  nResetCause;       // Reset Cause
    uint32_t nStackAtReset;     // Reset stack pointer
} __attribute__(( packed ));

//
// Structure holding tiemd ex mode state
//
struct iceqube_timed_ex_mode
{
    uint8_t  nStructVersion;
    uint8_t  nTimedExModeEn;
} __attribute__(( packed ));

//
// State change
//
struct iceqube_state_change
{
    uint16_t nTOffsetL;
    uint8_t  nTOffsetH : 1;
    uint8_t  nState    : 7; // Recorded state
} __attribute__(( packed ));

//
// Day Data
//
struct day_data
{
    uint16_t                    nSize;              // 2   Size of struct
    uint32_t                    nStart;             // 4   Start time
    uint32_t                    nEnd;               // 4   End time
    uint16_t                    nSteps[96];         // 192 Step count per 15 minutes
    uint16_t                    nMotionIndex[96];   // 192 Motion index per 15 minutes
    uint8_t                     nChangeOvers;       // 1   Number of changeovers
    struct iceqube_state_change hChangeOver[57];    // 171 Changeovers lying->standing etc.
    uint16_t                    nFlags;             // 2   Flags (e.g. rollover)
    uint32_t                    nStartEx;           // 4   Upper 4 bytes of start time
    uint32_t                    nCrc;               // 4   CRC
} __attribute__(( packed ));                        // ---
                                                    // 576

//
// Start Firmware upload
//
struct iceqube_firmware_start
{
    uint8_t                  nStructVersion;
    uint16_t                 nVersionMajor;
    uint16_t                 nVersionMinor;
    uint32_t                 nAppSize;
    uint16_t                 nAppCrc;
} __attribute__(( packed ));

//
// Upload data block
//
struct iceqube_firmware_block
{
    uint8_t                  nStructVersion;
    uint32_t                 nBlockOffset;
    uint16_t                 nBlockLen;
    uint8_t                  hData[ 512 ];
} __attribute__((packed));

//
// Get firmware state
//
struct iceqube_firmware_state
{
    uint8_t                  nStructVersion;
    uint16_t                 nVersionMajor;
    uint16_t                 nVersionMinor;
    uint32_t                 nAppSize;
    uint16_t                 nAppCrc;
    uint32_t                 nNextExpectedOffset;
} __attribute__(( packed ));

struct iceqube_bootloader_version
{
    uint8_t                  nStructVersion;
    uint16_t                 nBootloaderVersionMajor;
    uint16_t                 nBootloaderVersionMinor;    
} __attribute__((packed));

struct log_config
{
    uint8_t                  nStructVersion;
    uint8_t                  nLogFilter;
} __attribute__((packed));

struct log_count 
{
    uint8_t                  nStructVersion;
    uint16_t                 nCount;
} __attribute__((packed));

struct log_dump
{
    uint8_t                  nStructVersion;
    uint16_t                 nOffset;
    uint16_t                 nCount;
} __attribute__((packed));

//
// Binary log entry
//
struct log_entry
{
    uint8_t  nValid;        // Event entry valid (0xFF = invalid)
    uint64_t nTimeStamp;    // 64-bit system timestamp
    uint8_t  nFilter;       // Type of event (filterable)
    uint16_t nEvent;        // Event ID
    uint32_t nData;         // Assocaited event data

} __attribute__((packed));

//
// Trigger Settings
//
struct trigger_config
{
    uint8_t  nStructVersion;

    uint8_t  nEnabledTriggers;      // TRIGGER_* mask of enabled triggers

    uint8_t  n2PulseFreq[3];        // 3 frequencies for 2 pulse trigger
    uint8_t  n3PulseFreq[3];        // 3 frequencies for 3 pulse trigger
    uint8_t  n2LongPulseFreq[3];    // 3 frequencies for long 2 pulse trigger
    uint8_t  nFieldFreq[3];         // 3 frequencies for field based trigger
    uint8_t  nTimedFreq[3];         // 3 frequencies for timed download

    uint32_t nConnectStandoffEn;    // TRIGGER_INDUCTOR_* mask of triggers that should have a stand-off
    uint32_t nConnectStandoffs[9];  // Stand-off times
    
    uint8_t  nTimedFlags;           // Flags for timed trigger
    uint32_t nTimedOffset;          // Timed download initial offset (if not enabled)
    uint32_t nTimedInterval;        // Timed download connection interval
    uint32_t nTimedExInterval;      // Timed download connection inverval when in ex mode
    uint32_t nTimedExDuration;      // Duration of ex mode

    // Non persistent data
    uint8_t  nStandoffIndex[ 4 ];   // Current Standoff Index
} __attribute__(( packed ));

//
// Data Interpretation Configuration
//
struct iceqube_data_capture_config
{
    uint8_t nStructVersion;
    uint8_t nSampleRate;        // Accelerometer Sample Rate
    uint8_t nSensitivity;
} __attribute__(( packed ));

//
// Data Interpretation Configuration
//
struct iceqube_data_interpreter_config
{
    uint8_t  nStructVersion;
    uint16_t nThreshStepG;
    uint8_t  nThresh_MTB_steps;
    uint16_t nThreshLying;
    uint16_t nThreshStanding;
    uint8_t  nCapG;
} __attribute__(( packed ));

//
// Cap array delta tuning
//
struct iceqube_cap_array_delta_config
{
    uint8_t nStructVersion;
    uint8_t nDeltaSet;
    int8_t  nCapArrayDelta;
} __attribute__(( packed ));

/////////////////////////////////////////////
// DATA DECLARATIONS
//

/////////////////////////////////////////////
// PROTOTYPES
//

/////////////////////////////////////////////
// CLASSES
//

#ifdef __cplusplus
    } // extern "C"
#endif

#endif // !__TYPES_H__
//////////////////////////////////// EOF //////////////////////////////////////

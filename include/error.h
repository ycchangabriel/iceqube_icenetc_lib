//FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   error.h
//
// PROJECT:     Ice Qube (Next Gen)
//
// PLATFORM:    Texas Instruments CC1310 (ARM Cortex-M3)
//
// IceQube error codes
// 
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis    
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-17 Peacock Technology Limited or its suppliers. 
// All rights reserved.
// 
// This software is protected by national and international copyright and
// other laws. Unauthorised use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
// 
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Limited.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef __ERROR_H__
#define __ERROR_H__

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>

/////////////////////////////////////////////
// DEFINES
//

//
// Generic
//
#define ERROR_OK                    ((int8_t) 0 )
#define ERROR_FAIL                  ((int8_t) 1 )
#define ERROR_TIMEOUT               ((int8_t) 2 )
#define ERROR_CRC                   ((int8_t) 3 )
#define ERROR_VERIFY                ((int8_t) 4 )
#define ERROR_INVALID               ((int8_t) 5 )
#define ERROR_NOT_AVAILABLE         ((int8_t) 6 )
#define ERROR_VERSION               ((int8_t) 7 )

//
// Accelerometer errors
//
#define ERROR_ACCEL_IC_UNSUPPORTED  ((int8_t) 10 )
#define ERROR_ACCEL_SELF_TEST_FAIL  ((int8_t) 11 )
#define ERROR_ACCEL_COMMS           ((int8_t) 12 )
#define ERROR_ACCEL_TIMEOUT         ((int8_t) 13 )

//
// RTC errors
//
#define ERROR_RTC_HW_FAILURE        ((int8_t) 15 )

//
// External Flash IC errors
//
#define ERROR_FLASH_IC_UNSUPPORTED  ((int8_t) 20 )
#define ERROR_FLASH_IC_MISSING      ((int8_t) 21 )
#define ERROR_FLASH_BUSY            ((int8_t) 22 )
#define ERROR_POST_FAILED           ((int8_t) 23 )

//
// RF Errors
//
#define ERROR_RF_INIT               ((int8_t) 30 )
#define ERROR_RF_NOT_OPEN           ((int8_t) 31 )
#define ERROR_RF_READ               ((int8_t) 32 )
#define ERROR_RF_WRITE              ((int8_t) 33 )
#define ERROR_RF_CALIBRATION        ((int8_t) 34 )
#define ERROR_RF_PCKT_SIZE          ((int8_t) 35 )
#define ERROR_RF_CCA_BUSY           ((int8_t) 36 )  
#define ERROR_RF_INVALID_CHANNEL    ((int8_t) 37 )
#define ERROR_RF_TEST_MODE          ((int8_t) 38 )
#define ERROR_RF_BREAK              ((int8_t) 39 )

#define ERROR_DLL_PARAM             ((int8_t) 40 )
#define ERROR_TL_RESET              ((int8_t) 41 ) // transport reset
#define ERROR_TL_NACK               ((int8_t) 42 )
#define ERROR_TL_SEQUENCE           ((int8_t) 43 )
#define ERROR_SL_BUSY               ((int8_t) 44 )
#define ERROR_SL_NOT_CONNECTED      ((int8_t) 45 )
#define ERROR_PEER_INVALID          ((int8_t) 46 )
#define ERROR_SL_SEQUENCE           ((int8_t) 47 )
#define ERROR_SL_RXBUFFER_TOO_SMALL ((int8_t) 48 )
#define ERROR_SL_NO_PROTOCOL        ((int8_t) 49 )

//
// JSON interpretor
//
#define ERROR_JSON_FORMAT           ((int8_t) 60 )
#define ERROR_JSON_INCOMPLETE       ((int8_t) 61 )
#define ERROR_JSON_INVALID_PARAM    ((int8_t) 62 )
#define ERROR_JSON_INVALID_VALUE    ((int8_t) 63 )
#define ERROR_JSON_BUSY             ((int8_t) 64 )
#define ERROR_JSON_FORMAT_TOO_LONG  ((int8_t) 65 )
#define ERROR_JSON_ACCEL_FAILED     ((int8_t) 66 )

//
// Bootloader errors
//
#define ERROR_NO_FIRMWARE           ((int8_t) 80 )
#define ERROR_INVALID_FIRMWARE_SIZE ((int8_t) 81 )
#define ERROR_FIRMWARE_SEQUENCE_ERR ((int8_t) 82 ) // Start not called
#define ERROR_FIRMWARE_ALIGN        ((int8_t) 83 ) // Alignment error
#define ERROR_DATA_CORRUPTION       ((int8_t) 84 ) // F/w Image data corrupt 
#define ERROR_INCOMPLETE		    ((int8_t) 85 ) // upload not complete yet

/////////////////////////////////////////////
// DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PROTOTYPES
//

/////////////////////////////////////////////
// PUBLIC DATA
//

#ifdef __cplusplus
    } // extern "C"
#endif

#endif // !__RTC_H__

//////////////////////////////////// EOF //////////////////////////////////////

# iceqube_icenetc_lib library project #

* The IceRobotics\iceqube_icenetc_lib git repository contains a simple API intended for crisp, wrapped into a library that permits iceqube compatible binary communications sessions over radio to an iceqube. The library is intended to drive a cc1310 crisp plug-in board.
* Build tools: yocto (ice_hub repositry)
* Deployed through yocto into ice_hub.

### Build platform configuration ###

* See ice_hub project.

### Building the iceqube_icenetc_lib library manually ###

Manual command line build is possible outwith yocto, where the following steps are required (linux). 
First the yocto built arm toolchain needs to be set as the build source (this only needs to be run once in each terminal window session):

```
#!dos
iceqube\iceqube_icenetc_lib> source /opt/poky/V.V.V/environment-setup-armv5e-poky-linux-gnueabi

```

Then the command to build the library is:
```
#!dos
iceqube\iceqube_icenetc_lib> make -f.\Makefile\makefile all

```

Options to 'clean' and 'rebuild' can be used instead of 'all' too. 

The built library can be found in the \iceqube\iceqube_icenetc_lib\bin\... folder, where two libraries are built:

...\bin\arm\libiceqube_icenetc.a		Static build of library
...\bin\arm\libiceqube_icenetc.so		Dynamic library

Associated elf, map and symbol files are also created. 

### Installing iceqube_icenetc libraries ###

* Copy .a & .so files into /usr/lib of a crispbread SD card image.

## Who do I talk to? ##

* Repo owner: Oliver Lewis (oliver@peacocktech.co.uk)
* Other community or team contact: Fraser Cadger
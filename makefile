##FILE HEADER##################################################################
## 
## FILE NAME:   makefile
##
## PROJECT:     iceqube_terminal
##
## Makefile in main folder. This was introduced to allow yocto
## builds without custom rules.
## 
#############################################
##
## CONTRIBUTORS:
##      OL      Oliver Lewis   
##
#############################################
##
## Copyright (c) 2016-2017 Peacock Tehnology Limited or its suppliers
## All rights reserved.
## 
## This software is protected by national and international copyright and
## other laws. Unauthorised use, storage, reproduction, transmission
## and/or distribution of this software, or any part of it, may result in
## civil and/or criminal proceedings.
## 
## This software is confidential and should not be disclosed, in whole or
## in part, to any person without the prior written permission of
## Peacock Tehnology Limited.
## 
###############################################################################

.PHONY: all clean rebuild install uninstall

#
# Rebuild (clean and build)
#
rebuild:
	make -f./make/makefile rebuild
	make -f./examples/makefile_examples rebuild

#
# Clean
#
clean:
	make -f./make/makefile clean
	make -f./examples/makefile_examples clean

#
# all
#
#
all:
	make -f./make/makefile all
	make -f./examples/makefile_examples all

#
# Install
#
install:
	make -f./make/makefile install
	make -f./examples/makefile_examples install

#
# uninstall
#
uninstall:
	make -f./make/makefile uninstall
	make -f./examples/makefile_examples uninstall
			
############################### EOF ###########################################

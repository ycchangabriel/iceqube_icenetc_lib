// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   host_port.c
//
// PROJECT:     crisp host
//
// PLATFORM:    Crispbread/arm/linux
//
// Implements host port communications for communicating with linux driver
// for cc1310 expansion module.
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include "host_port.h"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <syslog.h>

#include <time.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <string.h>

/////////////////////////////////////////////
// PRIVATE DEFINES
//

//
// IOCTLS for communication with driver
//
#define CC1310_MAGIC           'R'

#define IOCTL_PING             _IOR ( CC1310_MAGIC, 10, uint32_t )
#define IOCTL_GET_MAC          _IOR ( CC1310_MAGIC, 20, uint32_t )
#define IOCTL_SET_MAC          _IOW ( CC1310_MAGIC, 30, uint32_t )
#define IOCTL_GET_PROT         _IOR ( CC1310_MAGIC, 40, uint16_t )
#define IOCTL_SET_PROT         _IOW ( CC1310_MAGIC, 50, uint16_t )
#define IOCTL_GET_LISTEN_CHAN  _IOR ( CC1310_MAGIC, 60, uint32_t )
#define IOCTL_SET_LISTEN_CHAN  _IOW ( CC1310_MAGIC, 70, uint32_t )
#define IOCTL_GET_SESSION_CHAN _IOR ( CC1310_MAGIC, 80, uint32_t )
#define IOCTL_SET_SESSION_CHAN _IOW ( CC1310_MAGIC, 90, uint32_t )
#define IOCTL_CLOSE_SESSON     _IO  ( CC1310_MAGIC, 100 )
#define IOCTL_GET_VERSION      _IOR ( CC1310_MAGIC, 110, uint32_t )
#define IOCTL_GET_SESSION_PEER _IOR ( CC1310_MAGIC, 120, uint32_t )
#define IOCTL_GET_REGULATORY   _IOR ( CC1310_MAGIC, 130, uint32_t )
#define IOCTL_GET_LAST_RSSI    _IOR ( CC1310_MAGIC, 150, uint32_t )
#define IOCTL_SET_CCA_RANK     _IOW ( CC1310_MAGIC, 160, uint32_t )
#define IOCTL_GET_CCA_RANK     _IOR ( CC1310_MAGIC, 170, uint32_t )
#define IOCTL_PINGNORSP        _IO  ( CC1310_MAGIC, 180 )
#define IOCTL_SET_RF_OFFSET    _IOW ( CC1310_MAGIC, 190, uint32_t )
#define IOCTL_GET_RF_OFFSET    _IOR ( CC1310_MAGIC, 200, uint32_t )

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

int host_port_getmacaddress(
         uint8_t *szMacAddress, 
         uint8_t nBufLen 
         );

/////////////////////////////////////////////
// PUBLIC DATA
//

/////////////////////////////////////////////
// PRIVATE DATA
//

//
// Crisp module file desciptor
//
static int sg_nFd = INVALID_FD;
static uint16_t  sg_nProtocol = 0;

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_port_open_ex
//
// Opens a host port
//
// ARGUMENTS:
//  (IN)        uint8_t     Crisp slot (0-2, or 0xFF for auto)
//  (IN)        uint8_t     Number of listen channels
//  (IN)        uint8_t*    Array of listen channels
//  (IN)        uint8_t     Rank of hub for listen channels (must be unique)
//  (IN)        uint8_t     Number of session channels
//  (IN)        uint8_t*    Array of session channels
//  (IN)        uint16_t    Protocol
//  (OUT)       uint8_t*    Regulatory of crisp module (1=EU, 2=FCC)
//  (IN)        int32_t     Frequency offset in Hz
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_port_open(
        uint8_t   nCrispSlot,
        uint8_t   nListenChannels,
        uint8_t * pListenChannels,
        uint8_t   nRank,
        uint8_t   nSessionChannels,
        uint8_t * pSessionChannels,
        uint16_t  nProtocol,
        uint8_t * pRegulatory,
        int32_t   nFrequencyOffset
        )
{
    char     szFileName[50];
    uint8_t  i;
    uint8_t  nMin = 0;
    uint8_t  nMax = 2;
    uint16_t nNoProtocol = 0;
    uint32_t nData32; // Temporary value used to receive integers from the host
    uint8_t  szMacAddress[ 6 ];

    //
    // Close port, if it's already open
    //
	if ( sg_nFd != INVALID_FD )
	{
        close ( sg_nFd );
	}
	sg_nFd = INVALID_FD;
    sg_nProtocol = 0;

    //
    // Parse parameters
    //
    if (( nRank > RANK_MAX ) && ( nRank != RANK_RANDOM_TIME ))
    {
        return -EINVAL;
    }
    if (( nListenChannels < 1 ) || ( nListenChannels > 3 ))
	{
		return - EINVAL;
	}
    if (( nSessionChannels < 1 ) || ( nSessionChannels > 3 ))
	{
		return - EINVAL;
	}
    if (( nProtocol < 1 ) || ( nProtocol > 3 ))
	{
		return - EINVAL;
	}

    if ( nCrispSlot != 0xFF )
    {
        nMin = nCrispSlot;
        nMax = nCrispSlot;
    }

    //
    // Open device
    //
    for ( i = nMin; i <= nMax; i ++ )
    {
        sprintf ( szFileName, "/dev/crisp_cc1310_%d", i );
        sg_nFd = open ( szFileName, O_RDWR | O_NOCTTY | O_SYNC );

        if ( sg_nFd >= 0 )
		{
			break;
		}
    }
    if ( sg_nFd < 0 )
	{
        return - ENODEV;
	}

    //
    // Get something as a unique ID for radio MAC
    //
    if ( host_port_getmacaddress( szMacAddress, 6 ) != 0 )
    {
        do
        {
            struct timespec hTp;
        	clock_gettime( CLOCK_MONOTONIC, &hTp);
    	    hTp.tv_nsec /= 1000;
            nData32 = (uint32_t)( hTp.tv_nsec | time( NULL ) ) & 0xFFFFFFFF;
        }while(( nData32 == 0 ) || ( nData32 == 0xFFFFFFFF ));

        openlog( "iceqube_icenetc_lib", LOG_NDELAY, LOG_USER );
        syslog( LOG_INFO, "host_port_open: No eth0, cc1310 MAC set to rand 0x%X\n" );
        closelog( ); 
    }
    else
    {
        nData32 = szMacAddress[2];
        nData32 <<= 8;
        nData32 |= szMacAddress[3];
        nData32 <<= 8;
        nData32 |= szMacAddress[4];
        nData32 <<= 8;
        nData32 |= szMacAddress[5];        
        if ( i == 1 )
            nData32 ^= 0x80000000;
        if ( i == 2 )
            nData32 ^= 0x40000000;

        openlog( "iceqube_icenetc_lib", LOG_NDELAY, LOG_USER );
        syslog( LOG_INFO, "host_port_open: cc1310 MAC set to 0x%X\n" );
        closelog( ); 
    }

    // Reset active protocol 
    ioctl( sg_nFd, IOCTL_SET_PROT, &nNoProtocol );

    //
    // Set Host MAC address
    //
    if ( ioctl ( sg_nFd, IOCTL_SET_MAC, &nData32 ) < 0 )
    {
        host_port_close ( );
        return - ENODEV;
    }

    //
    // Set rank for multihub behaviour
    //
    nData32 = nRank;
    if ( ioctl ( sg_nFd, IOCTL_SET_CCA_RANK, &nData32 ) < 0 )
    {
        host_port_close ( );
        return - ENODEV;
    }

    //
    // Set frequency offset
    //
    if ( ioctl( sg_nFd, IOCTL_SET_RF_OFFSET, (uint32_t*) &nFrequencyOffset ) < 0 )   
    {
        host_port_close ( );
        return - ENODEV;
    }

    //
    // Verify version - will mean device is not correct version
    //
    if ( ioctl ( sg_nFd, IOCTL_GET_VERSION, &nData32 ) < 0 )
    {
        host_port_close ( );
        return - ENODEV;
    }

    //
    // Set regulatory
    //
    if ( pRegulatory )
    {
        if ( ioctl ( sg_nFd, IOCTL_GET_REGULATORY, &nData32 ) < 0 )
        {
            host_port_close ( );
            return - ENODEV;
        }
        *pRegulatory = (uint8_t)( nData32 & 0xFF );
    }

    //
    // Set channels
    //
    nData32   = nListenChannels;
    nData32 <<= 8;
    nData32  |= pListenChannels[0];
    nData32 <<= 8;
    nData32  |= ( nListenChannels >= 2 ) ? pListenChannels[1] : 0xFF;
    nData32 <<= 8;
    nData32  |= ( nListenChannels >= 3 ) ? pListenChannels[2] : 0xFF;
    if ( ioctl ( sg_nFd, IOCTL_SET_LISTEN_CHAN, &nData32 ) < 0 )
    {
        host_port_close ( );
        return - ENODEV;
    }

    nData32   = nSessionChannels;
    nData32 <<= 8;
    nData32  |= pSessionChannels[0];
    nData32 <<= 8;
    nData32  |= ( nSessionChannels >= 2 ) ? pSessionChannels[1] : 0xFF;
    nData32 <<= 8;
    nData32  |= ( nSessionChannels >= 3 ) ? pSessionChannels[2] : 0xFF;
    if ( ioctl ( sg_nFd, IOCTL_SET_SESSION_CHAN, &nData32 ) < 0 )
    {
        host_port_close ( );
        return - ENODEV;
    }

    //
    // Set active protocol
    //
    if ( ioctl ( sg_nFd, IOCTL_SET_PROT, &nProtocol ) < 0 )
    {
        host_port_close ( );
        return - ENODEV;
    }

    sg_nProtocol = nProtocol;
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_port_getmacaddress
//
// Gets eth0 MAC address
//
// ARGUMENTS:
//  (OUT)       uint8_t*    Buffer for MAC address (must be 6 bytes long)
//  (IN)        uint8_t     Size of buffer
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_port_getmacaddress( uint8_t *szMacAddress, uint8_t nBufLen )
{
	int			 nSocket;
	struct ifreq hBuffer;
	int			 nError;

	nSocket = socket(PF_INET, SOCK_DGRAM, 0);

	if ( nSocket == -1 )
		return -1;

	memset( &hBuffer, 0x00, sizeof( hBuffer ));
	strcpy( hBuffer.ifr_name, "eth0");

	nError =ioctl( nSocket, SIOCGIFHWADDR, &hBuffer);

	if ( nError < 0 )
		return -1;

	close( nSocket );
	
    if ( nBufLen )
    {
        memset( szMacAddress, 0, nBufLen );
        if ( nBufLen > 6 )
            nBufLen = 6;
        memcpy( szMacAddress,hBuffer.ifr_hwaddr.sa_data, nBufLen ); 
    }
	return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_port_get_regulatory
//
// Get current regulatory settings
//
// ARGUMENTS:
//  (OUT)       uint8_t*    Regulatory ( 1= EU, 2=FCC)
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_port_get_regulatory( uint8_t *pRegulatory )
{
    uint32_t nData32;
    //
    // Set regulatory
    //
    if ( ioctl ( sg_nFd, IOCTL_GET_REGULATORY, &nData32 ) < 0 )
        return - ENODEV;

    if ( pRegulatory )
        *pRegulatory = (uint8_t )( nData32 & 0xFF );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_port_get_listen_channels
//
// Current listen channels
//
// ARGUMENTS:
//  (OUT)       uint8_t*    Count of channels
//  (IN/OUT)    uint8_t*    Buffer for channel values (must be 3 bytes long)
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_port_get_listen_channels( uint8_t *pCount, uint8_t *pChannels )
{
    uint32_t nData32;
    uint8_t  nCount;
    if ( ioctl ( sg_nFd, IOCTL_GET_LISTEN_CHAN, &nData32 ) < 0 )
        return - ENODEV;

    nCount = ( nData32 >> 24 ) & 0xFF;
    if ( pCount )
        *pCount = nCount;
    
    if ( pChannels )
    {
        pChannels[ 0 ] = ( nData32 >> 16 ) & 0xFF;
        pChannels[ 1 ] = ( nData32 >> 8 ) & 0xFF;
        pChannels[ 2 ] = ( nData32 & 0xFF );
        if ( nCount < 3 )
            pChannels[ 2 ] = 0xFF;
        if ( nCount < 2 )
            pChannels[ 1 ] = 0xFF;
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_port_set_listen_channels
//
// Set new listen channels
//
// ARGUMENTS:
//  (IN)        uint8_t     Count of channels
//  (IN)        uint8_t     Array of channels 
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_port_set_listen_channels( uint8_t nChannels, uint8_t *pChannels )
{
    uint32_t nData32;

    nData32   = nChannels;
    nData32 <<= 8;
    nData32  |= pChannels[0];
    nData32 <<= 8;
    nData32  |= ( nChannels >= 2 ) ? pChannels[1] : 0xFF;
    nData32 <<= 8;
    nData32  |= ( nChannels >= 3 ) ? pChannels[2] : 0xFF;

    if ( ioctl ( sg_nFd, IOCTL_SET_LISTEN_CHAN, &nData32 ) < 0 )
        return - ENODEV;

    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_port_get_session_channels
//
// Current session channels
//
// ARGUMENTS:
//  (OUT)       uint8_t*    Count of channels
//  (IN/OUT)    uint8_t*    Buffer for channel values (must be 3 bytes long)
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_port_get_session_channels( uint8_t *pCount, uint8_t *pChannels )
{
    uint32_t nData32;
    uint8_t  nCount;

    if ( ioctl ( sg_nFd, IOCTL_GET_SESSION_CHAN, &nData32 ) < 0 )
        return - ENODEV;

    nCount = ( nData32 >> 24 ) & 0xFF;
    if ( pCount )
        *pCount = nCount;
    
    if ( pChannels )
    {
        pChannels[ 0 ] = ( nData32 >> 16 ) & 0xFF;
        pChannels[ 1 ] = ( nData32 >> 8 ) & 0xFF;
        pChannels[ 2 ] = ( nData32 & 0xFF );
        if ( nCount < 3 )
            pChannels[ 2 ] = 0xFF;
        if ( nCount < 2 )
            pChannels[ 1 ] = 0xFF;
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_port_set_session_channels
//
// Set new session channels
//
// ARGUMENTS:
//  (IN)        uint8_t     Count of channels
//  (IN)        uint8_t     Array of channels 
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_port_set_session_channels( uint8_t nChannels, uint8_t *pChannels )
{
    uint32_t nData32;

    nData32   = nChannels;
    nData32 <<= 8;
    nData32  |= pChannels[0];
    nData32 <<= 8;
    nData32  |= ( nChannels >= 2 ) ? pChannels[1] : 0xFF;
    nData32 <<= 8;
    nData32  |= ( nChannels >= 3 ) ? pChannels[2] : 0xFF;
    if ( ioctl ( sg_nFd, IOCTL_SET_SESSION_CHAN, &nData32 ) < 0 )
        return - ENODEV;
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_port_get_fd
//
// Returns the file descriptor of the device for use with file operations
// (i.e. poll/select/ioctl/read/write)
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int     File descriptor
//
///////////////////////////////////////////////////////////////////////////////
int host_port_get_fd( void )
{
    return sg_nFd;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_port_read
//
// Reads received data from the crisp module. This is protocol specific data.
// Select should be used to see if data is available on the port, before 
// reading.
//
// ARGUMENTS:
//  (IN/OUT)    int*        Size of buffer/amount read
//  (OUT)       uint8_t*    Buffer with read data.
//
// RETURNS:
//              int         0 on success, else -1 (use errno for code)
//
///////////////////////////////////////////////////////////////////////////////
int host_port_read( int * nLen, uint8_t * pBuffer )
{
    int    nRead;
    size_t nToRead = *nLen;

    nRead = read ( sg_nFd, pBuffer, nToRead );

    if ( nRead >= 0 )
    {
        *nLen = nRead;
        return nRead;
    }
    *nLen = 0;
    return nRead;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_port_write
//
// Writes data to a peer over rf using the current protocol. Device should be
// connected before calling write.
//
// ARGUMENTS:
//  (IN)        int         Amount of data to write
//  (IN)        uint8_t*    Buffer with the data
//
// RETURNS:
//              int         0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_port_write( int nLen, uint8_t * pPacket )
{
    while ( nLen )
    {
        int nWritten;

        nWritten = write ( sg_nFd, pPacket, nLen );

        if ( nWritten < 0 )
		{
			return -errno;
		}
        if ( nWritten == 0 )
		{
			return - ETIME;
		}

        nLen    -= nWritten;
        pPacket += nWritten;
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_disconnect_session
//
// Disconnects a sessio with a peer. This will allow a new peer to connect.
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int     0 on success, else -1 (use errno for code)
//
///////////////////////////////////////////////////////////////////////////////
int host_disconnect_session( void )
{
    int nRetry = 3;
    int nErr;
    while( nRetry-- )
    {
        nErr = ioctl ( sg_nFd, IOCTL_CLOSE_SESSON, NULL );
        if (( nErr == 0 ) || ( nErr == -ECONNRESET ))
            break;
    }
    return nErr;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_get_peer
//
// If connected, returns the peer device ID/MAC
//
// ARGUMENTS:
//  (OUT)       uint32_t*       Peer ID
//
// RETURNS:
//              int             0 on success, else -1 (see errno for code)
//
///////////////////////////////////////////////////////////////////////////////
int host_get_peer( uint32_t * pPeer )
{
    return ioctl ( sg_nFd, IOCTL_GET_SESSION_PEER, pPeer );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_get_last_rssi
//
// If connected, returns the receive signal strength of the last communication
// with the host.
//
// ARGUMENTS:
//  (OUT)       int32_t*       RSSI value
//
// RETURNS:
//              int             0 on success, else -1 (see errno for code)
//
///////////////////////////////////////////////////////////////////////////////
int host_get_last_rssi( int32_t *pLastRssi )
{
    return ioctl( sg_nFd, IOCTL_GET_LAST_RSSI, (uint32_t*) pLastRssi );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_port_close
//
// Closes the connection to the linux driver
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void host_port_close( void )
{
    if ( sg_nFd >= 0 )
    {
        uint16_t nProtocol = 0;

        // No more protocol requirements
        ioctl( sg_nFd, IOCTL_SET_PROT, &nProtocol );

        // Close the file descriptor
        close( sg_nFd );
    }
	sg_nFd = INVALID_FD;
    sg_nProtocol = 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_port_ping
//
// Issues a ping to the cc1310 module. No function, but can be used for
// keep-alives
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int              0 on success, else -1 (see errno for code)
//
///////////////////////////////////////////////////////////////////////////////
int host_port_ping( )
{
    return ioctl( sg_nFd, IOCTL_PING, NULL );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_port_ping_norsp
//
// Issues a ping to the cc1310 module, but doesn't expect a response. Should
// be used for keepalives.
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int              0 on success, else -1 (see errno for code)
//
///////////////////////////////////////////////////////////////////////////////
int host_port_ping_norsp( )
{
    return ioctl( sg_nFd, IOCTL_PINGNORSP, NULL );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_port_refresh_protocol
//
// Can be used to re-fresh the active protocol after a keep-alive timeout.
// If this is sent before wait-for-connection, then there should be no need
// to call open_port again.
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int              0 on success, else -1 (see errno for code)
//
///////////////////////////////////////////////////////////////////////////////
int host_port_refresh_protocol( )
{
    return ioctl( sg_nFd, IOCTL_SET_PROT, &sg_nProtocol );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_port_set_frequency_offset
//
// Sets a constant frequency offset for the radio centre for every channel 
//
// ARGUMENTS:
//  (IN)        int32_t         Offset in Hz. Can go from -50000 to +50000.
//
// RETURNS:
//              int              0 on success, else -1 (see errno for code)
//
///////////////////////////////////////////////////////////////////////////////
int  host_port_set_frequency_offset( int32_t nOffset )
{
    return ioctl( sg_nFd, IOCTL_SET_RF_OFFSET, (uint32_t*) &nOffset );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_port_get_frequency_offset
//
// Returns frequency offset for the radio centre for every channel 
//
// ARGUMENTS:
//  (OUT)       int32_t*        Offset in Hz.
//
// RETURNS:
//              int              0 on success, else -1 (see errno for code)
//
///////////////////////////////////////////////////////////////////////////////
int  host_port_get_frequency_offset( int32_t *pOffset )
{
    return ioctl( sg_nFd, IOCTL_GET_RF_OFFSET, (uint32_t*) pOffset );
}
//////////////////////////////////// EOF //////////////////////////////////////

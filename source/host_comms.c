// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   host_comms.c
//
// PROJECT:     crisp host
//
// PLATFORM:    Crispbread/arm/linux
//
// Implements of host to iceqube communications.
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include "host_comms.h"
#include "host_port.h"
#include "types.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>

/////////////////////////////////////////////
// PRIVATE DEFINES
//

// #define DEBUG

#ifdef DEBUG
#define dbg_printf      fprintf
#else
#define dbg_printf
#endif

#define DEFAULT_RETRIES         4

//
// Commands                                           host sends                              IceQube Rsp  
//

//
// Config/Monitoring
//
#define ICEC_NET_CMD_MODE                     0x03 // struct icec_net_set_mode                struct icec_net_rsp                     
#define ICEC_NET_CMD_GET_DEVICE_INFO          0x05 // struct icec_net_protocol                struct icec_device_state                
#define ICEC_NET_CMD_GET_POST_INFO            0x06 // struct icec_net_protocol                struct icec_device_post_state           
#define ICEC_NET_CMD_SET_TIME                 0x08 // struct icec_net_set_time_protocol       struct icec_net_rsp                     
#define ICEC_NET_CMD_SET_MAC                  0x0A // struct icec_net_set_mac                 struct icec_net_rsp                     
#define ICEC_NET_CMD_SET_ACTIVATE_MODE        0x0B // struct icec_net_set_activate_mode       struct icec_net_rsp             
#define ICEC_NET_CMD_GET_TRIGGER_CONF         0x0C // struct icec_net_protocol                struct icec_net_trigger_config          
#define ICEC_NET_CMD_SET_TRIGGER_CONF         0x0D // struct icec_net_trigger_config          struct icec_net_rsp                     
#define ICEC_NET_CMD_GET_DATA_CAPTURE_CONF    0x0E // struct icec_net_protocol                struct icec_net_data_capture_config     
#define ICEC_NET_CMD_SET_DATA_CAPTURE_CONF    0x0F // struct icec_net_data_capture_config     struct icec_net_rsp                     
#define ICEC_NET_CMD_GET_DATA_INTERPRET_CONF  0x10 // struct icec_net_protocol                struct icec_net_data_interpretor_config 
#define ICEC_NET_CMD_SET_DATA_INTERPRET_CONF  0x11 // struct icec_net_data_interpretor_config struct icec_net_rsp                     
#define ICEC_NET_CMD_GET_TIMED_EX_MODE        0x12 // struct icec_net_protocol                struct icec_net_timed_ex_mode           
#define ICEC_NET_CMD_SET_TIMED_EX_MODE        0x13 // struct icec_net_timed_ex_mode           struct icec_net_rsp                     
#define ICEC_NET_CMD_SET_REGULATORY           0x14 // struct icec_net_protocol                struct icec_net_rsp                     

//
// Logs
//
#define ICEC_NET_CMD_GET_LOG_FILTER           0x15 // struct icec_net_protocol                struct icec_log_config                 
#define ICEC_NET_CMD_SET_LOG_FILTER           0x16 // struct icec_log_config                  struct icec_net_rsp                    
#define ICEC_NET_CMD_GET_LOG_ENTRIES          0x17 // struct icec_net_protocol                struct icec_log_entry_count            
#define ICEC_NET_CMD_DUMP_LOG                 0x18 // struct icec_log_dump                    struct icec_log_dump_rsp               

//
// Firmware
//
#define ICEC_NET_CMD_UPLOAD_FIRMWARE_START    0x19 // struct icec_firmware_start              struct icec_firmware_state         
#define ICEC_NET_CMD_UPLOAD_FIRMWARE          0x20 // struct icec_firmware_block              struct icec_net_rsp                
#define ICEC_NET_CMD_FIRMWARE_STATE           0x21 // struct icec_net_protocol                struct icec_firmware_state          
#define ICEC_NET_CMD_GET_BOOTLOADER_VERSION   0x22 // struct icec_net_protocol                struct icec_bootloader_version     

//
// Raw sample access
//
#define ICEC_NET_CMD_GET_DATA_SAMPLE          0x23 // struct icec_get_sample                  struct icec_sample                      

// 
// Cap array tuning
//
#define ICEC_NET_CMD_SET_CAP_ARRAY_DELTA      0x25 // struct icec_net_cap_array_delta         struct icec_net_rsp                     
#define ICEC_NET_CMD_GET_CAP_ARRAY_DELTA      0x26 // struct icec_net_protocol                struct icec_net_cap_array_delta         

//
// Data access
//
#define ICEC_NET_CMD_GET_DATA                 0x30 // struct icec_get_day                     struct icec_day_data                   

//
// Device Error Codes
//

//
// Generic
//
#define ERROR_OK                    ((int8_t)   0 )
#define ERROR_FAIL                  ((int8_t)  -1 )
#define ERROR_TIMEOUT               ((int8_t)  -2 )
#define ERROR_CRC                   ((int8_t)  -3 )
#define ERROR_VERIFY                ((int8_t)  -4 )
#define ERROR_INVALID               ((int8_t)  -5 )
#define ERROR_NOT_AVAILABLE         ((int8_t)  -6 )
#define ERROR_VERSION               ((int8_t)  -7 )

//
// Accelerometer errors
//
#define ERROR_ACCEL_IC_UNSUPPORTED  ((int8_t) -10 )
#define ERROR_ACCEL_SELF_TEST_FAIL  ((int8_t) -11 )
#define ERROR_ACCEL_COMMS           ((int8_t) -12 )

//
// RTC errors
//
#define ERROR_RTC_HW_FAILURE        ((int8_t) -15 )

//
// External Flash IC errors
//
#define ERROR_FLASH_IC_UNSUPPORTED  ((int8_t) -20 )
#define ERROR_FLASH_IC_MISSING      ((int8_t) -21 )
#define ERROR_FLASH_BUSY            ((int8_t) -22 )
#define ERROR_POST_FAILED           ((int8_t) -23 )

//
// RF Errors
//
#define ERROR_RF_INIT               ((int8_t) -30 )
#define ERROR_RF_NOT_OPEN           ((int8_t) -31 )
#define ERROR_RF_READ               ((int8_t) -32 )
#define ERROR_RF_WRITE              ((int8_t) -33 )
#define ERROR_RF_CALIBRATION        ((int8_t) -34 )
#define ERROR_RF_PCKT_SIZE          ((int8_t) -35 )
#define ERROR_RF_CCA_BUSY           ((int8_t) -36 )  
#define ERROR_RF_INVALID_CHANNEL    ((int8_t) -37 )
#define ERROR_RF_TEST_MODE          ((int8_t) -38 )
#define ERROR_RF_BREAK              ((int8_t) -39 )

#define ERROR_DLL_PARAM             ((int8_t) -40 )
#define ERROR_TL_RESET              ((int8_t) -41 ) // transport reset
#define ERROR_TL_NACK               ((int8_t) -42 )
#define ERROR_TL_SEQUENCE           ((int8_t) -43 )
#define ERROR_SL_BUSY               ((int8_t) -44 )
#define ERROR_SL_NOT_CONNECTED      ((int8_t) -45 )
#define ERROR_PEER_INVALID          ((int8_t) -46 )
#define ERROR_SL_SEQUENCE           ((int8_t) -47 )
#define ERROR_SL_RXBUFFER_TOO_SMALL ((int8_t) -48 )
#define ERROR_SL_NO_PROTOCOL        ((int8_t) -49 )

//
// JSON interpretor
//
#define ERROR_JSON_FORMAT           ((int8_t) -60 )
#define ERROR_JSON_INCOMPLETE       ((int8_t) -61 )
#define ERROR_JSON_INVALID_PARAM    ((int8_t) -62 )
#define ERROR_JSON_INVALID_VALUE    ((int8_t) -63 )
#define ERROR_JSON_BUSY             ((int8_t) -64 )
#define ERROR_JSON_FORMAT_TOO_LONG  ((int8_t) -65 )
#define ERROR_JSON_ACCEL_FAILED     ((int8_t) -66 )

//
// Bootloader errors
//
#define ERROR_NO_FIRMWARE           ((int8_t) -80 )
#define ERROR_INVALID_FIRMWARE_SIZE ((int8_t) -81 )
#define ERROR_FIRMWARE_SEQUENCE_ERR ((int8_t) -82 ) // Start not called
#define ERROR_FIRMWARE_ALIGN        ((int8_t) -83 ) // Alignment error
#define ERROR_DATA_CORRUPTION       ((int8_t) -84 ) // F/w Image data corrupt 
#define ERROR_INCOMPLETE		    ((int8_t) -85 ) // upload not complete yet


/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

//
// struct icec_net_protocol 
//   Binary IceC-net command header. Where commands have no associated data, 
//   this structure is assumed.
//
struct icec_net_protocol
{
    uint8_t  nCmd;      // Command
    uint16_t nLength;   // Length of data following this structure
} __attribute__(( packed ));

//
// struct icec_net_set_mode 
//   Command structure to set the mode ICEC_NET_CMD_MODE 
//
struct icec_net_set_mode
{
    struct icec_net_protocol       hHeader;
    struct iceqube_set_device_mode hMode;
} __attribute__(( packed ));

//
// struct icec_net_set_time_protocol_v1
//   Commands structure to set time ICEC_NET_CMD_SET_TIME
//
struct icec_net_set_time_protocol_v1
{
    struct icec_net_protocol          hHeader;
    struct iceqube_set_device_time_v1 hTimeInfo;
} __attribute__(( packed ));

//
// struct icec_net_set_time_protocol_v2
//   Commands structure to set time ICEC_NET_CMD_SET_TIME
//
struct icec_net_set_time_protocol_v2
{
    struct icec_net_protocol          hHeader;
    struct iceqube_set_device_time_v2 hTimeInfo;
} __attribute__( ( packed ) );

//
// struct icec_net_set_mac
//   Command structure to set device mac ICEC_NET_CMD_SET_MAC
//
struct icec_net_set_mac
{
    struct icec_net_protocol       hHeader;
    struct iceqube_set_device_mac  hMacInfo;
} __attribute__(( packed ));

//
// struct icec_net_set_activate_mode
//    Command structure to set activation mode ICEC_NET_CMD_SET_ACTIVETE_MODE
//
struct icec_net_set_activate_mode
{
    struct icec_net_protocol         hHeader;
    struct iceqube_set_activate_mode hMode;
} __attribute__(( packed ));

//
// struct icec_net_set_device_region
//   Command structure to set device mac ICEC_NET_CMD_SET_REGULATORY
//
struct icec_net_set_device_region
{
    struct icec_net_protocol         hHeader;
    struct iceqube_set_device_region hRegionInfo;
} __attribute__(( packed ));

//
// struct icec_net_trigger_config
//   Command structure to exchange trigger configuration. Used by
//   both ICEC_NET_CMD_GET_TRIGGER_CONF and ICEC_NET_CMD_SET_TRIGGER_CONF
//
struct icec_net_trigger_config
{
    struct icec_net_protocol       hHeader;
    struct trigger_config          hConfig;
} __attribute__(( packed ));

//
// struct icec_net_data_capture_config
//   Command structure to exchange data capture configuration. Used by
//   ICEC_NET_CMD_GET_DATA_CAPTURE_CONF and ICEC_NET_CMD_SET_DATA_CAPTURE_CONF
//
struct icec_net_data_capture_config
{
    struct icec_net_protocol            hHeader;
    struct iceqube_data_capture_config  hConfig;
} __attribute__(( packed ));

//
// struct icec_net_data_interpretor_config
//   Command structure to exchange data interpetation config. Used by 
//   ICEC_NET_CMD_GET_DATE_INTERPRET_CONF and ICEC_NET_CMD_SET_DATE_INTERPRET_CONF
//
struct icec_net_data_interpretor_config
{
    struct icec_net_protocol               hHeader;
    struct iceqube_data_interpreter_config hConfig;
} __attribute__(( packed ));

//
// struct icec_net_timed_ex_mode
//   Command structure to get/set timed download ex mode. Used by
//   ICEC_NET_CMD_GET_TIMED_EX_MODE and ICEC_NET_CMD_SET_TIMED_EX_MODE 
//
struct icec_net_timed_ex_mode
{
    struct icec_net_protocol               hHeader;
    struct iceqube_timed_ex_mode           hMode;
} __attribute__(( packed ));

//
// Command to start new firmware upload
//
struct icec_firmware_start
{
    struct icec_net_protocol      hHeader;
    struct iceqube_firmware_start hData;
} __attribute__(( packed ));

//
// Command to provide new firmware data packet
//
struct icec_firmware_block
{
    struct icec_net_protocol      hHeader;
    struct iceqube_firmware_block hData;
} __attribute__(( packed ));

//
// Command structure for ICEC_NET_CMD_GET_DATA_SAMPLE
// used to get raw sample packet
//
struct icec_get_sample
{
    struct icec_net_protocol      hHeader;
    struct iceqube_get_sample     hData;
} __attribute__( (packed) );

//
// Response to ICEC_NET_CMD_GET_DATA_SAMPLE
//
struct icec_sample
{
    struct icec_net_protocol      hHeader;
    struct iceqube_sample         hData;
} __attribute__( (packed) );

//
// Response to ICEC_NET_CMD_FIRMWARE_STATE
//
struct icec_firmware_state
{
    struct icec_net_protocol hHeader;
    struct iceqube_firmware_state hState;
} __attribute__(( packed ));

//
// struct icec_log_config
//   Command structure to exchange log configuration. Used by
//   both ICEC_NET_CMD_SET_LOG_FILTER and ICEC_NET_CMD_GET_LOG_FILTER
//
struct icec_log_config
{
    struct icec_net_protocol   hHeader;
    struct log_config          hConfig;
} __attribute__(( packed ));

//
// struct icec_log_entry_count
//   Command structure to get log entry count. Used by
//   ICEC_NET_CMD_GET_LOG_ENTRIES
//
struct icec_log_entry_count
{
    struct icec_net_protocol   hHeader;
    struct log_count           hConfig;
} __attribute__(( packed ));

//
// struct icec_log_entry_count
//   Command structure to get log entry count. Used by
//   ICEC_NET_CMD_DUMP_LOG
//
struct icec_log_dump
{
    struct icec_net_protocol   hHeader;
    struct log_dump            hConfig;
} __attribute__(( packed ));

struct icec_log_dump_rsp
{
    struct icec_net_protocol   hHeader;
    struct log_dump            hConfig;
    struct log_entry           hLogs[30]; // max number is 30
} __attribute__(( packed ));

//
// struct icec_net_rsp
//   Basic response, when no data is sent to the host. Returns just and error 
//   code. See error.h.
//
struct icec_net_rsp
{
    struct icec_net_protocol hHeader;
    int8_t nErrorCode;
} __attribute__(( packed ));

//
// struct icec_device_state
//   Response to ICEC_NET_CMD_GET_DEVICE_INFO
//
struct icec_device_state
{ 
    struct icec_net_protocol    hHeader;
    struct iceqube_device_state hDeviceState;
}__attribute__(( packed ));

//
// struct icec_device_post_state
//   Response to ICEC_NET_CMD_GET_POST_INFO
//
struct icec_device_post_state
{
    struct icec_net_protocol hHeader;
    struct iceqube_system_power_up_state hPostState;
}__attribute__(( packed ));

//
// struct icec_get_day
//   Command structure to query day data. Used with
//   ICEC_NET_CMD_GET_DATA.
//
struct icec_get_day
{
    struct icec_net_protocol hHeader;
    struct iceqube_get_day   hQuery;
}__attribute__(( packed ));

//
// struct icec_day_data
//   Response to ICEC_NET_CMD_GET_DATA
//
struct icec_day_data
{
    struct icec_net_protocol hHeader;
    uint8_t                  nDayDataVersion;
    struct day_data          hDay;
}__attribute__(( packed ));

//
// struct icec_bootloader_version
//   Response to ICEC_NET_CMD_GET_BOOTLOADER_VERSION
//
struct icec_bootloader_version
{
    struct icec_net_protocol            hHeader;
    struct iceqube_bootloader_version   hVersion;
}__attribute__(( packed ));

//
// structure icec_net_cap_array_delta
//  Used for ICEC_NET_CMD_G/SET_CAP_ARRAY_DELTA
//
struct icec_net_cap_array_delta
{
    struct icec_net_protocol                hHeader;
    struct iceqube_cap_array_delta_config   hCapSettings;
}__attribute__(( packed ));

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

int host_comms_device_error_to_sys_error( int8_t nDeviceError );
int host_comms_clear_buffer( uint32_t nTimeoutS );

/////////////////////////////////////////////
// PUBLIC DATA
//

/////////////////////////////////////////////
// PRIVATE DATA
//

//
// I/O buffer
//
static uint8_t   sg_hBuffer[ 800 ];
static uint8_t   sg_nInternalRetries = DEFAULT_RETRIES;

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_connect
//
// Initialises communications at host side.
//
// ARGUMENTS:
//  (IN)        uint8_t             Crisp slot (0-2, or 0xFF for auto)
//  (OUT)       uint8_t*            Regulatory of crisp module (1=EU, 2=FCC)
//  (IN)        uint8_t             CCA rank
//  (IN)        uint8_t             Number of listen channels
//  (IN)        uint8_t*            Array of listen channels
//  (IN)        uint8_t             Number of session channels
//  (IN)        uint8_t*            Array of session channels
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_connect(
            uint8_t   nCrispSlot,
            uint8_t * pRegulatory,
            uint8_t   nRank,
            uint8_t   nListenChannels,
            uint8_t * pListenChannels,
            uint8_t   nSessionChannels,
            uint8_t * pSessionChannels,
            int32_t   nFrequencyOffset
            )
{
    int nErr;

    //
    // Sanity check
    //
    if ( nListenChannels > 3 )
	{
		return -EFAULT;
	}
    if ( nSessionChannels > 3 )
	{
		return -EFAULT;
	}

    //
    // Open the port
    //
    nErr = host_port_open( 
                nCrispSlot,
                nListenChannels,
                pListenChannels,
                nRank,
                nSessionChannels,
                pSessionChannels,
                PROTOCOL_ICEC,
                pRegulatory,
                nFrequencyOffset
                );

    if ( nErr )
	{
		return -EFAULT;
	}

	if ( host_port_get_fd( ) == INVALID_FD )
	{
		return -EFAULT;
	}
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_close
//
// Closes a host port
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void host_comms_close( void )
{
    host_disconnect_session( );
    host_port_close( );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_wait_for_connect
//
// Waits for a qube to connect
//
// ARGUMENTS:
//  (IN)        uint32_t        Timeout in MS, or 0 for infinite
//  (IN/OUT)    uint32_t*       Connected peer ID, if function returns 0
//
// RETURNS:
//              int             0 if a peer connects, pPeer will be filled
//                              if this is the case.
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_wait_for_connect( uint32_t nTimeout, uint32_t *pPeer )
{
    int             nErr;
    fd_set          hFds;
    struct timeval  hTimeout;
    int             nFd = host_port_get_fd( );
    
    //
    // Sanity check
    //
	if ( nFd == INVALID_FD )
	{
		return -EBADF;
	}

    //
    // Reset protocol (might be required after a keep-alive
    // timeout).
    //
    nErr = host_port_refresh_protocol( );
    if ( nErr < 0 )
        return -errno;

    //
    // Configure fd_set
    //
    FD_ZERO( &hFds );
    FD_SET( nFd, &hFds );

    //
    // Work out timeout
    //
    if ( nTimeout != 0 )
    {
        while ( nTimeout )
        {
            //
            // Update timeout in 2s intervals
            // this allows a keepalive to be sent to the
            // cc1310 module every 2 seconds.
            //
            if ( nTimeout > 2000 )
            {
                hTimeout.tv_sec = 2;
                hTimeout.tv_usec = 0;
                nTimeout -= 2000;
            }
            else
            {
                hTimeout.tv_sec = nTimeout / 1000;
                hTimeout.tv_usec = ( nTimeout % 1000 ) * 1000;
                nTimeout = 0;
            }

            //
            // Wait for port to become writeable. That means a
            // peer is in a session.
            //
            nErr = select( nFd + 1, NULL, &hFds, NULL, &hTimeout );

            // Timeout ?
            if ( nErr == 0 )
            {
                //
                // Issue keep-alive
                //
                nErr = host_port_ping_norsp( );
                if ( nErr < 0 )
                    return -errno;

                //
                // Try again, until actual timeout has elapsed
                //
                continue;
            }

            // Some error
            if ( nErr < 0 )
            {
                return -errno;
            }

            // OK, set peer ID and return
            if ( pPeer )
            {
                nErr = host_get_peer( pPeer );
                return 0;
            }
            return 0;
        }        

        // Loop went until timeout.
        // return timeout
        return -ETIME;
    }

    //
    // Infinite timeout required
    //
    while ( true )
    {
        //
        // Break select at 2s intervals
        // to issue keepalive
        //
        hTimeout.tv_sec = 2;
        hTimeout.tv_usec = 0;
       
        //
        // Wait for port to become writeable. That means a
        // peer is in a session.
        //
        nErr = select( nFd + 1, NULL, &hFds, NULL, &hTimeout );
        if ( nErr == 0 )
        {
            //
            // Issue keepalive
            //
            nErr = host_port_ping_norsp( );
            if ( nErr < 0 )
                return -errno;
            continue;
        }

        if ( nErr < 0 )
        {
            return -errno;
        }

        if ( pPeer )
        {
            nErr = host_get_peer( pPeer );
            return 0;
        }
        return 0;
    }
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_disconnect
//
// Tell a peer to disconnect from current session
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int             0 on success
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_disconnect( void )
{
    int             nErr;
    fd_set          hFds;
    struct timeval  hTimeout = { 1, 0 };
    int             nFd = host_port_get_fd( );

	if ( nFd == INVALID_FD )
	{
		return -EBADF;
	}

    if ( host_port_ping_norsp( ) < 0 )
        return -errno;

    //
    // Disconnect
    //
    if ( host_disconnect_session( ) != 0 )
	{
		return 0; // already disconnected        
	}

    //
    // Configure fd_set
    //
    FD_ZERO( &hFds );
    FD_SET( nFd, &hFds );

    //
    // Wait for disconnect.
    //
    nErr = select ( nFd + 1, NULL, NULL, &hFds, &hTimeout );

    if ( nErr == 0 )
	{
        host_port_ping_norsp( );
		return -ETIME;
	}

    if ( host_port_ping_norsp( ) < 0 )
        return -errno;
    return 0;    
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_send_command
//
// Sends a command to a connected qube and waits for a response
//
// ARGUMENTS:
//  (IN)        struct icec_net_protocol*       Pointer to command struct
//  (IN/OUT)    struct icec_net_protocol**      Pointer to response. Rsp memory
//                                              is a cast static data array.
//                                              Data is valid until next I/O.
//
// RETURNS:
//              int                             0 on success
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_send_command(
        struct icec_net_protocol *pCmd,
        struct icec_net_protocol **pRsp
        )
{
    return host_comms_send_command_ex( pCmd, pRsp, 2 );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_send_command_ex
//
// Sends a command to a connected qube and waits for a response for a given 
// timeout.
//
// ARGUMENTS:
//  (IN)        struct icec_net_protocol*       Pointer to command struct
//  (IN/OUT)    struct icec_net_protocol**      Pointer to response. Rsp memory
//                                              is a cast static data array.
//                                              Data is valid until next I/O.
//  (IN)        uint32_t                        Timeout in seconds
//
// RETURNS:
//              int                             0 on success
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_send_command_ex(
        struct icec_net_protocol *pCmd,
        struct icec_net_protocol **pRsp,
        uint32_t                  nTimeoutS
        )
{
    fd_set          hFds;
    fd_set          hExcept;
    struct timeval  hTimeout;
    uint32_t        nTimeoutSecs;
    int             nErr = 0;
    int             nLen;
    int             nFd = host_port_get_fd( );
    uint8_t         nRetries = sg_nInternalRetries;

    if ( nTimeoutS == 0 )
    {
        return -EBADF;
    }

    if ( nFd == INVALID_FD )
	{
		return -EBADF;
	}
    
    while (( nRetries-- ) && ( nErr != -ECONNRESET ))
    {
        FD_ZERO( &hFds );
        FD_ZERO( &hExcept );
        FD_SET( nFd, &hFds );
        FD_SET( nFd, &hExcept );

        nLen = pCmd->nLength + sizeof( struct icec_net_protocol );    
        nErr = host_port_write( nLen, (uint8_t*) pCmd );
        if ( nErr < 0 )
        {
            dbg_printf( stderr, "host_comms::write error %d\n", nErr );
            continue;
        }

        nErr = 0;
        nTimeoutSecs = nTimeoutS;
        while (( nErr == 0 ) && ( nTimeoutSecs ))
        {
            if ( nTimeoutSecs >= 2 )
            {
                hTimeout.tv_sec = 2;
                hTimeout.tv_usec = 0;
                nTimeoutSecs -= 2;
            }
            else
            {
                hTimeout.tv_sec = nTimeoutSecs;
                hTimeout.tv_usec = 0;
                nTimeoutSecs = 0;
            }

            nErr = select( nFd + 1, &hFds, NULL, &hExcept, &hTimeout );

            if ( nErr == 0 )
            {
                if ( host_port_ping_norsp( ) < 0 )
                    return -errno;
                continue;
            }
        }

        if ( nErr == 0 )
        {
            nErr = -ETIME;
            dbg_printf( stderr, "host_comms::select timeout %d\n", nErr );
            continue;
        }

        if ( nErr < 0 )
        {
            dbg_printf( stderr, "host_comms::select error %d\n", nErr );
            continue;
        }

        if ( FD_ISSET( nFd, &hExcept ))
        {
            nErr = -ECONNRESET;
            dbg_printf( stderr, "host_comms::select reports disconnect %d\n", nErr );
            break;
        }

        if ( FD_ISSET ( nFd, &hFds ))
        {
            nLen = 800;
            nErr = host_port_read( &nLen, sg_hBuffer );
            if ( nErr < 0 )
            {
                dbg_printf( stderr, "host_comms::read error %d\n", nErr );
                continue;
            }
            if ( nErr == 0 )
            {
                nErr = -ETIME;
                dbg_printf( stderr, "host_comms::read timeout %d\n", nErr );
                continue;
            }
            *pRsp = (struct icec_net_protocol*) sg_hBuffer;
            return 0;
        }
        nErr = -ETIME;
        dbg_printf( stderr, "host_comms::select no fd_set %d\n", nErr );
    }
    return nErr;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_send_command_ex
//
// Clears pending input buffer. 
//
// ARGUMENTS:
//  (IN)        uint32_t                        Timeout in seconds
//
// RETURNS:
//              int                             0 on success
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_clear_buffer( uint32_t nTimeoutS )
{
    fd_set          hFds;
    struct timeval  hTimeout;
    int             nErr;
    int             nLen;
    int             nFd = host_port_get_fd( );

    if ( nTimeoutS == 0 )
    {
        return -EBADF;
    }

    hTimeout.tv_sec  = nTimeoutS;
    hTimeout.tv_usec = 0;

    if ( nFd == INVALID_FD )
	{
		return -EBADF;
	}

    if ( host_port_ping_norsp( ) < 0 )
        return -errno;

    while ( nTimeoutS )
    {
        if ( nTimeoutS > 2 )
        {
            hTimeout.tv_sec = 2;
            hTimeout.tv_usec = 0;
            nTimeoutS -= 2;
        }
        else
        {
            hTimeout.tv_sec = nTimeoutS;
            hTimeout.tv_usec = 0;
            nTimeoutS = 0;
        }

        FD_ZERO( &hFds );
        FD_SET( nFd, &hFds );

        nErr = select( nFd + 1, &hFds, NULL, NULL, &hTimeout );
        if ( nErr < 0 )
            return -nErr;
        if ( nErr == 0 )
        {
            if ( host_port_ping_norsp( ) < 0 )
                return -errno;
            continue;
        }

        if ( FD_ISSET( nFd, &hFds ))
        {
            nLen = 800;
            nErr = host_port_read( &nLen, sg_hBuffer );
            if ( nErr < 0 )
                return nErr;

            if ( nErr == 0 )
                return -ETIME;
            return 0;
        }
    }
    return -1;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_set_mode
//
// Changes the current mode of the connected device
//
// ARGUMENTS:
//  (IN)        uint8_t         New Mode (see #define DEVICE_STATE_*)
//
// RETURNS:
//              int             0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_set_mode( uint8_t nNewMode )
{
    struct icec_net_set_mode  hCmd;
    struct icec_net_protocol *pRsp;
    int8_t                    nErr;

    hCmd.hHeader.nCmd = ICEC_NET_CMD_MODE;
    hCmd.hHeader.nLength = sizeof( struct iceqube_set_device_mode );
    hCmd.hMode.nStructVersion = STRUCT_ICEQUBE_SET_DEVICE_MODE_VERSION;
    hCmd.hMode.nNewMode = nNewMode;

    nErr = host_comms_send_command( 
                (struct icec_net_protocol *) &hCmd, 
                &pRsp 
                );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.hHeader.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != 1 )
	{
		return -EPROTO;
	}

    return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_get_device_info
//
// Queries info block/device_state structure of a connected device.
//
// ARGUMENTS:
//  (IN)        struct iceqube_device_state*  Structure to be filled by device
//                                            query.
//
// RETURNS:
//              int                           0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_get_device_info( struct iceqube_device_state *pInfo )
{
    struct icec_net_protocol hCmd;
    struct icec_net_protocol *pRsp;

    int                      nErr;
    hCmd.nCmd = ICEC_NET_CMD_GET_DEVICE_INFO;
    hCmd.nLength = 0;

    nErr = host_comms_send_command( &hCmd, &pRsp );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != sizeof( struct iceqube_device_state ))
    {
        if ( pRsp->nLength != 1 )
		{
			return -EPROTO;
		}

        return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
    }
    memcpy( 
        pInfo, 
        &((struct icec_device_state*)pRsp)->hDeviceState, 
        sizeof( struct iceqube_device_state )
        );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_get_post_info
//
// Queries power-on self-test info structure of a connected device.
//
// ARGUMENTS:
//  (IN)        struct iceqube_system_power_up_state*  Structure to be filled 
//                                                     by device query.
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_get_post_info( struct iceqube_system_power_up_state *pInfo )
{
    struct icec_net_protocol hCmd;
    struct icec_net_protocol *pRsp;

    int                      nErr;

    hCmd.nCmd = ICEC_NET_CMD_GET_POST_INFO;
    hCmd.nLength = 0;

    nErr = host_comms_send_command( &hCmd, &pRsp );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != sizeof( struct iceqube_system_power_up_state ))
    {
        if ( pRsp->nLength != 1 )
		{
			return -EPROTO;
		}

        return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
    }
    memcpy( 
        pInfo, 
        &(( struct icec_device_post_state* )pRsp)->hPostState, 
        sizeof( struct iceqube_system_power_up_state )
        );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_set_time
//
// Update the device time of a connected device.
//
// ARGUMENTS:
//  (IN)        uint64_t    New device time (0 = 1/1/1970).
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_set_time( uint64_t nNewTime )
{
    struct icec_net_set_time_protocol_v1  hCmd;
    struct icec_net_protocol             *pRsp;
    int                                   nErr = 0;
    int                                   nRetries    = sg_nInternalRetries;
    int                                   nOldRetries = sg_nInternalRetries;
    uint64_t                              nStartTime  = ( uint64_t ) time( NULL );

    while (( nRetries-- ) && ( nErr != -ECONNRESET ))
    {
        uint64_t nDiff = 0;

        //
        // Fill header
        //
        hCmd.hHeader.nCmd = ICEC_NET_CMD_SET_TIME;
        hCmd.hHeader.nLength = sizeof( struct iceqube_set_device_time_v1 );
        hCmd.hTimeInfo.nStructVersion = 1;

        //
        // Work out time difference between first send and now.
        //
        if ( nRetries == ( sg_nInternalRetries - 1 ))
            nDiff = 0;
        else
            nDiff = (( uint64_t ) time( NULL )) - nStartTime;

        //
        // Set the new time and send the command
        //
        hCmd.hTimeInfo.nNewTime       = nNewTime + nDiff;

        //
        // No internal retreis in the send_command
        // as retreis are handled here.
        //
        sg_nInternalRetries = 1;
        nErr = host_comms_send_command(
                            ( struct icec_net_protocol* ) &hCmd, 
                            &pRsp 
                            );
        sg_nInternalRetries = nOldRetries;

        if ( nErr != 0 )
            continue;

        if (( pRsp->nCmd != hCmd.hHeader.nCmd ) ||
            ( pRsp->nLength != 1 ))
	    {
            nErr = -EPROTO;
            continue;
	    }

        //
        // Successful Rx, return qube ack
        //
        return host_comms_device_error_to_sys_error(
                    (( struct icec_net_rsp* ) pRsp )->nErrorCode
                    );
    }
    return nErr;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_set_time_as_offset
//
// Update the device time by setting an offset to the current reported time
//
// ARGUMENTS:
//  (IN)        int64_t     offset to current time. Cannot be 0.
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_set_time_as_offset( int64_t nTimeOffset )
{
    printf(
        "ERROR: host_comms_set_time_as_offset is not implemented any more."
        );
    return -ENOSYS;
}


// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_set_mac
//
// Sets a new device MAC/ID. ID will be assumed with next session.
//
// ARGUMENTS:
//  (IN)        uint32_t    New device ID (0 and 0xFFFFFFFF cannot be used)
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_set_mac( uint32_t nNewQubeMac )
{
    struct icec_net_set_mac   hCmd;
    struct icec_net_protocol *pRsp;
    int                       nErr;

    hCmd.hHeader.nCmd = ICEC_NET_CMD_SET_MAC;
    hCmd.hHeader.nLength = sizeof( struct iceqube_set_device_mac );
    hCmd.hMacInfo.nStructVersion = STRUCT_ICEQUBE_SET_DEVICE_MAC_VERSION;
    hCmd.hMacInfo.nNewMac        = nNewQubeMac;

    nErr = host_comms_send_command(
                        (struct icec_net_protocol *) &hCmd, 
                        &pRsp 
                        );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.hHeader.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != 1 )
	{
		return -EPROTO;
	}

    return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_set_activation_mode
//
// Sets activation mode
//
// ARGUMENTS:
//  (IN)        bool        true to clear data on activate, else false
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_set_activation_mode( bool fClearDataOnActivate )
{
    struct icec_net_set_activate_mode hCmd;    
    struct icec_net_protocol *pRsp;
    int                       nErr;

    hCmd.hHeader.nCmd = ICEC_NET_CMD_SET_ACTIVATE_MODE;
    hCmd.hHeader.nLength = sizeof( struct iceqube_set_activate_mode );
    hCmd.hMode.nStructVersion = STRUCT_ICEQUBE_SET_ACTIVATE_MODE_VERSION;
    hCmd.hMode.nActivateMode  = fClearDataOnActivate ? 1 : 0;

    nErr = host_comms_send_command(
                        (struct icec_net_protocol *) &hCmd, 
                        &pRsp 
                        );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.hHeader.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != 1 )
	{
		return -EPROTO;
	}

    return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_get_trigger_config
//
// Queries current trigger configuration of a connected device.
//
// ARGUMENTS:
//  (IN)        struct trigger_config*      Structure to be filled in by
//                                          query
//
// RETURNS:
//              int                         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_get_trigger_config( struct trigger_config *pInfo )
{
    struct icec_net_protocol hCmd;
    struct icec_net_protocol *pRsp;

    int                      nErr;

    hCmd.nCmd = ICEC_NET_CMD_GET_TRIGGER_CONF;
    hCmd.nLength = 0;

    nErr = host_comms_send_command( &hCmd, &pRsp );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != sizeof( struct trigger_config ))
    {
        if ( pRsp->nLength != 1 )
		{
			return -EPROTO;
		}

        return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
    }
    memcpy( 
        pInfo, 
        &((struct icec_net_trigger_config*) pRsp)->hConfig, 
        sizeof( struct trigger_config )
        );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_set_trigger_config
//
// Updates current trigger configuration of a connected device.
//
// ARGUMENTS:
//  (IN)        struct trigger_config*      New trigger configuration
//
// RETURNS:
//              int                         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_set_trigger_config( struct trigger_config *pInfo )
{
    struct icec_net_trigger_config hCmd;
    struct icec_net_protocol      *pRsp;
    int                            nErr;

    hCmd.hHeader.nCmd = ICEC_NET_CMD_SET_TRIGGER_CONF;
    hCmd.hHeader.nLength = sizeof( struct trigger_config );
    memcpy( &hCmd.hConfig, pInfo, sizeof( struct trigger_config ));

    nErr = host_comms_send_command(
                        ( struct icec_net_protocol* ) &hCmd, 
                        &pRsp 
                        );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.hHeader.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != 1 )
	{
		return -EPROTO;
	}

    return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_get_data_capture_config
//
// Query current data capture configuration of a connected device.
//
// ARGUMENTS:
//  (IN)        struct iceqube_data_capture_config*   Structure to be filled   
//                                                    by query
//
// RETURNS:
//              int                         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_get_data_capture_config( 
                struct iceqube_data_capture_config *pConfig 
                )
{
    struct icec_net_protocol hCmd;
    struct icec_net_protocol *pRsp;

    int                      nErr;

    hCmd.nCmd = ICEC_NET_CMD_GET_DATA_CAPTURE_CONF;
    hCmd.nLength = 0;

    nErr = host_comms_send_command( &hCmd, &pRsp );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != sizeof( struct iceqube_data_capture_config ))
    {
        if ( pRsp->nLength != 1 )
		{
			return -EPROTO;
		}

        return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
    }
    memcpy( 
        pConfig, 
        &((struct icec_net_data_capture_config*) pRsp)->hConfig, 
        sizeof( struct iceqube_data_capture_config )
        );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_set_data_capture_config
//
// Updates data capture configuration of a connected device.
//
// ARGUMENTS:
//  (IN)        struct iceqube_data_capture_config*      
//                                          New data capture configuration
//
// RETURNS:
//              int                         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_set_data_capture_config( 
                struct iceqube_data_capture_config *pConfig 
                )
{
    struct icec_net_data_capture_config hCmd;
    struct icec_net_protocol           *pRsp;
    int                                 nErr;

    hCmd.hHeader.nCmd = ICEC_NET_CMD_SET_DATA_CAPTURE_CONF;
    hCmd.hHeader.nLength = sizeof( struct iceqube_data_capture_config );

    memcpy( 
        &hCmd.hConfig, 
        pConfig, 
        sizeof( struct iceqube_data_capture_config )
        );

    nErr = host_comms_send_command(
                        ( struct icec_net_protocol* ) &hCmd, 
                        &pRsp 
                        );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.hHeader.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != 1 )
	{
		return -EPROTO;
	}

    return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_get_data_interpreter_config
//
// Query data interpretor configuration of a connected device.
//
// ARGUMENTS:
//  (IN)        struct iceqube_data_interpreter_config*      
//                                          Structure to be filled by query
//
// RETURNS:
//              int                         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_get_data_interpreter_config( 
                    struct iceqube_data_interpreter_config *pConfig 
                    )
{
    struct icec_net_protocol hCmd;
    struct icec_net_protocol *pRsp;

    int                      nErr;

    hCmd.nCmd = ICEC_NET_CMD_GET_DATA_INTERPRET_CONF;
    hCmd.nLength = 0;

    nErr = host_comms_send_command( &hCmd, &pRsp );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != sizeof( struct iceqube_data_interpreter_config ))
    {
        if ( pRsp->nLength != 1 )
		{
			return -EPROTO;
		}

        return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
    }
    memcpy( 
        pConfig, 
        &((struct icec_net_data_interpretor_config*) pRsp)->hConfig, 
        sizeof( struct iceqube_data_interpreter_config )
        );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_set_data_interpreter_config
//
// Update data interpretor configuration of a connected device.
//
// ARGUMENTS:
//  (IN)        struct iceqube_data_interpreter_config*      
//                                          New data interpretor config
//
// RETURNS:
//              int                         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_set_data_interpreter_config( 
            struct iceqube_data_interpreter_config *pConfig 
            )
{
    struct icec_net_data_interpretor_config hCmd;
    struct icec_net_protocol               *pRsp;
    int                                     nErr;

    hCmd.hHeader.nCmd = ICEC_NET_CMD_SET_DATA_INTERPRET_CONF;
    hCmd.hHeader.nLength = sizeof( struct iceqube_data_capture_config );

    memcpy( 
        &hCmd.hConfig, 
        pConfig, 
        sizeof( struct iceqube_data_interpreter_config )
        );

    nErr = host_comms_send_command(
                        ( struct icec_net_protocol* )&hCmd, 
                        &pRsp 
                        );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.hHeader.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != 1 )
	{
		return -EPROTO;
	}

    return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_get_timed_ex_mode
//
// Query extended timed download modes of a connected device.
//
// ARGUMENTS:
//  (IN)        struct iceqube_timed_ex_mode*  Structure to be filled by query
//
// RETURNS:
//              int                            0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_get_timed_ex_mode( struct iceqube_timed_ex_mode *pConfig )
{
    struct icec_net_protocol hCmd;
    struct icec_net_protocol *pRsp;

    int                      nErr;

    hCmd.nCmd = ICEC_NET_CMD_GET_TIMED_EX_MODE;
    hCmd.nLength = 0;

    nErr = host_comms_send_command( &hCmd, &pRsp );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != sizeof( struct iceqube_timed_ex_mode ))
    {
        if ( pRsp->nLength != 1 )
		{
			return -EPROTO;
		}

        return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
    }
    memcpy( 
        pConfig, 
        &((struct icec_net_timed_ex_mode*) pRsp)->hMode, 
        sizeof( struct iceqube_timed_ex_mode )
        );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_set_timed_ex_mode
//
// Updates extended timed download modes of a connected device.
//
// ARGUMENTS:
//  (IN)        struct iceqube_timed_ex_mode*  New timed ex mode
//
// RETURNS:
//              int                            0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_set_timed_ex_mode( struct iceqube_timed_ex_mode *pConfig )
{
    struct icec_net_timed_ex_mode hCmd;
    struct icec_net_protocol     *pRsp;
    int                           nErr;

    hCmd.hHeader.nCmd = ICEC_NET_CMD_SET_TIMED_EX_MODE;
    hCmd.hHeader.nLength = sizeof( struct iceqube_timed_ex_mode );
    memcpy( &hCmd.hMode, pConfig, sizeof( struct iceqube_timed_ex_mode ));

    nErr = host_comms_send_command(
                    ( struct icec_net_protocol* ) &hCmd, 
                    &pRsp 
                    );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.hHeader.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != 1 )
	{
		return -EPROTO;
	}

    return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_set_regulatory_region
//
// Updates RF regulatory region of a device.
//
// ARGUMENTS:
//  (IN)        struct iceqube_set_device_region* New regulatory settings
//
// RETURNS:
//              int                           0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_set_regulatory_region( 
                struct iceqube_set_device_region *pRegion 
                )
{
    struct icec_net_set_device_region hCmd;
    struct icec_net_protocol         *pRsp;
    int                               nErr;

    hCmd.hHeader.nCmd = ICEC_NET_CMD_SET_REGULATORY;
    hCmd.hHeader.nLength = sizeof( struct iceqube_set_device_region );

    memcpy( 
        &hCmd.hRegionInfo, 
        pRegion, 
        sizeof( struct iceqube_set_device_region )
        );

    nErr = host_comms_send_command(
                    ( struct icec_net_protocol* ) &hCmd, 
                    &pRsp 
                    );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.hHeader.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != 1 )
	{
		return -EPROTO;
	}

    return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_get_log_filter
//
// Queries logging filters of a connected device.
//
// ARGUMENTS:
//  (IN)        struct log_config*            Structure filled in by query
//
// RETURNS:
//              int                           0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_get_log_filter( struct log_config *pConfig )
{
    struct icec_net_protocol hCmd;
    struct icec_net_protocol *pRsp;

    int                      nErr;

    hCmd.nCmd = ICEC_NET_CMD_GET_LOG_FILTER;
    hCmd.nLength = 0;

    nErr = host_comms_send_command( &hCmd, &pRsp );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != sizeof( struct log_config ))
    {
        if ( pRsp->nLength != 1 )
		{
			return -EPROTO;
		}

        return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
    }
    memcpy( 
        pConfig, 
        &((struct icec_log_config*) pRsp)->hConfig, 
        sizeof( struct log_config )
        );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_set_log_filter
//
// Updates logging filters of a connected device.
//
// ARGUMENTS:
//  (IN)        struct log_config*            New logger settings
//
// RETURNS:
//              int                           0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_set_log_filter( struct log_config *pConfig )
{
    struct icec_log_config    hCmd;
    struct icec_net_protocol *pRsp;
    int                       nErr;

    hCmd.hHeader.nCmd = ICEC_NET_CMD_SET_LOG_FILTER;
    hCmd.hHeader.nLength = sizeof( struct log_config );
    memcpy( &hCmd.hConfig, pConfig, sizeof( struct log_config ));

    nErr = host_comms_send_command(
                    ( struct icec_net_protocol* ) &hCmd, 
                    &pRsp 
                    );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.hHeader.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != 1 )
	{
		return -EPROTO;
	}

    return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_get_log_entry_count
//
// Queries number of log entries in a device
//
// ARGUMENTS:
//  (IN)        struct log_count*   Structure filled in by query
//
// RETURNS:
//              int                 0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_get_log_entry_count( struct log_count *pConfig )
{
    struct icec_net_protocol hCmd;
    struct icec_net_protocol *pRsp;

    int                      nErr;

    hCmd.nCmd = ICEC_NET_CMD_GET_LOG_ENTRIES;
    hCmd.nLength = 0;

    nErr = host_comms_send_command( &hCmd, &pRsp );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != sizeof( struct log_count ))
    {
        if ( pRsp->nLength != 1 )
		{
			return -EPROTO;
		}

        return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
    }
    memcpy( 
        pConfig, 
        &((struct icec_log_entry_count*) pRsp)->hConfig, 
        sizeof( struct log_count )
        );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_get_log
//
// Query up to 30 log entries
//
// ARGUMENTS:
//  (IN)        struct log_dump*    Structure with count and offset declared
//  (IN)        struct log_entry*   Array of log entries to be filled in by
//                                  query.
//
// RETURNS:
//              int                 0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_get_log( struct log_dump  *pConfig, struct log_entry *pEntries )
{
    struct icec_log_dump      hCmd;
    struct icec_net_protocol *pRsp;
    uint32_t                  nExpectedSize;
    int                       nErr;

    hCmd.hHeader.nCmd    = ICEC_NET_CMD_DUMP_LOG;
    hCmd.hHeader.nLength = sizeof( struct log_dump );
    memcpy( &hCmd.hConfig, pConfig, sizeof( struct log_dump ));

    nErr = host_comms_send_command(
                        ( struct icec_net_protocol* ) &hCmd, 
                        &pRsp 
                        );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.hHeader.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength < sizeof( struct log_dump ))
    {
        if ( pRsp->nLength != 1 )
		{
			return -EPROTO;
		}

        return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
    }

    nExpectedSize = (( struct icec_log_dump_rsp* )pRsp )->hConfig.nCount;
    nExpectedSize *= sizeof( struct log_entry );    
    nExpectedSize += sizeof( struct log_dump );

    if ( pRsp->nLength != nExpectedSize )
	{
		return -EPROTO;
	}

    if ( (( struct icec_log_dump_rsp* )pRsp )->hConfig.nCount > 30 )
	{
		return - EPROTO;
	}

    memcpy( 
        pConfig, 
        &((struct icec_log_dump_rsp*) pRsp)->hConfig, 
        sizeof( struct log_dump )
        );

    nExpectedSize = (( struct icec_log_dump_rsp* )pRsp )->hConfig.nCount;
    nExpectedSize *= sizeof( struct log_entry );

    memcpy( 
        pEntries, 
        ((struct icec_log_dump_rsp*) pRsp)->hLogs, 
        nExpectedSize
        );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_start_firmware_upload
//
// Starts a firmware upload. Must call this before uploading data.
//
// ARGUMENTS:
//  (IN)        struct iceqube_firmware_start*      Info aboout firmware
//  (IN/OUT)    struct iceqube_firmware_state*      State of firmware on device
//
// RETURNS:
//              int                 0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_start_firmware_upload( 
                struct iceqube_firmware_start *pInfo,
                struct iceqube_firmware_state *pState 
                )
{
    struct icec_firmware_start hCmd;
    struct icec_net_protocol  *pRsp;
    int                        nErr;

    hCmd.hHeader.nCmd = ICEC_NET_CMD_UPLOAD_FIRMWARE_START;
    hCmd.hHeader.nLength = sizeof( struct iceqube_firmware_start );
    memcpy( &hCmd.hData, pInfo, sizeof( struct iceqube_firmware_start ) );

    nErr = host_comms_send_command_ex(
        ( struct icec_net_protocol* ) &hCmd,
        &pRsp,
        5 // longer timeout here
        );

    if ( nErr != 0 )
    {
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.hHeader.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != sizeof( struct iceqube_firmware_state ) )
    {
        if ( pRsp->nLength != 1 )
		{
			return -EPROTO;
		}

        return host_comms_device_error_to_sys_error(
            ( ( struct icec_net_rsp* ) pRsp )->nErrorCode
            );
    }

    if ( pState )
    {
        memcpy(
            pState,
            &( ( struct icec_firmware_state* ) pRsp )->hState,
            sizeof( struct iceqube_firmware_state )
            );
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_upload_firmware_chunk
//
// Upload a firmware chunk. This is the only function that doesn't do any
// retries. This is because it requires a resync (get current offset in
// firmware) before retrying. Any retries should be handled at application
// level.
//
// ARGUMENTS:
//  (IN)        struct iceqube_firmware_block*   Firmware block to upload
//
// RETURNS:
//              int                 0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_upload_firmware_chunk( struct iceqube_firmware_block *pBlock )
{
    struct icec_firmware_block hCmd;
    struct icec_net_protocol  *pRsp;
    int                        nErr;
    uint8_t                    nRetriesBefore = sg_nInternalRetries;

    hCmd.hHeader.nCmd = ICEC_NET_CMD_UPLOAD_FIRMWARE;
    hCmd.hHeader.nLength = sizeof( struct iceqube_firmware_block );
    memcpy( &hCmd.hData, pBlock, sizeof( struct iceqube_firmware_block ));

    sg_nInternalRetries = 1;
    nErr = host_comms_send_command(
                    ( struct icec_net_protocol* ) &hCmd, 
                    &pRsp 
                    );
    sg_nInternalRetries = nRetriesBefore;

	if (nErr != 0)
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.hHeader.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != 1 )
	{
		return -EPROTO;
	}

    return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_get_firmware_state
//
// Query current firmware upload position and state
//
// ARGUMENTS:
//  (IN)        struct iceqube_firmware_state*  Structure to be fill
//
// RETURNS:
//              int                             0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_get_firmware_state( struct iceqube_firmware_state *pState )
{
    struct icec_net_protocol hCmd;
    struct icec_net_protocol *pRsp;

    int                      nErr;

    hCmd.nCmd = ICEC_NET_CMD_FIRMWARE_STATE;
    hCmd.nLength = 0;

    nErr = host_comms_send_command( &hCmd, &pRsp );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength != sizeof( struct iceqube_firmware_state ))
    {
        if ( pRsp->nLength != 1 )
		{
			return -EPROTO;
		}

        return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
    }
    memcpy( 
        pState, 
        &((struct icec_firmware_state*) pRsp)->hState, 
        sizeof( struct iceqube_firmware_state )
        );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_get_bootloader_version
//
// Query bootloader version of a connected device
//
// ARGUMENTS:
//  (IN/OUT)    uint16_t*           Major version
//  (IN/OUT)    uint16_t*           Minor version
//
// RETURNS:
//              int                 0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_get_bootloader_version( uint16_t *pVerMaj, uint16_t *pVerMin )
{
    struct icec_net_protocol        hCmd;
    struct icec_bootloader_version *pRsp;
    int                             nErr;

    hCmd.nCmd = ICEC_NET_CMD_GET_BOOTLOADER_VERSION;
    hCmd.nLength = 0;

    nErr = host_comms_send_command( &hCmd, (struct icec_net_protocol**) &pRsp );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->hHeader.nCmd != hCmd.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->hHeader.nLength != sizeof( struct iceqube_bootloader_version ))
    {
        if ( pRsp->hHeader.nLength != 1 )
		{
			return -EPROTO;
		}

        return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
    }

    if ( pVerMaj )
	{
		*pVerMaj = pRsp->hVersion.nBootloaderVersionMajor;
	}
    if ( pVerMin )
	{
		*pVerMin = pRsp->hVersion.nBootloaderVersionMinor;
	}
    return 0;
}


// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_get_cap_array_delta
//
// Query cap array delta for HF XOSC
//
// ARGUMENTS:
//  (IN/OUT)    int8_t*             Current delta
//
// RETURNS:
//              int                 0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_get_cap_array_delta( int8_t *pDelta )
{
    struct icec_net_protocol hCmd;
    struct icec_net_cap_array_delta *pRsp;
    int                             nErr;

    hCmd.nCmd = ICEC_NET_CMD_GET_CAP_ARRAY_DELTA;
    hCmd.nLength = 0;

    nErr = host_comms_send_command(
        ( struct icec_net_protocol* ) &hCmd,
        ( struct icec_net_protocol** ) &pRsp
        );

    if ( nErr != 0 )
    {
        return nErr;
    }

    if ( pRsp->hHeader.nCmd != hCmd.nCmd )
    {
        return -EPROTO;
    }

    if ( pRsp->hHeader.nLength != sizeof( struct iceqube_cap_array_delta_config ))
    {
        if ( pRsp->hHeader.nLength != 1 )
        {
            return -EPROTO;
        }
        return host_comms_device_error_to_sys_error(
            (( struct icec_net_rsp* ) pRsp )->nErrorCode
            );
    }

    if ( pDelta )
    {
        *pDelta = pRsp->hCapSettings.nCapArrayDelta;
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_set_cap_array_delta
//
// Sets new cap array delta for HF XOSC
//
// ARGUMENTS:
//  (IN/OUT)    int8_t              New delta value
//
// RETURNS:
//              int                 0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_set_cap_array_delta( int8_t nDelta )
{
    struct icec_net_cap_array_delta hCmd;
    struct icec_net_protocol       *pRsp;
    int                             nErr;

    hCmd.hHeader.nCmd = ICEC_NET_CMD_SET_CAP_ARRAY_DELTA;
    hCmd.hHeader.nLength = sizeof( struct iceqube_cap_array_delta_config );
    hCmd.hCapSettings.nStructVersion = STRUCT_ICEQUBE_CAPARRAYDELTA_VERSION;
    hCmd.hCapSettings.nDeltaSet = 0xAA;
    hCmd.hCapSettings.nCapArrayDelta = nDelta;

    nErr = host_comms_send_command(
        ( struct icec_net_protocol* ) &hCmd,
        &pRsp
        );

    if ( nErr != 0 )
    {
        return nErr;
    }

    if ( pRsp->nCmd != hCmd.hHeader.nCmd )
    {
        return -EPROTO;
    }

    if ( pRsp->nCmd != hCmd.hHeader.nCmd )
    {
        return -EPROTO;
    }

    if ( pRsp->nLength != 1 )
    {
        return -EPROTO;
    }

    return host_comms_device_error_to_sys_error(
        ( ( struct icec_net_rsp* ) pRsp )->nErrorCode
        );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_get_sample
//
// Query a raw accelerometer sample
//
// ARGUMENTS:
//  (IN)        uint8_t                     Units of sample (0=raw, 1=mg,
//                                          2=cg, 3=dg, 4= g)
//  (IN)        struct iceqube_sample*      Structure to be filled by query
//
// RETURNS:
//              int                         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_get_sample( uint8_t nUnits, struct iceqube_sample *pSample )
{
    int    nErr = 0;
    struct icec_get_sample hCmd;
    struct icec_sample    *pRsp;
    hCmd.hHeader.nCmd = ICEC_NET_CMD_GET_DATA_SAMPLE;
    hCmd.hHeader.nLength = sizeof( struct iceqube_get_sample );
    hCmd.hData.nStructVersion = 1;
    hCmd.hData.nSampleUnits = nUnits;

    nErr = host_comms_send_command(
                (struct icec_net_protocol*) &hCmd, 
                (struct icec_net_protocol**) &pRsp 
                );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->hHeader.nCmd != hCmd.hHeader.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->hHeader.nLength != sizeof( struct iceqube_sample ))
    {
        if ( pRsp->hHeader.nLength != 1 )
		{
			return -EPROTO;
		}
        return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
    }

    if ( pSample )
        memcpy( pSample, &pRsp->hData, sizeof( struct iceqube_sample ));
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_get_data
//
// Query recorded data
//
// ARGUMENTS:
//  (IN)        struct iceqube_get_day*     Day to query
//  (IN)        struct day_data *           Structure to be filled by query
//
// RETURNS:
//              int                         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_get_data( 
            struct iceqube_get_day  *pCount, 
            struct day_data         *pDayData 
            )
{
    struct icec_get_day       hCmd;
    struct icec_net_protocol *pRsp;
    int                       nErr;

    hCmd.hHeader.nCmd = ICEC_NET_CMD_GET_DATA;
    hCmd.hHeader.nLength = sizeof( struct iceqube_get_day );
    memcpy( &hCmd.hQuery, pCount, sizeof( struct iceqube_get_day ));

    nErr = host_comms_send_command( 
                        ( struct icec_net_protocol* ) &hCmd, 
                        &pRsp 
                        );

    if ( nErr != 0 )
	{
		return nErr;
	}

    if ( pRsp->nCmd != hCmd.hHeader.nCmd )
	{
		return -EPROTO;
	}

    if ( pRsp->nLength < sizeof( struct day_data ))
    {
        if ( pRsp->nLength != 1 )
		{
			return -EPROTO;
		}

        return host_comms_device_error_to_sys_error(
                    ((struct icec_net_rsp*) pRsp)->nErrorCode 
                    );
    }

    memcpy( 
        pDayData, 
        &((struct icec_day_data*) pRsp)->hDay, 
        sizeof( struct day_data )
        );

    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_device_error_to_sys_error
//
// Qube error codes are simple turned positive. Linux errors stay negative.
//
// ARGUMENTS:
//  (IN)        int8_t      iceqube error code
//
// RETURNS:
//              int         linux error code
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_device_error_to_sys_error( int8_t nDeviceError )
{
    if ( nDeviceError < 0 )
	{
		return -nDeviceError;
	}
    return nDeviceError;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_set_internal_retries
//
// Sets internal comms retries. (1 is assumed, retries can be 0-4)
//
// ARGUMENTS:
//  (IN)        uint8_t      Additional comms attempts on a failure.
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void host_comms_set_internal_retries( uint8_t nRetries )
{
    nRetries++; // Need to try at least once
    if (( nRetries < 1 ) || ( nRetries > 5 ))
        nRetries = 5;

    sg_nInternalRetries = nRetries;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_get_internal_retries
//
// Gets internal comms retries. (1 is assumed, retries can be 0-4)
//
// ARGUMENTS:
//              none
//
// RETURNS:
//              uint8_t      Retries
//
///////////////////////////////////////////////////////////////////////////////
uint8_t host_comms_get_internal_retries( void )
{
    return ( sg_nInternalRetries - 1 );
}

//////////////////////////////////// EOF //////////////////////////////////////

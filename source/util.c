//FILE HEADER//////////////////////////////////////////////////////////////////
// 
// FILE NAME:   util.c
//
// PROJECT:     crisp host
//
// PLATFORM:    Crispbread/arm/linux
//
// Utility function to support iceqube_icenetc_lib
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis    
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
// 
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "util.h"

/////////////////////////////////////////////
// DEFINES
//

//
// Macros used by util_localtime
//
#define LEAPYEAR( year )          (!((year) % 4) && (((year) % 100) || !((year) % 400)))
#define YEARSIZE( year )          ( LEAPYEAR( year ) ? 366 : 365 )

/////////////////////////////////////////////
// DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PROTOTYPES
//

int     util_compare_time( struct tm *t1, struct tm * t2 );
uint8_t util_days_per_month( bool fLeapYear, int nMonth );

/////////////////////////////////////////////
// PUBLIC DATA
//

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   util_mktime
//
// Converts a struct tm into a 64-bit time value
//
// ARGUMENTS:
//  (IN)        struct tm*      Time in structure
//
// RETURNS:
//              uint64_t        Time as a uint64_t
//
///////////////////////////////////////////////////////////////////////////////
uint64_t util_mktime( struct tm * tp )
{
    uint8_t   i;
    
    uint64_t  t = 0;
    struct tm temp, tm_t;

    // Make a local copy just in case tp points to localtime's buffer 
    temp = *tp;

    // Check for zero 
    util_localtime( 0, &tm_t ); 
    if ( util_compare_time( &temp, &tm_t) == 0 ) 
	{
		return 0;
	}
    
    //
    // Do a binary search, until the right time is found 
    // start with high bit, work toward zero 
    //
    for ( i = 63; --i >= 0 ; ) 
    {   
        int32_t   nDiff;
        // Set the bit, then compare
	    t |= ( 1ul << i );          

        util_localtime( t, &tm_t ); 
        nDiff = util_compare_time( &temp, &tm_t );
	    if ( nDiff < 0) 
        {
            // time is later, so clear the bit again
	        t &= ~(1ul << i);    
        }
	    else if ( nDiff == 0 ) 
        {
            // times match, so return 
	        return t;    
	    }
    }
    //
    // If we get here, then tp didn't point to valid data. 
    //
    return -1;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   util_compare_time
//
// Compare two `struct tm's.
//
// ARGUMENTS:
//  (IN)        struct tm*      Time structure 1
//  (IN)        struct tm*      Time structure 2
//
// RETURNS:
//              int             0 if equel, positive if t1 is later than t2,
//                              negative is t2 is later than t1
//
///////////////////////////////////////////////////////////////////////////////
int util_compare_time( struct tm *t1, struct tm * t2)
{
    int diff;

	if (( diff = t1->tm_year - t2->tm_year ) != 0 )
	{
		return diff;
	}
	if (( diff = t1->tm_mon - t2->tm_mon ) != 0 )
	{
		return diff;
	}
    if (( diff = t1->tm_mday - t2->tm_mday ) != 0 ) 
	{
		return diff;
	}
    if (( diff = t1->tm_hour - t2->tm_hour ) != 0 ) 
	{
		return diff;
	}
	if (( diff = t1->tm_min - t2->tm_min ) != 0 )
	{
		return diff;
	}
    if (( diff = t1->tm_sec - t2->tm_sec ) != 0 ) 
	{
		return diff;
	}
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   util_localtime
//
// Fast conversion from 64 bit time value to struct tm.
//
// ARGUMENTS:
//  (IN)        const uint64_t*   Pointer to time value 
//  (IN/OUT)    struct tm*      tm structure to take converted time 
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void util_localtime( const uint64_t nTime, struct tm *tmbuf ) 
{
    uint64_t dayno, temp;
    uint32_t dayclock;
    int      year = 1970; // EPOCH_YEAR

    dayno    = nTime / 86400;
    dayclock = nTime % 86400;

    tmbuf->tm_sec  = dayclock % 60;
    tmbuf->tm_min  = ( dayclock % 3600 ) / 60;
    tmbuf->tm_hour = dayclock / 3600;
    temp = dayno + 4;
    tmbuf->tm_wday = temp % 7;

    while ( dayno >= ( YEARSIZE( year ))) 
    {
        dayno -= YEARSIZE( year );
        year++;
    }

    tmbuf->tm_year = year - 1900; // YEAR0
    tmbuf->tm_yday = dayno;
    tmbuf->tm_mon  = 0;
  
    while ( dayno >= util_days_per_month( LEAPYEAR( year ), tmbuf->tm_mon )) 
    {
        dayno -= util_days_per_month( LEAPYEAR( year ), tmbuf->tm_mon );
        tmbuf->tm_mon++;
    }
    tmbuf->tm_mday = dayno + 1;
    tmbuf->tm_isdst = 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   util_days_per_month
//
// Returns number of days in a month
//
// ARGUMENTS:
//  (IN)        bool            True if a leap year
//  (IN)        int             Month (0-11)
//
// RETURNS:
//              uint8_t         Number of days
//
///////////////////////////////////////////////////////////////////////////////
uint8_t util_days_per_month( bool fLeapYear, int nMonth )
{
    switch( nMonth )
    {   
    default: // Jan, Mar, May, Jul, Aug, Oct, Dec
        return 31;
    case 1: // Feb
        if ( fLeapYear )
		{
			return 29;
		}
        return 28;
    case 3: // Apr
    case 5: // Jun
    case 8: // Sep
    case 10: // Nov
        return 30;
    }    
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   util_time
//
// Function to query system time as a uint64_t on systems where time_t is
// 32-bit.
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              uint64_t        Time value as counter since 1/1/1970
//
///////////////////////////////////////////////////////////////////////////////
uint64_t util_time( void )
{
    char      szCmd[80];
    char      szTmpFile[40];
    uint64_t  nDate = 0;
    FILE     *pFile = 0;

    //
    // Use libc call, it supports 64-bit time_t (only normally on 64-bit OS)
    //
    if ( sizeof( time_t ) == 8 )
	{
		return (uint64_t) time( NULL );
	}

    //
    // Make up a temp file name
    //
    sprintf( szTmpFile, "/tmp/da%d.tmp", (int) ( getpid( ) & 0x7FFFFFFF ));

    //
    // Query the system time using 'date' cmd, piped to a temp file
    //
    sprintf( szCmd, "date -u +'%%s'>%s", szTmpFile );
    system( szCmd );

    //
    // Read in the temp-file content
    //
    pFile = fopen( szTmpFile, "r" );
    if ( pFile == NULL )
    {
        remove( szTmpFile );
		return (uint64_t) time( NULL );
    }

    if ( fscanf( pFile, "%lu", &nDate ) < 1 )
	{
        fclose( pFile );
        remove( szTmpFile );
		return (uint64_t) time( NULL );
	}

    fclose( pFile );
    remove( szTmpFile );

    //
    // Return the date
    //
    if ( nDate == 0 )
        return (uint64_t) time( NULL );
    return nDate;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   util_tzoffset
//
// Returns local timezone offset in seconds
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int        Time zone offset between utc and local-time
//
///////////////////////////////////////////////////////////////////////////////
int util_tzoffset( void )
{
    char      szCmd[80];
    char      szTmpFile[40];
    int       nOffsetHours;
    int       nOffsetMinutes;
    int       nOffsetSeconds;
    int       nOffset = 0;
    FILE     *pFile = 0;

    //
    // Make up a temp file name
    //
    sprintf( szTmpFile, "/tmp/tz%d.tmp", (int) ( getpid( ) & 0x7FFFFFFF ));

    //
    // Query the system time using 'date' cmd, piped to a temp file
    //
    sprintf( szCmd, "date +'%%::z'>%s", szTmpFile );
    system( szCmd );

    //
    // Read in the temp-file content
    //
    pFile = fopen( szTmpFile, "r" );
    if ( pFile == NULL )
    {
        remove( szTmpFile );
        return 0;
    }

    if ( fscanf( pFile, "%d:%d:%d", &nOffsetHours, &nOffsetMinutes, &nOffsetSeconds ) < 3 )
    {
        nOffsetHours = 0;
        nOffsetMinutes = 0;
        nOffsetSeconds = 0;
    }
    fclose( pFile );
    remove( szTmpFile );

    //
    // Return the offset
    //
    nOffset = nOffsetHours;
    nOffset *= 60;
    nOffset += nOffsetMinutes;
    nOffset *= 60;
    nOffset += nOffsetSeconds;
    return nOffset;
}

//////////////////////////////////// EOF //////////////////////////////////////


// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   example.c
//
// PROJECT:     iceqube_icenetc_lib example
//
// PLATFORM:    crisp linux/arm/gcc
//
// Connect test example
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <time.h>
#include <syslog.h>

#include "host_comms.h"
#include "types.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

#define MAX_DEVICES         300

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

struct tagStats
{
    uint32_t  nMac;
    uint32_t  nConnects;
    uint32_t  nInfos;
    uint32_t  nDownloads;
    uint32_t  nErrors;
};

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

extern bool parse_int_param( 
                const char* szParam, 
                int *pVal, 
                int nArgv, 
                char **pArgc 
                );

void update_stats( 
                uint32_t nId, 
                bool fInfo, 
                bool fDownload, 
                bool fError 
                );

/////////////////////////////////////////////
// PUBLIC DATA
//

/////////////////////////////////////////////
// PRIVATE DATA
//

static uint32_t sg_nMaxDaysToDownload = 0;

static struct tagStats sg_hStats[ MAX_DEVICES ];
static uint32_t        sg_nDevices = 0;

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_run
//
// Called by main.c to download data from a connected device
//
// ARGUMENTS:
//  (IN)        uint32_t    nConnectedPeer
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int host_example_run( uint32_t nConnectedPeer )
{
    struct iceqube_device_state hInfo;
    uint8_t                     nTotalDays, i;
    int                         nErr;
    int                         nLastRssi;

    //
    // Get last RSSI for info
    //
    nErr = host_get_last_rssi( &nLastRssi );
    if ( nErr != 0 )
    {
        nLastRssi = -32768;
    }

    fprintf( stdout,  "\n" );
    fprintf( stdout, "DEVICE ID:             %d  (%ddBm)\n", nConnectedPeer, nLastRssi );

    //
    // Get device info
    //
    nErr = host_comms_get_device_info( &hInfo );
    if ( nErr != 0 )
    {
        update_stats( nConnectedPeer, false, false, true );
        return nErr;
    }


    if ( sg_nMaxDaysToDownload )
    {
        nTotalDays = hInfo.nActiveDay + hInfo.nDays;
        if ( nTotalDays == 0 )
        {
            update_stats( nConnectedPeer, true, false, false );
            return 0;
        }

        if ( nTotalDays > sg_nMaxDaysToDownload )
            nTotalDays = sg_nMaxDaysToDownload;

        for( i = 0; i < nTotalDays; i ++ )
        {
            struct iceqube_get_day      hDayQuery;
            struct day_data             hDayData;

            hDayQuery.nStructVersion = STRUCT_ICEQUBE_GET_DAY_VERSION;
            hDayQuery.nDay = i;

            nErr = host_comms_get_data( &hDayQuery, &hDayData );
            if ( nErr != 0 )
            {
                update_stats( nConnectedPeer, true, false, true );
                return nErr;
            }
        }
        update_stats( nConnectedPeer, true, true, false );
        return 0;
    }
    update_stats( nConnectedPeer, true, false, false );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   update_stats
//
// Updates stats-list
//
// ARGUMENTS:
//  (IN)        uint32_t    nConnectedPeer
//  (IN)        bool        Info success
//  (IN)        bool        Download success
//  (IN)        bool        Error
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void update_stats( uint32_t nId, bool fInfo, bool fDownload, bool fError )
{
    uint32_t i;
    for( i = 0; i < sg_nDevices; i ++ )
    {
        if ( sg_hStats[ i ].nMac == nId )
        {
            sg_hStats[ i ].nConnects++;
            sg_hStats[ i ].nInfos += fInfo ? 1 : 0;
            sg_hStats[ i ].nDownloads += fDownload ? 1: 0;
            sg_hStats[ i ].nErrors += fError ? 1 : 0;
            return;
        }
    }

    if ( sg_nDevices < MAX_DEVICES )
    {
        sg_hStats[ sg_nDevices ].nMac = nId;
        sg_hStats[ sg_nDevices ].nConnects = 1;
        sg_hStats[ sg_nDevices ].nInfos = fInfo ? 1 : 0;
        sg_hStats[ sg_nDevices ].nDownloads = fDownload ? 1 : 0;
        sg_hStats[ sg_nDevices ].nErrors = fError ? 1 : 0;
        sg_nDevices++;
    }
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   example_dump_stats
//
// Clear all stats
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void example_dump_stats( void )
{
    uint32_t i;
    fprintf( stdout, "= STATS DUMP ============================\n\n");

    if ( sg_nDevices == 0 )
    {
        fprintf( stdout, "0 devices connected \n");
    }
    else
    {
        fprintf( 
            stdout, 
            "DEVICE ID   CONNECTS   INFO       DOWNLOADS  FAILURES\n"
            );

        for( i = 0; i < sg_nDevices; i ++ )
        {
            fprintf( 
                stdout, 
                "%-9u   %-8u   %-8u   %-8u   %-8u\n",
                sg_hStats[ i ].nMac,
                sg_hStats[ i ].nConnects,
                sg_hStats[ i ].nInfos,
                sg_hStats[ i ].nDownloads,
                sg_hStats[ i ].nErrors
                );            
        }
    }
    fprintf( stdout, "\n= END ===================================\n");
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   example_clear_stats
//
// Clear all stats
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void example_clear_stats( void )
{
    sg_nDevices = 0;
    fprintf( stdout, "Statistics reset\n");
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_get_param_count
//
// Returns number of command line parameters supported by example
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int     Parameter count
//
///////////////////////////////////////////////////////////////////////////////
int host_example_get_param_count( void )
{
    return 1;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_parse_params
//
// Passes a command line parameter for parsing. 'p', 'i', 's' & 'l' are
// reserved by the application.
//
// ARGUMENTS:
//  (IN)        char*   Command line
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_parse_params( char *pParam )
{
    int n;
    char szStr[ 255 ];

    if ( parse_int_param( "-n=", &n, 1, &pParam ))
    {
        sg_nMaxDaysToDownload = ( uint32_t ) n;
        if (( sg_nMaxDaysToDownload < 0 ) || ( sg_nMaxDaysToDownload > 500 ))
        {
            fprintf( stdout, "Invalid maximum number of days (0-500)\n" );
            return -1;
        }
		return 0;
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_print_param_help
//
// Prints help string for parameters
//
// ARGUMENTS:
//  (IN)        FILE*   Stream to write help message to
//  (IN)        int     Index of parameter to print help message for
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_print_param_help( FILE *pStream, int nIndex )
{
    switch( nIndex )
    {
    case 0:
        fprintf( pStream, "\t-n=[0-500]      Simulate number of days to download (default 0)\n");
        break;

    default:
        break;
    }
    return 0;
}

//////////////////////////////////// EOF //////////////////////////////////////

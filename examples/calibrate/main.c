// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   main.c
//
// PROJECT:     iceqube_icenetc_lib example
//
// PLATFORM:    crisp linux/arm/gcc
//
// Entry point for example application
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <unistd.h> 
#include <string.h>
#include <errno.h>
#include <setjmp.h>

#include "host_comms.h"
#include "host_port.h"
#include "iceqube_icenetc.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

#define STDIN       0

#define qprintf( ... )  fprintf( stdout, __VA_ARGS__ ); 

#define RF_OFFSET_FILE      "/etc/rf_offset"

#ifndef MAX_PATH
#define MAX_PATH        255
#endif

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

//
// Output usage
//
void print_usage( void );

//
// SIG_STOP handler
//
void StopSigHandler( int nSignal, siginfo_t *pSig, void *pSecret );

//
// Parameter parsing
//

bool parse_int_param( 
            const char* szParam, 
            int *pVal, 
            int nArgv, 
            char **pArgc 
            );

bool parse_param_set( 
            const char* szParam, 
            int nArgv, 
            char **pArgc 
            );

bool parse_string_param( 
            const char* szParam, 
            char  *szBuffer,
            int    nMaxLen,
            int    nArgv, 
            char **pArgc            
            );

bool parse_frequency_param( 
            const char* szParam, 
            int   *pVal,
            int    nArgv, 
            char **pArgc            
            );

uint32_t ChannelToFrequenct( 
            uint8_t nRegulatory,
            uint8_t nChannel 
            );

/////////////////////////////////////////////
// PUBLIC DATA
//

bool sg_fQuit = false;

uint8_t sg_nRegulatory; // regulatory

char    sg_szOffsetFile[ MAX_PATH ];


static const uint32_t sg_nChannelToFreqLtbl_88Khz[ ] =
            {
                865176000, 865264000, 865352000, 865440000,
                865528000, 865616000, 865704000, 865792000,
                865880000, 865968000, 866056000, 866144000,
                866232000, 866320000, 866408000, 866496000,
                866584000, 866672000, 866760000, 866848000,
                866936000, 867024000, 867112000, 867200000,
                867288000, 867376000, 867464000, 867552000,
                867640000, 867728000, 867816000,
                867904000, 867992000, 868080000,
                868176000, 868264000, 868352000
            };


/////////////////////////////////////////////
// PRIVATE DATA
//

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   main
//
// Application entry point. 
//
// ARGUMENTS:
//  (IN)        int         Argument count
//  (IN)        char**      List of arguments
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int main( int nArgc, char **pArgc )
{
    //
    // Application parameters (set by command line)
    //
    bool           fListenChannelsSet     = false; // any listen channel set?
    uint8_t        nCrispSlot             = 0xFF;  // auto
    uint8_t        nListenChannels        = 1;     // default number of listen channels
    uint8_t        hListenChannels[3]     = { 0, 0, 0 }; // default listen channels
    uint8_t        hNewListenChannels[3]  = { 0xFF, 0xFF, 0xFF }; // no new listen channels yet
    int32_t        nFrequencyOffset       = 0;     // Default is no frequency offset
    int32_t        nFrequencyOffsetChange = 0;
    int            n;    

    sg_szOffsetFile[0] = 0;
    sg_nRegulatory  = 0; // default regulatory

    //
    // Input (keyboard) handling
    //
    struct termios hOldSettings;        
    struct termios hNewTty; 

    //
    // SIG_STOP handler
    //
    struct sigaction hSigAction;

    //
    // Trap stop signal handler (for a clean exit)
    //
    hSigAction.sa_sigaction = StopSigHandler;
    sigemptyset( &hSigAction.sa_mask );
    hSigAction.sa_flags = SA_SIGINFO;
    sigaction( SIGTERM, &hSigAction, NULL );

    //
    // Disable stdout buffering
    //
    setbuf( stdout, NULL );

    //
    // Print header
    //
    qprintf( "iceqube_icenetc_lib example fn: CALIBRATE\n" );
    qprintf( 
        "iceqube_icenetc_lib version %d.%03d\n", 
        ICEQUBE_ICENETC_VERSION_MAJ,
        ICEQUBE_ICENETC_VERSION_MIN
        );

    //
    // Parse the command line parameters
    //
    if ( parse_param_set( "-?", nArgc, pArgc ))
    {
        print_usage( );
        return 0;
    }

    if ( parse_int_param( "-p=", &n, nArgc, pArgc ))
    {
        nCrispSlot = n;
        if (( nCrispSlot != 0xFF ) && ( nCrispSlot > 2 ))
        {
            fprintf( stdout,"Invalid card slot (-p=%d). Must be port 0-2, or 255 for auto.\n", nCrispSlot );
            print_usage( );
            return -1;
        }
    }

    if ( parse_int_param( "-l=", &n, nArgc, pArgc ))
    {
        hNewListenChannels[0] = n;
        fListenChannelsSet = true;
    }

    parse_string_param( "-s=", sg_szOffsetFile, 255, nArgc, pArgc );
    
    if ( parse_frequency_param( "-rf=", &n, nArgc, pArgc ))
    {
        nFrequencyOffset = n;
    }

    if ( sg_szOffsetFile[0] == 0 )
        strcpy( sg_szOffsetFile, RF_OFFSET_FILE );

    //
    // Assume new parameters
    //   
    if ( fListenChannelsSet )
        memcpy( hListenChannels, hNewListenChannels, 3 );
    nListenChannels = 0;
    if ( hListenChannels[0] != 0xFF ) 
        nListenChannels++;

    //
    // No listen channels?
    //
    if ( nListenChannels == 0 ) 
    {
        qprintf( "Invalid channel count. Requires at least 1 valid channel.\n" );
        print_usage( );
        return -1;
    }

    //
    // Open cc1310
    //
    if ( host_port_open( 
            nCrispSlot,
            nListenChannels,
            hListenChannels,
            0,
            nListenChannels,
            hListenChannels,
            PROTOCOL_ICEZ,
            &sg_nRegulatory,
            nFrequencyOffset
            ) != 0 )
    {
        if ( sg_nRegulatory != 0 )
        {
            if ( hListenChannels[ 0 ] > MAX_CHANNEL( sg_nRegulatory ))
            {
                qprintf(                     
                    "Invalid channel (-l=%u). Must be 0-%u\n", 
                    hListenChannels[ 0 ], 
                    MAX_CHANNEL( sg_nRegulatory ) 
                    );
                print_usage( );
                return -1;
            }
        }
        fprintf( stdout,"Failed to open cc1310 host\n" );
        return -1;
    }

    //
    // Output Connection Details
    //
    qprintf(
        "Connected to host\n"
        );
    qprintf( 
        "Regulatory : %s\n", 
        ( sg_nRegulatory == REGULATORY_EU_2000_299_EC1 ) ? "eu_2000/299/ec1" : "fcc"
        );

    qprintf ( "Save path : %s\n",  sg_szOffsetFile );       
    qprintf ( "Calibrating for channel : %d\n", hListenChannels[0] );   
    qprintf ( "Expected centre frequency : %uHz\n\n", ChannelToFrequenct( sg_nRegulatory, hListenChannels[0] ));   

    qprintf( "  press '+' or '=' to increase frequency by 10Hz\n" );
    qprintf( "  press ']' or '}' to increase frequency by 100Hz\n" );
    qprintf( "  press '#' or '~' to increase frequency by 1000Hz\n" );
    qprintf( "  press '-' or '_' to decrease frequency by 10Hz\n" );
    qprintf( "  press '[' or '{' to decrease frequency by 100Hz\n" );
    qprintf( "  press '\'' or '@' to decrease frequency by 1000Hz\n" );
    qprintf( "  press 'q' to quit without saving\n" );
    qprintf( "  press any other key to save and exit\n\n" );

    qprintf ( "Current Frequency Offset : %d Hz     ", nFrequencyOffset ); 

    //
    // Just force an update here
    //
    usleep( 100000 ); // small delay
    nFrequencyOffsetChange = nFrequencyOffset;
    if ( nFrequencyOffsetChange > 0 )
        nFrequencyOffsetChange--;
    else
        nFrequencyOffsetChange++;

    if ( host_port_set_frequency_offset( nFrequencyOffsetChange ) < 0 )
    {
        qprintf( "\nFailed to update frequency\n" );
        host_comms_close( );
        tcsetattr( 0, TCSANOW, &hOldSettings ); 
        return -1;
    }

    usleep( 100000 ); // small delay

    if ( host_port_set_frequency_offset( nFrequencyOffset ) < 0 )
    {
        qprintf( "\nFailed to update frequency\n" );
        host_comms_close( );
        tcsetattr( 0, TCSANOW, &hOldSettings ); 
        return -1;
    }

    //
    // Configure stdin
    //
    tcgetattr( STDIN, &hOldSettings );
    hNewTty = hOldSettings;
    hNewTty.c_lflag      &= ~ICANON;
    hNewTty.c_lflag      &= ~ECHO;
    hNewTty.c_lflag      &= ~ISIG;
    hNewTty.c_cc[ VMIN ]  = 0;
    hNewTty.c_cc[ VTIME ] = 0;
    tcsetattr( 0, TCSANOW, &hNewTty );

    //
    // Round robin loop
    //
    while( sg_fQuit == false )
    {
        int             nError;
        struct timeval  hTimeout;
        fd_set          hFds;
        bool            fUpdateFrequency = false;

        // Configure fd_set
        FD_ZERO( &hFds );
        FD_SET( STDIN, &hFds );

        hTimeout.tv_sec = 2;
        hTimeout.tv_usec = 0;

        //
        // Wait for something to happen (key in or host in)
        //
        nError = select(
                    STDIN + 1,
                    &hFds,
                    NULL,
                    NULL,
                    &hTimeout
                    );
        
        if ( nError == 0 )
		{
            // Maintain keep-alive
            if ( host_port_ping_norsp( ) < 0 )
            {
                qprintf( "\nFailed to issue cc1310 ping\n" );
                break;
            }
			continue;
		}        
        // Key stroke
        //
        if ( FD_ISSET ( STDIN, &hFds ))
        {
            char nChar[3];
            
            // Read single character
            if ( read ( STDIN, &nChar, 1 ) > 0 )
            {
                if (( nChar[0] == 'q' ) || ( nChar[0] == 'Q' ))
                {
                    break; // break loop without setting quit flag
                }
                // Check character pressed
                switch( nChar[0] )                
                {
                case '+':
                case '=':
                    nFrequencyOffset += 10;
                    if ( nFrequencyOffset > 50000 )
                        nFrequencyOffset = 50000;
                    fUpdateFrequency = true;
                    break;
                case '}':
                case ']':
                    nFrequencyOffset += 100;
                    if ( nFrequencyOffset > 50000 )
                        nFrequencyOffset = 50000;
                    fUpdateFrequency = true;
                    break;
                case '#':
                case '~':
                    nFrequencyOffset += 1000;
                    if ( nFrequencyOffset > 50000 )
                        nFrequencyOffset = 50000;
                    fUpdateFrequency = true;
                    break;
                case '-':
                case '_':
                    nFrequencyOffset -= 10;
                    if ( nFrequencyOffset < -50000 )
                        nFrequencyOffset = -50000;
                    fUpdateFrequency = true;
                    break;
                case '{':
                case '[':
                    nFrequencyOffset -= 100;
                    if ( nFrequencyOffset < -50000 )
                        nFrequencyOffset = -50000;
                    fUpdateFrequency = true;
                    break;
                case '@':
                case '\'':
                    nFrequencyOffset -= 1000;
                    if ( nFrequencyOffset < -50000 )
                        nFrequencyOffset = -50000;
                    fUpdateFrequency = true;
                    break;

                default:
                    sg_fQuit = true;
                    break;            
                }
            }

            if ( fUpdateFrequency )
            {
                fUpdateFrequency = false;
                if ( host_port_set_frequency_offset( nFrequencyOffset ) < 0 )
                {
                    qprintf( "\nFailed to update frequency\n" );
                    break;
                }

                // Display
                qprintf( "\rCurrent Frequency Offset : %d Hz     ", nFrequencyOffset ); 
            }
        }
    }

    qprintf( "\n" );

    if ( sg_fQuit == true )
    {
        FILE *pFile;

        qprintf( "\nSaving new offset to file %s\n", sg_szOffsetFile );
    
        pFile = fopen( sg_szOffsetFile, "w+t" );
        if ( pFile == NULL )
        {
            fprintf( stdout, "Failed to open output file\n" );
        }
        else
        {
            if ( fprintf( pFile,"%d", nFrequencyOffset ) <= 0 )
                fprintf( stdout, "Failed to write to output file\n" );
            fclose( pFile );
        }
    }
    else
    {
        qprintf( "\nWarning: Offset changed not saved\n");
    }

    //
    // Close host connection
    //
    host_comms_close( );

    tcsetattr( 0, TCSANOW, &hOldSettings );
    
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   print_usage
//
// Print command line usage
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void print_usage( void )
{
    int i;

    fprintf( stdout, "usage:\n");
    fprintf( stdout, "  iceqube_icenetc_example.elf [-p=X][-l=X ][-rf=X]\n");
    fprintf( stdout, "\t-p=[0-2,255]    cc1310 crisp module extension slot \n");
    fprintf( stdout, "\t                0-2, or 255 for auto (default 255)\n");
    fprintf( stdout, "\t-l=[0-max]     listen channel 0 (default 0)\n" );
    fprintf( stdout, "\t-rf=[-50000-50000, or file] frequency offset, or \n" );
    fprintf( stdout, "\t                file containing offset (default %s). \n", RF_OFFSET_FILE );
    fprintf( stdout, "\t-s=[file]       File to save configuration in\n" );
    fprintf( stdout, "\t                (default %s, or file specidied in -rf). \n", RF_OFFSET_FILE );

    fprintf( 
        stdout, 
        "\t- For 868Mhz the max channel number is %d\n", 
        MAX_CHANNEL( REGULATORY_EU_2000_299_EC1 )
        );

    fprintf( 
        stdout, 
        "\t- for 915Mhz the max channel number is %d\n\n", 
        MAX_CHANNEL( REGULATORY_FCC )
        );
    fflush( stdout );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   StopSigHandler
//
// Overrides default STOP signal handler, flags that process should stop.
//
// ARGUMENTS:
//  (IN)        int         Signal ID
//  (IN)        siginfo_t*  Signal info
//  (IN)        void*       Unused
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void StopSigHandler( int nSignal, siginfo_t *pSig, void *pSecret )
{
    sg_fQuit = true;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   parse_int_param
//
// Parses an integer paramter from the command line arguments
//
// ARGUMENTS:
//  (IN)     const char*     Command line paramter to search for (e.g."-r=")
//  (IN/OUT) int*            Interpreted value
//  (IN)     int             Command line paramter count
//  (IN)     char**          Command line paramters
//
// RETURNS:
//           bool            true if parameter was found and interpreted
//
///////////////////////////////////////////////////////////////////////////////
bool parse_int_param( const char* szParam, int *pVal, int nArgv, char **pArgc )
{
    int i;
    int nParamLen = strlen( szParam );

    if (( szParam == NULL ) || ( pVal == NULL ))
        return false;

    for( i = 0; i < nArgv; i ++ )
    {
        char *szCurParam = pArgc[ i ];
        int   nCurParamLen;

        if (( szCurParam == NULL ) || ( szCurParam[0] != '-' ))
            continue;
    
        nCurParamLen = strlen( szCurParam );

        if ( nCurParamLen < nParamLen )
            continue;

        if ( memcmp( szParam, szCurParam, nParamLen ) == 0 )
        {
            char* szEquals = strchr( szCurParam, '=' );
            if ( szEquals )
            {
                szEquals++;
                *pVal = atoi( szEquals );
                return true;
            }
        }
    }
    return false;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   parse_param_set
//
// Checks if a command line paramter is set
//
// ARGUMENTS:
//  (IN)     const char*     Command line paramter to search for (e.g."-?")
//  (IN)     int             Command line paramter count
//  (IN)     char**          Command line paramters
//
// RETURNS:
//           bool            true if parameter was found
//
///////////////////////////////////////////////////////////////////////////////
bool parse_param_set( const char* szParam, int nArgv, char **pArgc )
{
    int i;
    int nParamLen = strlen( szParam );

    if ( szParam == NULL )
        return false;

    for( i = 0; i < nArgv; i ++ )
    {
        char *szCurParam = pArgc[ i ];
        int   nCurParamLen;

        if (( szCurParam == NULL ) || ( szCurParam[0] != '-' ))
            continue;
    
        nCurParamLen = strlen( szCurParam );

        if ( nCurParamLen < nParamLen )
            continue;

        if ( memcmp( szParam, szCurParam, nParamLen ) == 0 )
            return true;
    }
    return false;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   parse_string_param
//
// Parses a string paramter from the command line arguments
//
// ARGUMENTS:
//  (IN)     const char*     Command line paramter to search for (e.g."-r=")
//  (IN/OUT) char*           Buffer to take string value
//  (IN)     int             Buffer size
//  (IN)     int             Command line paramter count
//  (IN)     char**          Command line paramters
//
// RETURNS:
//           bool            true if parameter was found and interpreted
//
///////////////////////////////////////////////////////////////////////////////
bool parse_string_param( 
            const char* szParam, 
            char  *szBuffer,
            int    nMaxLen,
            int    nArgv, 
            char **pArgc            
            )
{
    int i;
    int nParamLen = strlen( szParam );

    if ( szParam == NULL )
        return false;

    for( i = 0; i < nArgv; i ++ )
    {
        char *szCurParam = pArgc[ i ];
        int   nCurParamLen;

        if (( szCurParam == NULL ) || ( szCurParam[0] != '-' ))
            continue;
    
        nCurParamLen = strlen( szCurParam );

        if ( nCurParamLen < nParamLen )
            continue;

        if ( memcmp( szParam, szCurParam, nParamLen ) == 0 )
        {
            char* szEquals = strchr( szCurParam, '=' );
            if ( szEquals )
            {
                szEquals++;
                nCurParamLen = strlen( szEquals );
                if ( nCurParamLen < nMaxLen )            
                {
                    strcpy( szBuffer, szEquals );
                }
                else
                {
                    memcpy ( szBuffer, szEquals, nMaxLen - 2 );
                    szBuffer[ nMaxLen - 1 ] = 0;
                }
                return true;
            }
        }
    }
    return false;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   parse_frequency_param
//
// Parses a paramter from the command line arguments, looking for frequency.
// This can either be a numeric value between -50000 and +50000, or a file
// name containing the value.
//
// ARGUMENTS:
//  (IN)     const char*     Command line paramter to search for (e.g."-r=")
//  (IN/OUT) int*            Frequency value
//  (IN)     int             Command line paramter count
//  (IN)     char**          Command line paramters
//
// RETURNS:
//           bool            true if parameter was found and interpreted
//
///////////////////////////////////////////////////////////////////////////////
bool parse_frequency_param( 
            const char* szParam, 
            int   *pVal,
            int    nArgv, 
            char **pArgc            
            )
{
    int  i;
    char szStringParam[ MAX_PATH ];
    int  nParamLen = strlen( szParam );
    bool fFound = false;
    bool fIsDigit = true;

    if ( szParam == NULL )
        return false;

    if ( pVal == NULL )
        return false;

    for( i = 0; i < nArgv; i ++ )
    {
        char *szCurParam = pArgc[ i ];
        int   nCurParamLen;

        if (( szCurParam == NULL ) || ( szCurParam[0] != '-' ))
            continue;
    
        nCurParamLen = strlen( szCurParam );

        if ( nCurParamLen < nParamLen )
            continue;

        if ( memcmp( szParam, szCurParam, nParamLen ) == 0 )
        {
            char* szEquals = strchr( szCurParam, '=' );
            if ( szEquals )
            {
                szEquals++;
                nCurParamLen = strlen( szEquals );
                if ( nCurParamLen < ( MAX_PATH - 2 ))            
                {
                    strcpy( szStringParam, szEquals );
                }
                else
                {
                    memcpy ( szStringParam, szEquals, MAX_PATH - 2 );
                    szStringParam[ MAX_PATH - 1 ] = 0;
                }
                fFound = true;
                break;
            }
        }
    }

    // Not found, look for default file    
    if ( fFound == false )
    {
        strcpy( szStringParam, RF_OFFSET_FILE );
        fFound = true;
    }

    //
    // OK, got parameter
    //
    if ( fFound )
    {
        nParamLen = strlen( szStringParam );

        //
        // See if it's a number
        //
        for( i = 0; i < nParamLen; i ++ )
        {
            if ((( szStringParam[i] >= '0' ) &&
                 ( szStringParam[i] <= '9' )) ||
                 ( szStringParam[i] == '-' ))
                continue;
            fIsDigit = false;
            break;
        }

        if ( fIsDigit )
        {
            *pVal = atoi( szStringParam );

            // cap
            if ( *pVal < -50000 )
                *pVal = -50000;
            if ( *pVal > 50000 )
                *pVal = 50000;
            return true;
        }   
    }

    //
    // OK, but it's a filename
    //
    if ( fFound )
    {
        FILE *pFile;

        if ( sg_szOffsetFile[0] == 0 )
            strcpy( sg_szOffsetFile, szStringParam );

        pFile = fopen( szStringParam, "rt" );
        if ( pFile == NULL )
        {
            return false;
        }
        if ( fscanf( pFile,"%d", pVal ) <= 0 )
        {
            fclose( pFile );
            return false;
        }
        fclose( pFile );
        return true;
    }
    return false;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   ChannelToFrequenct
//
// Converts channel to actual frequency.
//
// ARGUMENTS:
//  (IN)     uint8_t         Regulatory
//  (IN)     uint8_t         Channel
//
// RETURNS:
//           uint32_t        Frequency in Hz
//
///////////////////////////////////////////////////////////////////////////////
uint32_t ChannelToFrequenct( uint8_t nRegulatory, uint8_t nChannel )
{
    uint32_t nFrequency = 0;

    if ( sg_nRegulatory == REGULATORY_EU_2000_299_EC1 )
    {
        if ( nChannel <= MAX_CHANNEL( REGULATORY_EU_2000_299_EC1 ))
        {
            nFrequency = sg_nChannelToFreqLtbl_88Khz[ nChannel ];
        }
    }
    else
    {
        uint32_t nMinFccFreq = 902000000ul;
        uint32_t nBw = 622000ul;
        if ( nChannel <= MAX_CHANNEL( REGULATORY_FCC ))
        {
            //
            // Work out offset from base frequency
            //
            nFrequency = nChannel;
            nFrequency *= nBw;

            //
            // Add on base frequency
            //
            nFrequency += nMinFccFreq;

            //
            // Add 2x the bandwidth, so as to avoid overlaping band boundaries
            //
            nFrequency += ( nBw << 1 );
        }
    }
    return nFrequency;
}

//////////////////////////////////// EOF //////////////////////////////////////

// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   example.c
//
// PROJECT:     iceqube_icenetc_lib example
//
// PLATFORM:    crisp linux/arm/gcc
//
// Standoff_cfg example
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2019-2020 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "host_comms.h"
#include "types.h"
#include "util.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

#define MAX_RETRY                       3

//
// Non user-settable states
//
#define DEVICE_STATE_LOW_POWER_SLEEP    0x10
#define DEVICE_STATE_RADIO_TX_POWER     0x11
#define DEVICE_STATE_RADIO_TX_CW_POWER  0x12
#define DEVICE_STATE_RADIO_RX_POWER     0x13
#define DEVICE_STATE_POST_FAIL          0xFF

//
// Enabled trigger mask
//
#define TRIGGER_INDUCTOR_2PULSE        1
#define TRIGGER_INDUCTOR_2PULSEDEL     2
#define TRIGGER_INDUCTOR_3PULSE        4
#define TRIGGER_INDUCTOR_FIELD         8
#define TRIGGER_TIMED                  16

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

const char *state_to_string( uint8_t nState );

const char *triggers_to_string( uint8_t nEnabledTriggers );

extern bool parse_int_param( 
                const char* szParam, 
                int *pVal, 
                int nArgv, 
                char **pArgc 
                );

/////////////////////////////////////////////
// PUBLIC DATA
//

/////////////////////////////////////////////
// PRIVATE DATA
//

static uint8_t  sg_nStandoffDis = 0;
static uint8_t  sg_nStandoffEn = 0;

static char     sg_szMsg[ 200 ];

static uint32_t  sg_nStandoffMod = 0;
static uint32_t  sg_nNewStandOff[ 8 ];

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_run
//
// Called by main.c to activate a connected device
//
// ARGUMENTS:
//  (IN)        uint32_t    nConnectedPeer
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int host_example_run( uint32_t nConnectedPeer )
{
    struct iceqube_device_state hInfo;
    int                         nErr;
    struct trigger_config       hTriggerConfig;
    uint32_t                    i;

    fprintf( stdout, "\n" );
    fprintf( stdout, "DEVICE ID:           %d\n", nConnectedPeer );

    //
    // Get device info
    //
    nErr = host_comms_get_device_info( &hInfo );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query device info %d\n", nErr );
        return nErr;
    }

    //
    // Display device info
    //
    fprintf( 
        stdout, 
        "  Device State:      %s\n", 
        state_to_string( hInfo.nDeviceState )
        );
    
    //
    // Get current trigger settings
    //
    nErr = host_comms_get_trigger_config( &hTriggerConfig );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query trigger configuration %d\n", nErr );
        return nErr;
    }

    //
    // Set required Stand offs
    //
    for( i = 0; i < 8; i++ )
    {
        if (( sg_nStandoffMod & ( 1 << i )) == 0 )
            continue;
        hTriggerConfig.nConnectStandoffs[ i ] = sg_nNewStandOff[ i ];
    }
    hTriggerConfig.nConnectStandoffEn &= ~sg_nStandoffDis;
    hTriggerConfig.nConnectStandoffEn |= sg_nStandoffEn;

    // Don't update the current standoff index
    hTriggerConfig.nStandoffIndex[0] = 0xFF;
    hTriggerConfig.nStandoffIndex[1] = 0xFF;
    hTriggerConfig.nStandoffIndex[2] = 0xFF;
    hTriggerConfig.nStandoffIndex[3] = 0xFF;

    nErr = host_comms_set_trigger_config( &hTriggerConfig );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query trigger configuration %d\n", nErr );
        return nErr;
    }
    
    fprintf( 
        stdout, 
        "  Enabled stand-offs:  %s\n", 
        triggers_to_string( hTriggerConfig.nConnectStandoffEn ) 
        );

    fprintf(
        stdout,
        "  Stand-offs: %ums, %ums, %ums, %ums,\n",
        hTriggerConfig.nConnectStandoffs[ 0 ], 
        hTriggerConfig.nConnectStandoffs[ 1 ],
        hTriggerConfig.nConnectStandoffs[ 2 ],
        hTriggerConfig.nConnectStandoffs[ 3 ]
        );

    fprintf(
        stdout,
        "              %ums, %ums, %ums, %ums\n",
        hTriggerConfig.nConnectStandoffs[ 4 ], 
        hTriggerConfig.nConnectStandoffs[ 5 ],
        hTriggerConfig.nConnectStandoffs[ 6 ],
        hTriggerConfig.nConnectStandoffs[ 7 ]
        );

    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   state_to_string
//
// Utility function to convert a device state into a string
//
// ARGUMENTS:
//  (IN)        uint8_t         Device state
//
// RETURNS:
//              const char*     Device state string
//
///////////////////////////////////////////////////////////////////////////////
const char *state_to_string( uint8_t nState )
{
    switch( nState )
    {
    case DEVICE_STATE_IDLE:
        return "IDLE";
    case DEVICE_STATE_ACTIVE:
        return "ACTIVE";
    case DEVICE_STATE_FIRMWARE:
        return "FIRMWARE";
    case DEVICE_STATE_LOW_POWER_SLEEP:
    case DEVICE_STATE_RADIO_TX_POWER:
    case DEVICE_STATE_RADIO_TX_CW_POWER:
    case DEVICE_STATE_RADIO_RX_POWER:
        return "TEST STATE";
    case DEVICE_STATE_POST_FAIL:
        return "POST FAILURE STATE";
    default:
        break;
    }
    return "UNKNOWN";
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   triggers_to_string
//
// Utility function to convert a mask of enabled trigger types into an ascii
// string.
//
// ARGUMENTS:
//  (IN)        uint8_t         Mask of enabled triggers
//
// RETURNS:
//              const char*     Ascii string of enabled triggers
//
///////////////////////////////////////////////////////////////////////////////
const char *triggers_to_string( uint8_t nEnabledTriggers )
{
    sg_szMsg[ 0 ] = 0;

    if ( nEnabledTriggers & TRIGGER_INDUCTOR_2PULSE )
        strcat( sg_szMsg, "2 pulse" );
    if ( nEnabledTriggers & TRIGGER_INDUCTOR_2PULSEDEL )
    {
        if ( sg_szMsg[ 0 ] != 0 )
            strcat( sg_szMsg, ", " );
        strcat( sg_szMsg, "2 long pulse" );
    }
    if ( nEnabledTriggers & TRIGGER_INDUCTOR_3PULSE )
    {
        if ( sg_szMsg[ 0 ] != 0 )
            strcat( sg_szMsg, ", " );
        strcat( sg_szMsg, "3 pulse" );
    }
    if ( nEnabledTriggers & TRIGGER_INDUCTOR_FIELD )
    {
        if ( sg_szMsg[ 0 ] != 0 )
            strcat( sg_szMsg, ", " );
        strcat( sg_szMsg, "field" );
    }
    if ( nEnabledTriggers & TRIGGER_TIMED )
    {
        if ( sg_szMsg[ 0 ] != 0 )
            strcat( sg_szMsg, ", " );
        strcat( sg_szMsg, "timed" );
    }
    if ( sg_szMsg[ 0 ] == 0 )
        strcat( sg_szMsg, "none" );

    return sg_szMsg;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_get_param_count
//
// Returns number of command line parameters supported by example
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int     Parameter count
//
///////////////////////////////////////////////////////////////////////////////
int host_example_get_param_count( void )
{
    return 12;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_parse_params
//
// Passes a command line parameter for parsing. 'p', 'i', 's', 'r' & 'l' are
// reserved by the application.
//
// ARGUMENTS:
//  (IN)        char*   Command line 
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_parse_params( char *pParam )
{
    int n;
    int i;

    if ( parse_int_param( "-2=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_nStandoffDis |= TRIGGER_INDUCTOR_2PULSE;
        else
            sg_nStandoffEn |= TRIGGER_INDUCTOR_2PULSE;
		return 0;
    }

    if ( parse_int_param( "-2l=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_nStandoffDis |= TRIGGER_INDUCTOR_2PULSEDEL;
        else
            sg_nStandoffEn |= TRIGGER_INDUCTOR_2PULSEDEL;
		return 0;
    }

    if ( parse_int_param( "-3=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_nStandoffDis |= TRIGGER_INDUCTOR_3PULSE;
        else
            sg_nStandoffEn |= TRIGGER_INDUCTOR_3PULSE;
		return 0;
    }

    if ( parse_int_param( "-f=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_nStandoffDis |= TRIGGER_INDUCTOR_FIELD;
        else
            sg_nStandoffEn |= TRIGGER_INDUCTOR_FIELD;
        return 0;
    }

    for( i = 0; i < 8; i ++ )
    {
        char szParam[ 5 ];
        sprintf( szParam, "-o%d=", i );
        if ( parse_int_param( szParam, &n, 1, &pParam ))
        {
            if (( uint32_t ) n > 3600000 )
            {
                fprintf( 
                    stdout,
                    "Invalid stand off time (-o%d=%u). Must be 0-3600000\n", 
                    i, 
                    n 
                    );            
                return -1;
            }
            sg_nStandoffMod |= ( 1 << i );
            sg_nNewStandOff[ i ] = n;
            return 0;
        }
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_print_param_help
//
// Prints help string for parameters
//
// ARGUMENTS:
//  (IN)        FILE*   Stream to write help message to
//  (IN)        int     Index of parameter to print help message for
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_print_param_help( FILE *pStream, int nIndex )
{
    switch( nIndex )
    {
    case 0:
        fprintf( pStream, "\t-2=[0-1]        Enable 2 pulse stand-offs (default 1 )\n" );
        break;

    case 1:
        fprintf( pStream, "\t-2l=[0-1]       Enable 2 long pulse stand-offs (default 1)\n" );
        break;

    case 2:
        fprintf( pStream, "\t-3=[0-1]        Enable 3 pulse stand-offs (default 1)\n" );
        break;

    case 3:
        fprintf( pStream, "\t-f=[0-1]        Enable field trigger stand-offs (default 1)\n" );
        break;

    case 4:
        fprintf( pStream, "\t-o0=[0-3600000] First stand-off [ms]\n" );
        break;

    case 5:
        fprintf( pStream, "\t-o1=[0-3600000] Second stand-off [ms]\n" );
        break;

    case 6:
        fprintf( pStream, "\t-o2=[0-3600000] Third stand-off [ms]\n" );
        break;

    case 7:
        fprintf( pStream, "\t-o3=[0-3600000] Fourth stand-off [ms]\n" );
        break;

    case 8:
        fprintf( pStream, "\t-o4=[0-3600000] Fifth stand-off [ms]\n" );
        break;

    case 9:
        fprintf( pStream, "\t-o5=[0-3600000] Sixth stand-off [ms]\n" );
        break;

    case 10:
        fprintf( pStream, "\t-o6=[0-3600000] Seventh stand-off [ms]\n" );
        break;

    case 11:
        fprintf( pStream, "\t-o7=[0-3600000] Eighth stand-off [ms]\n" );
        break;

    default:
        break;
    }
    return 0;
}

//////////////////////////////////// EOF //////////////////////////////////////

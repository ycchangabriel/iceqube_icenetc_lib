// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   example.c
//
// PROJECT:     iceqube_icenetc_lib example
//
// PLATFORM:    crisp linux/arm/gcc
//
// Deactivate example
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "host_comms.h"
#include "types.h"
#include "util.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

#define MAX_RETRY                           3

//
// Non user-settable states
//
#define DEVICE_STATE_LOW_POWER_SLEEP    0x10
#define DEVICE_STATE_RADIO_TX_POWER     0x11
#define DEVICE_STATE_RADIO_TX_CW_POWER  0x12
#define DEVICE_STATE_RADIO_RX_POWER     0x13
#define DEVICE_STATE_POST_FAIL          0xFF

//
// Enabled trigger mask
//
#define TRIGGER_INDUCTOR_2PULSE        1
#define TRIGGER_INDUCTOR_2PULSEDEL     2
#define TRIGGER_INDUCTOR_3PULSE        4
#define TRIGGER_INDUCTOR_FIELD         8
#define TRIGGER_TIMED                  16

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

const char *state_to_string( uint8_t nState );
const char *triggers_to_string( uint8_t nEnabledTriggers );
const char *channels_to_string( uint8_t *pChannels );

extern bool parse_int_param( 
                const char* szParam, 
                int *pVal, 
                int nArgv, 
                char **pArgc 
                );

extern bool parse_string_param( 
                const char* szParam, 
                char  *szBuffer,
                int    nMaxLen,
                int    nArgv, 
                char **pArgc            
                );

/////////////////////////////////////////////
// PUBLIC DATA
//

/////////////////////////////////////////////
// PRIVATE DATA
//

static char sg_szMsg[200];
static bool sg_fSetTime = false;
static bool sg_fUTC     = true;
static bool sg_fSample  = false;

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_run
//
// Called by main.c to gather device information from a connected device
//
// ARGUMENTS:
//  (IN)        uint32_t    nConnectedPeer
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int host_example_run( uint32_t nConnectedPeer )
{
    struct iceqube_device_state          hInfo;
    struct iceqube_system_power_up_state hPostState;
    struct trigger_config                hTriggerConfig;
    struct iceqube_timed_ex_mode         hTimedExMode;
    struct iceqube_data_capture_config   hDataCaptureConfig;

    int                                  nErr;
    int                                  nLastRssi;
    struct tm                            hLocalTime;
    int                                  nTzOffset = sg_fUTC ? 0 : util_tzoffset( );
    int64_t                              nTimeDiff;

    //
    // Get last RSSI for info
    //
    nErr = host_get_last_rssi( &nLastRssi );
    if ( nErr != 0 )
    {
        fprintf( stdout, "failed to query RSSI %d\n", nErr );    
        nLastRssi = -32768;
    }

    fprintf( stdout, "\n" );
    fprintf( stdout, "DEVICE ID:             %d  (%ddBm)\n", nConnectedPeer, nLastRssi );

    //
    // Get device info
    //
    nErr = host_comms_get_device_info( &hInfo );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query device info %d\n", nErr );
        return nErr;
    }

    //
    // Work out time difference between reported time and 
    // icehub time
    //
    if ( sg_fSetTime )
    {        
        nTimeDiff = util_time( ) - hInfo.nDeviceTime;
    }

    //
    // Display device info
    //
    fprintf( stdout, "  Device State:          %s\n", state_to_string( hInfo.nDeviceState ));
    fprintf( stdout, "  Firmware Version:      %d.%03d\n", ( hInfo.nFwVersion >> 8 ), ( hInfo.nFwVersion & 0xFF ));

    util_localtime(
        hInfo.nDeviceTime + nTzOffset,
        &hLocalTime
        );

    fprintf( stdout, "  Device Time:           %s", asctime( &hLocalTime ));// Adds \n to end of string
    fprintf( stdout, "  Battery level:         %d mV\n", hInfo.nBatteryLevel );
    fprintf( stdout, "  Data:                  %d days\n", hInfo.nDays + hInfo.nActiveDay );
    fprintf( stdout, "  Clear-on-activate:     %s\n", hInfo.nClearDataOnActivate ? "true" : "false" );
    fprintf( stdout, "  Regulatory:            %s\n", hInfo.nRegulatory == 1 ? "eu_2013_752" : "us_fcc" );
    if ( hInfo.nFlags == 0 )
        fprintf( stdout, "  Flags:                 OK\n");
    else
        fprintf( stdout, "  Flags:                 unexpected reset\n");

    //
    // Post failure state, if applicable
    //
    if (( hInfo.nDeviceState == DEVICE_STATE_POST_FAIL ) || ( hInfo.nFlags != 0 ))
    {
        nErr = host_comms_get_post_info( &hPostState );
        if ( nErr != 0 )
        {
            fprintf( stdout, "  ERROR: Failed to query power-up self-test info %d\n", nErr );
            return nErr;
        }

        // Post failure cause
        if ( hInfo.nDeviceState == DEVICE_STATE_POST_FAIL )
        {
            fprintf( stdout, "  POWER-UP SELF-TEST FAILURES:\n");
            if ( hPostState.nRtc != 0 )
                fprintf( stdout, "      RTC:             %d\n", hPostState.nRtc );
            if ( hPostState.nAccelerometer != 0 )
                fprintf( stdout, "      Accelerometer:   %d\n", hPostState.nAccelerometer );
            if ( hPostState.nExternflash != 0 )
                fprintf( stdout, "      Flash:           %d\n", hPostState.nExternflash );
            if ( hPostState.nCircularBuffer != 0 )
                fprintf( stdout, "      Buffer:          %d\n", hPostState.nCircularBuffer );
        }

        // Reset cause (on POST or if unexpected reset flag is set in device info)
        fprintf( stdout, "  Reset Cause:           %d\n", hPostState.nResetCause );
        fprintf( stdout, "  Reset Stack Ptr:       0x%X\n", hPostState.nStackAtReset );
    }

    //
    // Trigger Configuration
    //
    nErr = host_comms_get_trigger_config( &hTriggerConfig );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query trigger config %d\n", nErr );
        return nErr;
    }

    fprintf( stdout, "  Enabled Triggers:      %s\n", triggers_to_string( hTriggerConfig.nEnabledTriggers ));
    fprintf( stdout, "  2-pulse chnl:          %s\n", channels_to_string( hTriggerConfig.n2PulseFreq ));
    fprintf( stdout, "  2-long pulse chnl:     %s\n", channels_to_string( hTriggerConfig.n2LongPulseFreq ));
    fprintf( stdout, "  3-pulse chnl:          %s\n", channels_to_string( hTriggerConfig.n3PulseFreq ));
    fprintf( stdout, "  Field chnl:            %s\n", channels_to_string( hTriggerConfig.nFieldFreq ));
    fprintf( stdout, "  Timed chnl:            %s\n", channels_to_string( hTriggerConfig.nTimedFreq ));
    fprintf( stdout, "  Trigger stand-off on:  %s\n", triggers_to_string( hTriggerConfig.nConnectStandoffEn ));
    fprintf( 
        stdout, 
        "  Stand-off times:       %d ms, %d ms, %d ms, %d ms,\n", 
        hTriggerConfig.nConnectStandoffs[0], 
        hTriggerConfig.nConnectStandoffs[1], 
        hTriggerConfig.nConnectStandoffs[2], 
        hTriggerConfig.nConnectStandoffs[3] 
        );
    fprintf( 
        stdout, 
        "                         %d ms, %d ms, %d ms, %d ms\n", 
        hTriggerConfig.nConnectStandoffs[4], 
        hTriggerConfig.nConnectStandoffs[5], 
        hTriggerConfig.nConnectStandoffs[6], 
        hTriggerConfig.nConnectStandoffs[7] 
        );
    fprintf( 
        stdout, 
        "  User stand-off:        %d ms\n", 
        hTriggerConfig.nConnectStandoffs[8] 
        );

    fprintf( 
        stdout, 
        "  2-pulse stand-off:     Index %d (%d ms)\n", 
        hTriggerConfig.nStandoffIndex[0], 
        hTriggerConfig.nConnectStandoffs[ (hTriggerConfig.nStandoffIndex[0] <= 8) ? hTriggerConfig.nStandoffIndex[0]: 0] 
        );
    fprintf( 
        stdout, 
        "  2-lng-pulse stand-off: Index %d (%d ms)\n", 
        hTriggerConfig.nStandoffIndex[1], 
        hTriggerConfig.nConnectStandoffs[ (hTriggerConfig.nStandoffIndex[1] <= 8) ? hTriggerConfig.nStandoffIndex[1]: 0] 
        );
    fprintf( 
        stdout, 
        "  3-pulse stand-off:     Index %d (%d ms)\n", 
        hTriggerConfig.nStandoffIndex[2], 
        hTriggerConfig.nConnectStandoffs[ (hTriggerConfig.nStandoffIndex[2] <= 8) ? hTriggerConfig.nStandoffIndex[2]: 0] 
        );
    fprintf( 
        stdout, 
        "  field stand-off:       Index %d (%d ms)\n", 
        hTriggerConfig.nStandoffIndex[3], 
        hTriggerConfig.nConnectStandoffs[ (hTriggerConfig.nStandoffIndex[3] <= 8) ? hTriggerConfig.nStandoffIndex[3]: 0]
        );

    fprintf( 
        stdout, 
        "  Timed connect:         %s\n", 
        hTriggerConfig.nTimedFlags & 1 ? "When active & idle" : "When active only" 
        );

    fprintf( stdout, "  Timed offset:          %d s\n", hTriggerConfig.nTimedOffset );
    fprintf( stdout, "  Timed interval:        %d s\n", hTriggerConfig.nTimedInterval );
    fprintf( stdout, "  Timed ex interval:     %d s\n", hTriggerConfig.nTimedExInterval );
    fprintf( stdout, "  Timed ex duration:     %d s\n", hTriggerConfig.nTimedExDuration );

    //
    // Timed EX configuration
    //
    nErr = host_comms_get_timed_ex_mode( &hTimedExMode );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query timed ex config %d\n", nErr );
        return nErr;
    }
    fprintf( stdout, "  Timed ex:              %s\n", hTimedExMode.nTimedExModeEn ? "enabled" : "disabled" );
    
    //
    // Data Capture Configuration
    //
    nErr = host_comms_get_data_capture_config( &hDataCaptureConfig );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query data capture config %d\n", nErr );
        return nErr;
    }
    switch( hDataCaptureConfig.nSampleRate )
    {
    case 0:
        fprintf( stdout, "  Sample rate:           6.25 Hz\n" );
        break;
    case 1:
        fprintf( stdout, "  Sample rate:           12.5 Hz\n" );
        break;
    case 2:
        fprintf( stdout, "  Sample rate:           25.0 Hz\n" );
        break;
    default:
        fprintf( stdout, "  Sample rate:           unknown rate\n" );
        break;
    }    
    switch( hDataCaptureConfig.nSensitivity )
    {
    case 0:
        fprintf( stdout, "  Sensitivity:           +-2 G\n" );
        break;
    case 1:
        fprintf( stdout, "  Sensitivity:           +-4 G\n" );
        break;
    case 2:
        fprintf( stdout, "  Sensitivity:           +-8 G\n" );
        break;
    default:
        fprintf( stdout, "  Sensitivity:           unknown\n" );
        break;
    }

    //
    // Get a raw sample if required
    //
    if ( sg_fSample )
    {
        //
        // Only if device is IDLE or ACTIVE, won't work
        // in any other state
        //
        if (( hInfo.nDeviceState == DEVICE_STATE_IDLE ) ||
            ( hInfo.nDeviceState == DEVICE_STATE_ACTIVE ))
        {
            struct iceqube_sample hRawData;

            //
            // Get a raw sample 
            //
            nErr = host_comms_get_sample( 1, &hRawData );
            if ( nErr != 0 )
            {
                fprintf( stdout, "  ERROR: Failed to query accelerometer sample %d\n", nErr );
                return nErr;
            }

            fprintf( 
                stdout, 
                "  Sample [mG]:           %d, %d, %d\n",
                hRawData.nX,
                hRawData.nY,
                hRawData.nZ
                 );
        }
    }

    //
    // Only as required
    //
    if ( sg_fSetTime )
    {
        if ( nTimeDiff != 0 )
        {
            //
            // Update device time 
            //
            nErr = host_comms_set_time( util_time( ));

            if ( nErr != 0 )
            {
                fprintf( stdout, "  ERROR: Failed to set device time %d\n", nErr );
                return nErr;
            }
            fprintf( stdout, "  Device time updated\n" );
        }
        else
        {
            fprintf( stdout, "  Device time identical to system time, not updated\n" );
        }
    }

    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   state_to_string
//
// Utility function to convert a device state into a string
//
// ARGUMENTS:
//  (IN)        uint8_t         Device state
//
// RETURNS:
//              const char*     Device state string
//
///////////////////////////////////////////////////////////////////////////////
const char *state_to_string( uint8_t nState )
{
    switch( nState )
    {
    case DEVICE_STATE_IDLE:
        return "IDLE";
    case DEVICE_STATE_ACTIVE:
        return "ACTIVE";
    case DEVICE_STATE_FIRMWARE:
        return "FIRMWARE";
    case DEVICE_STATE_LOW_POWER_SLEEP:
    case DEVICE_STATE_RADIO_TX_POWER:
    case DEVICE_STATE_RADIO_TX_CW_POWER:
    case DEVICE_STATE_RADIO_RX_POWER:
        return "TEST STATE";
    case DEVICE_STATE_POST_FAIL:
        return "POST FAILURE STATE";
    default:
        break;
    }
    return "UNKNOWN";
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   triggers_to_string
//
// Utility function to convert a mask of enabled trigger types into an ascii
// string.
//
// ARGUMENTS:
//  (IN)        uint8_t         Mask of enabled triggers
//
// RETURNS:
//              const char*     Ascii string of enabled triggers
//
///////////////////////////////////////////////////////////////////////////////
const char *triggers_to_string( uint8_t nEnabledTriggers )
{
    sg_szMsg[0] = 0;

    if ( nEnabledTriggers & TRIGGER_INDUCTOR_2PULSE )
        strcat( sg_szMsg, "2 pulse" );
    if ( nEnabledTriggers & TRIGGER_INDUCTOR_2PULSEDEL )
    {
        if ( sg_szMsg[0] != 0 )
            strcat( sg_szMsg, ", " );
        strcat( sg_szMsg, "2 long pulse" );
    }
    if ( nEnabledTriggers & TRIGGER_INDUCTOR_3PULSE )
    {
        if ( sg_szMsg[0] != 0 )
            strcat( sg_szMsg, ", " );
        strcat( sg_szMsg, "3 pulse" );
    }
    if ( nEnabledTriggers & TRIGGER_INDUCTOR_FIELD )
    {
        if ( sg_szMsg[0] != 0 )
            strcat( sg_szMsg, ", " );
        strcat( sg_szMsg, "field" );
    }
    if ( nEnabledTriggers & TRIGGER_TIMED )
    {
        if ( sg_szMsg[0] != 0 )
            strcat( sg_szMsg, ", " );
        strcat( sg_szMsg, "timed" );
    }
    if ( sg_szMsg[0] == 0 )
        strcat( sg_szMsg, "none" );

    return sg_szMsg;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   channels_to_string
//
// Utility function to convert an array of channels into an ascii string list,
//
// ARGUMENTS:
//  (IN)        uint8_t*        Array of 3 channels
//
// RETURNS:
//              const char*     Ascii string of channels
//
///////////////////////////////////////////////////////////////////////////////
const char* channels_to_string( uint8_t *pChannels )
{
    char szTmp[10];
    sg_szMsg[0] = 0;

    if (( pChannels[0] == 0xFF ) && 
        ( pChannels[1] == 0xFF ) &&
        ( pChannels[2] == 0xFF ))
    {
        strcpy( sg_szMsg, "none" ); // Shouldn't get here
        return sg_szMsg;
    }
    if ( pChannels[0] != 0xFF )
    {
        sprintf( szTmp, "%d", pChannels[0] );
        strcat( sg_szMsg, szTmp );
    }
    if ( pChannels[1] != 0xFF )
    {
        if ( sg_szMsg[0] != 0 )
            strcat( sg_szMsg, ", " );
        sprintf( szTmp, "%d", pChannels[1] );
        strcat( sg_szMsg, szTmp );
    }
    if ( pChannels[2] != 0xFF )
    {
        if ( sg_szMsg[0] != 0 )
            strcat( sg_szMsg, ", " );
        sprintf( szTmp, "%d", pChannels[2] );
        strcat( sg_szMsg, szTmp );
    }
    return sg_szMsg;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_get_param_count
//
// Returns number of command line parameters supported by example
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int     Parameter count
//
///////////////////////////////////////////////////////////////////////////////
int host_example_get_param_count( void )
{
    return 3;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_parse_params
//
// Passes a command line parameter for parsing. 'p', 'i', 's', 'r' & 'l' are
// reserved by the application.
//
// ARGUMENTS:
//  (IN)        char*   Command line 
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_parse_params( char *pParam )
{
    int  n;
    char szStr[ 10 ];

    if ( parse_int_param( "-x=", &n, 1, &pParam ))
    {
        if ( n == 0 )
                sg_fSetTime = 0;
            else
                sg_fSetTime = 1;
		return 0;
    }

    if ( parse_string_param( "-t=", szStr, 10, 1, &pParam ))
    {
        if ( strchr( szStr, 'l' ))
            sg_fUTC = false;
        else if ( strchr( szStr, 'u' ))
            sg_fUTC = true;
        else
        {
            fprintf( stdout, "Invalid time local specifed. Must be u or l.\n");
            return -1;
        }
		return 0;
    }

    if ( parse_int_param( "-a=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_fSample = 0;
        else
            sg_fSample = 1;
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_print_param_help
//
// Prints help string for parameters
//
// ARGUMENTS:
//  (IN)        FILE*   Stream to write help message to
//  (IN)        int     Index of parameter to print help message for
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_print_param_help( FILE *pStream, int nIndex )
{
    switch( nIndex )
    {
    case 0:
        fprintf( pStream, "\t-x=[0-1]        Sync time (default 0)\n" );
        break;
    case 1:
        fprintf( pStream, "\t-t=[l/u]        Display time as local time or utc (default utc)\n" );
        break;
    case 2:
        fprintf( pStream, "\t-a=[0-1]        Take accelerometer reading (default 0)\n" );
        break;
    default:
        break;
    }
    return 0;
}

//////////////////////////////////// EOF //////////////////////////////////////

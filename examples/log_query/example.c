// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   example.c
//
// PROJECT:     iceqube_icenetc_lib example
//
// PLATFORM:    crisp linux/arm/gcc
//
// Log dump example
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "host_comms.h"
#include "types.h"
#include "util.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

#define MAX_RETRY                           3

//
// Maximum number of log entries possible
//
#define MAX_DUMP                768

//
// Filter types
//
#define LOG_FILTER_SYSTEM                   0x00
#define LOG_FILTER_CONFIG                   0x01
#define LOG_FILTER_RF                       0x02
#define LOG_FILTER_DATA                     0x04
#define LOG_FILTER_TRIGGER                  0x08
#define LOG_FILTER_PROFILER_SLEEP           0x10
#define LOG_FILTER_PROFILER_DATA_CAPTURE    0x20
#define LOG_FILTER_PROFILER_RADIO_SESSION   0x40
#define LOG_FILTER_PROFILER_FIRMWARE_UPLOAD 0x80

//
// Log event IDs
// New entries must also be added to log_msg_to_string( )
//
#define LOG_SYS_START                       0x0001
#define LOG_SYS_DEVICE_STATE                0x0002
#define LOG_SYS_RESET_REASON                0x0003
#define LOG_SYS_RESET_STACK                 0x0004
#define LOG_SYS_TIME_UPDATED                0x0005
#define LOG_SYS_REGULATORY_CHANGED          0x0006
#define LOG_SYS_DEVICEID_SET                0x0007
#define LOG_SYS_MGF_PORT_ATTACHED           0x0008
#define LOG_SYS_LOG_CLEARED                 0x0009
#define LOG_SYS_ACTIVATION_MODE_CHANGED     0x0010
#define LOG_SYS_FIRMWARE_UPDATED            0x0011
#define LOG_SYS_BOOTLOADER_UPDATED          0x0012
#define LOG_SYS_SESSION_TIMEOUT             0x0080

#define LOG_CFG_DATA_CAPTURE_CHANGED        0x0101
#define LOG_CFG_DATA_INTERPRETOR_CHANGED    0x0102
#define LOG_CFG_TRIGGER_CHANGED             0x0103

#define LOG_RF_SESSION_ATTEMPT              0x0201
#define LOG_RF_SESSION_ICEC_CONNECTION      0x0202
#define LOG_RF_SESSION_ICET_CONNECTION      0x0203
#define LOG_RF_SESSION_EXCHANGE             0x0204
#define LOG_RF_SESSION_DISCONNECTED         0x0205

#define LOG_PROFILE_SLEEP                   0x0301
#define LOG_PROFILE_DATA_CAPTURE            0x0302
#define LOG_PROFILE_RADIO_SESSION           0x0303
#define LOG_PROFILE_FIRMWARE_UPLOAD         0x0304
#define LOG_PROFILE_CPU_ON_TIME             0x0305
#define LOG_PROFILE_CPU_SLEEP_TIME          0x0306
#define LOG_PROFILE_RADIO_SESSION_TIME      0x0307
#define LOG_PROFILE_RADIO_TX_TIME           0x0308
#define LOG_PROFILE_RADIO_RX_TIME           0x0309
#define LOG_PROFILE_MEM_IC_TIME             0x030A
#define LOG_PROFILE_ACCELEROMETER_TIME      0x030B
#define LOG_DATA_DAY_STORED                 0x0401

#define LOG_TRIG_EVENT                      0x0801

//
// Reset causes
//
#define RESET_CAUSE_NORMAL               (uint8_t) 0x00
#define RESET_CAUSE_PIN_RESET            (uint8_t) 0x01
#define RESET_CAUSE_VDDS_LOSS            (uint8_t) 0x02
#define RESET_CAUSE_VDD_LOSS             (uint8_t) 0x03
#define RESET_CAUSE_VDDR_LOSS            (uint8_t) 0x04
#define RESET_CAUSE_CLOCK_LOSS           (uint8_t) 0x05
#define RESET_CAUSE_SYSRESET             (uint8_t) 0x06
#define RESET_CAUSE_PRCM_WARM            (uint8_t) 0x07
#define RESET_CAUSE_WAKEUP_FROM_SHUTDOWN (uint8_t) 0x08
#define RESET_CAUSE_NM_FAULT             (uint8_t) 0x0A
#define RESET_CAUSE_HARD_FAULT           (uint8_t) 0x0B
#define RESET_CAUSE_MPU_FAULT            (uint8_t) 0x0C
#define RESET_CAUSE_BUS_FAULT            (uint8_t) 0x0D
#define RESET_CAUSE_USAGE_FAULT          (uint8_t) 0x0E
#define RESET_CAUSE_WATCHDOG             (uint8_t) 0x0F
#define RESET_CAUSE_FIRMWARE_UPDATE      (uint8_t) 0x10

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

void host_example_print_log( 
                FILE *pStream, 
                struct log_entry *pEntry 
                );

const char* logeventtype_to_string( 
                uint8_t nFilter 
                );

extern bool parse_int_param( 
                const char* szParam, 
                int *pVal, 
                int nArgv, 
                char **pArgc 
                );

extern bool parse_string_param( 
                const char* szParam, 
                char  *szBuffer,
                int    nMaxLen,
                int    nArgv, 
                char **pArgc            
                );
/////////////////////////////////////////////
// PUBLIC DATA
//

/////////////////////////////////////////////
// PRIVATE DATA
//

static int  sg_nLocalTzOffset = 0;
static bool sg_fUTC = true;
static int  sg_nMaxDownload = 50;

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_run
//
// Called by main.c to log dump a connected device
//
// ARGUMENTS:
//  (IN)        uint32_t    nConnectedPeer
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int host_example_run( uint32_t nConnectedPeer )
{
    struct log_count            hCount;    
    int                         nErr    = 0;
    int                         nCount  = MAX_DUMP;
    int                         nOffset = 0;
    struct log_entry            hEntries[ MAX_DUMP ];
    int                         nTotal  = 0;
    int                         i;

    // Local timezone offset
    sg_nLocalTzOffset = ( sg_fUTC ? 0 : util_tzoffset( ));

    fprintf( stdout, "\n" );
    fprintf( stdout, "DEVICE ID:           %d\n", nConnectedPeer );

    //
    // Get log count
    //
    nErr = host_comms_get_log_entry_count( &hCount );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query log info %d\n", nErr );
        return nErr;
    }

    if ( hCount.nCount == 0 )
    {
        fprintf( stdout, "  ERROR: Device log is empty\n" );
        return 0;
    }

    // Cap to lowest common amount of entries
    if ( nCount > MAX_DUMP )
        nCount = MAX_DUMP;
    if ( nCount > hCount.nCount )
        nCount = hCount.nCount;
    if ( nCount > sg_nMaxDownload )
        nCount = sg_nMaxDownload;

    //
    // Download all the logs (no more than 30 at a time)
    //
    while( nCount )
    {
        struct log_dump             hDump;

        hDump.nStructVersion = STRUCT_LOG_DUMP_VERSION;
        hDump.nOffset = nOffset;
        hDump.nCount  = ( nCount < 30  )? nCount : 30;

        nErr = host_comms_get_log( &hDump, &hEntries[ nOffset ] );

        if ( nErr != 0 )
        {
            fprintf( stdout, "  ERROR: Failed to query device log %d\n", nErr );
            break; // print out what we have so far
        }
    
        if ( hDump.nCount == 0 )
            break;

        nCount  -= hDump.nCount;
        nOffset += hDump.nCount;
        nTotal  += hDump.nCount;
    }

    //
    // Print the logs
    //
    for( i = 0; i < nTotal; i ++ )
    {
        host_example_print_log( stdout, &hEntries[ i ] );
    }

    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_print_log
//
// Formatted output of an iceqube device log
//
// ARGUMENTS:
//  (IN)        FILE*           Output stream
//  (IN)        log_entry*      Log entry to display
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void host_example_print_log( FILE *pStream, struct log_entry *pEntry )
{
    char       szMsg[200];    
    struct tm  hLocalTime;

    if ( pEntry->nValid == 0xFF )
        return;

    //
    // Get the timestamp
    //
    util_localtime( 
        pEntry->nTimeStamp + sg_nLocalTzOffset, 
        &hLocalTime 
        );

    //
    // Interpret event info
    //
    switch( pEntry->nEvent )
    {
        //
        // SYSTEM messages
        //
    case LOG_SYS_START:
        sprintf( szMsg, "iceqube start-up (error code: %d)", (int8_t)( pEntry->nData & 0xFF ));
        break;

    case LOG_SYS_DEVICE_STATE:
        sprintf( szMsg, "device state set to 0x%X", pEntry->nData );
        break;

    case LOG_SYS_RESET_REASON:
        if ( pEntry->nData == RESET_CAUSE_WATCHDOG )
            sprintf( szMsg, "reset cause 0x%X - watchdog", pEntry->nData );
        else if ( pEntry->nData == RESET_CAUSE_FIRMWARE_UPDATE )
            sprintf( szMsg, "reset cause 0x%X - firmware update", pEntry->nData );
        else
            sprintf( szMsg, "reset cause 0x%X", pEntry->nData );
        break;

    case LOG_SYS_RESET_STACK:
        sprintf( szMsg, "reset at sp 0x%08X", pEntry->nData );
        break;

    case LOG_SYS_TIME_UPDATED:
        sprintf( szMsg, "system time updated" );
        break;

    case LOG_SYS_REGULATORY_CHANGED:
        if ( pEntry->nData == 1 )
            sprintf( szMsg, "regulatory region changed to 2013/752/eu" );
        else if ( pEntry->nData == 2 )
            sprintf( szMsg, "regulatory region changed to fcc" );
        else
            sprintf( szMsg, "regulatory region changed to unknown region (%d)", pEntry->nData );
        break;

    case LOG_SYS_DEVICEID_SET:
        sprintf( szMsg, "device id/mac changed to %d", pEntry->nData );
        break;

    case LOG_SYS_LOG_CLEARED:
        sprintf( szMsg, "log file cleared" );
        break;

    case LOG_SYS_MGF_PORT_ATTACHED:
        sprintf( szMsg, "manufacturing uart %s", pEntry->nData ? "attached" : "detached");
        break;

    case LOG_SYS_ACTIVATION_MODE_CHANGED:
        sprintf( szMsg, "clear data on activate, set to %s", pEntry->nData ? "true" : "false");
        break;

    case LOG_SYS_FIRMWARE_UPDATED:
        sprintf( szMsg, "firmware updated to %d.%03d", ( pEntry->nData >> 16 ), ( pEntry->nData & 0xFFFF ));
        break;

    case LOG_SYS_BOOTLOADER_UPDATED:
        sprintf( szMsg, "bootloader updated to %d.%03d", ( pEntry->nData >> 16 ), ( pEntry->nData & 0xFFFF ));
        break;

    case LOG_SYS_SESSION_TIMEOUT:
        sprintf( szMsg, "no activity timeout, session closed" );
        break;

        //
        // CONFIG messages
        //
    case LOG_CFG_DATA_CAPTURE_CHANGED:
        sprintf( szMsg, "data capture configuration changed" );
        break;

    case LOG_CFG_DATA_INTERPRETOR_CHANGED:
        sprintf( szMsg, "data interpretor configuration changed" );
        break;

    case LOG_CFG_TRIGGER_CHANGED:
        sprintf( szMsg, "trigger configuration changed" );
        break;

        //
        // RF messages
        //
    case LOG_RF_SESSION_ATTEMPT:
        sprintf( 
            szMsg, 
            "session connect started (%d,%d,%d)",
            (( pEntry->nData >> 16 ) & 0xFF ),
            (( pEntry->nData >> 8 ) & 0xFF ),
            ( pEntry->nData & 0xFF )
            );
        break;

    case LOG_RF_SESSION_ICEC_CONNECTION:
        sprintf( szMsg, "icec/binary session (re)connected to %d", pEntry->nData );
        break;

    case LOG_RF_SESSION_ICET_CONNECTION:
        sprintf( szMsg, "icet/terminal session (re)connected to %d", pEntry->nData );
        break;

    case LOG_RF_SESSION_EXCHANGE:
        sprintf( szMsg, "peer data exchanged" );
        break;

    case LOG_RF_SESSION_DISCONNECTED:
        sprintf( szMsg, "session closed" );
        break;

        //
        // Data
        //
    case LOG_DATA_DAY_STORED:
        sprintf( szMsg, "Day data stored" );
        break;

        //
        // Trigger messages
        //
    case LOG_TRIG_EVENT:       
        sprintf( szMsg, "trigger event - source = 0x%02X", pEntry->nData );
        break;

        //
        // Profiler
        //
    case LOG_PROFILE_SLEEP:
        sprintf( szMsg, "test duration %d ms", pEntry->nData );
        break;

    case LOG_PROFILE_DATA_CAPTURE:
        sprintf( szMsg, "test duration %d ms", pEntry->nData );
        break;

    case LOG_PROFILE_RADIO_SESSION:
        sprintf( szMsg, "session duration %d ms", pEntry->nData );
        break;

    case LOG_PROFILE_FIRMWARE_UPLOAD:
        sprintf( szMsg, "total upload duration %d ms", pEntry->nData );
        break;

    case LOG_PROFILE_CPU_ON_TIME:
        sprintf( szMsg, "cpu on time %d ms", pEntry->nData );
        break;

    case LOG_PROFILE_CPU_SLEEP_TIME:
        sprintf( szMsg, "cpu sleep time %d ms", pEntry->nData );
        break;

    case LOG_PROFILE_RADIO_SESSION_TIME:
        sprintf( szMsg, "radio session time %d ms", pEntry->nData );
        break;

    case LOG_PROFILE_RADIO_TX_TIME:
        sprintf( szMsg, "radio tx time %d ms", pEntry->nData );
        break;

    case LOG_PROFILE_RADIO_RX_TIME:
        sprintf( szMsg, "radio rx time %d ms", pEntry->nData );
        break;

    case LOG_PROFILE_MEM_IC_TIME:
        sprintf( szMsg, "flash memory IC power time %d ms", pEntry->nData );
        break;

    case LOG_PROFILE_ACCELEROMETER_TIME:
        sprintf( szMsg, "accelerometer IC power time %d ms", pEntry->nData );
        break;

    default:
        sprintf( szMsg, "unknown log entry 0x%X, data %d", pEntry->nEvent, pEntry->nData );
        break;
    }
    
    //
    // Output the log entry
    //
    fprintf( 
        pStream, 
        "%02d/%02d/%04d %02d:%02d:%02d - %s::%s\n", 
        hLocalTime.tm_mday,
        hLocalTime.tm_mon+1,
        hLocalTime.tm_year+1900,
        hLocalTime.tm_hour,
        hLocalTime.tm_min,
        hLocalTime.tm_sec,
        logeventtype_to_string( pEntry->nFilter ),
        szMsg
        );

}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   logeventtype_to_string
//
// Converts a log event ID to a string
//
// ARGUMENTS:
//  (IN)        uint8_t         Log event ID
//
// RETURNS:
//              const char*     String description
//
///////////////////////////////////////////////////////////////////////////////
const char* logeventtype_to_string( uint8_t nFilter )
{
    switch( nFilter )
    {
    case LOG_FILTER_SYSTEM:
        return "system";
    case LOG_FILTER_CONFIG:
        return "config";
    case LOG_FILTER_RF:
        return "radio";
    case LOG_FILTER_DATA:
        return "data";
    case LOG_FILTER_TRIGGER:
        return "trigger";
    case LOG_FILTER_PROFILER_SLEEP:
        return "sleep profiler";
    case LOG_FILTER_PROFILER_DATA_CAPTURE:
        return "data capture profiler";
    case LOG_FILTER_PROFILER_RADIO_SESSION:
        return "radio session profiler";
    case LOG_FILTER_PROFILER_FIRMWARE_UPLOAD:
        return "firmware upload profiler";

    default:
        break;
    }
    return "unknown";
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_get_param_count
//
// Returns number of command line parameters supported by example
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int     Parameter count
//
///////////////////////////////////////////////////////////////////////////////
int host_example_get_param_count( void )
{
    return 2;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_parse_params
//
// Passes a command line parameter for parsing. 'p', 'i', 's', 'r' & 'l' are
// reserved by the application.
//
// ARGUMENTS:
//  (IN)        char*   Command line 
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_parse_params( char *pParam )
{
    int n;
    char szStr[ 10 ];
 
    if ( parse_string_param( "-t=", szStr, 10, 1, &pParam ))
    {
        if ( strchr( szStr, 'l' ))
            sg_fUTC = false;
        else if ( strchr( szStr, 'u' ))
            sg_fUTC = true;
        else
        {
            fprintf( stdout, "Invalid time local specifed. Must be u or l.\n");
            return -1;
        }
		return 0;
    } 

    if ( parse_int_param( "-n=", &n, 1, &pParam ))
    {
        if (( n < 1 ) || ( n > MAX_DUMP ))
        {
            fprintf( stdout, "Invalid log count set. Must be between 1 and %d.\n", MAX_DUMP );
            return -1;
        }
        sg_nMaxDownload = n;  
		return 0;
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_print_param_help
//
// Prints help string for parameters
//
// ARGUMENTS:
//  (IN)        FILE*   Stream to write help message to
//  (IN)        int     Index of parameter to print help message for
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_print_param_help( FILE *pStream, int nIndex )
{
    switch( nIndex )
    {
    case 0:
        fprintf( pStream, "\t-t=[l/u]        Output time as local time or utc (default utc)\n" );
        break;
    case 1:
        fprintf( pStream, "\t-n=[1...768]    Number of entries to query (default 50)\n" );
        break;
    default:
        break;
    }
    return 0;
}

//////////////////////////////////// EOF //////////////////////////////////////

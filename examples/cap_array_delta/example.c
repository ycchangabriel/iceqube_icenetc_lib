// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   example.c
//
// PROJECT:     iceqube_icenetc_lib example
//
// PLATFORM:    crisp linux/arm/gcc
//
// Trigger_en example
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2019-2020 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "host_comms.h"
#include "types.h"
#include "util.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

#define MAX_RETRY                       3

#define MIN_DELTA                       -7
#define MAX_DELTA                       -2

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

extern bool parse_int_param( 
                const char* szParam, 
                int *pVal, 
                int nArgv, 
                char **pArgc 
                );

/////////////////////////////////////////////
// PUBLIC DATA
//

extern uint8_t sg_nRegulatory; // Regulatory

/////////////////////////////////////////////
// PRIVATE DATA
//

static bool     sg_fSetCapArray = false;
static int8_t   sg_nNewCapValue = -4;

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_run
//
// Called by main.c to activate a connected device
//
// ARGUMENTS:
//  (IN)        uint32_t    nConnectedPeer
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int host_example_run( uint32_t nConnectedPeer )
{
    int                         nErr;
    int8_t                      nDelta;

    fprintf( stdout, "\n" );
    fprintf( stdout, "DEVICE ID:                    %d\n", nConnectedPeer );

    //
    // Get current delta
    //
    nErr = host_comms_get_cap_array_delta( &nDelta );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query current cap array delta %d\n", nErr );
        return nErr;
    }
    fprintf( stdout, "  Current Cap Array Delta:    %d\n", nDelta );

    if ( sg_fSetCapArray )
    {
        nErr = host_comms_set_cap_array_delta( sg_nNewCapValue );
        if ( nErr != 0 )
        {
            fprintf( stdout, "  ERROR: Failed to set cap array delta %d\n", nErr );
            return nErr;
        }
        fprintf( stdout, "  New Cap Array Delta:    %d\n", sg_nNewCapValue );
    }
    return 0;
}


// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_get_param_count
//
// Returns number of command line parameters supported by example
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int     Parameter count
//
///////////////////////////////////////////////////////////////////////////////
int host_example_get_param_count( void )
{
    return 1;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_parse_params
//
// Passes a command line parameter for parsing. 'p', 'i', 's', 'r' & 'l' are
// reserved by the application.
//
// ARGUMENTS:
//  (IN)        char*   Command line 
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_parse_params( char *pParam )
{
    int n;
    if ( parse_int_param( "-c=", &n, 1, &pParam ))
    {
        if (( n < MIN_DELTA ) || ( n > MAX_DELTA ))
            return -1;

        sg_fSetCapArray = true;
        sg_nNewCapValue = n;
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_print_param_help
//
// Prints help string for parameters
//
// ARGUMENTS:
//  (IN)        FILE*   Stream to write help message to
//  (IN)        int     Index of parameter to print help message for
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_print_param_help( FILE *pStream, int nIndex )
{
    switch( nIndex )
    {
    case 0:
        fprintf(
            pStream, 
            "\t-c=[%d .. %d]   Set cap array delta (default: not updated)\n",
            MIN_DELTA,
            MAX_DELTA 
            );
        break;

    default:
        break;
    }
    return 0;
}

//////////////////////////////////// EOF //////////////////////////////////////

// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   example.c
//
// PROJECT:     iceqube_icenetc_lib example
//
// PLATFORM:    crisp linux/arm/gcc
//
// Trigger_en example
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2019-2020 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "host_comms.h"
#include "types.h"
#include "util.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

#define MAX_RETRY                       3

//
// Non user-settable states
//
#define DEVICE_STATE_LOW_POWER_SLEEP    0x10
#define DEVICE_STATE_RADIO_TX_POWER     0x11
#define DEVICE_STATE_RADIO_TX_CW_POWER  0x12
#define DEVICE_STATE_RADIO_RX_POWER     0x13
#define DEVICE_STATE_POST_FAIL          0xFF

//
// Enabled trigger mask
//
#define TRIGGER_INDUCTOR_2PULSE        1
#define TRIGGER_INDUCTOR_2PULSEDEL     2
#define TRIGGER_INDUCTOR_3PULSE        4
#define TRIGGER_INDUCTOR_FIELD         8
#define TRIGGER_TIMED                  16

#ifndef MAX_CHANNEL
#define MAX_CHANNEL( nRegulatory ) ( nRegulatory == 2 ? 37 : 36 )
#endif
/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

const char *state_to_string( uint8_t nState );
const char *triggers_to_string( uint8_t nEnabledTriggers );

extern bool parse_int_param( 
                const char* szParam, 
                int *pVal, 
                int nArgv, 
                char **pArgc 
                );

/////////////////////////////////////////////
// PUBLIC DATA
//

extern uint8_t sg_nRegulatory; // Regulatory

/////////////////////////////////////////////
// PRIVATE DATA
//

static uint8_t  sg_nTriggerDis = 0;
static uint8_t  sg_nTriggerEn  = 0;
static char     sg_szMsg[ 200 ];

static bool     sg_f2PulseChannel = false;
static uint8_t  sg_n2PulseChannel[ 3 ] = { 0xFF, 0xFF, 0xFF };
static bool     sg_f2LongPulseChannel = false;
static uint8_t  sg_n2LongPulseChannel[ 3 ] = { 0xFF, 0xFF, 0xFF };
static bool     sg_f3PulseChannel = false;
static uint8_t  sg_n3PulseChannel[ 3 ] = { 0xFF, 0xFF, 0xFF };
static bool     sg_fFieldPulseChannel = false;
static uint8_t  sg_nFieldPulseChannel[ 3 ] = { 0xFF, 0xFF, 0xFF };
static bool     sg_fTimedChannel = false;
static uint8_t  sg_nTimedChannel[ 3 ] = { 0xFF, 0xFF, 0xFF };

static uint32_t sg_nTimedOffset = 0;
static bool     sg_fTimedOffset = false;
static uint32_t sg_nTimedInterval = 0;
static bool     sg_fTimedInterval = false;
static uint8_t  sg_nEnableTimedTriggerInIdle = 0;
static bool     sg_fEnableTimedTriggerInIdle = false;

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_run
//
// Called by main.c to activate a connected device
//
// ARGUMENTS:
//  (IN)        uint32_t    nConnectedPeer
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int host_example_run( uint32_t nConnectedPeer )
{
    struct iceqube_device_state hInfo;
    int                         nErr;
    struct trigger_config       hTriggerConfig;
    uint8_t                     i;

    fprintf( stdout, "\n" );
    fprintf( stdout, "DEVICE ID:           %d\n", nConnectedPeer );

    //
    // Get device info
    //
    nErr = host_comms_get_device_info( &hInfo );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query device info %d\n", nErr );
        return nErr;
    }

    //
    // Display device info
    //
    fprintf( 
        stdout, 
        "  Device State:      %s\n", 
        state_to_string( hInfo.nDeviceState )
        );
    
    //
    // Get current trigger settings
    //
    nErr = host_comms_get_trigger_config( &hTriggerConfig );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query trigger configuration %d\n", nErr );
        return nErr;
    }

    //
    // Enable required triggering 
    // 2-pulse cannot be disabled
    //
    hTriggerConfig.nEnabledTriggers &= ~( sg_nTriggerDis );
    hTriggerConfig.nEnabledTriggers |= ( sg_nTriggerEn | TRIGGER_INDUCTOR_2PULSE );

    if ( sg_f2PulseChannel )
    {
        if (( sg_n2PulseChannel[ 0 ] == 0xFF ) && 
            ( sg_n2PulseChannel[ 1 ] == 0xFF ) &&
            ( sg_n2PulseChannel[ 2 ] == 0xFF ))
        {
            fprintf( stdout, "  ERROR: Cannot set all 2 pulse channels to 255\n" );
            return -1;
        }
        for( i = 0; i < 3; i++ )
        {
            if (( sg_n2PulseChannel[ i ] != 0xFF ) &&
                ( sg_n2PulseChannel[ i ] > MAX_CHANNEL( sg_nRegulatory )))
            {
                fprintf( 
                    stdout, 
                    "  ERROR: invalid 2 pulse channel (-2%d=%u) Must be 0-%u\n", 
                    i,
                    sg_n2PulseChannel[ i ],
                    MAX_CHANNEL( sg_nRegulatory ) 
                    );
                return -1;
            }
        }
        hTriggerConfig.n2PulseFreq[ 0 ] = sg_n2PulseChannel[ 0 ];
        hTriggerConfig.n2PulseFreq[ 1 ] = sg_n2PulseChannel[ 1 ];
        hTriggerConfig.n2PulseFreq[ 2 ] = sg_n2PulseChannel[ 2 ];
    }
    if ( sg_f2LongPulseChannel )
    {
        if (( sg_n2LongPulseChannel[ 0 ] == 0xFF ) && 
            ( sg_n2LongPulseChannel[ 1 ] == 0xFF ) &&
            ( sg_n2LongPulseChannel[ 2 ] == 0xFF ))
        {
            fprintf( stdout, "  ERROR: Cannot set all 2 long pulse channels to 255\n" );
            return -1;
        }
        for( i = 0; i < 3; i++ )
        {
            if (( sg_n2LongPulseChannel[ i ] != 0xFF ) &&
                ( sg_n2LongPulseChannel[ i ] > MAX_CHANNEL( sg_nRegulatory )))
            {
                fprintf( 
                    stdout, 
                    "  ERROR: invalid 2 long pulse channel (-2l%d=%u) Must be 0-%u\n", 
                    i,
                    sg_n2LongPulseChannel[ i ],
                    MAX_CHANNEL( sg_nRegulatory ) 
                    );
                return -1;
            }
        }
        hTriggerConfig.n2LongPulseFreq[ 0 ] = sg_n2LongPulseChannel[ 0 ];
        hTriggerConfig.n2LongPulseFreq[ 1 ] = sg_n2LongPulseChannel[ 1 ];
        hTriggerConfig.n2LongPulseFreq[ 2 ] = sg_n2LongPulseChannel[ 2 ];
    }
    if ( sg_f3PulseChannel )
    {
        if (( sg_n3PulseChannel[ 0 ] == 0xFF ) && 
            ( sg_n3PulseChannel[ 1 ] == 0xFF ) &&
            ( sg_n3PulseChannel[ 2 ] == 0xFF ))
        {
            fprintf( stdout, "  ERROR: Cannot set all 3 pulse channels to 255\n" );
            return -1;
        }
        for( i = 0; i < 3; i++ )
        {
            if (( sg_n3PulseChannel[ i ] != 0xFF ) &&
                ( sg_n3PulseChannel[ i ] > MAX_CHANNEL( sg_nRegulatory )))
            {
                fprintf( 
                    stdout, 
                    "  ERROR: invalid 3 pulse channel (-3%d=%u) Must be 0-%u\n", 
                    i,
                    sg_n3PulseChannel[ i ],
                    MAX_CHANNEL( sg_nRegulatory ) 
                    );
                return -1;
            }
        }
        hTriggerConfig.n3PulseFreq[ 0 ] = sg_n3PulseChannel[ 0 ];
        hTriggerConfig.n3PulseFreq[ 1 ] = sg_n3PulseChannel[ 1 ];
        hTriggerConfig.n3PulseFreq[ 2 ] = sg_n3PulseChannel[ 2 ];
    }

    if ( sg_fFieldPulseChannel )
    {
        if (( sg_nFieldPulseChannel[ 0 ] == 0xFF ) && 
            ( sg_nFieldPulseChannel[ 1 ] == 0xFF ) &&
            ( sg_nFieldPulseChannel[ 2 ] == 0xFF ))
        {
            fprintf( stdout, "  ERROR: Cannot set all field channels to 255\n" );
            return -1;
        }
        for( i = 0; i < 3; i++ )
        {
            if (( sg_nFieldPulseChannel[ i ] != 0xFF ) &&
                ( sg_nFieldPulseChannel[ i ] > MAX_CHANNEL( sg_nRegulatory )))
            {
                fprintf( 
                    stdout, 
                    "  ERROR: invalid field trigger channel (-f%d=%u) Must be 0-%u\n", 
                    i,
                    sg_nFieldPulseChannel[ i ],
                    MAX_CHANNEL( sg_nRegulatory ) 
                    );
                return -1;
            }
        }
        hTriggerConfig.nFieldFreq[ 0 ] = sg_nFieldPulseChannel[ 0 ];
        hTriggerConfig.nFieldFreq[ 1 ] = sg_nFieldPulseChannel[ 1 ];
        hTriggerConfig.nFieldFreq[ 2 ] = sg_nFieldPulseChannel[ 2 ];
    }

    if ( sg_fTimedChannel )
    {
        if (( sg_nTimedChannel[ 0 ] == 0xFF ) && 
            ( sg_nTimedChannel[ 1 ] == 0xFF ) &&
            ( sg_nTimedChannel[ 2 ] == 0xFF ))
        {
            fprintf( stdout, "  ERROR: Cannot set all timed channels to 255\n" );
            return -1;
        }
        for( i = 0; i < 3; i++ )
        {
            if (( sg_nTimedChannel[ i ] != 0xFF ) &&
                ( sg_nTimedChannel[ i ] > MAX_CHANNEL( sg_nRegulatory )))
            {
                fprintf( 
                    stdout, 
                    "  ERROR: invalid timed trigger channel (-t%d=%u) Must be 0-%u\n", 
                    i,
                    sg_nTimedChannel[ i ],
                    MAX_CHANNEL( sg_nRegulatory ) 
                    );
                return -1;
            }
        }

        hTriggerConfig.nTimedFreq[ 0 ] = sg_nTimedChannel[ 0 ];
        hTriggerConfig.nTimedFreq[ 1 ] = sg_nTimedChannel[ 1 ];
        hTriggerConfig.nTimedFreq[ 2 ] = sg_nTimedChannel[ 2 ];
    }

    if ( sg_fTimedOffset )
    {
        hTriggerConfig.nTimedOffset = sg_nTimedOffset;
    }
    if ( sg_fTimedInterval )
    {
        hTriggerConfig.nTimedInterval = sg_nTimedInterval;
    }

    if ( sg_fEnableTimedTriggerInIdle )
    {
        hTriggerConfig.nTimedFlags = sg_nEnableTimedTriggerInIdle;
    }

    // Don't update the current standoff index
    hTriggerConfig.nStandoffIndex[0] = 0xFF;
    hTriggerConfig.nStandoffIndex[1] = 0xFF;
    hTriggerConfig.nStandoffIndex[2] = 0xFF;
    hTriggerConfig.nStandoffIndex[3] = 0xFF;

    nErr = host_comms_set_trigger_config( &hTriggerConfig );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query trigger configuration %d\n", nErr );
        return nErr;
    }
    
    fprintf( 
        stdout, 
        "  Enabled Triggers:  %s\n", 
        triggers_to_string( hTriggerConfig.nEnabledTriggers ) 
        );

    if ( sg_f2PulseChannel )
    {
        fprintf( 
            stdout, 
            "  2-pulse channels:  %d, %d, %d\n", 
            sg_n2PulseChannel[ 0 ],
            sg_n2PulseChannel[ 1 ],
            sg_n2PulseChannel[ 2 ]
            );
    }

    if ( sg_f2LongPulseChannel )
    {
        fprintf( 
            stdout, 
            "  2 long pulse channels:  %d, %d, %d\n", 
            sg_n2LongPulseChannel[ 0 ],
            sg_n2LongPulseChannel[ 1 ],
            sg_n2LongPulseChannel[ 2 ]
            );
    }

    if ( sg_f3PulseChannel )
    {
        fprintf( 
            stdout, 
            "  3-pulse channels:  %d, %d, %d\n", 
            sg_n3PulseChannel[ 0 ],
            sg_n3PulseChannel[ 1 ],
            sg_n3PulseChannel[ 2 ]
            );
    }

    if ( sg_fFieldPulseChannel )
    {
        fprintf( 
            stdout, 
            "  Field channels:  %d, %d, %d\n", 
            sg_nFieldPulseChannel[ 0 ],
            sg_nFieldPulseChannel[ 1 ],
            sg_nFieldPulseChannel[ 2 ]
            );
    }

    if ( sg_fTimedChannel )
    {
        fprintf( 
            stdout, 
            "  Timed channels:  %d, %d, %d\n", 
            sg_nTimedChannel[ 0 ],
            sg_nTimedChannel[ 1 ],
            sg_nTimedChannel[ 2 ]
            );
    }

    if ( sg_fTimedOffset )
    {
        fprintf(
            stdout,
            "  Timed offset:  %d s\n",
            sg_nTimedOffset
            );
    }
    if ( sg_fTimedInterval )
    {
        fprintf(
            stdout,
            "  Timed interval:  %d s\n",
            sg_nTimedInterval
            );
    }

    if ( sg_fEnableTimedTriggerInIdle )
    {
        fprintf(
            stdout,
            "  Timed trigger in IDLE:  %s\n",
            ( sg_nEnableTimedTriggerInIdle ? "true" : "false" )
            );
    }

    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   state_to_string
//
// Utility function to convert a device state into a string
//
// ARGUMENTS:
//  (IN)        uint8_t         Device state
//
// RETURNS:
//              const char*     Device state string
//
///////////////////////////////////////////////////////////////////////////////
const char *state_to_string( uint8_t nState )
{
    switch( nState )
    {
    case DEVICE_STATE_IDLE:
        return "IDLE";
    case DEVICE_STATE_ACTIVE:
        return "ACTIVE";
    case DEVICE_STATE_FIRMWARE:
        return "FIRMWARE";
    case DEVICE_STATE_LOW_POWER_SLEEP:
    case DEVICE_STATE_RADIO_TX_POWER:
    case DEVICE_STATE_RADIO_TX_CW_POWER:
    case DEVICE_STATE_RADIO_RX_POWER:
        return "TEST STATE";
    case DEVICE_STATE_POST_FAIL:
        return "POST FAILURE STATE";
    default:
        break;
    }
    return "UNKNOWN";
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   triggers_to_string
//
// Utility function to convert a mask of enabled trigger types into an ascii
// string.
//
// ARGUMENTS:
//  (IN)        uint8_t         Mask of enabled triggers
//
// RETURNS:
//              const char*     Ascii string of enabled triggers
//
///////////////////////////////////////////////////////////////////////////////
const char *triggers_to_string( uint8_t nEnabledTriggers )
{
    sg_szMsg[ 0 ] = 0;

    if ( nEnabledTriggers & TRIGGER_INDUCTOR_2PULSE )
        strcat( sg_szMsg, "2 pulse" );
    if ( nEnabledTriggers & TRIGGER_INDUCTOR_2PULSEDEL )
    {
        if ( sg_szMsg[ 0 ] != 0 )
            strcat( sg_szMsg, ", " );
        strcat( sg_szMsg, "2 long pulse" );
    }
    if ( nEnabledTriggers & TRIGGER_INDUCTOR_3PULSE )
    {
        if ( sg_szMsg[ 0 ] != 0 )
            strcat( sg_szMsg, ", " );
        strcat( sg_szMsg, "3 pulse" );
    }
    if ( nEnabledTriggers & TRIGGER_INDUCTOR_FIELD )
    {
        if ( sg_szMsg[ 0 ] != 0 )
            strcat( sg_szMsg, ", " );
        strcat( sg_szMsg, "field" );
    }
    if ( nEnabledTriggers & TRIGGER_TIMED )
    {
        if ( sg_szMsg[ 0 ] != 0 )
            strcat( sg_szMsg, ", " );
        strcat( sg_szMsg, "timed" );
    }
    if ( sg_szMsg[ 0 ] == 0 )
        strcat( sg_szMsg, "none" );

    return sg_szMsg;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_get_param_count
//
// Returns number of command line parameters supported by example
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int     Parameter count
//
///////////////////////////////////////////////////////////////////////////////
int host_example_get_param_count( void )
{
    return 22;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_parse_params
//
// Passes a command line parameter for parsing. 'p', 'i', 's', 'r' & 'l' are
// reserved by the application.
//
// ARGUMENTS:
//  (IN)        char*   Command line 
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_parse_params( char *pParam )
{
    int n;
    if ( parse_int_param( "-2l=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_nTriggerDis |= TRIGGER_INDUCTOR_2PULSEDEL;
        else
            sg_nTriggerEn |= TRIGGER_INDUCTOR_2PULSEDEL;
		return 0;
    }

    if ( parse_int_param( "-3=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_nTriggerDis |= TRIGGER_INDUCTOR_3PULSE;
        else
            sg_nTriggerEn |= TRIGGER_INDUCTOR_3PULSE;
		return 0;
    }

    if ( parse_int_param( "-f=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_nTriggerDis |= TRIGGER_INDUCTOR_FIELD;
        else
            sg_nTriggerEn |= TRIGGER_INDUCTOR_FIELD;
        return 0;
    }

    if ( parse_int_param( "-t=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_nTriggerDis |= TRIGGER_TIMED;
        else
            sg_nTriggerEn |= TRIGGER_TIMED;    
        return 0;
    }

    if ( parse_int_param( "-20=", &n, 1, &pParam ))
    {
        sg_n2PulseChannel[0] = n;        
        sg_f2PulseChannel = true;
        return 0;
    }

    if ( parse_int_param( "-21=", &n, 1, &pParam ))
    {
        sg_n2PulseChannel[1] = n;        
        sg_f2PulseChannel = true;
        return 0;
    }

    if ( parse_int_param( "-22=", &n, 1, &pParam ))
    {
        sg_n2PulseChannel[2] = n;        
        sg_f2PulseChannel = true;
        return 0;
    }

    if ( parse_int_param( "-2l0=", &n, 1, &pParam ))
    {
        sg_n2LongPulseChannel[0] = n;        
        sg_f2LongPulseChannel = true;
        return 0;
    }

    if ( parse_int_param( "-2l1=", &n, 1, &pParam ))
    {
        sg_n2LongPulseChannel[1] = n;        
        sg_f2LongPulseChannel = true;
        return 0;
    }

    if ( parse_int_param( "-2l2=", &n, 1, &pParam ))
    {
        sg_n2LongPulseChannel[2] = n;        
        sg_f2LongPulseChannel = true;
        return 0;
    }

    if ( parse_int_param( "-30=", &n, 1, &pParam ))
    {
        sg_n3PulseChannel[0] = n;        
        sg_f3PulseChannel = true;
        return 0;
    }

    if ( parse_int_param( "-31=", &n, 1, &pParam ))
    {
        sg_n3PulseChannel[1] = n;        
        sg_f3PulseChannel = true;
        return 0;
    }

    if ( parse_int_param( "-32=", &n, 1, &pParam ))
    {
        sg_n3PulseChannel[2] = n;        
        sg_f3PulseChannel = true;
        return 0;
    }

    if ( parse_int_param( "-f0=", &n, 1, &pParam ))
    {
        sg_nFieldPulseChannel[0] = n;        
        sg_fFieldPulseChannel = true;
        return 0;
    }

    if ( parse_int_param( "-f1=", &n, 1, &pParam ))
    {
        sg_nFieldPulseChannel[1] = n;        
        sg_fFieldPulseChannel = true;
        return 0;
    }

    if ( parse_int_param( "-f2=", &n, 1, &pParam ))
    {
        sg_nFieldPulseChannel[2] = n;        
        sg_fFieldPulseChannel = true;
        return 0;
    }

    if ( parse_int_param( "-t0=", &n, 1, &pParam ))
    {
        sg_nTimedChannel[0] = n;        
        sg_fTimedChannel = true;
        return 0;
    }

    if ( parse_int_param( "-t1=", &n, 1, &pParam ))
    {
        sg_nTimedChannel[1] = n;        
        sg_fTimedChannel = true;
        return 0;
    }

    if ( parse_int_param( "-t2=", &n, 1, &pParam ))
    {
        sg_nTimedChannel[2] = n;        
        sg_fTimedChannel = true;
        return 0;
    }

    if ( parse_int_param( "-o=", &n, 1, &pParam ) )
    {
        if ( n < 0 )
            n = 0;
        if ( n > 604800 )
            n = 604800;
        sg_nTimedOffset = n;
        sg_fTimedOffset = true;
        return 0;
    }
    if ( parse_int_param( "-v=", &n, 1, &pParam ) )
    {
        if ( n < 5 )
            n = 5;
        if ( n > 604800 )
            n = 604800;
        sg_nTimedInterval = n;
        sg_fTimedInterval = true;
        return 0;
    }
    if ( parse_int_param( "-i=", &n, 1, &pParam ) )
    {
        if ( n == 0 )
            sg_nEnableTimedTriggerInIdle = 0;
        else
            sg_nEnableTimedTriggerInIdle = 1;
        sg_fEnableTimedTriggerInIdle = true;
        return 0;
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_print_param_help
//
// Prints help string for parameters
//
// ARGUMENTS:
//  (IN)        FILE*   Stream to write help message to
//  (IN)        int     Index of parameter to print help message for
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_print_param_help( FILE *pStream, int nIndex )
{
    switch( nIndex )
    {
    case 0:
        fprintf( pStream, "\t-20=[0-max]     Set 2 pulse channel 0\n" );
        break;

    case 1:
        fprintf( pStream, "\t-21=[0-max,255] Set 2 pulse channel 1\n" );
        break;

    case 2:
        fprintf( pStream, "\t-22=[0-max,255] Set 2 pulse channel 2\n" );
        break;

    case 3:
        fprintf( pStream, "\t-2l=[0-1]       En/disable 2-long pulse (default 1)\n" );
        break;

    case 4:
        fprintf( pStream, "\t-2l0=[0-max]    Set 2-long pulse channel 0\n" );
        break;

    case 5:
        fprintf( pStream, "\t-2l1=[0-mx,255] Set 2-long pulse channel 1\n" );
        break;

    case 6:
        fprintf( pStream, "\t-2l2=[0-mx,255] Set 2-long pulse channel 2\n" );
        break;

    case 7:
        fprintf( pStream, "\t-3=[0-1]        En/disable 3-pulse (default 1)\n" );
        break;

    case 8:
        fprintf( pStream, "\t-30=[0-max]     Set 3 pulse channel 0\n" );
        break;

    case 9:
        fprintf( pStream, "\t-31=[0-max,255] Set 3 pulse channel 1\n" );
        break;

    case 10:
        fprintf( pStream, "\t-32=[0-max,255] Set 3 pulse channel 2\n" );
        break;

    case 11:
        fprintf( pStream, "\t-f=[0-1]        En/disable field trigger (default 0)\n" );
        break;

    case 12:
        fprintf( pStream, "\t-f0=[0-max]     Set field channel 0\n" );
        break;

    case 13:
        fprintf( pStream, "\t-f1=[0-max,255] Set field channel 1\n" );
        break;

    case 14:
        fprintf( pStream, "\t-f2=[0-max,255] Set field channel 2\n" );
        break;

    case 15:
        fprintf( pStream, "\t-t=[0-1]        En/disable timed trigger (default 0)\n" );
        break;

    case 16:
        fprintf( pStream, "\t-t0=[0-max]     Set timed channel 0\n" );
        break;

    case 17:
        fprintf( pStream, "\t-t1=[0-max,255] Set timed channel 1\n" );
        break;

    case 18:
        fprintf( pStream, "\t-t2=[0-max,255] Set timed channel 2\n" );
        break;

    case 19:
        fprintf( pStream, "\t-o=[0-604800]   Timed offset (default 0 s)\n" );
        break;

    case 20:
        fprintf( pStream, "\t-v=[5-604800]   Timed interval (default 86400 s)\n" );
        break;

    case 21:
        fprintf( pStream, "\t-i=[0-1]        Enable timed trigger in idle (default 0)\n" );
        break;

    default:
        break;
    }
    return 0;
}

//////////////////////////////////// EOF //////////////////////////////////////

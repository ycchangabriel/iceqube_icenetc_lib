// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   main.c
//
// PROJECT:     iceqube_icenetc_lib example
//
// PLATFORM:    crisp linux/arm/gcc
//
// Entry point for example application
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <unistd.h> 
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <setjmp.h>

#include "host_comms.h"
#include "host_port.h"
#include "iceqube_icenetc.h"
#include "example.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

#define STDIN       0

#define qprintf( ... )  { \
                            if ( sg_fService == false ) \
                                fprintf( stdout, __VA_ARGS__ ); \
                            else \
                                syslog( LOG_INFO, __VA_ARGS__ ); \
                        }


#define RF_OFFSET_FILE      "/etc/rf_offset"

#ifndef MAX_PATH
#define MAX_PATH        255
#endif

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

//
// Output usage
//
void print_usage( void );
int  kbhit( void );

//
// SIG_STOP handler
//
void StopSigHandler( int nSignal, siginfo_t *pSig, void *pSecret );

//
// Parameter parsing
//

bool parse_int_param( 
            const char* szParam, 
            int *pVal, 
            int nArgv, 
            char **pArgc 
            );

bool parse_param_set( 
            const char* szParam, 
            int nArgv, 
            char **pArgc 
            );

bool parse_string_param( 
            const char* szParam, 
            char  *szBuffer,
            int    nMaxLen,
            int    nArgv, 
            char **pArgc            
            );

bool parse_frequency_param( 
            const char* szParam, 
            int   *pVal,
            int    nArgv, 
            char **pArgc            
            );

/////////////////////////////////////////////
// PUBLIC DATA
//

bool sg_fService = false;
bool sg_fQuit = false;

uint8_t sg_nRegulatory; // regulatory

/////////////////////////////////////////////
// PRIVATE DATA
//

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   main
//
// Application entry point. 
//
// ARGUMENTS:
//  (IN)        int         Argument count
//  (IN)        char**      List of arguments
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int main( int nArgc, char **pArgc )
{
    //
    // Application parameters (set by command line)
    //
    bool           fListenChannelsSet     = false; // any listen channel set?
    bool           fSessionChannelsSet    = false; // any session channel set?
    uint8_t        nCrispSlot             = 0xFF;  // auto
    uint8_t        nListenChannels        = 3;     // default number of listen channels
    uint8_t        hListenChannels[3]     = { 0, 6, 12 }; // default listen channels
    uint8_t        hNewListenChannels[3]  = { 0xFF, 0xFF, 0xFF }; // no new listen channels yet
    uint8_t        nSessionChannels       = 1;     // default number of session channels
    uint8_t        hSessionChannels[3]    = { 18, 0xFF, 0xFF }; // default session channels
    uint8_t        hNewSessionChannels[3] = { 0xFF, 0xFF, 0xFF }; // no new session channels yet
    uint8_t        nRank                  = 0;
    int32_t        nFrequencyOffset       = 0;     // Default is no frequency offset
    int            n;

    sg_nRegulatory = 0; // default regulatory

    //
    // Input (keyboard) handling
    //
    struct termios hOldSettings;        
    struct termios hNewTty; 

    //
    // SIG_STOP handler
    //
    struct sigaction hSigAction;

    //
    // Check if we're running as a service first
    //
    if ( parse_param_set( "--", nArgc, pArgc ))
    {
        sg_fService = true;
    }

    //
    // Trap stop signal handler (for a clean exit)
    //
    hSigAction.sa_sigaction = StopSigHandler;
    sigemptyset( &hSigAction.sa_mask );
    hSigAction.sa_flags = SA_SIGINFO;
    sigaction( SIGTERM, &hSigAction, NULL );

    //
    // Set-up terminal for a nice
    //
    if( sg_fService == false )
    {
        //
        // Disable stdout buffering
        //
        setbuf( stdout, NULL );
    }
    else
    {
        //
        // Output to sys log instead of stdout
        //
        openlog( "iceqube_icenetc_lib", LOG_NDELAY, LOG_USER );
    }

    //
    // Print header
    //
    qprintf( "iceqube_icenetc_lib example fn: %s\n", EXAMPLE_FN );
    qprintf( 
        "iceqube_icenetc_lib version %d.%03d\n", 
        ICEQUBE_ICENETC_VERSION_MAJ,
        ICEQUBE_ICENETC_VERSION_MIN
        );
    if( sg_fService == false )
        qprintf( "  press any key to exit\n" );


    //
    // Parse the command line parameters
    //
    if ( parse_param_set( "-?", nArgc, pArgc ))
    {
        print_usage( );
        return 0;
    }

    if ( parse_int_param( "-p=", &n, nArgc, pArgc ))
    {
        nCrispSlot = n;
        if (( nCrispSlot != 0xFF ) && ( nCrispSlot > 2 ))
        {
            fprintf( stdout,"Invalid card slot (-p=%d). Must be port 0-2, or 255 for auto.\n", nCrispSlot );
            print_usage( );
            return -1;
        }
    }

    if ( parse_int_param( "-r=", &n, nArgc, pArgc ))
    {
        if (( n < -1 ) || ( n > 6 ))
        {
            fprintf( stdout,"Invalid rank (-r=%d). Must be port 0-6, or -1 for random.\n", n );
            print_usage( );
            return -1;
        }
        nRank = (uint8_t) ( n & 0xFF );
    }

    if ( parse_int_param( "-s0=", &n, nArgc, pArgc ))
    {
        hNewSessionChannels[0] = n;
        fSessionChannelsSet = true;
    }

    if ( parse_int_param( "-s1=", &n, nArgc, pArgc ))
    {
        hNewSessionChannels[1] = n;
        fSessionChannelsSet = true;
    }
    if ( parse_int_param( "-s2=", &n, nArgc, pArgc ))
    {
        hNewSessionChannels[2] = n;
        fSessionChannelsSet = true;
    }

    if ( parse_int_param( "-l0=", &n, nArgc, pArgc ))
    {
        hNewListenChannels[0] = n;
        fListenChannelsSet = true;
    }

    if ( parse_int_param( "-l1=", &n, nArgc, pArgc ))
    {
        hNewListenChannels[1] = n;
        fListenChannelsSet = true;
    }

    if ( parse_int_param( "-l2=", &n, nArgc, pArgc ))
    {
        hNewListenChannels[2] = n;
        fListenChannelsSet = true;
    }

    if ( parse_frequency_param( "-rf=", &n, nArgc, pArgc ))
    {
        nFrequencyOffset = n;
    }

    //
    // Parse example parameters
    //
    for( n = 1; n < nArgc; n++ )
    {
        char *szArg = pArgc[n];
        if ( *szArg !='-' )
            continue;

        if ( host_example_parse_params( szArg ) != 0 )
        {
             print_usage( );
            return -1;
        }
    }

    //
    // Assume new parameters
    //   
    if ( fListenChannelsSet )
        memcpy( hListenChannels, hNewListenChannels, 3 );
    nListenChannels = 0;
    if ( hListenChannels[0] != 0xFF ) 
        nListenChannels++;
    if ( hListenChannels[1] != 0xFF ) 
        nListenChannels++;
    if ( hListenChannels[2] != 0xFF ) 
        nListenChannels++;

    if ( fSessionChannelsSet )
        memcpy( hSessionChannels, hNewSessionChannels, 3 );
    nSessionChannels = 0;
    if ( hSessionChannels[0] != 0xFF )
        nSessionChannels++;
    if ( hSessionChannels[1] != 0xFF )
        nSessionChannels++;
    if ( hSessionChannels[2] != 0xFF )
        nSessionChannels++;

    //
    // No listen channels?
    //
    if (( nListenChannels == 0 ) || ( nSessionChannels == 0 ))
    {
        qprintf( "Invalid channel count. Requires at least 1 valid listen and 1 valid session channel.\n" );
        print_usage( );
        return -1;
    }

    //
    // Open cc1310
    //
    if ( host_comms_connect(            
             nCrispSlot,
             &sg_nRegulatory,
             nRank,
             nListenChannels,
             hListenChannels,
             nSessionChannels,
             hSessionChannels,
             nFrequencyOffset             
             ) != 0 )
    {

        if ( sg_nRegulatory != 0 )
        {
    	    uint8_t i;
            for( i = 0; i < 3; i ++ )
            {
                if (( hListenChannels[ i ] != 0xFF ) &&
                    ( hListenChannels[ i ] > MAX_CHANNEL( sg_nRegulatory )))
                {
                    qprintf(                     
                        "Invalid listen channel (-l%d=%u). Must be 0-%u\n", 
                        i, 
                        hListenChannels[ i ], 
                        MAX_CHANNEL( sg_nRegulatory ) 
                        );
                    print_usage( );
                    return -1;
                }
                if (( hSessionChannels[ i ] != 0xFF ) &&
                    ( hSessionChannels[ i ] > MAX_CHANNEL( sg_nRegulatory )))
                {
                    qprintf( 
                        "Invalid session channel (-s%d=%u). Must be 0-%u\n", 
                        i, 
                        hSessionChannels[ i ], 
                        MAX_CHANNEL( sg_nRegulatory ) 
                        );
                    print_usage( );
                    return -1;
                }
            }
        }
        fprintf( stdout,"Failed to open cc1310 host\n" );
        return -1;
    }

    //
    // Output Connection Details
    //
    qprintf(
        "Connected to host\n"
        );
    qprintf( 
        "Regulatory : %s\n", 
        ( sg_nRegulatory == REGULATORY_EU_2000_299_EC1 ) ? "eu_2000/299/ec1" : "fcc"
        );

    if ( nListenChannels < 2 )
    {
        qprintf ( "Listen channels : %d\n", hListenChannels[0] );   
    }
    else 
    {
        if ( nListenChannels < 3 )
        {
            qprintf ( "Listen channels : %d, %d\n", hListenChannels[0], hListenChannels[1] );   
        }
        else
        {
            qprintf ( "Listen channels : %d, %d, %d\n", hListenChannels[0], hListenChannels[1], hListenChannels[2] );
        }
    }

    if ( nSessionChannels < 2 )
    {
        qprintf ( "Session channels : %d\n", hSessionChannels[0] ); 
    }
    else
    {
        if ( nSessionChannels < 3 )
        {
            qprintf ( "Session channels : %d, %d\n", hSessionChannels[0], hSessionChannels[1] );
        }
        else
        {
            qprintf ( "Session channels : %d, %d, %d\n", hSessionChannels[0], hSessionChannels[1], hSessionChannels[2] );
        }
    }

    qprintf ( "Frequency Offset : %d Hz\n", nFrequencyOffset ); 

    //
    // Configure stdin
    //
    if ( sg_fService == false )
    {
        tcgetattr( STDIN, &hOldSettings );
        hNewTty = hOldSettings;
        hNewTty.c_lflag      &= ~ICANON;
        hNewTty.c_lflag      &= ~ECHO;
        hNewTty.c_lflag      &= ~ISIG;
        hNewTty.c_cc[ VMIN ]  = 0;
        hNewTty.c_cc[ VTIME ] = 0;
        tcsetattr( 0, TCSANOW, &hNewTty );
    }

    //
    // Round robin loop
    //
    while( sg_fQuit == false )
    {
        uint32_t nPeer;
        
        //
        // Look for keyboard hit (only if not running as service)
        //
        if ( sg_fService == false )
        {
            if( kbhit( ) != 0 )
            {
                sg_fQuit = true;
                continue;
            }
        }

        //
        // Wait for a device to connect
        //
        if ( host_comms_wait_for_connect( 2000, &nPeer ) != 0 )
            continue;

        //
        // Perform example sequence of commands
        //
        host_example_run( nPeer );

        //
        // Disconnect device
        //
        host_comms_disconnect( );
    }

    //
    // Close host connection
    //
    host_comms_close( );

    if ( sg_fService == false )
        tcsetattr( 0, TCSANOW, &hOldSettings );
    
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   print_usage
//
// Print command line usage
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void print_usage( void )
{
    int i;
    
    if ( sg_fService == false )
    {
        fprintf( stdout, "usage:\n");
        fprintf( stdout, "  iceqube_icenetc_example.elf [-p=X][-l0=X [-l1=X [-l2=X]]][-s0=X [-s1=X [-s2=X]]][-rf=X]\n");
        fprintf( stdout, "\t-p=[0-2,255]    cc1310 crisp module extension slot \n");
        fprintf( stdout, "\t                0-2, or 255 for auto (default 255)\n");
        fprintf( stdout, "\t-r=[0-6,-1]     Multihub rank (0=default, -1=random)\n"); 
        fprintf( stdout, "\t-l0=[0-max]     listen channel 0 (default 0)\n" );
        fprintf( stdout, "\t-l1=[0-max,255] listen channel 1 (default 6)\n" );
        fprintf( stdout, "\t-l2=[0-max,255] listen channel 2 (default 12)\n" );
        fprintf( stdout, "\t-s0=[0-max]     session channel 0 (default 18)\n" );
        fprintf( stdout, "\t-s1=[0-max,255] session channel 1 (default 255)\n" );
        fprintf( stdout, "\t-s2=[0-max,255] session channel 2 (default 255)\n" );
        fprintf( stdout, "\t-rf=[-50000-50000, or file] frequency offset, or \n" );
        fprintf( stdout, "\t                file containing offset (default %s). \n", RF_OFFSET_FILE );
        fprintf( stdout, "\t--              run as service (no stdin/out)\n" );
        for( i = 0; i < host_example_get_param_count( ); i++ )
            host_example_print_param_help( stdout, i );

        fprintf( stdout, "\n\tsession channels must be separated from the listen channels\n" );
        fprintf( stdout, "\tby 2. Each listen channels uses channel + 1 for host responses.\n");

        fprintf( stdout, "\n\tto disable a channel, omit unused channels or set -l or -s\n" );
        fprintf( stdout, "\tvalue to 255; channel 0 cannot be unused. The max value depends\n");
        fprintf( stdout, "\ton the radio band:\n");

        fprintf( 
            stdout, 
            "\t- For 868Mhz the max channel number is %d\n", 
            MAX_CHANNEL( REGULATORY_EU_2000_299_EC1 )
            );

        fprintf( 
            stdout, 
            "\t- for 915Mhz the max channel number is %d\n\n", 
            MAX_CHANNEL( REGULATORY_FCC )
            );
        fflush( stdout );
    }
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   kbhit
//
// Test for keyboard hit.
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int         Non-zero if there is a keystroke in the input
//                          buffer.
//
///////////////////////////////////////////////////////////////////////////////
int kbhit( void )
{
    struct timeval hTv = { 0L, 0L };
    fd_set hFds;
    FD_ZERO( &hFds );
    FD_SET( STDIN, &hFds );
    return select( STDIN + 1, &hFds, NULL, NULL, &hTv );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   StopSigHandler
//
// Overrides default STOP signal handler, flags that process should stop.
//
// ARGUMENTS:
//  (IN)        int         Signal ID
//  (IN)        siginfo_t*  Signal info
//  (IN)        void*       Unused
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void StopSigHandler( int nSignal, siginfo_t *pSig, void *pSecret )
{
    sg_fQuit = true;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   parse_int_param
//
// Parses an integer paramter from the command line arguments
//
// ARGUMENTS:
//  (IN)     const char*     Command line paramter to search for (e.g."-r=")
//  (IN/OUT) int*            Interpreted value
//  (IN)     int             Command line paramter count
//  (IN)     char**          Command line paramters
//
// RETURNS:
//           bool            true if parameter was found and interpreted
//
///////////////////////////////////////////////////////////////////////////////
bool parse_int_param( const char* szParam, int *pVal, int nArgv, char **pArgc )
{
    int i;
    int nParamLen = strlen( szParam );

    if (( szParam == NULL ) || ( pVal == NULL ))
        return false;

    for( i = 0; i < nArgv; i ++ )
    {
        char *szCurParam = pArgc[ i ];
        int   nCurParamLen;

        if (( szCurParam == NULL ) || ( szCurParam[0] != '-' ))
            continue;
    
        nCurParamLen = strlen( szCurParam );

        if ( nCurParamLen < nParamLen )
            continue;

        if ( memcmp( szParam, szCurParam, nParamLen ) == 0 )
        {
            char* szEquals = strchr( szCurParam, '=' );
            if ( szEquals )
            {
                szEquals++;
                *pVal = atoi( szEquals );
                return true;
            }
        }
    }
    return false;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   parse_param_set
//
// Checks if a command line paramter is set
//
// ARGUMENTS:
//  (IN)     const char*     Command line paramter to search for (e.g."-?")
//  (IN)     int             Command line paramter count
//  (IN)     char**          Command line paramters
//
// RETURNS:
//           bool            true if parameter was found
//
///////////////////////////////////////////////////////////////////////////////
bool parse_param_set( const char* szParam, int nArgv, char **pArgc )
{
    int i;
    int nParamLen = strlen( szParam );

    if ( szParam == NULL )
        return false;

    for( i = 0; i < nArgv; i ++ )
    {
        char *szCurParam = pArgc[ i ];
        int   nCurParamLen;

        if (( szCurParam == NULL ) || ( szCurParam[0] != '-' ))
            continue;
    
        nCurParamLen = strlen( szCurParam );

        if ( nCurParamLen < nParamLen )
            continue;

        if ( memcmp( szParam, szCurParam, nParamLen ) == 0 )
            return true;
    }
    return false;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   parse_string_param
//
// Parses a string paramter from the command line arguments
//
// ARGUMENTS:
//  (IN)     const char*     Command line paramter to search for (e.g."-r=")
//  (IN/OUT) char*           Buffer to take string value
//  (IN)     int             Buffer size
//  (IN)     int             Command line paramter count
//  (IN)     char**          Command line paramters
//
// RETURNS:
//           bool            true if parameter was found and interpreted
//
///////////////////////////////////////////////////////////////////////////////
bool parse_string_param( 
            const char* szParam, 
            char  *szBuffer,
            int    nMaxLen,
            int    nArgv, 
            char **pArgc            
            )
{
    int i;
    int nParamLen = strlen( szParam );

    if ( szParam == NULL )
        return false;

    for( i = 0; i < nArgv; i ++ )
    {
        char *szCurParam = pArgc[ i ];
        int   nCurParamLen;

        if (( szCurParam == NULL ) || ( szCurParam[0] != '-' ))
            continue;
    
        nCurParamLen = strlen( szCurParam );

        if ( nCurParamLen < nParamLen )
            continue;

        if ( memcmp( szParam, szCurParam, nParamLen ) == 0 )
        {
            char* szEquals = strchr( szCurParam, '=' );
            if ( szEquals )
            {
                szEquals++;
                nCurParamLen = strlen( szEquals );
                if ( nCurParamLen < nMaxLen )            
                {
                    strcpy( szBuffer, szEquals );
                }
                else
                {
                    memcpy ( szBuffer, szEquals, nMaxLen - 2 );
                    szBuffer[ nMaxLen - 1 ] = 0;
                }
                return true;
            }
        }
    }
    return false;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   parse_frequency_param
//
// Parses a paramter from the command line arguments, looking for frequency.
// This can either be a numeric value between -50000 and +50000, or a file
// name containing the value.
//
// ARGUMENTS:
//  (IN)     const char*     Command line paramter to search for (e.g."-r=")
//  (IN/OUT) int*            Frequency value
//  (IN)     int             Command line paramter count
//  (IN)     char**          Command line paramters
//
// RETURNS:
//           bool            true if parameter was found and interpreted
//
///////////////////////////////////////////////////////////////////////////////
bool parse_frequency_param( 
            const char* szParam, 
            int   *pVal,
            int    nArgv, 
            char **pArgc            
            )
{
    int  i;
    char szStringParam[ MAX_PATH ];
    int  nParamLen = strlen( szParam );
    bool fFound = false;
    bool fIsDigit = true;

    if ( szParam == NULL )
        return false;

    if ( pVal == NULL )
        return false;

    for( i = 0; i < nArgv; i ++ )
    {
        char *szCurParam = pArgc[ i ];
        int   nCurParamLen;

        if (( szCurParam == NULL ) || ( szCurParam[0] != '-' ))
            continue;
    
        nCurParamLen = strlen( szCurParam );

        if ( nCurParamLen < nParamLen )
            continue;

        if ( memcmp( szParam, szCurParam, nParamLen ) == 0 )
        {
            char* szEquals = strchr( szCurParam, '=' );
            if ( szEquals )
            {
                szEquals++;
                nCurParamLen = strlen( szEquals );
                if ( nCurParamLen < ( MAX_PATH - 2 ))            
                {
                    strcpy( szStringParam, szEquals );
                }
                else
                {
                    memcpy ( szStringParam, szEquals, MAX_PATH - 2 );
                    szStringParam[ MAX_PATH - 1 ] = 0;
                }
                fFound = true;
                break;
            }
        }
    }

    // Not found, look for default file    
    if ( fFound == false )
    {
        strcpy( szStringParam, RF_OFFSET_FILE );
        fFound = true;
    }

    //
    // OK, got parameter
    //
    if ( fFound )
    {
        nParamLen = strlen( szStringParam );

        //
        // See if it's a number
        //
        for( i = 0; i < nParamLen; i ++ )
        {
            if ((( szStringParam[i] >= '0' ) &&
                 ( szStringParam[i] <= '9' )) ||
                 ( szStringParam[i] == '-' ))
                continue;
            fIsDigit = false;
            break;
        }

        if ( fIsDigit )
        {
            *pVal = atoi( szStringParam );

            // cap
            if ( *pVal < -50000 )
                *pVal = -50000;
            if ( *pVal > 50000 )
                *pVal = 50000;
            return true;
        }   
    }

    //
    // OK, but it's a filename
    //
    if ( fFound )
    {
        FILE *pFile;
        pFile = fopen( szStringParam, "rt" );
        if ( pFile == NULL )
        {
            return false;
        }
        if ( fscanf( pFile,"%d", pVal ) <= 0 )
        {
            fclose( pFile );
            return false;
        }
        fclose( pFile );
        return true;
    }
    return false;
}

//////////////////////////////////// EOF //////////////////////////////////////

// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   example.c
//
// PROJECT:     iceqube_icenetc_lib example
//
// PLATFORM:    crisp linux/arm/gcc
//
// Activate timed example
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "host_comms.h"
#include "types.h"
#include "util.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

//
// Non user-settable states
//
#define DEVICE_STATE_LOW_POWER_SLEEP    0x10
#define DEVICE_STATE_RADIO_TX_POWER     0x11
#define DEVICE_STATE_RADIO_TX_CW_POWER  0x12
#define DEVICE_STATE_RADIO_RX_POWER     0x13
#define DEVICE_STATE_POST_FAIL          0xFF

//
// Enabled trigger mask
//
#define TRIGGER_INDUCTOR_2PULSE        1
#define TRIGGER_INDUCTOR_2PULSEDEL     2
#define TRIGGER_INDUCTOR_3PULSE        4
#define TRIGGER_INDUCTOR_FIELD         8
#define TRIGGER_TIMED                  16

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

const char *state_to_string( uint8_t nState );
const char *triggers_to_string( uint8_t nEnabledTriggers );

extern bool parse_int_param( 
                const char* szParam, 
                int *pVal, 
                int nArgv, 
                char **pArgc 
                );

extern bool parse_string_param( 
                const char* szParam, 
                char  *szBuffer,
                int    nMaxLen,
                int    nArgv, 
                char **pArgc            
                );

/////////////////////////////////////////////
// PUBLIC DATA
//

/////////////////////////////////////////////
// PRIVATE DATA
//

static bool     sg_fClearOnActivate = false;
static bool     sg_fSetTimeOnActivate = true;
static bool     sg_fUTC = true;
static uint32_t sg_nConnectionInterval = 86400; // daily
static uint32_t sg_nConnectionOffset = 0;
static char     sg_szMsg[ 200 ];

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_run
//
// Called by main.c to activate a connected device
//
// ARGUMENTS:
//  (IN)        uint32_t    nConnectedPeer
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int host_example_run( uint32_t nConnectedPeer )
{
    struct iceqube_device_state hInfo;
    int                         nErr;
    struct tm                   hLocalTime;
    struct trigger_config       hTriggerConfig;
    int                         nTzOffset = sg_fUTC ? 0 : util_tzoffset( );
    int64_t                     nTimeDiff;

    fprintf( stdout, "\n" );
    fprintf( stdout, "DEVICE ID:           %d\n", nConnectedPeer );

    //
    // Get device info
    //
    nErr = host_comms_get_device_info( &hInfo );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query device info %d\n", nErr );
        return nErr;
    }

    if ( sg_fSetTimeOnActivate )
    {
        nTimeDiff = util_time( ) - hInfo.nDeviceTime;
    }

    //
    // Display device info
    //
    fprintf( stdout, "  Device State:      %s\n", state_to_string( hInfo.nDeviceState ));
    fprintf( stdout, "  Firmware Version:  %d.%03d\n", ( hInfo.nFwVersion >> 8 ), ( hInfo.nFwVersion & 0xFF ));

    util_localtime( 
        hInfo.nDeviceTime + nTzOffset, 
        &hLocalTime 
        );

    fprintf( stdout, "  Device Time:       %s", asctime( &hLocalTime ));
    fprintf( stdout, "  Battery level:     %d mV\n", hInfo.nBatteryLevel );
    
    if ( hInfo.nDeviceState != DEVICE_STATE_IDLE )
    {
        fprintf( stdout, "  ERROR: Device is not idle, cannot activate\n" );
        return 0;
    }

    //
    // Set the activation mode
    //
    nErr = host_comms_set_activation_mode( sg_fClearOnActivate );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to set activation mode %d\n", nErr );
        return nErr;
    }
    fprintf( stdout, "  Clear data:        %s\n", ( sg_fClearOnActivate ? "true" : "false" ));
    
    //
    // Update the device time as required
    //
    if ( sg_fSetTimeOnActivate )
    {
        if ( nTimeDiff != 0 )
        {
            //
            // Update device time 
            //
            nErr = host_comms_set_time( util_time( ) );
 
            if ( nErr != 0 )
            {
                fprintf( stdout, "  ERROR: Failed to set device time %d\n", nErr );
                return nErr;
            }
        }
    }

    //
    // Get current trigger settings
    //
    nErr = host_comms_get_trigger_config( &hTriggerConfig );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query trigger configuration %d\n", nErr );
        return nErr;
    }

    //
    // Enable timed triggering and update the connection interval settings
    //
    hTriggerConfig.nEnabledTriggers |= TRIGGER_TIMED;
    hTriggerConfig.nTimedOffset = sg_nConnectionOffset;   
    hTriggerConfig.nTimedInterval = sg_nConnectionInterval;

    // Don't update the current standoff index
    hTriggerConfig.nStandoffIndex[0] = 0xFF;
    hTriggerConfig.nStandoffIndex[1] = 0xFF;
    hTriggerConfig.nStandoffIndex[2] = 0xFF;
    hTriggerConfig.nStandoffIndex[3] = 0xFF;

    nErr = host_comms_set_trigger_config( &hTriggerConfig );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query trigger configuration %d\n", nErr );
        return nErr;
    }
    fprintf( stdout, "  Enabled Triggers:  %s\n", triggers_to_string( hTriggerConfig.nEnabledTriggers ) );
    fprintf( stdout, "  Timed offset:      %d s\n", hTriggerConfig.nTimedOffset );
    fprintf( stdout, "  Timed interval:    %d s\n", hTriggerConfig.nTimedInterval );

    //
    // Activate device
    //
    nErr = host_comms_set_mode( DEVICE_STATE_ACTIVE );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to activat device %d\n", nErr );
        return nErr;
    }

    if ( sg_fSetTimeOnActivate )
        fprintf( stdout, "  Device activated (device time sync'd to system time)\n" );
    else
        fprintf( stdout, "  Device activated (device time not changed)\n" );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   state_to_string
//
// Utility function to convert a device state into a string
//
// ARGUMENTS:
//  (IN)        uint8_t         Device state
//
// RETURNS:
//              const char*     Device state string
//
///////////////////////////////////////////////////////////////////////////////
const char *state_to_string( uint8_t nState )
{
    switch( nState )
    {
    case DEVICE_STATE_IDLE:
        return "IDLE";
    case DEVICE_STATE_ACTIVE:
        return "ACTIVE";
    case DEVICE_STATE_FIRMWARE:
        return "FIRMWARE";
    case DEVICE_STATE_LOW_POWER_SLEEP:
    case DEVICE_STATE_RADIO_TX_POWER:
    case DEVICE_STATE_RADIO_TX_CW_POWER:
    case DEVICE_STATE_RADIO_RX_POWER:
        return "TEST STATE";
    case DEVICE_STATE_POST_FAIL:
        return "POST FAILURE STATE";
    default:
        break;
    }
    return "UNKNOWN";
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   triggers_to_string
//
// Utility function to convert a mask of enabled trigger types into an ascii
// string.
//
// ARGUMENTS:
//  (IN)        uint8_t         Mask of enabled triggers
//
// RETURNS:
//              const char*     Ascii string of enabled triggers
//
///////////////////////////////////////////////////////////////////////////////
const char *triggers_to_string( uint8_t nEnabledTriggers )
{
    sg_szMsg[ 0 ] = 0;

    if ( nEnabledTriggers & TRIGGER_INDUCTOR_2PULSE )
        strcat( sg_szMsg, "2 pulse" );
    if ( nEnabledTriggers & TRIGGER_INDUCTOR_2PULSEDEL )
    {
        if ( sg_szMsg[ 0 ] != 0 )
            strcat( sg_szMsg, ", " );
        strcat( sg_szMsg, "2 long pulse" );
    }
    if ( nEnabledTriggers & TRIGGER_INDUCTOR_3PULSE )
    {
        if ( sg_szMsg[ 0 ] != 0 )
            strcat( sg_szMsg, ", " );
        strcat( sg_szMsg, "3 pulse" );
    }
    if ( nEnabledTriggers & TRIGGER_INDUCTOR_FIELD )
    {
        if ( sg_szMsg[ 0 ] != 0 )
            strcat( sg_szMsg, ", " );
        strcat( sg_szMsg, "field" );
    }
    if ( nEnabledTriggers & TRIGGER_TIMED )
    {
        if ( sg_szMsg[ 0 ] != 0 )
            strcat( sg_szMsg, ", " );
        strcat( sg_szMsg, "timed" );
    }
    if ( sg_szMsg[ 0 ] == 0 )
        strcat( sg_szMsg, "none" );

    return sg_szMsg;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_get_param_count
//
// Returns number of command line parameters supported by example
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int     Parameter count
//
///////////////////////////////////////////////////////////////////////////////
int host_example_get_param_count( void )
{
    return 5;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_parse_params
//
// Passes a command line parameter for parsing. 'p', 'i', 's', 'r' & 'l' are
// reserved by the application.
//
// ARGUMENTS:
//  (IN)        char*   Command line 
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_parse_params( char *pParam )
{
    int n;
    char szStr[ 10 ];

    if ( parse_int_param( "-c=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_fClearOnActivate = 0;
        else
            sg_fClearOnActivate = 1;
		return 0;
    }

    if ( parse_int_param( "-x=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_fSetTimeOnActivate = 0;
        else
            sg_fSetTimeOnActivate = 1;
		return 0;
    }

    if ( parse_string_param( "-t=", szStr, 10, 1, &pParam ))
    {
        if ( strchr( szStr, 'l' ))
            sg_fUTC = false;
        else if ( strchr( szStr, 'u' ))
            sg_fUTC = true;
        else
        {
            fprintf( stdout, "Invalid time local specifed. Must be u or l.\n");
            return -1;
        }
		return 0;
    }

    if ( parse_int_param( "-o=", &n, 1, &pParam ))
    {
        if (( n >= 0 ) && ( n <= 604800 ))
            sg_nConnectionOffset = n;
        else
        {
            fprintf( stdout, "Invalid connection time offset specifed. Must be 0 to 604800.\n" );
            return -1;
        }
		return 0;
    }

    if ( parse_int_param( "-v=", &n, 1, &pParam ))
    {
        if (( n >= 5 ) && ( n <= 604800 ))
            sg_nConnectionInterval = n;
        else
        {
            fprintf( stdout, "Invalid connection time interval specifed. Must be 5 to 604800.\n" );
            return -1;
        }
		return 0;
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_print_param_help
//
// Prints help string for parameters
//
// ARGUMENTS:
//  (IN)        FILE*   Stream to write help message to
//  (IN)        int     Index of parameter to print help message for
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_print_param_help( FILE *pStream, int nIndex )
{
    switch( nIndex )
    {
    case 0:
        fprintf( pStream, "\t-c=[0-1]        Clear data on activate (default 0)\n" );
        break;
    case 1:
        fprintf( pStream, "\t-x=[0-1]        Sync time on activate (default 1)\n" );
        break;
    case 2:
        fprintf( pStream, "\t-t=[l/u]        Display time as local time or utc (default utc)\n" );
        break;
    case 3:
        fprintf( pStream, "\t-o=[0-604800]   Timed offset (default 0 s)\n" );
        break;
    case 4:
        fprintf( pStream, "\t-v=[5-604800]   Timed interval (default 86400 s)\n" );
        break;

    default:
        break;
    }
    return 0;
}

//////////////////////////////////// EOF //////////////////////////////////////

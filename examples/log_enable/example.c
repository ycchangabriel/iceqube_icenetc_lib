// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   example.c
//
// PROJECT:     iceqube_icenetc_lib example
//
// PLATFORM:    crisp linux/arm/gcc
//
// Log enable example
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "host_comms.h"
#include "types.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

#define MAX_RETRY                           3

//
// Log filter types
//
#define LOG_FILTER_SYSTEM                   0x00
#define LOG_FILTER_CONFIG                   0x01
#define LOG_FILTER_RF                       0x02
#define LOG_FILTER_DATA                     0x04
#define LOG_FILTER_TRIGGER                  0x08
#define LOG_FILTER_PROFILER_SLEEP           0x10
#define LOG_FILTER_PROFILER_DATA_CAPTURE    0x20
#define LOG_FILTER_PROFILER_RADIO_SESSION   0x40
#define LOG_FILTER_PROFILER_FIRMWARE_UPLOAD 0x80

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

extern bool parse_int_param( 
                const char* szParam, 
                int *pVal, 
                int nArgv, 
                char **pArgc 
                );

extern bool parse_string_param( 
                const char* szParam, 
                char  *szBuffer,
                int    nMaxLen,
                int    nArgv, 
                char **pArgc            
                );

/////////////////////////////////////////////
// PUBLIC DATA
//

/////////////////////////////////////////////
// PRIVATE DATA
//

static bool    sg_fEnableConfigLogs = false;
static bool    sg_fEnableRfLogs = false;
static bool    sg_fEnableDataLogs = false;
static bool    sg_fEnableTriggerLogs = false;
static uint8_t sg_nProfilerEn = 0;

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_run
//
// Called by main.c to enable logging on a connected device
//
// ARGUMENTS:
//  (IN)        uint32_t    nConnectedPeer
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int host_example_run( uint32_t nConnectedPeer )
{
    struct log_config           hFiltering;
    int                         nErr;

    fprintf( stdout, "\n" );
    fprintf( stdout, "DEVICE ID:           %d\n", nConnectedPeer );

    //
    // Configure filtering
    //
    hFiltering.nStructVersion = STRUCT_LOG_CONFIG_VERSION;
    hFiltering.nLogFilter = 0;
    if ( sg_fEnableConfigLogs )
        hFiltering.nLogFilter |= LOG_FILTER_CONFIG; 
    if ( sg_fEnableRfLogs )
        hFiltering.nLogFilter |= LOG_FILTER_RF;
    if ( sg_fEnableDataLogs )
        hFiltering.nLogFilter |= LOG_FILTER_DATA;
    if ( sg_fEnableTriggerLogs )
        hFiltering.nLogFilter |= LOG_FILTER_TRIGGER;
    switch ( sg_nProfilerEn )
    {
    default:
        break;
    case 1:
        hFiltering.nLogFilter |= LOG_FILTER_PROFILER_SLEEP;
        break;
    case 2:
        hFiltering.nLogFilter |= LOG_FILTER_PROFILER_DATA_CAPTURE;
        break;
    case 3:
        hFiltering.nLogFilter |= LOG_FILTER_PROFILER_RADIO_SESSION;
        break;
    case 4:
        hFiltering.nLogFilter |= LOG_FILTER_PROFILER_FIRMWARE_UPLOAD;
        break;
    }

    nErr = host_comms_set_log_filter( &hFiltering );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to update device logging %d\n", nErr );
        return nErr;
    }
    fprintf( stdout, "  Device log for:     system" );
    if ( sg_fEnableConfigLogs )
        fprintf( stdout, ", config" );
    if ( sg_fEnableRfLogs )
        fprintf( stdout, ", rf" );
    if ( sg_fEnableDataLogs )
        fprintf( stdout, ", data" );
    if ( sg_fEnableTriggerLogs )
        fprintf( stdout, ", triggering" );
    switch ( sg_nProfilerEn )
    {
    default:
        break;
    case 1:
        fprintf( stdout, ", sleep profiling" );
        break;
    case 2:
        fprintf( stdout, ", data capture profiling" );
        break;
    case 3:
        fprintf( stdout, ", radio session profiling" );
        break;
    case 4:
        fprintf( stdout, ", firmware upload profiling" );
        break;
    }
    fprintf( stdout, "\n" );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_get_param_count
//
// Returns number of command line parameters supported by example
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int     Parameter count
//
///////////////////////////////////////////////////////////////////////////////
int host_example_get_param_count( void )
{
    return 5;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_parse_params
//
// Passes a command line parameter for parsing. 'p', 'i', 's', 'r' & 'l' are
// reserved by the application.
//
// ARGUMENTS:
//  (IN)        char*   Command line
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_parse_params( char *pParam )
{
    int n;

    if ( parse_int_param( "-c=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_fEnableConfigLogs = 0;
        else
            sg_fEnableConfigLogs = 1;
		return 0;
    }

    if ( parse_int_param( "-w=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_fEnableRfLogs = 0;
        else
            sg_fEnableRfLogs = 1;
		return 0;
    }

    if ( parse_int_param( "-d=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_fEnableDataLogs = 0;
        else
            sg_fEnableDataLogs = 1;
		return 0;
    }

    if ( parse_int_param( "-t=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_fEnableTriggerLogs = 0;
        else
            sg_fEnableTriggerLogs = 1;
		return 0;
    }

    if ( parse_int_param( "-q=", &n, 1, &pParam ))
    {
        if (( n < 0 ) || ( n > 4 ))
        {
            fprintf( stdout, "Invalid profiler value. Must be 0-4.\n");
            return -1;
        }
        sg_nProfilerEn = n;
 		return 0;
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_print_param_help
//
// Prints help string for parameters
//
// ARGUMENTS:
//  (IN)        FILE*   Stream to write help message to
//  (IN)        int     Index of parameter to print help message for
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_print_param_help( FILE *pStream, int nIndex )
{
    switch( nIndex )
    {
    case 0:
        fprintf( pStream, "\t-c=[0-1]        Enable config logs (default 0)\n" );
        break;
    case 1:
        fprintf( pStream, "\t-w=[0-1]        Enable RF logs (default 0)\n" );
        break;
    case 2:
        fprintf( pStream, "\t-d=[0-1]        Enable data logs (default 0)\n" );
        break;
    case 3:
        fprintf( pStream, "\t-t=[0-1]        Enable trigger logs (default 0)\n" );
        break;
    case 4:
        fprintf( pStream, "\t-q=[0-4]        Enable profiler & profile logs (default 0)\n" );
        break;

    default:
        break;
    }
    return 0;
}

//////////////////////////////////// EOF //////////////////////////////////////

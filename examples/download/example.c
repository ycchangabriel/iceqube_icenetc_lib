// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   example.c
//
// PROJECT:     iceqube_icenetc_lib example
//
// PLATFORM:    crisp linux/arm/gcc
//
// Download example
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <time.h>
#include <syslog.h>

#include "host_comms.h"
#include "types.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

#define MAX_RETRY                           3

//
// #defines used by rawbin file format
//
#define PKT_HEADER			                0x49554E4B  //  'IUNK' 
#define PAUNKNOWN_CLASS_NAME                "IFSUNKNOWN_TYPE_RAWICETAG3DLITEDATA"

//
// Enabled trigger mask
//
#define TRIGGER_INDUCTOR_2PULSE             1
#define TRIGGER_INDUCTOR_2PULSEDEL          2
#define TRIGGER_INDUCTOR_3PULSE             4
#define TRIGGER_INDUCTOR_FIELD              8
#define TRIGGER_TIMED                       16


#define qprintf( ... )  { \
                            if ( sg_fService == false ) \
                                fprintf( stdout, __VA_ARGS__ ); \
                            else \
                                syslog( LOG_INFO, __VA_ARGS__ ); \
                        }

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

//
// Ice raw binary file header
//
struct IfsPacketHeader
{
	uint32_t  nPacketHeader;
	uint32_t  nHeaderSize;
	uint32_t  nClassNameHash;
	uint32_t  nClassVersion;
	uint32_t  nTimeStamp;
	uint32_t  nClassNameLen;
	char  *szClassName;
	uint32_t  nFlags;
} __attribute(( packed ));

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

int      save_day_data_rawbin( 
                struct iceqube_device_state *pInfo, 
                struct day_data *pDay 
                );

int      save_day_data_csv( 
                struct iceqube_device_state *pInfo, 
                struct day_data *pDay 
                );

int      save_day_data_json( 
                struct iceqube_device_state *pInfo, 
                struct day_data *pDay 
                );

uint32_t palib_gettickcount( void );
uint32_t palib_donamehash( char *szName );

void     calculate_timespec_diff( 
                struct timespec *pStart, 
                struct timespec *pEnd, 
                struct timespec *pDiff
                );

extern bool parse_int_param( 
                const char* szParam, 
                int *pVal, 
                int nArgv, 
                char **pArgc 
                );

extern bool parse_string_param( 
                const char* szParam, 
                char  *szBuffer,
                int    nMaxLen,
                int    nArgv, 
                char **pArgc            
                );

/////////////////////////////////////////////
// PUBLIC DATA
//

extern bool sg_fService;

/////////////////////////////////////////////
// PRIVATE DATA
//
static uint8_t  sg_nOutputMode = 0;
static bool     sg_fUTC = true;
static char     sg_szOutputPath[255] = { '.', '/', 0 };
static uint32_t sg_nDelayFieldTrigger = 0;
static bool     sg_fSetTimeOnActivate = false;
static uint32_t sg_nMaxDaysToDownload = 4;

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_run
//
// Called by main.c to download data from a connected device
//
// ARGUMENTS:
//  (IN)        uint32_t    nConnectedPeer
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int host_example_run( uint32_t nConnectedPeer )
{
    struct iceqube_device_state hInfo;
    uint8_t                     nTotalDays, i;
    int                         nErr;
    int                         nLastRssi;
    struct timespec             hStartTS, hEndTS, hDiff;

    clock_gettime( CLOCK_MONOTONIC_RAW, &hStartTS );

    //
    // Get last RSSI for info
    //
    nErr = host_get_last_rssi( &nLastRssi );
    if ( nErr != 0 )
    {
        qprintf( "failed to query RSSI %d\n", nErr );    
        nLastRssi = -32768;
    }

    if ( sg_fService == false )
        qprintf( "\n" );

    qprintf( "DEVICE ID:             %d  (%ddBm)\n", nConnectedPeer, nLastRssi );

    //
    // Get device info
    //
    nErr = host_comms_get_device_info( &hInfo );
    if ( nErr != 0 )
    {
        qprintf( "  ERROR: Failed to query device info %d\n", nErr );
        return nErr;
    }

    //
    // Only as required
    //
    if ( sg_fSetTimeOnActivate )
    {
        int64_t nTimeDiff = util_time( ) - hInfo.nDeviceTime;
        if ( nTimeDiff != 0 )
        {
            //
            // Update device time 
            //
            nErr = host_comms_set_time( util_time( ) );

            if ( nErr != 0 )
            {
                fprintf( stdout, "  ERROR: Failed to set device time %d\n", nErr );
                return nErr;
            }
        }
    }

    nTotalDays = hInfo.nActiveDay + hInfo.nDays;
    if ( nTotalDays == 0 )
    {
        qprintf( "  Device is empty\n" );
        return 0;
    }

    if ( nTotalDays > sg_nMaxDaysToDownload )
        nTotalDays = sg_nMaxDaysToDownload;

    for( i = 0; i < nTotalDays; i ++ )
    {
        struct iceqube_get_day      hDayQuery;
        struct day_data             hDayData;

        hDayQuery.nStructVersion = STRUCT_ICEQUBE_GET_DAY_VERSION;
        hDayQuery.nDay = i;

        nErr = host_comms_get_data( &hDayQuery, &hDayData );
        if ( nErr != 0 )
        {
            qprintf( "  ERROR: Failed to query day data %d\n", nErr );
            return nErr;
        }

        switch( sg_nOutputMode )
        {
        case 0:
            nErr = save_day_data_rawbin( &hInfo, &hDayData );
            break;
        case 1:
            nErr = save_day_data_csv( &hInfo, &hDayData );
            break;
        case 2:
            nErr = save_day_data_json( &hInfo, &hDayData );
            break;
        default:
            qprintf( "  ERROR: Unsupported output file format\n" );
            return nErr;
        }
        
        if ( nErr != 0 )
        {
            qprintf( "  ERROR: Failed to save day data %d\n", nErr );
            return nErr;
        }
    }

    if ( sg_fSetTimeOnActivate )
    {
        qprintf( "  %d days of data downloaded & device time sync'd\n", nTotalDays );
    }
    else
    {
        qprintf( "  %d days of data downloaded\n", nTotalDays );
    }

    //
    // Delay field triggering as required
    //
    if ( sg_nDelayFieldTrigger != 0 )
    {
        struct trigger_config hTriggerConfig;

        //
        // Get current trigger config
        //
        nErr = host_comms_get_trigger_config( &hTriggerConfig );

        if ( nErr != 0 )
        {
            qprintf( "  ERROR: Failed to query trigger config %d\n", nErr );
            return nErr;
        }

        //
        // Only delay, if field triggering is enabled
        //
        if ( hTriggerConfig.nEnabledTriggers & TRIGGER_INDUCTOR_FIELD )
        {
            //
            // Set user defined standoff (index 8) to required trigger delay
            //
            hTriggerConfig.nConnectStandoffs[ 8 ] = sg_nDelayFieldTrigger * 1000; // seconds to ms

            //
            // Update the field based triggering to use index 8 (user defined standoff)
            //
            hTriggerConfig.nStandoffIndex[ 0 ] = 0xFF; // Don't change 2-pulse
            hTriggerConfig.nStandoffIndex[ 1 ] = 0xFF; // Don't change 2-long-pulse
            hTriggerConfig.nStandoffIndex[ 2 ] = 0xFF; // Don't change 3-pulse
            hTriggerConfig.nStandoffIndex[ 3 ] = 8;    // Set field stand-off index 8 = user defined

            //
            // Update the trigger config
            //
            nErr = host_comms_set_trigger_config( &hTriggerConfig );

            if ( nErr != 0 )
            {
                qprintf( "  ERROR: Failed to update trigger config %d\n", nErr );
                return nErr;
            }

            qprintf( "  Trigger config updated, next field based trigger possible in %ds\n", sg_nDelayFieldTrigger );
        }
        else
        {
            qprintf( "  Field based triggering not enabled. Stand-off not set\n" );
        }
    }

    clock_gettime( CLOCK_MONOTONIC_RAW, &hEndTS );

    calculate_timespec_diff( &hStartTS, &hEndTS, &hDiff );

    if ( sg_fService == false )
        qprintf( "  Total download time: %d.%09ds\n", hDiff.tv_sec, hDiff.tv_nsec );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   save_day_data_csv
//
// Saves day data to a csv format file.
//
// ARGUMENTS:
//  (IN)        struct iceqube_device_state *   Device info
//  (IN)        struct iceqube_get_day*         Day data to save
//
// RETURNS:
//              int                             0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int save_day_data_csv( struct iceqube_device_state *pInfo, struct day_data *pDay )
{
    char                     szOutFile[ 255 ];
    char                     szLine[ 1000 ];
    struct tm                hTime;
    FILE                    *pFile;
    int                      nLength;
    int                      j;
    uint64_t                 nStart, nEnd;
    int                      nTzOffset = sg_fUTC ? 0 : util_tzoffset( );


    // Make up the file name
    nStart = pDay->nStartEx;
    nStart <<= 32;
    nStart += pDay->nStart;

    util_localtime( 
        nStart + nTzOffset, 
        &hTime 
        );

    // Create output directory for the device
    sprintf( szOutFile, "%s%04d-%02d-%02d", sg_szOutputPath, hTime.tm_year + 1900, hTime.tm_mon + 1, hTime.tm_mday );
    mkdir( szOutFile, 0777 );

    // Make up full the filename
    sprintf( 
        szOutFile,
        "%s%04d-%02d-%02d/IceQube+ %d %02d;%02d;%02d.csv",
        sg_szOutputPath, 
        hTime.tm_year + 1900,
        hTime.tm_mon + 1, 
        hTime.tm_mday, 
        pInfo->nDeviceID, 
        hTime.tm_hour, 
        hTime.tm_min, 
        hTime.tm_sec 
        );

    // Open the output file
    pFile = fopen( szOutFile, "w+b" );
    if ( pFile == NULL )
    {
        qprintf( "Failed to open file '%s'\n", szOutFile );
        return -errno;
    }

    //
    // Write header
    //
	nLength = sprintf( szLine, "Device ID,%d\r\n", pInfo->nDeviceID );
	if ( fwrite( szLine, nLength, 1, pFile ) < 1 )
	{
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        fclose( pFile );
		return -errno;
	}

    nLength = sprintf( szLine,"iceqube version,3.003\r\n" );
	if ( fwrite( szLine, nLength, 1, pFile ) < 1 )
	{
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        fclose( pFile );
		return -errno;
	}

	nLength = sprintf( szLine,"Firmware Version,%d.%03d\r\n", ( pInfo->nFwVersion >> 8 ) & 0xFF, ( pInfo->nFwVersion & 0xFF ));
	if ( fwrite( szLine, nLength, 1, pFile ) < 1 )
	{
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        fclose( pFile );
		return -errno;
	}

    nLength = sprintf( szLine, "Battery State,%d mV\r\n\r\n", pInfo->nBatteryLevel );
	if ( fwrite( szLine, nLength, 1, pFile ) < 1 )
	{
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        fclose( pFile );
		return -errno;
	}

    //
    // Write data
    //
	nLength = sprintf( szLine, "Start Date,Start Time,End Date,End Time,Motion Index,Steps\r\n" );
	if ( fwrite( szLine, nLength, 1, pFile ) < 1 )
	{
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        fclose( pFile );
		return -errno;
	}

    nStart = pDay->nStartEx;
    nStart <<= 32;
    nStart |= pDay->nStart;

    nEnd = pDay->nStartEx;
    nEnd <<= 32;
    nEnd |= pDay->nEnd;
    if ( nEnd < nStart )
        nEnd += 0x100000000;

	for( j = 0; j < 96; j++ )
	{
        uint64_t nBlockEndTime;
		uint32_t nTimeStep;
        uint16_t nY1, nMo1, nDa1, nHo1, nMi1, nSe1;
        uint16_t nY2, nMo2, nDa2, nHo2, nMi2, nSe2;

		if ( j == 0 )
			nTimeStep = 899 - ( nStart % 900 );
		else
			nTimeStep = 899;

        util_localtime( 
            nStart + nTzOffset, 
            &hTime 
            );

        nY1  = hTime.tm_year + 1900;
        nMo1 = hTime.tm_mon + 1;
        nDa1 = hTime.tm_mday;
        nHo1 = hTime.tm_hour;
        nMi1 = hTime.tm_min;
        nSe1 = hTime.tm_sec;

		if ( nStart + nTimeStep < nEnd )
            nBlockEndTime = nStart + nTimeStep;
        else
            nBlockEndTime = nEnd;

        util_localtime( 
            nBlockEndTime + nTzOffset, 
            &hTime 
            );

        nY2  = hTime.tm_year + 1900;
        nMo2 = hTime.tm_mon + 1;
        nDa2 = hTime.tm_mday;
        nHo2 = hTime.tm_hour;
        nMi2 = hTime.tm_min;
        nSe2 = hTime.tm_sec;


		nLength = sprintf( 
			szLine, 
			"%04d/%02d/%02d,%02d:%02d:%02d,%04d/%02d/%02d,%02d:%02d:%02d,%d,%d\r\n",
			nY1, nMo1, nDa1, nHo1, nMi1, nSe1,
			nY2, nMo2, nDa2, nHo2, nMi2, nSe2,
			pDay->nMotionIndex[j],
			pDay->nSteps[j]
			);

	    if ( fwrite( szLine, nLength, 1, pFile ) < 1 )
	    {
            qprintf( "Failed to write to file '%s'\n", szOutFile );
            fclose( pFile );
		    return -errno;
	    }

		if ( nStart + nTimeStep >= nEnd )
			break;

		nStart += ( nTimeStep + 1 );
	}

    //
    // Changeovers
    //
    nStart = pDay->nStartEx;
    nStart <<= 32;
    nStart |= pDay->nStart;
	
    nLength = sprintf( szLine, "\r\nEvent Date,Event Time,New Orientation\r\n" );
    if ( fwrite( szLine, nLength, 1, pFile ) < 1 )
    {
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        fclose( pFile );
	    return -errno;
    }			

    for( j = 0; j < pDay->nChangeOvers; j++ )
	{
		uint32_t nOffset;
        uint64_t nEventTime;
        nOffset = pDay->hChangeOver[j].nTOffsetL;
        nOffset += ( pDay->hChangeOver[j].nTOffsetH ? 0x10000 : 0 );

        nEventTime = nStart;
        nEventTime += nOffset;

        util_localtime( 
            nEventTime + nTzOffset, 
            &hTime 
            );

        nLength = sprintf( szLine, 
						"%04d/%02d/%02d,%02d:%02d:%02d,%s\r\n",
						hTime.tm_year + 1900,
                        hTime.tm_mon + 1, 
                        hTime.tm_mday, 
                        hTime.tm_hour, 
                        hTime.tm_min, 
                        hTime.tm_sec,
                        ( pDay->hChangeOver[j].nState ? "STANDING" : "LYING" )
						);

        if ( fwrite( szLine, nLength, 1, pFile ) < 1 )
        {
            qprintf( "Failed to write to file '%s'\n", szOutFile );
            fclose( pFile );
	        return -errno;
        }
    }

	if ( pDay->nFlags & 1 )
	{
        nLength = sprintf( szLine, 
					"Warning: Array of changeovers exceeded capacity\n"
					);

        if ( fwrite( szLine, nLength, 1, pFile ) < 1 )
        {
            qprintf( "Failed to write to file '%s'\n", szOutFile );
            fclose( pFile );
	        return -errno;
        }
    }
    fclose( pFile );
    return 0;

}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   save_day_data_json
//
// Saves day data to a json format file.
//
// ARGUMENTS:
//  (IN)        struct iceqube_device_state *   Device info
//  (IN)        struct iceqube_get_day*         Day data to save
//
// RETURNS:
//              int                             0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int save_day_data_json( struct iceqube_device_state *pInfo, struct day_data *pDay )
{
    char                     szOutFile[ 355 ];
    char                     szBuffer[ 1000 ];
    struct tm                hTime;
    FILE                    *pFile;
    int                      nLength;
    int                      j;
    uint64_t                 nDayStart, nStart, nEnd;
    int                      nTzOffset = sg_fUTC ? 0 : util_tzoffset( );

    // Make up the file name
    nStart = pDay->nStartEx;
    nStart <<= 32;
    nStart += pDay->nStart;

    util_localtime( 
        nStart + nTzOffset, 
        &hTime 
        );


    // Create output directory for the device
    sprintf( szOutFile, "%s%04d-%02d-%02d", sg_szOutputPath, hTime.tm_year + 1900, hTime.tm_mon + 1, hTime.tm_mday );
    mkdir( szOutFile, 0777 );

    // Make up full the filename
    sprintf( 
        szOutFile,
        "%s%04d-%02d-%02d/IceQube+ %d %02d;%02d;%02d.json",
        sg_szOutputPath, 
        hTime.tm_year + 1900,
        hTime.tm_mon + 1, 
        hTime.tm_mday, 
        pInfo->nDeviceID, 
        hTime.tm_hour, 
        hTime.tm_min, 
        hTime.tm_sec 
        );

    // Open the output file
    pFile = fopen( szOutFile, "w+b" );
    if ( pFile == NULL )
    {
        qprintf( "Failed to open file '%s'\n", szOutFile );
        return -errno;
    }

    nDayStart = pDay->nStartEx;
    nDayStart <<= 32;
    nDayStart += pDay->nStart;
    nStart = nDayStart;

    nEnd = pDay->nStartEx;
    nEnd <<= 32;
    nEnd += pDay->nEnd;
    if ( nEnd < nStart ) // End rolled
        nEnd += 0x100000000;
    
    util_localtime( 
        nDayStart + nTzOffset, 
        &hTime 
        );

    //
    // Header
    //  {
    //      "id":<id>
    //      "start":"HH:MM:SS DD:MM:YYYY"
    //      "version":"x.yyy"
    //      "battery":100
    //      "data":[
    //
    nLength = sprintf( 
                szBuffer,
                "{\n  \"id\":%d\n  \"start\":\"%02d:%02d:%02d %02d/%02d/%04d\",\n  \"version\":\"%d%03d\",\n  \"battery\":%d\n  \"data\":[\n",
                pInfo->nDeviceID,
                hTime.tm_hour,
                hTime.tm_min,
                hTime.tm_sec, 
                hTime.tm_mday, 
                hTime.tm_mon + 1, 
                hTime.tm_year + 1900,
                ( pInfo->nFwVersion >> 8 ) & 0xFF,
                ( pInfo->nFwVersion & 0xFF ),
                pInfo->nBatteryLevel
                );

    if ( fwrite( szBuffer, nLength, 1, pFile ) < 1 )
    {
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        fclose( pFile );
        return -errno;
    }

    //
    // Loop through data
    //
    for( j = 0; j < 96; j++ )
    {
        uint32_t nTimeStep;
        char     szDate[22];
        uint64_t nBlockEndTime;

        if ( j == 0 )
			nTimeStep = 899 - ( nStart % 900 );
		else
			nTimeStep = 899;

        nBlockEndTime = nStart + nTimeStep;
        if ( nBlockEndTime > nEnd )
            nBlockEndTime = nEnd;

        util_localtime( 
            nBlockEndTime + nTzOffset, 
            &hTime 
            );

        //  [
        //    "start":"HH:MM:SS DD/MM/YYYY"
        //    "end":"HH:MM:SS DD/MM/YYYY"
        //    "motion":n
        //    "steps":q
        //  ],

        sprintf( 
            szDate,
            "\"%02d:%02d:%02d %02d/%02d/%04d\"",
            hTime.tm_hour,
            hTime.tm_min,
            hTime.tm_sec, 
            hTime.tm_mday, 
            hTime.tm_mon + 1, 
            hTime.tm_year + 1900
            );

        util_localtime( 
            nStart + nTzOffset, 
            &hTime 
            );

        nLength = sprintf(
                    szBuffer,            
                    "%s{\n    \"start\":\"%02d:%02d:%02d %02d/%02d/%04d\",\n    \"end\":%s,\n    \"motion\":%d,\n    \"steps\":%d\n    }",
                    (( j == 0 ) ? "  " : ",\n  " ),
                    hTime.tm_hour,
                    hTime.tm_min,
                    hTime.tm_sec, 
                    hTime.tm_mday, 
                    hTime.tm_mon + 1, 
                    hTime.tm_year + 1900,
                    szDate,
                    pDay->nMotionIndex[j],
                    pDay->nSteps[j]
                    );

        if ( fwrite( szBuffer, nLength, 1, pFile ) < 1 )
        {
            qprintf( "Failed to write to file '%s'\n", szOutFile );
            fclose( pFile );
            return -errno;
        }

	    if (( nStart + nTimeStep ) >= nEnd )
		    break;
		nStart += ( nTimeStep + 1 );
    }

    nLength = sprintf( szBuffer, "\n  ],\n  \"orientation\":[\n" );
    if ( fwrite( szBuffer, nLength, 1, pFile ) < 1 )
    {
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        fclose( pFile );
        return -errno;
    }

    //
    // Changeovers
    //
    for( j = 0; j < pDay->nChangeOvers; j ++ )
    {
        uint64_t nChangeOverTime;
        nChangeOverTime = nDayStart;
        nChangeOverTime += ( pDay->hChangeOver[j].nTOffsetL );
        nChangeOverTime += ( pDay->hChangeOver[j].nTOffsetH ? 0x10000 : 0 );

        util_localtime( 
            nChangeOverTime + nTzOffset, 
            &hTime 
            );

        nLength = sprintf(
                    szBuffer,
                    "%s{\n    \"time\":\"%02d:%02d:%02d %02d/%02d/%04d\",\n    \"orientation\":%s,\n  }",
                    (( j == 0 ) ? "  " : ",\n  " ),
                    hTime.tm_hour,
                    hTime.tm_min,
                    hTime.tm_sec, 
                    hTime.tm_mday, 
                    hTime.tm_mon + 1, 
                    hTime.tm_year + 1900,
                    pDay->hChangeOver[j].nState ? "\"standing\"" : "\"lying\""
                    );

        if ( fwrite( szBuffer, nLength, 1, pFile ) < 1 )
        {
            qprintf( "Failed to write to file '%s'\n", szOutFile );
            fclose( pFile );
            return -errno;
        }
    }

    if ( pDay->nFlags & 1 )
    {
        nLength = sprintf( 
                    szBuffer, 
                    "\n  ],\n  \"warning\":\"orientation data overflow\"\n}\n" 
                    );
    }
    else
    {
        nLength = sprintf( szBuffer, "\n  ]\n}\n" );
    }
    if ( fwrite( szBuffer, nLength, 1, pFile ) < 1 )
    {
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        fclose( pFile );
        return -errno;
    }
    fclose( pFile );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   save_day_data_rawbin
//
// Saves day data to a raw binary icerobotics format file.
//
// ARGUMENTS:
//  (IN)        struct iceqube_device_state *   Device info
//  (IN)        struct iceqube_get_day*         Day data to save
//
// RETURNS:
//              int                             0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int save_day_data_rawbin( struct iceqube_device_state *pInfo, struct day_data *pDay )
{
    char                     szOutFile[ 355 ];
    struct tm                hTime;
    FILE                    *pFile;
    struct IfsPacketHeader   hHeader;
    uint32_t                 nDataType;
    uint32_t                 nTemp;
    uint16_t                 nTempShort;
    uint32_t                 nTimeStamp;
    uint8_t                  nTempByte;
    struct day_data          hLocalDay;
    uint64_t                 nTime, nEndTime, nStart;
	struct tm		         hNewZero;
	uint64_t		         nlNewZero;
    int                      nTzOffset = util_tzoffset( );


    // Make up the file name
    nStart = pDay->nStartEx;
    nStart <<= 32;
    nStart += pDay->nStart;

    util_localtime( 
        nStart + nTzOffset, 
        &hTime 
        );

    // Create output directory for the device
    sprintf( szOutFile, "%s%04d-%02d-%02d", sg_szOutputPath, hTime.tm_year + 1900, hTime.tm_mon + 1, hTime.tm_mday );
    mkdir( szOutFile, 0777 );

    // Make up full the filename
    sprintf( 
        szOutFile,
        "%s%04d-%02d-%02d/IceQube+ %d %02d;%02d;%02d.raw_bin",
        sg_szOutputPath, 
        hTime.tm_year + 1900,
        hTime.tm_mon + 1, 
        hTime.tm_mday, 
        pInfo->nDeviceID, 
        hTime.tm_hour, 
        hTime.tm_min, 
        hTime.tm_sec 
        );

    // Open the output file
    pFile = fopen( szOutFile, "w+b" );
    if ( pFile == NULL )
    {
        qprintf( "Failed to open file '%s'\n", szOutFile );
        return -errno;
    }

    //
    // Write it with backwards compatibility to IceQubeService
    //

    //
    // PaUnknown packet header
    //
    nDataType  = palib_donamehash( PAUNKNOWN_CLASS_NAME );
    nTimeStamp = palib_gettickcount( );
    hHeader.nPacketHeader  = htonl( PKT_HEADER );
	hHeader.nHeaderSize    = htonl( sizeof( hHeader ));
	hHeader.nClassNameHash = htonl( nDataType );
	hHeader.nClassVersion  = htonl( 1 );
	hHeader.nTimeStamp     = htonl( nTimeStamp );
	hHeader.nClassNameLen  = htonl( strlen( PAUNKNOWN_CLASS_NAME ));
	hHeader.szClassName    = PAUNKNOWN_CLASS_NAME;
	hHeader.nFlags         = htonl( 0 );

	//
	// Write the IfsProtocol packet header
	//
    if ( fwrite( &hHeader, sizeof( hHeader ), 1, pFile ) < 1 )
    {
        fclose( pFile );
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        return -errno;
    }

    if ( fwrite( PAUNKNOWN_CLASS_NAME, strlen( PAUNKNOWN_CLASS_NAME ), 1, pFile ) < 1 )
    {
        fclose( pFile );
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        return -errno;
    }

    // PaUnknown On Write 
    nTemp = htonl( nDataType );
    if ( fwrite( &nTemp, sizeof( nTemp ), 1, pFile ) < 1 )
    {
        fclose( pFile );
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        return -errno;
    }
    
    nTemp = htonl( 1 );
    if ( fwrite( &nTemp, sizeof( nTemp ), 1, pFile ) < 1 )
    {
        fclose( pFile );
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        return -errno;
    }

    // IfsUnknown On Write
    nTemp = 0;
    if ( fwrite( &nTemp, sizeof( nTemp ), 1, pFile ) < 1 )
    {
        fclose( pFile );
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        return -errno;
    }
    
    nTemp = htonl( nTimeStamp );
    if ( fwrite( &nTemp, sizeof( nTemp ), 1, pFile ) < 1 )
    {
        fclose( pFile );
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        return -errno;
    }

    //
    // IceTag3DLiteDataContainer On Write
    //

    // Device ID
    nTemp = htonl( pInfo->nDeviceID );
    if ( fwrite( &nTemp, sizeof( uint32_t ), 1, pFile ) < 1 )
    {
        fclose( pFile );
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        return -errno;
    }

    nTempShort = htons( 0x0303 ); // Not held on device
    if ( fwrite( &nTempShort, sizeof( nTempShort ), 1, pFile ) < 1 )
    {
        fclose( pFile );
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        return -errno;
    }

    nTempShort = htons( pInfo->nFwVersion ); // F/w version
    if ( fwrite( &nTempShort, sizeof( nTempShort ), 1, pFile ) < 1 )
    {
        fclose( pFile );
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        return -errno;
    }

    nTempShort = htons( 6 ); //6.25 sample freq
    if ( fwrite( &nTempShort, sizeof( nTempShort ), 1, pFile ) < 1 )
    {
        fclose( pFile );
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        return -errno;
    }

    nTemp = 0; // Reserved
    if (( fwrite( &nTemp, sizeof( nTemp ), 1, pFile ) < 1 ) ||
        ( fwrite( &nTemp, sizeof( nTemp ), 1, pFile ) < 1 ))
    {
        fclose( pFile );
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        return -errno;
    }

    nTemp = htonl( 1 ); // nDays
    if ( fwrite( &nTemp, sizeof( nTemp ), 1, pFile ) < 1 )
    {
        fclose( pFile );
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        return -errno;
    }

    memcpy( &hLocalDay, pDay, sizeof( struct day_data ));

    //
    // Ice start year is 2004, not 1/1/1970, but only 32 bytes long
    // subtract 34 years from start time 
    //
	hNewZero.tm_hour  = 0;
	hNewZero.tm_min   = 0;
	hNewZero.tm_sec   = 0;
	hNewZero.tm_mday  = 1;
	hNewZero.tm_mon   = 0;
	hNewZero.tm_year  = ( 2004 - 1900 );
	hNewZero.tm_isdst = -1;	
	nlNewZero = util_mktime( &hNewZero );

    nTime = hLocalDay.nStartEx;
    nTime <<= 32;
    nTime |= hLocalDay.nStart;

    nEndTime = hLocalDay.nStartEx;
    nEndTime <<= 32;
    nEndTime |= hLocalDay.nEnd;

    // Make sure there isn't a rounding issue
    if ( nEndTime < nTime )
        nEndTime += 0x100000000;

    if ( nTime > nlNewZero )
    {
        nTime    -= nlNewZero;
        nEndTime -= nlNewZero;
    }

    hLocalDay.nStart = ( nTime & 0xFFFFFFFF );   
    hLocalDay.nEnd   = ( nEndTime & 0xFFFFFFFF );
    hLocalDay.nStartEx = (( nTime >> 32 ) & 0xFFFFFFFF );

    if ( fwrite( &hLocalDay, sizeof( hLocalDay ), 1, pFile ) < 1 )
    {
        fclose( pFile );
        qprintf( "Failed to write to file '%s'\n", szOutFile );
        return -errno;
    }

    fclose( pFile );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   palib_gettickcount
//
// Utility function provides a tick count.
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              uint32_t        System tick count
//
///////////////////////////////////////////////////////////////////////////////
uint32_t palib_gettickcount( void )
{
    struct timespec hTp;
#if defined( CLOCK_MONOTONIC )
	clock_gettime( CLOCK_MONOTONIC, &hTp );
#elif defined( CLOCK_REALTIME )
    clock_gettime( CLOCK_MONOTONIC, &hTp );
#else
#error CLOCK_* not defined by target system
#endif
	return (uint32_t) ( hTp.tv_sec * 1000 + hTp.tv_nsec / 1000000 );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   palib_donamehash
//
// Name hashing function used by PaUnknown.
//
// ARGUMENTS:
//  (IN)        char*           String to hash
//
// RETURNS:
//              uint32_t        Simple name hash
//
///////////////////////////////////////////////////////////////////////////////
uint32_t palib_donamehash( char *szName )
{
	uint32_t  nHash	    = 0;
	uint32_t  naChar[4] = {0,0,0,0};
	uint32_t  nCurrent  = 0;
	uint32_t  nStartI   = 0;
    uint32_t  i;
	uint32_t  nNamelen;
	char *pPos;

	if ( szName == NULL )
		return 0;

	nNamelen = strlen( szName );

	pPos = strrchr( szName, '/' );

	if ( pPos != 0 )
		nStartI = ( pPos - szName ) + 1;

	for( i = nStartI; i < nNamelen; i++ )
	{
		naChar[ nCurrent ] += (uint8_t) szName[i];
		nCurrent++;
		if ( nCurrent >= 4 )
			nCurrent = 0;
	}

	nHash = ( naChar[0] & 0xFF ) +
			(( naChar[1] & 0xFF ) << 8 ) +
			(( naChar[2] & 0xFF ) << 16 ) +
			(( naChar[3] & 0xFF ) << 24 );

	return nHash;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_get_param_count
//
// Returns number of command line parameters supported by example
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int     Parameter count
//
///////////////////////////////////////////////////////////////////////////////
int host_example_get_param_count( void )
{
    return 6;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_parse_params
//
// Passes a command line parameter for parsing. 'p', 'i', 's', 'r' & 'l' are
// reserved by the application.
//
// ARGUMENTS:
//  (IN)        char*   Command line
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_parse_params( char *pParam )
{
    int n;
    char szStr[ 255 ];

    if ( parse_int_param( "-f=", &n, 1, &pParam ))
    {
        switch ( n )
        {
        case 0:
            sg_nOutputMode = 0;
            break;
        case 1:
            sg_nOutputMode = 1;
            break;
        case 2:
            sg_nOutputMode = 2;
            break;
        default:
            qprintf( "Invalid output format specified. Must be 0-2.\n");
            return -1;
        }
		return 0;
    }

    if ( parse_int_param( "-x=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_fSetTimeOnActivate = 0;
        else
            sg_fSetTimeOnActivate = 1;
		return 0;
    }

    if ( parse_string_param( "-t=", szStr, 10, 1, &pParam ))
    {
        if ( strchr( szStr, 'l' ))
            sg_fUTC = false;
        else if ( strchr( szStr, 'u' ))
            sg_fUTC = true;
        else
        {
            fprintf( stdout, "Invalid time local specifed. Must be u or l.\n");
            return -1;
        }
		return 0;
    }

    if ( parse_string_param( "-o=", szStr, 255, 1, &pParam ))
    {
        int nLen;

        strcpy( sg_szOutputPath, szStr );
        nLen = strlen( sg_szOutputPath );

        if (( sg_szOutputPath[0] == '\"' ) || ( sg_szOutputPath[0] =='\''))
        {
            int i;
            
            for( i = 1; i < nLen; i ++ )
            {
                sg_szOutputPath[ i - 1 ] = sg_szOutputPath[ i ];
                sg_szOutputPath[ i ] = 0;
            }
            nLen --;
        }

        if ( nLen == 0 )
        {
            qprintf( "Invalid output path.\n");
            return -1;
        }
    
        if (( sg_szOutputPath[nLen-1] == '\"' ) || ( sg_szOutputPath[nLen-1] =='\''))
        {
            sg_szOutputPath[nLen-1] = 0;
            nLen--;
        }

        if ( nLen == 0 )
        {
            qprintf( "Invalid output path.\n");
            return -1;
        }

        if ( sg_szOutputPath[ nLen - 1 ] != '/' )
            strcat( sg_szOutputPath, "/" );
		return 0;
    }

    if ( parse_int_param( "-d=", &n, 1, &pParam ))
    {
        sg_nDelayFieldTrigger = ( uint32_t ) n;
        if ( sg_nDelayFieldTrigger > 86400 )
        {
            qprintf( "Invalid post download field trigger delay (0-86400)s\n" );
            return -1;
        }
		return 0;
    }

    if ( parse_int_param( "-n=", &n, 1, &pParam ))
    {
        sg_nMaxDaysToDownload = ( uint32_t ) n;
        if (( sg_nMaxDaysToDownload < 1 ) || ( sg_nMaxDaysToDownload > 500 ))
        {
            qprintf( "Invalid maximum number of days (1-500)\n" );
            return -1;
        }
		return 0;
    }

    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_print_param_help
//
// Prints help string for parameters
//
// ARGUMENTS:
//  (IN)        FILE*   Stream to write help message to
//  (IN)        int     Index of parameter to print help message for
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_print_param_help( FILE *pStream, int nIndex )
{
    switch( nIndex )
    {
    case 0:
        fprintf( pStream, "\t-f=[0-2]        Output file mode, 0=raw_bin, 1=csv, 2=json (default 0)\n" );
        break;
    
    case 1:
        fprintf( pStream, "\t-t=[l/u]        Timestamps in local time (l) or utc (u) (default utc)\n" );
        fprintf( pStream, "\t                this option is only used for csv and json files.\n" );
        break;

    case 2:
        fprintf( pStream, "\t-o=<path>       Output path of files (default .)\n" );
        break;

    case 3:
        fprintf( pStream, "\t-n=[1-500]      Maximum number of days to download (default 4)\n");
        break;

    case 4:
        fprintf( pStream, "\t-d=[0-86400]    Post download field based trigger delay/stand-off (default 0)\n" );
        break;

    case 5:
        fprintf( pStream, "\t-x=[0-1]        Sync time on activate (default 0)\n" );
        break;

    default:
        break;
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   calculate_timespec_diff
//
// Calculates time difference between two struct timespec values
//
// ARGUMENTS:
//  (IN)        struct timespec*     Pointer to start time
//  (IN)        struct timespec*     Pointer to end time
//  (IN/OUT)    struct timespec*     Pointer to difference
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void calculate_timespec_diff( 
                struct timespec *pStart, 
                struct timespec *pEnd, 
                struct timespec *pDiff
                )
{
    if (( pEnd->tv_nsec - pStart->tv_nsec ) < 0 )
    {
        pDiff->tv_sec = pEnd->tv_sec - pStart->tv_sec - 1;
        pDiff->tv_nsec = pEnd->tv_nsec - pStart->tv_nsec + 1000000000;
    }
    else
    {
        pDiff->tv_sec = pEnd->tv_sec - pStart->tv_sec;
        pDiff->tv_nsec = pEnd->tv_nsec - pStart->tv_nsec;
    }
}

//////////////////////////////////// EOF //////////////////////////////////////

// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   example.c
//
// PROJECT:     iceqube_icenetc_lib example
//
// PLATFORM:    crisp linux/arm/gcc
//
// Set ID example
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "host_comms.h"
#include "types.h"
#include "util.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

#define MAX_RETRY                           3

//
// Non user-settable states
//
#define DEVICE_STATE_LOW_POWER_SLEEP    0x10
#define DEVICE_STATE_RADIO_TX_POWER     0x11
#define DEVICE_STATE_RADIO_TX_CW_POWER  0x12
#define DEVICE_STATE_RADIO_RX_POWER     0x13
#define DEVICE_STATE_POST_FAIL          0xFF

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

const char *state_to_string( uint8_t nState );

extern bool parse_int_param( 
                const char* szParam, 
                int *pVal, 
                int nArgv, 
                char **pArgc 
                );

extern bool parse_string_param( 
                const char* szParam, 
                char  *szBuffer,
                int    nMaxLen,
                int    nArgv, 
                char **pArgc            
                );

/////////////////////////////////////////////
// PUBLIC DATA
//

/////////////////////////////////////////////
// PRIVATE DATA
//

static char     sg_szMsg[200];
static uint32_t sg_nNextID = 1;
static bool     sg_fAutoIncrement = true;

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_run
//
// Called by main.c to gather device information from a connected device
//
// ARGUMENTS:
//  (IN)        uint32_t    nConnectedPeer
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int host_example_run( uint32_t nConnectedPeer )
{
    struct iceqube_device_state          hInfo;
    int                                  nErr;

    fprintf( stdout, "\n" );
    fprintf( stdout, "DEVICE ID:             %d\n", nConnectedPeer );

    //
    // Get device info
    //
    nErr = host_comms_get_device_info( &hInfo );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query device info %d\n", nErr );
        return nErr;
    }

    //
    // Display device info
    //
    fprintf( stdout, "  Device State:          %s\n", state_to_string( hInfo.nDeviceState ));

    //
    // Check that we can set the ID
    //
    if ( hInfo.nDeviceState != DEVICE_STATE_IDLE ) 
    {
        fprintf( stdout, "  ERROR: Cannot set ID if device is not IDLE\n" );
        return -1;
    }

    //
    // Set the new ID
    //
    nErr = host_comms_set_mac( sg_nNextID );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to set new device ID %d\n", nErr );
        return nErr;
    }
    fprintf( stdout, "  New Device ID:         %d\n", sg_nNextID );

    //
    // Auto increment ID as required
    //
    if ( sg_fAutoIncrement )
    {
        sg_nNextID ++;

        //
        // Skip invalid IDs (0xFFFFFFFE - uninitialised, 0 & 0xFFFFFFFF - broadcast IDs)
        //
        if (( sg_nNextID == 0xFFFFFFFE ) || 
            ( sg_nNextID == 0xFFFFFFFF ) ||
            ( sg_nNextID == 0 ))
            sg_nNextID = 1;
    }

    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   state_to_string
//
// Utility function to convert a device state into a string
//
// ARGUMENTS:
//  (IN)        uint8_t         Device state
//
// RETURNS:
//              const char*     Device state string
//
///////////////////////////////////////////////////////////////////////////////
const char *state_to_string( uint8_t nState )
{
    switch( nState )
    {
    case DEVICE_STATE_IDLE:
        return "IDLE";
    case DEVICE_STATE_ACTIVE:
        return "ACTIVE";
    case DEVICE_STATE_FIRMWARE:
        return "FIRMWARE";
    case DEVICE_STATE_LOW_POWER_SLEEP:
    case DEVICE_STATE_RADIO_TX_POWER:
    case DEVICE_STATE_RADIO_TX_CW_POWER:
    case DEVICE_STATE_RADIO_RX_POWER:
        return "TEST STATE";
    case DEVICE_STATE_POST_FAIL:
        return "POST FAILURE STATE";
    default:
        break;
    }
    return "UNKNOWN";
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_get_param_count
//
// Returns number of command line parameters supported by example
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int     Parameter count
//
///////////////////////////////////////////////////////////////////////////////
int host_example_get_param_count( void )
{
    return 2;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_parse_params
//
// Passes a command line parameter for parsing. 'p', 'i', 's', 'r' & 'l' are
// reserved by the application.
//
// ARGUMENTS:
//  (IN)        char*   Command line 
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_parse_params( char *pParam )
{
    int n;

    if ( parse_int_param( "-a=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_fAutoIncrement = false;
        else
            sg_fAutoIncrement = true;
		return 0;
    }

    if ( parse_int_param( "-m=", &n, 1, &pParam ))
    {        
        sg_nNextID = ( uint32_t ) n;

        //
        // Don't permit invalid IDs 
        // (0xFFFFFFFE - uninitialised, 0 & 0xFFFFFFFF - broadcast IDs)
        //
        if (( sg_nNextID == 0xFFFFFFFF ) || 
            ( sg_nNextID == 0xFFFFFFFE ) ||
            ( sg_nNextID == 0 ))
        {
            fprintf( stdout, "Invalid device ID, must be l - 4294967293.\n");
            return -1;
        }
		return 0;
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_print_param_help
//
// Prints help string for parameters
//
// ARGUMENTS:
//  (IN)        FILE*   Stream to write help message to
//  (IN)        int     Index of parameter to print help message for
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_print_param_help( FILE *pStream, int nIndex )
{
    switch( nIndex )
    {
    case 0:
        fprintf( pStream, "\t-a=[0-1]        Auto increment ID (default 0)\n" );
        break;
    case 1:
        fprintf( pStream, "\t-m=[l/u]        Next device ID/MAC (default 1)\n" );
        break;
    default:
        break;
    }
    return 0;
}

//////////////////////////////////// EOF //////////////////////////////////////

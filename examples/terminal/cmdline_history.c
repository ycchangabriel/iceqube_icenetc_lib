// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   cmdline_history.c
//
// PROJECT:     iceqube_terminal
//
// PLATFORM:    crisp linux/arm/gcc
//
// Wrapper for managing command line history (normally accessible through
// up/down keys)
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <string.h>
#include <errno.h>

#include "cmdline_history.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

#define MAX_LINELENGTH          200
#define PERSISTENT_FILE         "./.cmdline_history"

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

/////////////////////////////////////////////
// PUBLIC DATA
//

/////////////////////////////////////////////
// PRIVATE DATA
//

uint8_t     sg_nHistory;
uint8_t     sg_nStart;
static char sg_hHistory[ CMDLINE_HISTORY_MAX ][ MAX_LINELENGTH ];

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   cmdline_history_init
//
// Initialises buffer and pointers. Opens existing command line history buffer
// if one exists.
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void cmdline_history_init( void )
{
    FILE   *pBackup;
    uint8_t i;

    for ( i = 0; i < CMDLINE_HISTORY_MAX; i++ )
	{
		memset( sg_hHistory[ i ], 0, MAX_LINELENGTH );
	}

    sg_nHistory = 0;
    sg_nStart   = 0;

    pBackup = fopen( PERSISTENT_FILE, "rb" );
    if ( pBackup )
    {
        if ( fread( &sg_nHistory, 1, 1, pBackup ) > 0 )
        {
            if ( fread( &sg_nStart, 1, 1, pBackup ) > 0 )
            {
                for ( i = 0; i < sg_nHistory; i++ )
                {
                    if ( fread( sg_hHistory[ i ], MAX_LINELENGTH, 1, pBackup ) <= 0 )
                    {
                        // Oops some failure
                        sg_nHistory = 0;
                        sg_nStart = 0;
                        break;
                    }
                }
            }
            else
            {
                // Oops, failed to read start
                sg_nHistory = 0;
                sg_nStart = 0;
            }
        }
        else
        {
            // Oops, failed to read history
            sg_nHistory = 0;
            sg_nStart = 0;
        }
        fclose( pBackup );
    }
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   cmdline_history_close
//
// Maks buffer persistent.
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void cmdline_history_close( void )
{
    FILE   *pBackup;
    uint8_t i;

    pBackup = fopen( PERSISTENT_FILE, "w+b" );
    if ( pBackup )
    {
        if ( fwrite( &sg_nHistory, 1, 1, pBackup ) > 0 )
        {
            if ( fwrite( &sg_nStart, 1, 1, pBackup ) > 0 )
            {
                for ( i = 0; i < sg_nHistory; i++ )
                {
                    if ( fwrite( sg_hHistory[ i ], MAX_LINELENGTH, 1, pBackup ) <= 0 )
					{
						break;
					}
                }
            }
        }
        fclose( pBackup );
    }
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   cmdline_history_append
//
// Appends a string onto the history
//
// ARGUMENTS:
//  (IN)        const char*     String to append
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void cmdline_history_append( const char* szHistory )
{
    int  i, nLen;
    char szTmp[ MAX_LINELENGTH ];

    if ( szHistory == 0 )
	{
		return;
	}

    // Sanity check
    nLen = strlen( szHistory );
    if ( nLen == 0 )
	{
		return;
	}

    // Cap length
    if ( nLen >= MAX_LINELENGTH )
	{
		nLen = MAX_LINELENGTH - 1;
	}

    // Copy string (stop before a \n)
    for ( i = 0; i < nLen; i ++ )
    {
        if ( szHistory[ i ] == '\n' )
        {
            if ( i == 0 ) // not interrested in 0 length strings
			{
				return;
			}
            break;
        }
        szTmp[ i ] = szHistory[ i ];
    }
    szTmp[ i ] = 0;

    if ( sg_nHistory < CMDLINE_HISTORY_MAX )
    {
        strcpy( sg_hHistory[ sg_nHistory ], szTmp );
        sg_nHistory++;
        sg_nStart = 0;
    }
    else
    {
        sg_nHistory = CMDLINE_HISTORY_MAX;
        strcpy( sg_hHistory[ sg_nStart ], szTmp );
        sg_nStart++;
        if ( sg_nStart >= CMDLINE_HISTORY_MAX )
		{
			sg_nStart = 0;
		}
    }
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   cmdline_history_size
//
// Gets history buffer size
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              uint8_t         Size of history buffer
//
///////////////////////////////////////////////////////////////////////////////
uint8_t cmdline_history_size( void )
{
    return sg_nHistory;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   cmdline_history_get
//
// Returns a history string
//
// ARGUMENTS:
//  (IN/OUT)    int8_t          Index of string (0-cmdline_histoy_size), where
//                              0 is youngest string.
//
// RETURNS:
//              const char*     String
//
///////////////////////////////////////////////////////////////////////////////
const char* cmdline_history_get( uint8_t nIndex )
{
    // Bounds limit
    nIndex %= CMDLINE_HISTORY_MAX;

    if ( sg_nHistory < CMDLINE_HISTORY_MAX )
    {
        // Nothing in the array yet?
        if ( sg_nHistory == 0 )
		{
			return "\0";
		}

        // Reverse position in array from 0 
        nIndex = ( sg_nHistory - 1 ) - nIndex;
    }
    else
    {
        // Reverse position in array from start position
        nIndex = ( CMDLINE_HISTORY_MAX - 1 ) - nIndex;
        nIndex += sg_nStart;
        while ( nIndex >= CMDLINE_HISTORY_MAX )
            nIndex -= CMDLINE_HISTORY_MAX;
    }
    return sg_hHistory[ nIndex ];
}

//////////////////////////////////// EOF //////////////////////////////////////

// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   main.c
//
// PROJECT:     iceqube_terminal
//
// PLATFORM:    crisp linux/arm/gcc
//
// Entry point for iceqube_terminal, console application for communicating
// with icequbes over IceT protocol.
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <unistd.h> 
#include <string.h>
#include <errno.h>

#include "host_port.h"
#include "iceqube_icenetc.h"
#include "cmdline_history.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

//
// Buffer sizes
//
#define CONSOLE_LINE_BUFFER			200
#define RX_BUFFER					800

//
// Default channels
//
#define MAX_NUM_CHANNELS			3
#define CHAN_UNUSED					0xFF

#define LISTEN_CHANS				3
#define LISTEN_CHAN_0				0
#define LISTEN_CHAN_1				6
#define LISTEN_CHAN_2				12

#define SESSION_CHANS				1
#define SESSION_CHAN_0				18
#define SESSION_CHAN_1				CHAN_UNUSED
#define SESSION_CHAN_2				CHAN_UNUSED

//
// IceHub slot to use 0-2 or auto
//
#define MAX_CRISP_SLOT				2
#define CRISP_SLOT_AUTO				0xFF

#define RF_OFFSET_FILE      "/etc/rf_offset"

#ifndef MAX_PATH
#define MAX_PATH        255
#endif

//
// Input stream
//
#define STDIN						0

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

//
// Handle printable and non-printable chars in terminal
//
void handle_char( char nChar, char * szLineBuffer, int * pLineLength );
void handle_escape_char( char * pChar, char * szLineBuffer, int * pLineLength );

//
// Output usage
//
void print_usage( void );
void print_clear_line( void );

//
// Parameter parsing
//

bool parse_int_param( 
            const char* szParam, 
            int *pVal, 
            int nArgv, 
            char **pArgc 
            );

bool parse_param_set( 
            const char* szParam, 
            int nArgv, 
            char **pArgc 
            );

bool parse_frequency_param( 
            const char* szParam, 
            int   *pVal,
            int    nArgv, 
            char **pArgc            
            );

/////////////////////////////////////////////
// PUBLIC DATA
//

/////////////////////////////////////////////
// PRIVATE DATA
//

static int8_t      sg_nHistoryIndex = -1;

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   main
//
// Application entry point. Opens port and terminal and processes text based
// exchange.
//
// ARGUMENTS:
//  (IN)        int         Argument count
//  (IN)        char**      List of arguments
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int main( int nArgc, char **pArgc )
{
    //
    // Application parameters (set by command line)
    //
    bool           fListenChannelsSet     = false; // any listen channel set?
    bool           fSessionChannelsSet    = false; // any session channel set?
    uint8_t        nCrispSlot             = CRISP_SLOT_AUTO;  // auto
	uint8_t        nListenChannels		  = LISTEN_CHANS;     // default number of listen channels
	uint8_t        hListenChannels[ ]     = { LISTEN_CHAN_0, LISTEN_CHAN_1, LISTEN_CHAN_2 }; // default listen channels
	uint8_t        hNewListenChannels[]   = { CHAN_UNUSED, CHAN_UNUSED, CHAN_UNUSED }; // no new listen channels yet
    uint8_t        nSessionChannels       = SESSION_CHANS;     // default number of session channels
	uint8_t        hSessionChannels[]     = { SESSION_CHAN_0, SESSION_CHAN_1, SESSION_CHAN_2 }; // default session channels
	uint8_t        hNewSessionChannels[]  = { CHAN_UNUSED, CHAN_UNUSED, CHAN_UNUSED }; // no new session channels yet
    uint8_t        nRegulatory            = 0;
    uint8_t        nRank                  = 0;
    int32_t        nFrequencyOffset       = 0;     // Default is no frequency offset
    int            n;

    //
    // Input (keyboard) handling
    //
    struct termios hOldSettings;        
    struct termios hNewTty; 
	char           szLineBuffer[CONSOLE_LINE_BUFFER];
    int            nLineLength = 0;

    //
    // IceQube connection handling
    //
    bool           fConnectionState = false;
    uint32_t       nPeer;
    int            nFdMax;
    int            nFd;

    //
    // Init the command line history buffer
    //
    cmdline_history_init( );

    //
    // Disable stdout buffering
    //
    setbuf( stdout, NULL );

    //
    // Print header
    //
    fprintf( stdout,"iceqube_terminal\n" );
    fprintf( 
        stdout,
        "iceqube_icenetc_lib version %d.%03d\n", 
        ICEQUBE_ICENETC_VERSION_MAJ,
        ICEQUBE_ICENETC_VERSION_MIN
        );

    //
    // Parse the command line parameters
    //
    if ( parse_param_set( "-?", nArgc, pArgc ))
    {
        print_usage( );
        return 0;
    }

    if ( parse_int_param( "-p=", &n, nArgc, pArgc ))
    {
        nCrispSlot = n;
        if (( nCrispSlot != 0xFF ) && ( nCrispSlot > 2 ))
        {
            fprintf( stdout,"Invalid card slot (-p=%d). Must be port 0-2, or 255 for auto.\n", nCrispSlot );
            print_usage( );
            return -1;
        }
    }

    if ( parse_int_param( "-r=", &n, nArgc, pArgc ))
    {
        if (( n < -1 ) || ( n > 6 ))
        {
            fprintf( stdout,"Invalid rank (-r=%d). Must be port 0-6, or -1 for random.\n", n );
            print_usage( );
            return -1;
        }
        nRank = (uint8_t) ( n & 0xFF );
    }

    if ( parse_int_param( "-s0=", &n, nArgc, pArgc ))
    {
        hNewSessionChannels[0] = n;
        fSessionChannelsSet = true;
    }

    if ( parse_int_param( "-s1=", &n, nArgc, pArgc ))
    {
        hNewSessionChannels[1] = n;
        fSessionChannelsSet = true;
    }
    if ( parse_int_param( "-s2=", &n, nArgc, pArgc ))
    {
        hNewSessionChannels[2] = n;
        fSessionChannelsSet = true;
    }

    if ( parse_int_param( "-l0=", &n, nArgc, pArgc ))
    {
        hNewListenChannels[0] = n;
        fListenChannelsSet = true;
    }

    if ( parse_int_param( "-l1=", &n, nArgc, pArgc ))
    {
        hNewListenChannels[1] = n;
        fListenChannelsSet = true;
    }

    if ( parse_int_param( "-l2=", &n, nArgc, pArgc ))
    {
        hNewListenChannels[2] = n;
        fListenChannelsSet = true;
    }

    if ( parse_frequency_param( "-rf=", &n, nArgc, pArgc ))
    {
        nFrequencyOffset = n;
    }
    //
    // Assume new parameters
    //   
    if ( fListenChannelsSet )
        memcpy( hListenChannels, hNewListenChannels, 3 );
    nListenChannels = 0;
    if ( hListenChannels[0] != 0xFF ) 
        nListenChannels++;
    if ( hListenChannels[1] != 0xFF ) 
        nListenChannels++;
    if ( hListenChannels[2] != 0xFF ) 
        nListenChannels++;

    if ( fSessionChannelsSet )
        memcpy( hSessionChannels, hNewSessionChannels, 3 );
    nSessionChannels = 0;
    if ( hSessionChannels[0] != 0xFF )
        nSessionChannels++;
    if ( hSessionChannels[1] != 0xFF )
        nSessionChannels++;
    if ( hSessionChannels[2] != 0xFF )
        nSessionChannels++;

    //
    // No listen channels?
    //
    if (( nListenChannels == 0 ) || ( nSessionChannels == 0 ))
    {
        fprintf( stdout,"Invalid channel count. Requires at least 1 valid listen and 1 valid session channel.\n" );
        print_usage( );
        return -EFAULT;
    }

    fprintf( stdout,"Frequency Offset : %d Hz\n", nFrequencyOffset );
    fprintf( stdout,"  press esc to disconnect\n" );
    fprintf( stdout,"  press ctrl+c to exit\n\n" );

    //
    // Open cc1310
    //
    if ( host_port_open(
             nCrispSlot,
             nListenChannels,
             hListenChannels,
             nRank,
             nSessionChannels,
             hSessionChannels,
             PROTOCOL_ICET,
             &nRegulatory,
             nFrequencyOffset
             ) != 0 )
    {
        if ( nRegulatory != 0 )
        {
	        uint8_t i;
            for( i = 0; i < 3; i ++ )
            {
                if (( hListenChannels[ i ] != 0xFF ) &&
                    ( hListenChannels[ i ] > MAX_CHANNEL( nRegulatory )))
                {
                    fprintf( 
                        stdout,                     
                        "Invalid listen channel (-l%d=%u). Must be 0-%u\n", 
                        i, 
                        hListenChannels[ i ], 
                        MAX_CHANNEL( nRegulatory ) 
                        );
                    print_usage( );
                    return -1;
                }
                if (( hSessionChannels[ i ] != 0xFF ) &&
                    ( hSessionChannels[ i ] > MAX_CHANNEL( nRegulatory )))
                {
                    fprintf( 
                        stdout,
                        "Invalid session channel (-s%d=%u). Must be 0-%u\n", 
                        i, 
                        hSessionChannels[ i ], 
                        MAX_CHANNEL( nRegulatory ) 
                        );
                    print_usage( );
                    return -1;
                }
            }
        }
        fprintf( stdout,"Failed to open cc1310 host\n" );
        return -EFAULT;
    }

    nFd    = host_port_get_fd( );
    nFdMax = ( nFd > STDIN ) ? nFd : STDIN;

    //
    // Configure stdin
    //
    tcgetattr( STDIN, &hOldSettings );
    hNewTty = hOldSettings;
    hNewTty.c_lflag      &= ~ICANON;
    hNewTty.c_lflag      &= ~ECHO;
    hNewTty.c_lflag      &= ~ISIG;
    hNewTty.c_cc[ VMIN ]  = 0;
    hNewTty.c_cc[ VTIME ] = 0;
    tcsetattr( 0, TCSANOW, &hNewTty );

    //
    // Round robin loop
    //
    while( 1 )
    {
        int             nError;
        struct timeval  hTimeout;
        fd_set hFds;
        fd_set hErrfd;
        fd_set hWrite;

        // Configure fd_set
        FD_ZERO( &hFds );
        if ( fConnectionState )
		{
			FD_SET( nFd, &hFds );
		}
        FD_SET( STDIN, &hFds );

        FD_ZERO( &hErrfd );
        FD_SET( nFd, &hErrfd );

        FD_ZERO( &hWrite );
        FD_SET( nFd, &hWrite );

        hTimeout.tv_sec = 2;
        hTimeout.tv_usec = 0;

        //
        // Wait for something to happen (key in or host in)
        //
        nError = select(
                    nFdMax + 1,
                    &hFds,
                    fConnectionState ? NULL : &hWrite,
                    fConnectionState ? &hErrfd : NULL,
                    &hTimeout
                    );
        
        if ( nError == 0 )
		{
            if ( host_port_ping_norsp( ) < 0 )
            {
                fprintf( stdout, "Failed to issue cc1310 ping\n" );
                return -errno;
            }
			continue;
		}

        if ( nError < 0 )
        {
            continue;
        }

        //
        // Key stroke
        //
        if ( FD_ISSET ( STDIN, &hFds ))
        {
            char nChar[3];
            
            // Read single character
            if ( read ( STDIN, &nChar, 1 ) > 0 )
            {
                // CTRL+C
                if ( nChar[0] == 3 )
                {
                    printf("ctrl+c - exiting\n");
                    break;
                }

                // Not an escape sequence?
                if ( nChar[ 0 ] != '\e' )
                {
                    
                    handle_char( nChar[0], szLineBuffer, &nLineLength );
                }
                else
                {
                    memset ( nChar, 0, 3 );

                    // Read escaped char
                    if ( read ( STDIN, &nChar, 3 ) > 0 )
                    {
                        handle_escape_char( 
                                    nChar, 
                                    szLineBuffer,
                                    &nLineLength 
                                    );
                    }                    
                    else
                    {
                        if ( fConnectionState )
						{
							host_disconnect_session( ); // ESC = disconnect session
						}
                    }
                }
            }
        }

        //
        // WX host
        //      
        if ( fConnectionState )
        {
            if ( FD_ISSET( nFd, &hErrfd ))
            {
                fConnectionState = false;
                fprintf( stdout, "{\"disconnected\":%d}\n", nPeer );
            }
            else
            {
                if ( host_port_ping_norsp( ) < 0 )
                {
                    fprintf( stdout, "Failed to issue cc1310 ping\n" );
                    return -errno;
                }
            }
        }
        else
        {  
            if ( FD_ISSET ( nFd, &hWrite ))
            {
                if ( host_get_peer( &nPeer ) == 0 )
                {
                    fprintf( stdout, "{\"connected\":%d}\n", nPeer );
                    fConnectionState = true;
                }
            }
            else
            {
                if ( host_port_ping_norsp( ) < 0 )
                {
                    fprintf( stdout, "Failed to issue cc1310 ping\n" );
                    return -errno;
                }
            }
        }

        //
        // RX from host
        //        
        if ( FD_ISSET ( nFd, &hFds ))
        {
            char szBuffer[ RX_BUFFER ];
			int  nRead = RX_BUFFER;
            int  nErr;

            // Read the data and output it
            nErr = host_port_read ( &nRead, (uint8_t*) szBuffer );

            if ( nErr > 0 )
            {
                if ( fConnectionState == false )
                {
                    if ( host_get_peer( &nPeer ) == 0 )
                    {
                        fprintf( stdout, "{\"connected\":%d}\n", nPeer );
                        fConnectionState = true;
                    }
                }
                szBuffer[ nRead ] = 0;
                fprintf( stdout, szBuffer );
            }
            else
            {
                if ( errno != -ECONNRESET )
				{
					fprintf( stdout, "\n#read failed (-%d)#\n", errno );
				}
            }
        }
        else
        {
            if ( host_port_ping_norsp( ) < 0 )
            {
                fprintf( stdout, "Failed to issue cc1310 ping\n" );
                return -errno;
            }
        }
    }

    //
    // Close host connection
    //
    host_port_close ( );

    //
    // Backup command line history for next time
    //
    cmdline_history_close( );

    //
    // Restore tty settings
    //
    tcsetattr( 0, TCSANOW, &hOldSettings );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   handle_char
//
// Handles a non-escaped character press
//
// ARGUMENTS:
//  (IN)        char        Character from keyboard
//  (IN/OUT)    char*       Buffer with current tx line
//  (IN/OUT)    int*        Current length of tx line buffer
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void handle_char( char nChar, char * szLineBuffer, int * pLineLength )
{
    //
    // Ignore these chars
    //
    if (( nChar == '\r' ) || ( nChar == '\0' ))
	{
		return;
	}

    //
    //  Handle backspace / delete characters
    //
    if (( nChar == 8 ) || ( nChar == 127 ))
    {
        if ( *pLineLength > 0 )
        {
            // Do a delete on the command line
            fprintf( stdout, "\177" );
            *pLineLength = *pLineLength - 1;
        }
        return;
    }

    // Non printable char except \n
    if (( nChar != '\n' ) && (( nChar > '~' ) || ( nChar < ' ' )))
	{
		return;
	}

    fprintf( stdout, "%c", nChar );

    // Append char to line to send
    if ( *pLineLength < CONSOLE_LINE_BUFFER )
    {
        szLineBuffer[ *pLineLength ] = nChar;
        *pLineLength = *pLineLength + 1;
    }

    // Buffer full, or end of line.
    if (( nChar == '\n' ) || ( *pLineLength == CONSOLE_LINE_BUFFER ))
    {
#if 0  // Enable to see raw output send to qube
        int i;
       
        fprintf( stdout, "Sending:\n" );
        for( i = 0; i < *pLineLength; i ++ )
            fprintf( stdout, "[%2X]", szLineBuffer[i] );
        fprintf( stdout, "\n" );
#endif
        // Append to history buffer
        // Reset up/down key index
        szLineBuffer[ *pLineLength ] = 0;
        cmdline_history_append( szLineBuffer );
        sg_nHistoryIndex = -1;

        // Write to host
        if ( host_port_write ( *pLineLength, (uint8_t*) szLineBuffer ) != 0 )
        {
            if ( errno != ECONNRESET )
			{
				fprintf( stdout, "\n#write failed (-%d)#\n", errno );
			}
            else
			{
				fprintf( stdout, "\n#not connected#\n" );
			}
        }
        *pLineLength = 0;
    }
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   handle_escape_char
//
// Handle escape character sequences
//
// ARGUMENTS:
//  (IN)        char*       Escape sequence buffer
//  (IN/OUT)    char*       Buffer with current tx line
//  (IN/OUT)    int*        Current length of tx line buffer
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void handle_escape_char( char * pChar,
                         char * szLineBuffer,
                         int  * pLineLength )
{
    int i;
    //
    // Test escape sequence type
    //
    switch ( pChar[0] )
    {
    case '[':
        //
        // Up arrow 
        //
        if ( pChar[ 1 ] == 'A' ) // up arrow
        {
            // Set char to 0, so it isn't added to the string
            pChar[ 0 ] = '\0';

            //
            // Check that we have a history counter and we're not
            // at the end of the history list
            //
            if ( cmdline_history_size( ) == 0 )
			{
				break;
			}
            if ( sg_nHistoryIndex >= ( cmdline_history_size( ) - 1 ) )
			{
				break;
			}

            //
            // Clear line
            //
            print_clear_line( );
            *pLineLength = 0;
            szLineBuffer[0] = 0;

            //
            // Increment history index
            //
            sg_nHistoryIndex++;

            //
            // Copy history index to current line buffer and display
            //
            *pLineLength = sprintf( szLineBuffer, "%s", cmdline_history_get( sg_nHistoryIndex ));
            if ( *pLineLength )
			{
				fprintf( stdout, szLineBuffer );
			}
            break;
        }
        if ( pChar[1] == 'B' ) // down arrow
        {
            // Set char to 0, so it isn't added to the string
            pChar[ 0 ] = '\0';

            //
            // Check that we have a history counter we can decriment
            //
            if ( sg_nHistoryIndex == -1 )
			{
				break;
			}

            //
            // Clear line
            //
            print_clear_line( );
            *pLineLength = 0;
            szLineBuffer[0] = 0;

            //
            // Decrement history index
            //
            sg_nHistoryIndex--;
            if ( sg_nHistoryIndex != -1 )
            {
                //
                // Copy history index to current line buffer and display
                //
                *pLineLength = sprintf( szLineBuffer, "%s", cmdline_history_get( sg_nHistoryIndex ));
                if ( *pLineLength )
				{
					fprintf( stdout, "%s", szLineBuffer );
				}
            }
            break;
        }
    
        //
        // Right arrow, treat as space
        //
        if ( pChar[1] == 'C' ) // right arrow
        {
            pChar[0] = ' ';
            break;
        }

        //
        // Left arrow, treat as backspace
        //
        if ( pChar[1] == 'D' ) // left arrow
        {
            pChar[0] = '\177';
            break;
        }

        // Null, ignore
        pChar[0] = '\0';
        break;

    default:
        // Null, ignored
        pChar[0] = '\0';
        break;
    }
    if ( pChar[0] == '\0' )
	{
		return;
	}

    handle_char ( pChar[0], szLineBuffer, pLineLength );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   print_usage
//
// Print command line usage
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void print_usage( void )
{
    printf("usage:\n");
    printf("  iceqube_terminal_arm.elf [-p=X][-i=X][-l0=X [-l1=X [-l2=X]]][-s0=X [-s1=X [-s2=X]]]\n");
    printf("\t-p=[0-2,255]    cc1310 crisp module extension slot \n");
    printf("\t                0-2, or 255 for auto (default 255)\n");
    printf("\t-r=[0-6,-1]     Multihub rank (0=default, -1=random)\n");
    printf("\t-i=[868/915]    radio band 868Mhz or 915Mhz\n");
    printf("\t-l0=[0-max]     listen channel 0 (default 0)\n" );
    printf("\t-l1=[0-max,255] listen channel 1 (default 6)\n" );
    printf("\t-l2=[0-max,255] listen channel 2 (default 12)\n" );
    printf("\t-s0=[0-max]     session channel 0 (default 18)\n" );
    printf("\t-s1=[0-max,255] session channel 1 (default 255)\n" );
    printf("\t-s2=[0-max,255] session channel 2 (default 255)\n" );
    printf("\t-rf=[-50000-50000, or file] frequency offset, or \n" );
    printf("\t                file containing offset (default %s). \n\n", RF_OFFSET_FILE );

    printf("\tsession channels must be separated from the listen channels\n" );
    printf("\tby 2. Each listen channels uses channel + 1 for host responses.\n\n");
    printf("\tto disable a channel, omit unused channels or set -l or -s\n" );
    printf("\tvalue to 255; channel 0 cannot be unused. The max value depends\n");
    printf("\ton the radio band:\n");
    printf("\t- For 868Mhz the max channel number is %d\n", MAX_CHANNEL( 1 ));
    printf("\t- for 915Mhz the max channel number is %d\n\n", MAX_CHANNEL( 2 ));
    fflush( stdout );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   print_clear_line
//
// Clears current console line
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void print_clear_line( void )
{
    char szEsc[5];

    // Erase line
    szEsc[0] = '\e';
    szEsc[1] = '[';
    szEsc[2] = '2';
    szEsc[3] = 'K';
    szEsc[4] = 0;
    fprintf( stdout, szEsc );

    // Cursor to start
    szEsc[0] = '\e';
    szEsc[1] = '[';
    szEsc[2] = '1';
    szEsc[3] = 'G';
    szEsc[4] = 0;
    fprintf( stdout, szEsc );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   parse_int_param
//
// Parses an integer paramter from the command line arguments
//
// ARGUMENTS:
//  (IN)     const char*     Command line paramter to search for (e.g."-r=")
//  (IN/OUT) int*            Interpreted value
//  (IN)     int             Command line paramter count
//  (IN)     char**          Command line paramters
//
// RETURNS:
//           bool            true if parameter was found and interpreted
//
///////////////////////////////////////////////////////////////////////////////
bool parse_int_param( const char* szParam, int *pVal, int nArgv, char **pArgc )
{
    int i;
    int nParamLen = strlen( szParam );

    if (( szParam == NULL ) || ( pVal == NULL ))
        return false;

    for( i = 0; i < nArgv; i ++ )
    {
        char *szCurParam = pArgc[ i ];
        int   nCurParamLen;

        if (( szCurParam == NULL ) || ( szCurParam[0] != '-' ))
            continue;
    
        nCurParamLen = strlen( szCurParam );

        if ( nCurParamLen < nParamLen )
            continue;

        if ( memcmp( szParam, szCurParam, nParamLen ) == 0 )
        {
            char* szEquals = strchr( szCurParam, '=' );
            if ( szEquals )
            {
                szEquals++;
                *pVal = atoi( szEquals );
                return true;
            }
        }
    }
    return false;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   parse_param_set
//
// Checks if a command line paramter is set
//
// ARGUMENTS:
//  (IN)     const char*     Command line paramter to search for (e.g."-?")
//  (IN)     int             Command line paramter count
//  (IN)     char**          Command line paramters
//
// RETURNS:
//           bool            true if parameter was found
//
///////////////////////////////////////////////////////////////////////////////
bool parse_param_set( const char* szParam, int nArgv, char **pArgc )
{
    int i;
    int nParamLen = strlen( szParam );

    if ( szParam == NULL )
        return false;

    for( i = 0; i < nArgv; i ++ )
    {
        char *szCurParam = pArgc[ i ];
        int   nCurParamLen;

        if (( szCurParam == NULL ) || ( szCurParam[0] != '-' ))
            continue;
    
        nCurParamLen = strlen( szCurParam );

        if ( nCurParamLen < nParamLen )
            continue;

        if ( memcmp( szParam, szCurParam, nParamLen ) == 0 )
            return true;
    }
    return false;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   parse_frequency_param
//
// Parses a paramter from the command line arguments, looking for frequency.
// This can either be a numeric value between -50000 and +50000, or a file
// name containing the value.
//
// ARGUMENTS:
//  (IN)     const char*     Command line paramter to search for (e.g."-r=")
//  (IN/OUT) int*            Frequency value
//  (IN)     int             Command line paramter count
//  (IN)     char**          Command line paramters
//
// RETURNS:
//           bool            true if parameter was found and interpreted
//
///////////////////////////////////////////////////////////////////////////////
bool parse_frequency_param( 
            const char* szParam, 
            int   *pVal,
            int    nArgv, 
            char **pArgc            
            )
{
    int  i;
    char szStringParam[ MAX_PATH ];
    int  nParamLen = strlen( szParam );
    bool fFound = false;
    bool fIsDigit = true;

    if ( szParam == NULL )
        return false;

    if ( pVal == NULL )
        return false;

    for( i = 0; i < nArgv; i ++ )
    {
        char *szCurParam = pArgc[ i ];
        int   nCurParamLen;

        if (( szCurParam == NULL ) || ( szCurParam[0] != '-' ))
            continue;
    
        nCurParamLen = strlen( szCurParam );

        if ( nCurParamLen < nParamLen )
            continue;

        if ( memcmp( szParam, szCurParam, nParamLen ) == 0 )
        {
            char* szEquals = strchr( szCurParam, '=' );
            if ( szEquals )
            {
                szEquals++;
                nCurParamLen = strlen( szEquals );
                if ( nCurParamLen < ( MAX_PATH - 2 ))            
                {
                    strcpy( szStringParam, szEquals );
                }
                else
                {
                    memcpy ( szStringParam, szEquals, MAX_PATH - 2 );
                    szStringParam[ MAX_PATH - 1 ] = 0;
                }
                fFound = true;
                break;
            }
        }
    }

    // Not found, look for default file    
    if ( fFound == false )
    {
        strcpy( szStringParam, RF_OFFSET_FILE );
        fFound = true;
    }

    //
    // OK, got parameter
    //
    if ( fFound )
    {
        nParamLen = strlen( szStringParam );

        //
        // See if it's a number
        //
        for( i = 0; i < nParamLen; i ++ )
        {
            if ((( szStringParam[i] >= '0' ) &&
                 ( szStringParam[i] <= '9' )) ||
                 ( szStringParam[i] == '-' ))
                continue;
            fIsDigit = false;
            break;
        }

        if ( fIsDigit )
        {
            *pVal = atoi( szStringParam );

            // cap
            if ( *pVal < -50000 )
                *pVal = -50000;
            if ( *pVal > 50000 )
                *pVal = 50000;
            return true;
        }   
    }

    //
    // OK, but it's a filename
    //
    if ( fFound )
    {
        FILE *pFile;
        pFile = fopen( szStringParam, "rt" );
        if ( pFile == NULL )
        {
            return false;
        }
        if ( fscanf( pFile,"%d", pVal ) <= 0 )
        {
            fclose( pFile );
            return false;
        }
        fclose( pFile );
        return true;
    }
    return false;
}

//////////////////////////////////// EOF //////////////////////////////////////

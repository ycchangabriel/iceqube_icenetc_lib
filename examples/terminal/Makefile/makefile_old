##FILE HEADER##################################################################
## 
## FILE NAME:   makefile
##
## PROJECT:     iceqube_terminal
##
## Main makefile for iceqube_terminal
## 
#############################################
##
## CONTRIBUTORS:
##      OL      Oliver Lewis   
##
#############################################
##
## Copyright (c) 2016-2017 Peacock Tehnology Limited or its suppliers
## All rights reserved.
## 
## This software is protected by national and international copyright and
## other laws. Unauthorised use, storage, reproduction, transmission
## and/or distribution of this software, or any part of it, may result in
## civil and/or criminal proceedings.
## 
## This software is confidential and should not be disclosed, in whole or
## in part, to any person without the prior written permission of
## Peacock Tehnology Limited.
## 
###############################################################################

#
# Include environment and compiler settings
#

#
# Define variables
#
PROJECT_HW_VER	    := arm

# Directory to place executable files
OUT_DIR = bin/arm

# Diretory to place object files
OBJ_DIR = obj_$(PROJECT_HW_VER)

# Application source files
PROJECT		         := iceqube_terminal_$(PROJECT_HW_VER)
PROJECT_DEFINES	     := LINUX
PROJECT_SOURCE_DIR   := source
PROJECT_SOURCE_FILES := $(notdir $(wildcard $(PROJECT_SOURCE_DIR)/*.c)) 
PROJECT_INCLUDE_DIR  := include

ifeq "$(RM)" ""
RM:=/bin/rm
endif

# STDLIB Directories & libs
USERLIBRARIES := 

# Source directories
SOURCE_FILES := \
  $(PROJECT_SOURCE_FILES)

# Include directories
INCLUDE_PATHS := \
  $(PROJECT_INCLUDE_DIR) \
  
INCLUDES = $(addprefix -I, $(INCLUDE_PATHS))

#Paths where source files are located
VPATH =  $(PROJECT_SOURCE_DIR) 

LIBRARY_DIRS := \
  $(STDLIB_DIR)
    
LIBRARIES    := $(addprefix -l, $(USERLIBRARIES))

# Include Version Information
-include ./make/Version
ifeq "$(BUILD_VERSION)" ""
BUILD_VERSION=0.000
endif

# Options for compiler and linker
USERDEFINES   = $(addprefix -D, $(PROJECT_DEFINES)) -DBUILD_VERSION=\"$(BUILD_VERSION)\"

# List of object files made from the list of source file 
OBJECTFILES  = $(patsubst %.c,$(OBJ_DIR)/%.o, $(SOURCE_FILES))

.PHONY : clean all rebuild

rebuild: clean all

clean:
	@echo Cleaning Project Files
	@-$(RM) $(OUT_DIR)/$(PROJECT).map
	@-$(RM) $(OUT_DIR)/$(PROJECT).elf
	@-$(RM) $(OUT_DIR)/$(PROJECT).dump
	@-$(RM) $(OBJ_DIR)/*.o
	@echo .done

all: $(OBJECTFILES) $(PROJECT).elf 

# Rule for creating object directory
$(OBJ_DIR):
	@mkdir $@
	
# Rule for creating output directory
$(OUT_DIR):
	@-mkdir bin
	@mkdir $@

# If any $(OBJECTFILES) must be built then $(OBJ_DIR) must be built first, 
# but if $(OBJ_DIR) is out of date (or doesn't exist), 
# that does not force $(OBJECTFILES) to be built. 
$(OBJECTFILES): | $(OBJ_DIR)

# If any output files must be built then $(OUT_DIR) must be built first, 
# but if $(OUT_DIR) is out of date (or doesn't exist), 
# that does not force output files to be built. 
$(PROJECT).elf : | $(OUT_DIR)

# Rule for building object files
$(OBJ_DIR)/%.o : %.c
	@echo $<
	@$(CC) -D_REENTRANT $(CCFLAGS) $(INCLUDES) $(USERDEFINES) -c $< -o $@
	
#
# Build elf file	
# 
$(PROJECT).elf :
	@echo $(PROJECT).elf
	@$(CC) $(OBJECTFILES) $(LDFLAGS) $(LIBRARIES) -o $(OUT_DIR)/$(PROJECT).elf

#
# Install
#
TARGET_LIB_PATH=/usr/lib
TARGET_APP_PATH=/home/root
TARGET_INITD=/etc/init.d

install:
	@mkdir -p $(DESTDIR)$(TARGET_APP_PATH)
	@cp  $(OUT_DIR)/$(PROJECT).elf $(DESTDIR)$(TARGET_APP_PATH)
	@chmod 755 $(DESTDIR)$(TARGET_APP_PATH)/$(PROJECT).elf

uninstall:
	@-rm -f $(DESTDIR)$(TARGET_APP_PATH)/$(PROJECT).elf

############################### EOF ###########################################

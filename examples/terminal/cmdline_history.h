// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   cmdline_history.h
//
// PROJECT:     crisp host
//
// PLATFORM:    Crispbread/arm/linux
//
// Wrapper for managing command line history (normally accessible through
// up/down keys)
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CMDLINE_HISTORY_H__
#define __CMDLINE_HISTORY_H__

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>

/////////////////////////////////////////////
// DEFINES
//

//
// Max history buffer size
//
#define CMDLINE_HISTORY_MAX         100

/////////////////////////////////////////////
// DATA TYPES
//

/////////////////////////////////////////////
// DATA DECLARATIONS
//

/////////////////////////////////////////////
// PROTOTYPES
//

//
// Init
//
void cmdline_history_init( void );
void cmdline_history_close( void );

//
// Append to history
//
void cmdline_history_append( const char* szHistory );

//
// Access history
//
uint8_t     cmdline_history_size( void );
const char* cmdline_history_get( uint8_t nIndex );

/////////////////////////////////////////////
// CLASSES
//

#endif // !__CMDLINE_HISTORY_H__

//////////////////////////////////// EOF //////////////////////////////////////

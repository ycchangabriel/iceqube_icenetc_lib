//FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   RS232Socket.h
//
// PROJECT:     icehub 
//
// PLATFORM:    crisp linux/arm/gcc
//
// Rs-232 I/O wrapper class
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2018-2019 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __RS232SOCKET_H__
#define __RS232SOCKET_H__

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include "bridge_cmds.h"

/////////////////////////////////////////////
// DEFINES
//

//
// Baud rate
//
#define RS232SOCKET_BAUD_1200		1200 
#define RS232SOCKET_BAUD_2400		2400 
#define RS232SOCKET_BAUD_9600		9600 
#define RS232SOCKET_BAUD_19200		19200 
#define RS232SOCKET_BAUD_38400		38400 
#define RS232SOCKET_BAUD_56000		56000 
#define RS232SOCKET_BAUD_115200		115200
#define RS232SOCKET_BAUD_230400		230400
#define RS232SOCKET_BAUD_460800		460800
#define RS232SOCKET_BAUD_500000		500000
#define RS232SOCKET_BAUD_576000		576000
#define RS232SOCKET_BAUD_921600		921600
#define RS232SOCKET_BAUD_1000000	1000000
#define RS232SOCKET_BAUD_1152000	1152000
#define RS232SOCKET_BAUD_1500000	1500000
#define RS232SOCKET_BAUD_2000000	2000000
#define RS232SOCKET_BAUD_2500000	2500000
#define RS232SOCKET_BAUD_3000000	3000000
#define RS232SOCKET_BAUD_3500000	3500000
#define RS232SOCKET_BAUD_4000000	4000000

//
// Parity
//
#define RS232SOCKET_PARITY_NONE		0x00
#define RS232SOCKET_PARITY_ODD		0x01
#define RS232SOCKET_PARITY_EVEN		0x02

//
// Data bits
// 
#define RS232SOCKET_DATABITS_5		5	 
#define RS232SOCKET_DATABITS_6		6 
#define RS232SOCKET_DATABITS_7		7 
#define RS232SOCKET_DATABITS_8		8

//
// Stop bits
//
#define RS232SOCKET_STOPBITS_1		0x01
#define	RS232SOCKET_STOPBITS_2		0x02


#define TIMEOUT_INFINITE            0xFFFFFFFF

/////////////////////////////////////////////
// DATA TYPES 
//

/////////////////////////////////////////////
// DATA DECLARATIONS
//

/////////////////////////////////////////////
// PROTOTYPES
//

/////////////////////////////////////////////
// CLASSES
//

class Rs232Socket 
{
public:
	Rs232Socket( );
	~Rs232Socket( );

    //
    // Creation
    //
	int Create(
			const char* szUart,
		    uint32_t    nBaudRate,
			uint8_t     nParity,
		    uint8_t     nDataBits,
			uint8_t     nStopbits
		    );
	void Close( );

	//
	// I/O functions
	//
	int Read( uint8_t *pData, uint32_t &nDataLen, uint32_t nTimeout );
	int Write( uint8_t *pData, uint32_t &nDataLen, uint32_t nTimeout );

    int WritePacket( uint8_t nCmd, uint16_t nLen, void *pData );
    int WriteError( int nErr );

    int ReadLine( uint8_t *pData, uint32_t &nDataLen, uint32_t nTimeout, char* nTerminator = 0 );

    //
    // Direct file descriptor access
    //
    int GetFd( ) { return m_nSocket; };

    //
    // Enable I/O debugging
    //
    void DebugEn( bool fDebug ) { m_fDebug = fDebug; };

protected:

    //
    // Protected member data
    //
    int             m_nSocket;
    bool            m_fDebug;
};

#endif // !__RS232SOCKET_H__
//////////////////////////////////// EOF //////////////////////////////////////

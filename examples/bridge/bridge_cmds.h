// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   bridge_cmds.h
//
// PROJECT:     icehub
//
// PLATFORM:    crisp linux/arm/gcc
//
// Defines bridge wrapper protocol and commands
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2018-2019 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __BRIDGE_CMDS_H__
#define __BRIDGE_CMDS_H__

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>

/////////////////////////////////////////////
// DEFINES
//

#define PCKT_HEADER         ':'

// HUB CONTROL
#define CMD_PING            0x01 
#define CMD_CONNECTED       0x02
#define CMD_DISCONNECTED    0x03
#define CMD_SET_CONFIG      0x04
#define CMD_GET_CONFIG      0x05

// READER CONTROL
#define CMD_DISCONNECT      0x10 
#define CMD_EXCHANGE        0x11
#define CMD_GET_LAST_RSSI   0x12
#define CMD_EN_CONNECT      0x13

// TRIGGER CONTROL
#define CMD_TRIGGER_EN      0x20
#define CMD_TRIGGER_GET_CFG 0x21
#define CMD_TRIGGER_SET_CFG 0x22
#define CMD_TRIGGER_GET_VER 0x23

// SYSTEM
#define CMD_SYSTEM          0x30

// Error Rsp
#define CMD_ERR             0xFF

/////////////////////////////////////////////
// DATA TYPES
//

//
// Bridge packet, data appended
//
struct bridge_packet
{
    uint8_t     nHeader;  // ':'
    uint8_t     nCmd;     // ID, see above
    uint16_t    nDataLen; // Length of appended data
}__attribute__(( packed ));

//
// Hub config packet
//
struct hub_cfg
{
    uint8_t nRegulatory;    // 1 = EU_2000_299_EC1, 2 = FCC
    uint8_t nListenChannels[ 3 ];
    uint8_t nSessionChannels[ 3 ];
} __attribute__(( packed ));

//
// Trigger config packet
//
struct bridge_trigger_cfg
{
    uint8_t    nMode;  // 1 = 2-pulse, 
                       // 2 = 2-long-pulse, 
                       // 3 =tripple pulse, 
                       // 4=field
    uint16_t   nDelay; // intrapulse delay 32ms to 1500ms
    uint8_t    nPower; // 1-12 
}__attribute__(( packed ));

#if 0
//
// Host config packet
//
struct host_cfg
{
    uint8_t    nRegulatory;  // 0 = EU, 1 = US
    uint8_t    nListenChannels[ 3 ]; 
    uint8_t    nSessionChannels[ 3 ]; 
}__attribute__(( packed ));
#endif

/////////////////////////////////////////////
// DATA DECLARATIONS
//

/////////////////////////////////////////////
// PROTOTYPES
//

/////////////////////////////////////////////
// CLASSES
//

#endif // !__BRIDGE_CMDS_H__

//////////////////////////////////// EOF //////////////////////////////////////

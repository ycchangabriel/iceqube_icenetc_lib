// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   Trigger.h
//
// PROJECT:     icehub
//
// PLATFORM:    crisp linux/arm/gcc
//
// Defines interface to trigger module
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2018-2019 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __TRIGGER_H__
#define __TRIGGER_H__

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>

/////////////////////////////////////////////
// DEFINES
//

/////////////////////////////////////////////
// DATA TYPES
//

/////////////////////////////////////////////
// DATA DECLARATIONS
//

/////////////////////////////////////////////
// PROTOTYPES
//

/////////////////////////////////////////////
// CLASSES
//

//
// Class Trigger - wraps trigger board control
//
class Trigger
{
public:
    //
    // Constructor/destructor
    //
    Trigger( );
    virtual ~Trigger( );

    //
    // Initialization
    //
    bool Open( char *szPath );
    void Close( );

    // 
    // Control
    //
    bool IsAvailable( );
    int  Enable( bool fEn );

    //
    // Configuration
    //
    int  GetVersion( uint8_t &nVersion);
    int  SetConfig( uint8_t nMode, uint16_t nDelay, uint8_t nPower );    
    int  GetConfig( uint8_t &nMode, uint16_t &nDelay, uint8_t &nPower );  

protected:
    //
    // Protected members
    //
    int     m_nFd;  
    uint8_t m_nVersion;
    bool    m_fEn;
};

#endif // !__TRIGGER_H__

//////////////////////////////////// EOF //////////////////////////////////////

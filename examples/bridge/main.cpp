// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   main.c
//
// PROJECT:     icehub
//
// PLATFORM:    crisp linux/arm/gcc
//
// Bridge application for remote control of cc1310 radio module and trigger 
// module through a UART.
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2018-2019 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <unistd.h> 
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <setjmp.h>

#include "host_comms.h"
#include "host_port.h"
#include "iceqube_icenetc.h"
#include "Rs232Socket.h"
#include "Trigger.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

#define STDIN       0

//
// Debug output
//
#define qprintf( ... )  { \
                            if ( sg_fService == false ) \
                                fprintf( stdout, __VA_ARGS__ ); \
                            else \
                                syslog( LOG_INFO, __VA_ARGS__ ); \
                        }

// #define DEBUG_CMD  

//
// Default paths
//
#define DEFAULT_BRIDGE_UART         "/dev/ttyUSB0" //"/dev/crisp_uart_0"
#define DEFAULT_TRIGGER             "/dev/crisp_trigger_0"

#define RF_OFFSET_FILE      "/etc/rf_offset"

#ifndef MAX_PATH
#define MAX_PATH        255
#endif
/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

//
// Internal structre used by icenetc_lib.
// Redeclared here as we override the wait for connect call 
//
struct icec_net_protocol
{
    uint8_t  nCmd;      // Command
    uint16_t nLength;   // Length of data following this structure
} __attribute__(( packed ));

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

//
// Output usage
//
void print_usage( void );
int  kbhit( void );

//
// SIG_STOP handler
//
void StopSigHandler( 
                int nSignal, 
                siginfo_t *pSig, 
                void *pSecret 
                );

//
// Wait for peer connection, but also service
// RS232 commands in the mean time
//
int host_comms_wait_for_connect( 
                uint32_t nTimeout, 
                uint32_t *pPeer,
                Rs232Socket *pRs232, 
                Trigger *pTrigger,
                bool *pEnConnect
                );

//
// Service Bridge Commands (these are commands
// that are always available, whether connected
// to an iceqube or not)
//
void service_unconnected_commands( 
                Rs232Socket *pRs232,
                uint32_t     nPeer,
                struct bridge_packet *pPckt, 
                uint8_t *pData, 
                Trigger *pTrigger ,
                bool *pEnConnect
                );

//
// Exchange bridge and host data when connected
// to an iceqube
//
void service_host_bridge_session( 
                uint32_t nPeer, 
                Rs232Socket* pBridge,
                Trigger *pTrigger ,
                bool *pEnConnect
                );

//
// Parse command line
//
int parse_command_line( 
                int     nArgc,
                char    **pArgc,
                uint8_t &nCrispSlot,
                uint8_t &nRank,
                uint8_t &nListenChannels,
                uint8_t *hListenChannels,
                uint8_t &nSessionChannels,
                uint8_t *hSessionChannels,
                char    *szBridgeUART,
                char    *szTrigger,
                int32_t &nFrequencyOffset
                );

bool parse_int_param( 
                const char* szParam, 
                int *pVal, 
                int nArgv, 
                char **pArgc 
                );

bool parse_param_set( 
                const char* szParam, 
                int nArgv, 
                char **pArgc 
                );

bool parse_string_param( 
                const char* szParam, 
                char  *szBuffer,
                int    nMaxLen,
                int    nArgv, 
                char **pArgc            
                );

bool parse_frequency_param( 
                const char* szParam, 
                int   *pVal,
                int    nArgv, 
                char **pArgc            
                );

//
// Update radio settings
//
int update_hub_radio_comms( 
                hub_cfg *pNewCfg 
                );

/////////////////////////////////////////////
// PUBLIC DATA
//

bool sg_fService = false;
bool sg_fQuit    = false;

/////////////////////////////////////////////
// PRIVATE DATA
//

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   main
//
// Application entry point. 
//
// ARGUMENTS:
//  (IN)        int         Argument count
//  (IN)        char**      List of arguments
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int main( int nArgc, char **pArgc )
{
    //
    // Application parameters (set by command line)
    //
    uint8_t        nCrispSlot;
    uint8_t        nListenChannels;
    uint8_t        hListenChannels[ 3 ];
    uint8_t        nSessionChannels;
    uint8_t        hSessionChannels[ 3 ];
    uint8_t        nRank  = 0;
    uint8_t        nRegulatory = 0;
    int32_t        nFrequencyOffset = 0; 
    char           szBridgeUART[ 50 ];
    char           szTrigger[ 50 ];
    int            n;
    bool           fEnConnect = false;

    //
    // Input (keyboard) handling
    //
    struct termios hOldSettings;        
    struct termios hNewTty; 

    //
    // SIG_STOP handler
    //
    struct sigaction hSigAction;

    //
    // Rs232 bridge conncetion
    //
    Rs232Socket      hBridge;

    //
    // Trigger control class
    //
    Trigger          hTrigger;
    Trigger         *pTrigger = NULL;

    //
    // Check if we're running as a service first
    //
    if ( parse_param_set( "--", nArgc, pArgc ))
    {
        sg_fService = true;
    }

    //
    // Trap stop signal handler (for a clean exit)
    //
    hSigAction.sa_sigaction = StopSigHandler;
    sigemptyset( &hSigAction.sa_mask );
    hSigAction.sa_flags = SA_SIGINFO;
    sigaction( SIGTERM, &hSigAction, NULL );

    //
    // Set-up terminal for a nice
    //
    if( sg_fService == false )
    {
        //
        // Disable stdout buffering
        //
        setbuf( stdout, NULL );
    }
    else
    {
        //
        // Output to sys log instead of stdout
        //
        openlog( "iceqube_icenetc_lib", LOG_NDELAY, LOG_USER );
    }

    //
    // Print header
    //
    qprintf( "iceqube_icenetc_lib example fn: BRIDGE\n" );
    qprintf( 
        "iceqube_icenetc_lib version %d.%03d\n", 
        ICEQUBE_ICENETC_VERSION_MAJ,
        ICEQUBE_ICENETC_VERSION_MIN
        );
    if( sg_fService == false )
        qprintf( "  press any key to exit\n" );

    //
    // Parse command line paramters
    //
    if ( parse_command_line( 
                    nArgc,
                    pArgc,
                    nCrispSlot,
                    nRank,
                    nListenChannels,
                    hListenChannels,
                    nSessionChannels,
                    hSessionChannels,
                    szBridgeUART,
                    szTrigger,
                    nFrequencyOffset
                    ) != 0 )
    {
        print_usage( );
        return -1;
    }

    //
    // Open cc1310
    //
    if ( host_comms_connect(            
             nCrispSlot,
             &nRegulatory,
             nRank,
             nListenChannels,
             hListenChannels,
             nSessionChannels,
             hSessionChannels,
             nFrequencyOffset             
             ) != 0 )
    {
        if ( nRegulatory != 0 )
        {
    	    uint8_t i;
            for( i = 0; i < 3; i ++ )
            {
                if (( hListenChannels[ i ] != 0xFF ) &&
                    ( hListenChannels[ i ] > MAX_CHANNEL( nRegulatory ))) 
                {
                    qprintf(                     
                        "Invalid listen channel (-l%d=%u). Must be 0-%u\n", 
                        i, 
                        hListenChannels[ i ], 
                        MAX_CHANNEL( nRegulatory ) 
                        );
                    print_usage( );
                    return -1;
                }
                if (( hSessionChannels[ i ] != 0xFF ) &&
                    ( hSessionChannels[ i ] > MAX_CHANNEL( nRegulatory )))
                {
                    qprintf( 
                        "Invalid session channel (-s%d=%u). Must be 0-%u\n", 
                        i, 
                        hSessionChannels[ i ], 
                        MAX_CHANNEL( nRegulatory ) 
                        );
                    print_usage( );
                    return -1;
                }
            }
        }
        qprintf( "Failed to open cc1310 host\n" );
        return -1;
    }

    //
    // Open Bridge UART
    //
    n = hBridge.Create(
			szBridgeUART,
		    RS232SOCKET_BAUD_115200,
			RS232SOCKET_PARITY_NONE,
		    RS232SOCKET_DATABITS_8,
			RS232SOCKET_STOPBITS_1
		    );

    if ( n != 0 )
    {
        qprintf( "Failed to open RS232 port %s (%d)\n", szBridgeUART, n );
        return -1;
    }

    //
    // Open Trigger
    //
    if ( strcmp( szTrigger, "none" ) == 0 )
        pTrigger = NULL;
    else
    {
        if ( hTrigger.Open( szTrigger ) != true )
        {
            qprintf( "Failed to open trigger (%s)\n", szTrigger );
            return -1;
        }       
        pTrigger = &hTrigger;
    }

    //
    // Output Connection Details
    //
    qprintf(
        "Connected to host\n"
        );
    qprintf( 
        "Regulatory : %s\n", 
        ( nRegulatory == REGULATORY_EU_2000_299_EC1 ) ? "eu_2000/299/ec1" : "fcc"
        );

    if ( nListenChannels < 2 )
    {
        qprintf ( 
            "Listen channels : %d\n", 
            hListenChannels[0] 
            );   
    }
    else 
    {
        if ( nListenChannels < 3 )
        {
            qprintf ( 
                "Listen channels : %d, %d\n", 
                hListenChannels[0], 
                hListenChannels[1] 
                );   
        }
        else
        {
            qprintf ( 
                "Listen channels : %d, %d, %d\n", 
                hListenChannels[0], 
                hListenChannels[1], 
                hListenChannels[2] 
                );
        }
    }

    if ( nSessionChannels < 2 )
    {
        qprintf ( 
            "Session channels : %d\n", 
            hSessionChannels[0] 
            ); 
    }
    else
    {
        if ( nSessionChannels < 3 )
        {
            qprintf ( 
                "Session channels : %d, %d\n", 
                hSessionChannels[0], 
                hSessionChannels[1] 
                );
        }
        else
        {
            qprintf ( 
                "Session channels : %d, %d, %d\n", 
                hSessionChannels[0], 
                hSessionChannels[1], 
                hSessionChannels[2] 
                );
        }
    }
    qprintf( "Frequency Offset : %d Hz\n", nFrequencyOffset ); 
    qprintf( "Bridge Port : %s\n", szBridgeUART );
    qprintf( "Trigger : %s\n", szTrigger );


    //
    // Configure stdin
    //
    if ( sg_fService == false )
    {
        tcgetattr( STDIN, &hOldSettings );
        hNewTty = hOldSettings;
        hNewTty.c_lflag      &= ~ICANON;
        hNewTty.c_lflag      &= ~ECHO;
        hNewTty.c_lflag      &= ~ISIG;
        hNewTty.c_cc[ VMIN ]  = 0;
        hNewTty.c_cc[ VTIME ] = 0;
        tcsetattr( 0, TCSANOW, &hNewTty );
    }

    //
    // Round robin loop
    //
    while( sg_fQuit == false )
    {
        uint32_t nPeer = 0;

   
        //
        // Look for keyboard hit (only if not running as service)
        //
        if (( sg_fService == false ) && ( kbhit( ) != 0 ))
        {
            sg_fQuit = true;
            continue;
        }      

        //
        // Wait for a device to connect (internally handles some rs232 commands
        // from bridge device)
        //
        if ( host_comms_wait_for_connect( 2000, &nPeer, &hBridge, pTrigger, &fEnConnect ) != 0 )
            continue;

        hBridge.WritePacket( CMD_CONNECTED, sizeof( uint32_t ) , &nPeer );

        //
        // Exchange between bridge and peer (and handle all rs232 commands
        // from bridge)
        //
        service_host_bridge_session( nPeer, &hBridge, pTrigger, &fEnConnect );

        //
        // Disconnect device
        //
        hBridge.WritePacket( CMD_DISCONNECTED, 0 , NULL );

        host_comms_disconnect( );

    }

    //
    // Close trigger
    //
    if ( pTrigger )
        pTrigger->Close( );

    //
    // Close UART
    //
    hBridge.Close( );

    //
    // Close host connection
    //
    host_comms_close( );

    //
    // Restore old stdio settings
    //
    if ( sg_fService == false )
        tcsetattr( 0, TCSANOW, &hOldSettings );
    
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   service_host_bridge_session
//
// Called once a session is established. This function will service incoming
// bridge commands, and will do any exchange with the icequbes. 
//
// ARGUMENTS:
//  (IN)        uint32_t        IceQube ID which is in the session
//  (IN)        Rs232Socketr*   Bridge connection
//  (IN)        Trigger*        Trigger device handler
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void service_host_bridge_session( 
                       uint32_t     nPeer, 
                       Rs232Socket *pBridge, 
                       Trigger     *pTrigger,
                       bool        *pEnConnect
                       )
{
    struct icec_net_protocol      *pRsp;
    int                            nErr;
    int32_t                        nRssi;

    //
    // Loop until the bridge device requests a disconnect.
    // Service all bridge device commands
    //
    while( 1 )
    {
        struct bridge_packet hPckt;
        uint8_t              nData[ 800 ];
        uint32_t             nLen;
        uint32_t             nDummy;

        //
        // Check for connection
        //
        if ( host_get_peer( &nDummy ) != 0 )
            return;

        //
        // Issue keepalive
        //
        nErr = host_port_ping_norsp( );
        if ( nErr < 0 )
            return;

        //
        // Read command from bridge device
        //
        nLen = sizeof( struct bridge_packet );
        if ( pBridge->Read((uint8_t*) &hPckt, nLen, 2000 ) != 0 )
            continue;

        if ( hPckt.nHeader != PCKT_HEADER )
            continue;

        if ( hPckt.nDataLen > 800 )
            return;

        nLen = hPckt.nDataLen;
        if (( nLen != 0 ) && ( pBridge->Read( nData, nLen, 1000 ) != 0 ))
            return; 

#ifdef DEBUG_CMD
        qprintf("Cmd: 0x%X\n", hPckt.nCmd );
#endif

        //
        // Parse command code
        //
        switch( hPckt.nCmd )
        {   
        case CMD_DISCONNECT:
            pBridge->WritePacket( CMD_DISCONNECT, 0, NULL );
            return;

        case CMD_EXCHANGE:
            if ( nLen < sizeof( struct icec_net_protocol ))
            {
                pBridge->WriteError( -EINVAL );
            }
            else
            {
                nErr = host_comms_send_command(
                            (struct icec_net_protocol* )nData, 
                            &pRsp 
                            );

                if ( nErr != 0 )
	            {
                    pBridge->WriteError( nErr );
                    return;  
	            }
                else
                {
                    if ( pRsp )
                    {           
                        pBridge->WritePacket( 
                            CMD_EXCHANGE, 
                            pRsp->nLength + sizeof( struct icec_net_protocol ), 
                            pRsp 
                            ); 
                    }
                }
            }
            break;

        case CMD_GET_LAST_RSSI:                               
            nErr = host_get_last_rssi( &nRssi );

            if ( nErr != 0 )
            {
                pBridge->WriteError( nErr );
                return;
            }
            else
            {
                pBridge->WritePacket( CMD_GET_LAST_RSSI, sizeof( int32_t), &nRssi );
            }
            break;
    
        default:

            //
            // Service all other uart commands, that can be done
            // when not in a connection
            //
            service_unconnected_commands( pBridge, nPeer, &hPckt, nData, pTrigger, pEnConnect );

            if ( *pEnConnect == false )
                return;
            break;
        }
    }
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_comms_wait_for_connect
//
// Waits for a qube to connect, unlike standard library version, this will
// also service bridge commands while waiting for a connection.
//
// ARGUMENTS:
//  (IN)        uint32_t        Timeout in MS, or 0 for infinite
//  (IN/OUT)    uint32_t*       Connected peer ID, if function returns 0
//  (IN)        Rs232Socket*    Bridge UART connection
//  (IN)        Trigger*        Access to trigger device
//
// RETURNS:
//              int             0 if a peer connects, pPeer will be filled
//                              if this is the case.
//
///////////////////////////////////////////////////////////////////////////////
int host_comms_wait_for_connect( 
                uint32_t nTimeout, 
                uint32_t *pPeer,
                Rs232Socket *pRs232,
                Trigger *pTrigger,
                bool *pEnConnect
                )
{
    int             nErr = -1;
    fd_set          hWriteFds;
    fd_set          hReadFds;
    struct timeval  hTimeout;
    int             nFd      = host_port_get_fd( );
    int             nFdRs232 = pRs232->GetFd( );
    int             nMax     = nFd > nFdRs232 ? nFd : nFdRs232;

	if (( nFd == INVALID_FD ) ||( nFdRs232 == INVALID_FD )||( nTimeout == 0 ))
	{
		return -EBADF;
	}

    //
    // Ping host (keep alive)
    //
    nErr = host_port_ping_norsp( );
    if ( nErr < 0 )
        return -errno;


    while ( nTimeout )
    {
        //
        // Work out timeout
        //
        if ( nTimeout > 2000 )
        {
            hTimeout.tv_sec = 2;
            hTimeout.tv_usec = 0;
            nTimeout -= 2000;
        }
        else
        {
            hTimeout.tv_sec = nTimeout / 1000;
            hTimeout.tv_usec = ( nTimeout % 1000 ) % 1000;
            nTimeout = 0;
        }

        //
        // Configure fd_set
        //
        FD_ZERO( &hWriteFds );
        FD_ZERO( &hReadFds );
        FD_SET( nFd, &hWriteFds );
        FD_SET( nFdRs232, &hReadFds );

        //
        // Wait for port to become writeable. That means a
        // peer is in a session. Also break if UART becomes readable
        //
        nErr = select( nMax + 1, &hReadFds, &hWriteFds, NULL, &hTimeout );

        if ( nErr == 0 )
        {
            //
            // Ping host (keep alive)
            //
            nErr = host_port_ping_norsp( );
            if ( nErr < 0 )
                return -errno;
            continue;
        }

        if ( nErr < 0 )
        {
            return -errno;
        }

        //
        // Host port connected.
        //
        if ( FD_ISSET( nFd, &hWriteFds ) )
        {
            if ( *pEnConnect == true )
            {
                if ( pPeer )
                {
                    nErr = host_get_peer( pPeer );
                }
            }
            else
            {
                host_disconnect_session( );
                nErr = -1;
            }
        }
        else
        {
            //
            // Ping host (keep alive)
            //
            nErr = host_port_ping_norsp( );
            if ( nErr < 0 )
                return -errno;
        }

        //
        // Data available on bridge uart
        //
        if ( FD_ISSET( nFdRs232, &hReadFds ) )
        {
            uint8_t              hData[ 800 ];
            struct bridge_packet hPckt;
            uint32_t             nLen;

            //
            // Read packet
            //
            nLen = sizeof( struct bridge_packet );
            if ( pRs232->Read( ( uint8_t* )&hPckt, nLen, 300 ) != 0 )
                return -1;

            //
            // Sanity check
            //
            if ( hPckt.nHeader != PCKT_HEADER )
                return -1;

            //
            // Get associated data
            //
            nLen = hPckt.nDataLen;
            if ( ( nLen ) && ( pRs232->Read( hData, nLen, 2000 ) != 0 ) )
                return -1;

#ifdef DEBUG_CMD
            qprintf( "Cmd: 0x%X\n", hPckt.nCmd );
#endif

            //
            // Service uart commands (only ones that can be done when
            // peer is not in session)
            //
            service_unconnected_commands( pRs232, *pPeer, &hPckt, hData, pTrigger, pEnConnect );
        }
        //
        // No peer connected, just return -1
        //
        return nErr;

    }

    //
    // Timeout
    //
    return -ETIME;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   service_unconnected_commands
//
// Services commands that can be done, when the host is not in a session with
// an iceqube.
//
// ARGUMENTS:
//  (IN)        Rs232Socket*    Bridge UART connection
//  (IN)        uint32_t        Current peer
//  (IN)        bridge_packet*  Packet from bridge
//  (IN)        uint8_t*        Data assocaited with packet
//  (IN)        Trigger*        Access to trigger device
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void service_unconnected_commands( 
                Rs232Socket *pRs232, 
                uint32_t     nPeer,
                struct bridge_packet *pPckt, 
                uint8_t *pData, 
                Trigger *pTrigger,
                bool * pEnConnect
                )
{
    int nErr;

    //
    // Parse command ID
    //
    switch( pPckt->nCmd )
    {
    case CMD_PING:        
        pRs232->WritePacket( CMD_PING, sizeof( uint32_t ) , &nPeer );
        break;

    case CMD_EN_CONNECT:
        if ( pPckt->nDataLen < 1 )
        {
            pRs232->WriteError( -EINVAL );
        }
        else
        {
            *pEnConnect = pData[ 0 ] ? true : false;
            pRs232->WritePacket( CMD_EN_CONNECT, 1 , pData );
        }
        break;

    case CMD_DISCONNECT:
        pRs232->WritePacket( CMD_DISCONNECT, 0, NULL );
        break;

    case CMD_SET_CONFIG:
        if ( pPckt->nDataLen < sizeof( hub_cfg ))
        {
            pRs232->WriteError( -EINVAL );
            break;
        }

        if ( *pEnConnect == true )
        {
            pRs232->WriteError( -EBUSY );
            break;
        }

        if ( update_hub_radio_comms(( hub_cfg * ) pData ) == 0 )
        {
            struct hub_cfg  hCfg;
            uint8_t         nCount;

            if ( host_port_get_regulatory( &hCfg.nRegulatory ) != 0 )
            {
                pRs232->WriteError( -ENODEV );
                break;
            }
            
            if ( host_port_get_listen_channels(  
                            &nCount,
                            hCfg.nListenChannels    
                            ) != 0 )
            {
                pRs232->WriteError( -ENODEV );
                break;
            }

            if ( host_port_get_session_channels(
                            &nCount,
                            hCfg.nSessionChannels
                            ) != 0 )
            {
                pRs232->WriteError( -ENODEV );
                break;
            }
            pRs232->WritePacket( CMD_SET_CONFIG, sizeof( hub_cfg ), &hCfg );            
        }
        else
            pRs232->WriteError( -EINVAL );
        break;

    case CMD_GET_CONFIG:
        {
            struct hub_cfg  hCfg;
            uint8_t         nCount;
            if ( host_port_get_regulatory( &hCfg.nRegulatory ) != 0 )
            {
                pRs232->WriteError( -ENODEV );
                break;
            }
            
            if ( host_port_get_listen_channels(  
                            &nCount,
                            hCfg.nListenChannels    
                            ) != 0 )
            {
                pRs232->WriteError( -ENODEV );
                break;
            }

            if ( host_port_get_session_channels(
                            &nCount,
                            hCfg.nSessionChannels
                            ) != 0 )
            {
                pRs232->WriteError( -ENODEV );
                break;
            }
            pRs232->WritePacket( CMD_GET_CONFIG, sizeof( hub_cfg ), &hCfg );            
        }
        break;

    case CMD_TRIGGER_EN:
        if ( pPckt->nDataLen < 1 )
        {
            pRs232->WriteError( -EINVAL );
        }
        else
        {
            if ( pTrigger )
            {
                nErr = pTrigger->Enable( pData[0] ? true : false );
                if ( nErr == 0 )
                    pRs232->WritePacket( CMD_TRIGGER_EN, 1, pData );
                else
                    pRs232->WriteError( nErr );
            }
            else
            {
                pRs232->WriteError( -ENXIO );
            }
        }
        break;

    case CMD_TRIGGER_GET_CFG:
        {
            struct bridge_trigger_cfg hTrigCfg;
            uint16_t                  nDelay;

            if ( pTrigger )
            {
                nErr = pTrigger->GetConfig( 
                            hTrigCfg.nMode, 
                            nDelay,
                            hTrigCfg.nPower 
                            );

                hTrigCfg.nDelay = nDelay;

                if ( nErr == 0 )
                {
                    pRs232->WritePacket( 
                            CMD_TRIGGER_GET_CFG, 
                            (uint16_t) sizeof( hTrigCfg ), 
                            (uint8_t*) &hTrigCfg 
                            );
                }
                else
                {
                    pRs232->WriteError( nErr );
                }
            }
            else
            {
                pRs232->WriteError( -ENXIO );
            }
        }
        break;

    case CMD_TRIGGER_SET_CFG:
        if ( pPckt->nDataLen < sizeof( struct bridge_trigger_cfg ))
        {
            pRs232->WriteError( -EINVAL );
        }
        else
        {
            struct bridge_trigger_cfg *pTrigCfg;

            if ( pTrigger )
            {
                pTrigCfg = ( struct bridge_trigger_cfg * ) pData;

                nErr = pTrigger->SetConfig( 
                            pTrigCfg->nMode, 
                            pTrigCfg->nDelay,
                            pTrigCfg->nPower 
                            );

                if ( nErr == 0 )
                {
                    pRs232->WritePacket( 
                            CMD_TRIGGER_SET_CFG, 
                            sizeof( struct bridge_trigger_cfg ), 
                            pData 
                            );
                }
                else
                {
                    pRs232->WriteError( nErr );
                }
            }
            else
            {
                pRs232->WriteError( -ENXIO );
            }
        }
        break;

    case CMD_TRIGGER_GET_VER:
        {
            uint8_t nVer;
            if ( pTrigger )
            {
                nErr = pTrigger->GetVersion( nVer );
                if ( nErr == 0 )
                {
                    pRs232->WritePacket( CMD_TRIGGER_GET_VER, 1, &nVer );
                }
                else
                {
                    pRs232->WriteError( nErr );
                }
            }
            else
            {
                pRs232->WriteError( -ENXIO );
            }
        }       
        break;

    case CMD_SYSTEM:
        if ( pPckt->nDataLen < 1 )
        {
            pRs232->WriteError( -EINVAL );
        }
        else
        {
            pData[ pPckt->nDataLen ] = 0;
            nErr = system((char*) pData );
            pRs232->WritePacket( CMD_SYSTEM, sizeof( nErr ), &nErr );
        }
        break;

    default:
        pRs232->WriteError( -EINVAL );
        break;
    }
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   update_hub_radio_comms
//
// Change radio parameters
//
// ARGUMENTS:
//  (IN)        hub_cfg*        New settings
//
// RETURNS:
//              int             0 on success
//
///////////////////////////////////////////////////////////////////////////////
int update_hub_radio_comms( hub_cfg *pNewCfg )
{
    uint8_t nRegulatory;
    uint8_t nListen;
    uint8_t nNewListen[ 3 ];
    uint8_t nSess;
    uint8_t nNewSess[ 3 ];
    uint8_t i;

    if ( host_port_get_regulatory( &nRegulatory ) != 0 )
        return -ENODEV;

    //
    // Make sure at least one channel is set
    //
    if (( pNewCfg->nListenChannels[ 0 ] == 0xFF ) &&
        ( pNewCfg->nListenChannels[ 1 ] == 0xFF ) &&
        ( pNewCfg->nListenChannels[ 2 ] == 0xFF ))
        return -1;
    if (( pNewCfg->nSessionChannels[ 0 ] == 0xFF ) &&
        ( pNewCfg->nSessionChannels[ 1 ] == 0xFF ) &&
        ( pNewCfg->nSessionChannels[ 2 ] == 0xFF ))
        return -1;

    //
    // Copy the new config, and bounds check
    //
    nListen = 0;
    memset( nNewListen, 0xFF, 3 );
    nSess = 0;
    memset( nNewSess, 0xFF, 3 );

    for( i = 0; i < 3; i ++ )
    {
        if ( pNewCfg->nListenChannels[ i ] != 0xFF )
        {
            if ( pNewCfg->nListenChannels[ i ] > 
                 MAX_CHANNEL( nRegulatory  ))
                return -1;

            nNewListen[ nListen++ ] = pNewCfg->nListenChannels[ i ];
        }
        if ( pNewCfg->nSessionChannels[ i ] != 0xFF )
        {
            if ( pNewCfg->nSessionChannels[ i ] > 
                 MAX_CHANNEL( nRegulatory  ))
                return -1;
            nNewSess[ nSess++ ] = pNewCfg->nSessionChannels[ i ];
        }
    }

    //
    // Disconnect session
    //
    host_disconnect_session( );

    if ( host_port_set_listen_channels( nListen, nNewListen ) != 0 )
        return -ENODEV;
    if ( host_port_set_session_channels( nSess, nNewSess ) != 0 )
        return -ENODEV;

    qprintf( "Radio Config Changed\n" );

    if ( nListen < 2 )
    {
        qprintf ( 
            "    Listen channels : %d\n", 
            nNewListen[0] 
            );   
    }
    else 
    {
        if ( nListen < 3 )
        {
            qprintf ( 
                "    Listen channels : %d, %d\n", 
                nNewListen[0], 
                nNewListen[1] 
                );   
        }
        else
        {
            qprintf ( 
                "    Listen channels : %d, %d, %d\n", 
                nNewListen[0], 
                nNewListen[1], 
                nNewListen[2] 
                );
        }
    }

    if ( nSess < 2 )
    {
        qprintf ( 
            "    Session channels : %d\n", 
            nNewSess[0] 
            ); 
    }
    else
    {
        if ( nSess < 3 )
        {
            qprintf ( 
                "    Session channels : %d, %d\n", 
                nNewSess[0], 
                nNewSess[1] 
                );
        }
        else
        {
            qprintf ( 
                "    Session channels : %d, %d, %d\n", 
                nNewSess[0], 
                nNewSess[1], 
                nNewSess[2] 
                );
        }
    }

    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   print_usage
//
// Print command line usage
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void print_usage( void )
{
    if ( sg_fService == false )
        return;

    fprintf( stdout, "usage:\n");
    fprintf( stdout, "  iceqube_icenetc_example.elf [-p=X][-u=x][-t=X][-l0/1/2=X][-s0/1/2=X]\n");
    fprintf( stdout, "\t-p=[0-2,255]    cc1310 crisp module extension slot \n");
    fprintf( stdout, "\t                0-2, or 255 for auto (default 255)\n");
    fprintf( stdout, "\t-r=[0-6,-1]     Multihub rank (0=default, -1=random)\n");
    fprintf( stdout, "\t-u=[device]     UART to bridged device (default /dev/crisp_uart_tty0)\n");
    fprintf( stdout, "\t-t=[device]     Trigger plug-in board (default /dev/crisp_trigger_0, or none)\n");
    fprintf( stdout, "\t-l0=[0-max]     listen channel 0 (default 0)\n" );
    fprintf( stdout, "\t-l1=[0-max,255] listen channel 1 (default 6)\n" );
    fprintf( stdout, "\t-l2=[0-max,255] listen channel 2 (default 12)\n" );
    fprintf( stdout, "\t-s0=[0-max]     session channel 0 (default 18)\n" );
    fprintf( stdout, "\t-s1=[0-max,255] session channel 1 (default 255)\n" );
    fprintf( stdout, "\t-s2=[0-max,255] session channel 2 (default 255)\n" );
    fprintf( stdout, "\t-rf=[-50000-50000, or file] frequency offset, or \n" );
    fprintf( stdout, "\t                file containing offset (default %s). \n", RF_OFFSET_FILE );
    fprintf( stdout, "\t--              run as service (no stdin/out)\n" );

    fprintf( stdout, "\n\tsession channels must be separated from the listen channels\n" );
    fprintf( stdout, "\tby 2. Each listen channels uses channel + 1 for host responses.\n");

    fprintf( stdout, "\n\tto disable a channel, omit unused channels or set -l or -s\n" );
    fprintf( stdout, "\tvalue to 255; channel 0 cannot be unused. The max value depends\n");
    fprintf( stdout, "\ton the radio band:\n");

    fprintf( 
        stdout, 
        "\t- For 868Mhz the max channel number is %d\n", 
        MAX_CHANNEL( REGULATORY_EU_2000_299_EC1 )
        );

    fprintf( 
        stdout, 
        "\t- for 915Mhz the max channel number is %d\n\n", 
        MAX_CHANNEL( REGULATORY_FCC )
        );

    fflush( stdout );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   kbhit
//
// Test for keyboard hit.
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int         Non-zero if there is a keystroke in the input
//                          buffer.
//
///////////////////////////////////////////////////////////////////////////////
int kbhit( void )
{
    struct timeval hTv = { 0L, 0L };
    fd_set hFds;
    FD_ZERO( &hFds );
    FD_SET( STDIN, &hFds );
    return select( STDIN + 1, &hFds, NULL, NULL, &hTv );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   StopSigHandler
//
// External handler for SIG_STOP. Allows for a clean shutdown.
//
// ARGUMENTS:
//  (IN)        int         ID of signal (only SIG_STOP trapped)
//  (IN)        siginfo_t*  Signal information
//  (IN)        void*       Unused.
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void StopSigHandler( int nSignal, siginfo_t *pSig, void *pSecret )
{
    sg_fQuit = true;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   parse_command_line
//
// Parses command line.
//
// ARGUMENTS:
//  (IN)        int         Number of command line args 
//  (IN)        char**      Array of strings, where first is always module name
//  (OUT)       uint8_t&    Crisp host slot (0,1,2 or 0xFF for auto)
//  (OUT)       uint8_t&    Number of listen channels
//  (OUT)       uint8_t*    Array of up to 3 listen channels
//  (OUT)       uint8_t&    Number of session channels
//  (OUT)       uint8_t*    Array of up to 3 session channels
//  (OUT)       char*       Path to bridge uart device
//  (OUT)       char*       Path to trigger device
//  (OUT)       int32_t&    Frequency offset
//
// RETURNS:
//              int         0 on success, else parse error.
//
///////////////////////////////////////////////////////////////////////////////
int parse_command_line( 
                    int     nArgc,
                    char    **pArgc,
                    uint8_t &nCrispSlot,
                    uint8_t &nRank,
                    uint8_t &nListenChannels,
                    uint8_t *hListenChannels,
                    uint8_t &nSessionChannels,
                    uint8_t *hSessionChannels,
                    char    *szBridgeUART,
                    char    *szTrigger,
                    int32_t &nFrequencyOffset
                    )
{
    bool           fListenChannelsSet     = false; // any listen channel set?
    bool           fSessionChannelsSet    = false; // any session channel set?
    uint8_t        hNewListenChannels[3]  = { 0xFF, 0xFF, 0xFF }; // no new listen channels yet
    uint8_t        hNewSessionChannels[3] = { 0xFF, 0xFF, 0xFF }; // no new session channels yet
    char           szStrParam[ 50 ];
    int            n;

    //
    // Set defaults
    //
    nCrispSlot             = 0xFF;  // auto
    nListenChannels        = 3;     // default number of listen channels
    hListenChannels[0]     = 0; 
    hListenChannels[1]     = 6; 
    hListenChannels[2]     = 12;
    
    nSessionChannels       = 1;     // default number of session channels
    hSessionChannels[0]    = 18;
    hSessionChannels[1]    = 0xFF;
    hSessionChannels[2]    = 0xFF;

    nRank                  = 0;
    
    strcpy( szBridgeUART, DEFAULT_BRIDGE_UART );
    strcpy( szTrigger, DEFAULT_TRIGGER );

    //
    // Parse command line paramters
    //
    if ( parse_param_set( "-?", nArgc, pArgc ))
        return -1;

    if ( parse_int_param( "-p=", &n, nArgc, pArgc ))
    {
        nCrispSlot = n;
        if (( nCrispSlot != 0xFF ) && ( nCrispSlot > 2 ))
        {
            qprintf( "Invalid card slot (-p=%d). Must be port 0-2, or 255 for auto.\n", nCrispSlot );
            return -1;
        }
    }

    if ( parse_int_param( "-r=", &n, nArgc, pArgc ))
    {
        if (( n < -1 ) || ( n > 6 ))
        {
            fprintf( stdout,"Invalid rank (-r=%d). Must be port 0-6, or -1 for random.\n", n );
            print_usage( );
            return -1;
        }
        nRank = (uint8_t) ( n & 0xFF );
    }

    if ( parse_int_param( "-s0=", &n, nArgc, pArgc ))
    {
        hNewSessionChannels[0] = n;
        fSessionChannelsSet = true;
    }

    if ( parse_int_param( "-s1=", &n, nArgc, pArgc ))
    {
        hNewSessionChannels[1] = n;
        fSessionChannelsSet = true;
    }
    if ( parse_int_param( "-s2=", &n, nArgc, pArgc ))
    {
        hNewSessionChannels[2] = n;
        fSessionChannelsSet = true;
    }

    if ( parse_int_param( "-l0=", &n, nArgc, pArgc ))
    {
        hNewListenChannels[0] = n;
        fListenChannelsSet = true;
    }

    if ( parse_int_param( "-l1=", &n, nArgc, pArgc ))
    {
        hNewListenChannels[1] = n;
        fListenChannelsSet = true;
    }

    if ( parse_int_param( "-l2=", &n, nArgc, pArgc ))
    {
        hNewListenChannels[2] = n;
        fListenChannelsSet = true;
    }

    if ( parse_string_param( "-u=", szStrParam, 50, nArgc, pArgc )) 
    {
        strcpy( szBridgeUART, szStrParam );
    }

    if ( parse_string_param( "-t=", szStrParam, 50, nArgc, pArgc )) 
    {
        strcpy( szTrigger, szStrParam );
    }

    if ( parse_frequency_param( "-rf=", &n, nArgc, pArgc ))
    {
        nFrequencyOffset = n;
    }

    //
    // Assume new parameters
    //   
    if ( fListenChannelsSet )
        memcpy( hListenChannels, hNewListenChannels, 3 );
    nListenChannels = 0;
    if ( hListenChannels[0] != 0xFF ) 
        nListenChannels++;
    if ( hListenChannels[1] != 0xFF ) 
        nListenChannels++;
    if ( hListenChannels[2] != 0xFF ) 
        nListenChannels++;

    if ( fSessionChannelsSet )
        memcpy( hSessionChannels, hNewSessionChannels, 3 );
    nSessionChannels = 0;
    if ( hSessionChannels[0] != 0xFF )
        nSessionChannels++;
    if ( hSessionChannels[1] != 0xFF )
        nSessionChannels++;
    if ( hSessionChannels[2] != 0xFF )
        nSessionChannels++;

    //
    // No listen channels?
    //
    if (( nListenChannels == 0 ) || ( nSessionChannels == 0 ))
    {
        qprintf( "Invalid channel count. Requires at least 1 valid listen and 1 valid session channel.\n" );
        return -1;
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   parse_int_param
//
// Parses an integer paramter from the command line arguments
//
// ARGUMENTS:
//  (IN)     const char*     Command line paramter to search for (e.g."-r=")
//  (IN/OUT) int*            Interpreted value
//  (IN)     int             Command line paramter count
//  (IN)     char**          Command line paramters
//
// RETURNS:
//           bool            true if parameter was found and interpreted
//
///////////////////////////////////////////////////////////////////////////////
bool parse_int_param( const char* szParam, int *pVal, int nArgv, char **pArgc )
{
    int i;
    int nParamLen = strlen( szParam );

    if (( szParam == NULL ) || ( pVal == NULL ))
        return false;

    for( i = 0; i < nArgv; i ++ )
    {
        char *szCurParam = pArgc[ i ];
        int   nCurParamLen;

        if (( szCurParam == NULL ) || ( szCurParam[0] != '-' ))
            continue;
    
        nCurParamLen = strlen( szCurParam );

        if ( nCurParamLen < nParamLen )
            continue;

        if ( memcmp( szParam, szCurParam, nParamLen ) == 0 )
        {
            char* szEquals = strchr( szCurParam, '=' );
            if ( szEquals )
            {
                szEquals++;
                *pVal = atoi( szEquals );
                return true;
            }
        }
    }
    return false;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   parse_param_set
//
// Checks if a command line paramter is set
//
// ARGUMENTS:
//  (IN)     const char*     Command line paramter to search for (e.g."-?")
//  (IN)     int             Command line paramter count
//  (IN)     char**          Command line paramters
//
// RETURNS:
//           bool            true if parameter was found
//
///////////////////////////////////////////////////////////////////////////////
bool parse_param_set( const char* szParam, int nArgv, char **pArgc )
{
    int i;
    int nParamLen = strlen( szParam );

    if ( szParam == NULL )
        return false;

    for( i = 0; i < nArgv; i ++ )
    {
        char *szCurParam = pArgc[ i ];
        int   nCurParamLen;

        if (( szCurParam == NULL ) || ( szCurParam[0] != '-' ))
            continue;
    
        nCurParamLen = strlen( szCurParam );

        if ( nCurParamLen < nParamLen )
            continue;

        if ( memcmp( szParam, szCurParam, nParamLen ) == 0 )
            return true;
    }
    return false;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   parse_string_param
//
// Parses a string paramter from the command line arguments
//
// ARGUMENTS:
//  (IN)     const char*     Command line paramter to search for (e.g."-r=")
//  (IN/OUT) char*           Buffer to take string value
//  (IN)     int             Buffer size
//  (IN)     int             Command line paramter count
//  (IN)     char**          Command line paramters
//
// RETURNS:
//           bool            true if parameter was found and interpreted
//
///////////////////////////////////////////////////////////////////////////////
bool parse_string_param( 
            const char* szParam, 
            char  *szBuffer,
            int    nMaxLen,
            int    nArgv, 
            char **pArgc            
            )
{
    int i;
    int nParamLen = strlen( szParam );

    if ( szParam == NULL )
        return false;

    for( i = 0; i < nArgv; i ++ )
    {
        char *szCurParam = pArgc[ i ];
        int   nCurParamLen;

        if (( szCurParam == NULL ) || ( szCurParam[0] != '-' ))
            continue;
    
        nCurParamLen = strlen( szCurParam );

        if ( nCurParamLen < nParamLen )
            continue;

        if ( memcmp( szParam, szCurParam, nParamLen ) == 0 )
        {
            char* szEquals = strchr( szCurParam, '=' );
            if ( szEquals )
            {
                szEquals++;
                nCurParamLen = strlen( szEquals );
                if ( nCurParamLen < nMaxLen )            
                {
                    strcpy( szBuffer, szEquals );
                }
                else
                {
                    memcpy ( szBuffer, szEquals, nMaxLen - 2 );
                    szBuffer[ nMaxLen - 1 ] = 0;
                }
                return true;
            }
        }
    }
    return false;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   parse_frequency_param
//
// Parses a paramter from the command line arguments, looking for frequency.
// This can either be a numeric value between -50000 and +50000, or a file
// name containing the value.
//
// ARGUMENTS:
//  (IN)     const char*     Command line paramter to search for (e.g."-r=")
//  (IN/OUT) int*            Frequency value
//  (IN)     int             Command line paramter count
//  (IN)     char**          Command line paramters
//
// RETURNS:
//           bool            true if parameter was found and interpreted
//
///////////////////////////////////////////////////////////////////////////////
bool parse_frequency_param( 
            const char* szParam, 
            int   *pVal,
            int    nArgv, 
            char **pArgc            
            )
{
    int  i;
    char szStringParam[ MAX_PATH ];
    int  nParamLen = strlen( szParam );
    bool fFound = false;
    bool fIsDigit = true;

    if ( szParam == NULL )
        return false;

    if ( pVal == NULL )
        return false;

    for( i = 0; i < nArgv; i ++ )
    {
        char *szCurParam = pArgc[ i ];
        int   nCurParamLen;

        if (( szCurParam == NULL ) || ( szCurParam[0] != '-' ))
            continue;
    
        nCurParamLen = strlen( szCurParam );

        if ( nCurParamLen < nParamLen )
            continue;

        if ( memcmp( szParam, szCurParam, nParamLen ) == 0 )
        {
            char* szEquals = strchr( szCurParam, '=' );
            if ( szEquals )
            {
                szEquals++;
                nCurParamLen = strlen( szEquals );
                if ( nCurParamLen < ( MAX_PATH - 2 ))            
                {
                    strcpy( szStringParam, szEquals );
                }
                else
                {
                    memcpy ( szStringParam, szEquals, MAX_PATH - 2 );
                    szStringParam[ MAX_PATH - 1 ] = 0;
                }
                fFound = true;
                break;
            }
        }
    }

    // Not found, look for default file    
    if ( fFound == false )
    {
        strcpy( szStringParam, RF_OFFSET_FILE );
        fFound = true;
    }

    //
    // OK, got parameter
    //
    if ( fFound )
    {
        nParamLen = strlen( szStringParam );

        //
        // See if it's a number
        //
        for( i = 0; i < nParamLen; i ++ )
        {
            if ((( szStringParam[i] >= '0' ) &&
                 ( szStringParam[i] <= '9' )) ||
                 ( szStringParam[i] == '-' ))
                continue;
            fIsDigit = false;
            break;
        }

        if ( fIsDigit )
        {
            *pVal = atoi( szStringParam );

            // cap
            if ( *pVal < -50000 )
                *pVal = -50000;
            if ( *pVal > 50000 )
                *pVal = 50000;
            return true;
        }   
    }

    //
    // OK, but it's a filename
    //
    if ( fFound )
    {
        FILE *pFile;
        pFile = fopen( szStringParam, "rt" );
        if ( pFile == NULL )
        {
            return false;
        }
        if ( fscanf( pFile,"%d", pVal ) <= 0 )
        {
            fclose( pFile );
            return false;
        }
        fclose( pFile );
        return true;
    }
    return false;
}

//////////////////////////////////// EOF //////////////////////////////////////

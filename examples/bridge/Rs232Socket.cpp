//FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   Rs232Socket.cpp
//
// PROJECT:     icehub
//
// PLATFORM:    crisp linux/arm/gcc
//
// Implementation of Rs232Socket for Linux
//  
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2018-2019 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd.
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include "Rs232Socket.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <asm/ioctls.h>

/////////////////////////////////////////////
// PRIVATE DEFINES
//

#define MAX_WRITE_CHUNK		2048

#define CRISPUART_MAGIC 'T'

#define CRISPUART_IOCTL_GET_SERIAL_PORT_NUM  _IOR( CRISPUART_MAGIC, 0, uint8_t )

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

/////////////////////////////////////////////
// DATA
//

//FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   Rs232Socket	(Constructor)
//
// Constructor for Rs232Socket
//
// ARGUMENTS:
//  [IN/OUT]    none
//
///////////////////////////////////////////////////////////////////////////////
Rs232Socket::Rs232Socket()
{
	m_nSocket = -1;
    m_fDebug  = false;
}
 
//FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   ~Rs232Socket		(Destructor)
//
// Closes socket if still open
//
// ARGUMENTS:
//  [IN/OUT]    none
//
///////////////////////////////////////////////////////////////////////////////
Rs232Socket::~Rs232Socket()
{
	if ( m_nSocket != -1 )
	{
		tcflush( m_nSocket, TCIOFLUSH );
		close( m_nSocket );
		m_nSocket = -1;
	}
}

//FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   Create
//
// Creates and opens an Rs232 connection using the specified parameters
//
// ARGUMENTS:
//  [IN]        const char*     Uart device  path
//  [IN]        uint32_t        Baudrate (use #define)
//  [IN]        uint8_t         Parity (use #define)
//  [IN]        uint8_t         Data bits (use #define)
//  [IN]        uint8_t         Stop bits (use #define)
//
// RETURNS:
//              int             0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int Rs232Socket::Create(
                    const char* szUart,
                    uint32_t    nBaudRate,
                    uint8_t     nParity,
                    uint8_t     nDataBits,
                    uint8_t     nStopbits
                    )
{
	struct termios	hOptions;
	speed_t			nPosixBaud = 0;
	int			    nFlags;
    char            szPath[ 100 ];

	if ( m_nSocket != -1 )
	{
		tcflush( m_nSocket, TCIOFLUSH );
		close( m_nSocket );
		m_nSocket = -1;
	}

    if ( strstr( szUart, "crisp_uart_" ) != NULL )
    {
        uint8_t  nNum;
        int      nErr;
        int      nLocal = open( szUart, O_RDWR | O_NOCTTY /*| O_NODELAY */ );

    	if ( nLocal == -1 )
	    {
	        return -errno;
	    }

        nErr = ioctl( nLocal, CRISPUART_IOCTL_GET_SERIAL_PORT_NUM, (uint8_t*) &nNum );
        if ( nErr != 0 )
        {
            close( nLocal );
            return -errno;
        }      
        close( nLocal );

        sprintf( szPath, "/dev/ttyAPP%d", nNum );
    }
    else
    {
        strcpy( szPath, szUart );
    }
	//
	// Open the COM port
	//
	m_nSocket = open( szPath, O_RDWR | O_NOCTTY /*| O_NODELAY */ );

	if ( m_nSocket == -1 )
	{
		m_nSocket = -1;
		return -errno;
	}

	//
	// Configure port reading
	//
	fcntl( m_nSocket, F_SETFL, FNDELAY );	
	
	//
	// Set baud rate
	//
	switch( nBaudRate )
	{
	case RS232SOCKET_BAUD_1200:
		nPosixBaud = B1200;
		break;

	case RS232SOCKET_BAUD_2400:
		nPosixBaud = B2400;
		break;
		
	case RS232SOCKET_BAUD_9600:
		nPosixBaud = B9600;
		break;

	case RS232SOCKET_BAUD_19200:
		nPosixBaud = B19200;
		break;

	case RS232SOCKET_BAUD_38400:
		nPosixBaud = B38400;
		break;

	case RS232SOCKET_BAUD_56000:
		nPosixBaud = B57600;
		break;

	case RS232SOCKET_BAUD_115200:
		nPosixBaud = B115200;
		break;

	case RS232SOCKET_BAUD_230400:
		nPosixBaud = B230400;
		break;

	case RS232SOCKET_BAUD_460800:
		nPosixBaud = B460800;
		break;

	case RS232SOCKET_BAUD_500000:
		nPosixBaud = B500000;
		break;

	case RS232SOCKET_BAUD_576000:
		nPosixBaud = B576000;
		break;

	case RS232SOCKET_BAUD_921600:
		nPosixBaud = B921600;
		break;

	case RS232SOCKET_BAUD_1000000:
		nPosixBaud = B1000000;
		break;

	case RS232SOCKET_BAUD_1152000:
		nPosixBaud = B1152000;
		break;

	case RS232SOCKET_BAUD_1500000:
		nPosixBaud = B1500000;
		break;

	case RS232SOCKET_BAUD_2000000:
		nPosixBaud = B2000000;
		break;

	case RS232SOCKET_BAUD_2500000:
		nPosixBaud = B2500000;
		break;

	case RS232SOCKET_BAUD_3000000:
		nPosixBaud = B3000000;
		break;

	case RS232SOCKET_BAUD_3500000:
		nPosixBaud = B3500000;
		break;

	case RS232SOCKET_BAUD_4000000:
		nPosixBaud = B4000000;
		break;

	default:
 		struct serial_struct hSerial;
		ioctl(m_nSocket, TIOCGSERIAL, &hSerial);
		hSerial.flags |= ASYNC_SPD_CUST;
		hSerial.custom_divisor = hSerial.baud_base / nBaudRate;
		ioctl(m_nSocket, TIOCSSERIAL, &hSerial);
		nPosixBaud = 0;
		break;
	}

	//
	// Get current options
	//
	tcgetattr( m_nSocket, &hOptions );

	if ( nPosixBaud )
	{
	    cfsetispeed( &hOptions, nPosixBaud );
	    cfsetospeed( &hOptions, nPosixBaud );
	}

	//
	// Set data size
	//

	hOptions.c_cflag &= ~CSIZE;
	switch( nDataBits )
	{
	case RS232SOCKET_DATABITS_5:
		hOptions.c_cflag |= CS5;
		break;

	case RS232SOCKET_DATABITS_6:
		hOptions.c_cflag |= CS6;
		break;

	case RS232SOCKET_DATABITS_7:
		hOptions.c_cflag |= CS7;
		break;

	case RS232SOCKET_DATABITS_8:
		hOptions.c_cflag |= CS8;
		break;

	default:
		close( m_nSocket );
		m_nSocket = -1;
		return -EINVAL;
	}

	//
	// Set parity
	//
	switch( nParity )
	{
	case RS232SOCKET_PARITY_ODD:
		hOptions.c_cflag |= PARENB;
		hOptions.c_cflag |= PARODD;
		break;

	case RS232SOCKET_PARITY_EVEN:
		hOptions.c_cflag |= PARENB;
		hOptions.c_cflag &= ~PARODD;
		break;

	case RS232SOCKET_PARITY_NONE:
	default:
		hOptions.c_cflag &= ~PARENB;
		break;
	}

	//
	// Set stop bits
	//
	switch( nStopbits )
	{
	case RS232SOCKET_STOPBITS_2:
		hOptions.c_cflag |= CSTOPB;
		break;

	case RS232SOCKET_STOPBITS_1:
	default:
		hOptions.c_cflag &= ~CSTOPB;
		break;
	}

	//
	// Enable DSR CTS handshaking
	//
	hOptions.c_lflag |= CRTSCTS;
	hOptions.c_lflag &= ~( ICANON | ECHO | ISIG | IEXTEN | ECHONL );
	hOptions.c_oflag &= ~OPOST;
	hOptions.c_iflag &= ~( IGNBRK | BRKINT |PARMRK | ISTRIP | INLCR |IGNCR | ICRNL| IXON );

	//
	// Set the options
	//
	if ( tcsetattr( m_nSocket, TCSANOW, &hOptions ) != 0 )
	{
		close( m_nSocket );
		m_nSocket = -1;
		return -errno;
	}

	//
	// All reads are actually non-blocking, for blocking mode, 
	// select is used to block, this supports timeout parameter.
	//
	nFlags = fcntl( m_nSocket, F_GETFL );
    fcntl( m_nSocket, F_SETFL, nFlags | O_NONBLOCK );
	return 0;
}

//FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   Read
//
// Reads a number of bytes off an rs232 socket. 
//
// ARGUMENTS:
//  [IN]        uint8_t*    Buffer to accept bytes
//	[IN/OUT]    uint32_t&   Length of buffer [in]
//                          Actual bytes read [out]
//  [IN]        uint32_t    Timeout for read.
//
// RETURNS:
//              int         0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int Rs232Socket::Read( uint8_t *pData, uint32_t &nDataLen, uint32_t nTimeout )
{
	uint32_t		nToRead;
	uint8_t		   *pPos;
	struct timeval  hTimeout;
	struct timeval *pTimeout;
	fd_set			hReadFds;


	//
	// Blocking read
	//
	FD_ZERO( &hReadFds );
	FD_SET( m_nSocket, &hReadFds ); 

	if ( nTimeout != TIMEOUT_INFINITE )
	{
		hTimeout.tv_sec  = nTimeout / 1000;
		hTimeout.tv_usec = ( nTimeout - ( hTimeout.tv_sec * 1000 )) * 1000;
		pTimeout = &hTimeout;
	}
	else
	{
		pTimeout = NULL;
	}

	nToRead =  nDataLen;
	pPos    =  pData;
	while( nToRead )
	{
		int nSel = select ( m_nSocket + 1, &hReadFds, NULL, NULL, pTimeout );

		if ( nSel < 0 )
			return -errno;

		if ( nSel == 0 )
			return -ETIME;

		int nRead = read( m_nSocket, (char*) pPos, nToRead ); 

		if ( nRead == 0 )
			return -EIO;		
		
		if ( nRead < 0 )
		{
			if (( errno != EAGAIN ) && 
				( errno != EWOULDBLOCK ) &&
				( errno != EINPROGRESS ))
				return -errno;

			nRead = 0;
		}
		nToRead -= nRead;
		pPos    += nRead;
	}		
	return 0;
}

//FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   Write
//
// Writes a number of bytes to a socket. 
//
// ARGUMENTS:
//  [IN]        uint8_t*    Buffer of bytes to write
//  [IN/OUT]    uint32_t*	Length of buffer [in]
//                          Actual bytes written [out]
//  [IN]        uint32_t    Timeout for write.
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int Rs232Socket::Write( uint8_t *pData, uint32_t &nDataLen, uint32_t nTimeout )
{
	uint32_t		 nToWrite;
	uint8_t			*pPos;
	struct timeval   hTimeout;
	struct timeval  *pTimeout;
	fd_set			 hWriteFds;

	//
	// Blocking write
	//
	FD_ZERO( &hWriteFds );
	FD_SET( m_nSocket, &hWriteFds ); 

	if ( nTimeout != TIMEOUT_INFINITE )
	{
		hTimeout.tv_sec  = nTimeout / 1000;
		hTimeout.tv_usec = ( nTimeout - ( hTimeout.tv_sec * 1000 )) * 1000;
		pTimeout = &hTimeout;
	}
	else
	{
		pTimeout = NULL;
	}


	nToWrite =  nDataLen;
	pPos     =  pData;

	while( nToWrite )
	{
		int nSel = select( m_nSocket + 1, NULL, &hWriteFds, NULL, pTimeout );

		if ( nSel == 0 )
			return -ETIME;

		if ( nSel < 0 )
			return -errno;

		uint32_t nBlockSize = 1;//nToWrite > MAX_WRITE_CHUNK ? MAX_WRITE_CHUNK : nToWrite;

        if ( m_fDebug )
            printf("DEBUG:WRITE [%X]\n", (char)*pPos );

		int nSent = write( m_nSocket, (char*) pPos, nBlockSize );

		if ( nSent == 0 )
			return -EIO;

		if ( nSent < 0 )
		{
			if (( errno != EAGAIN ) && 
				( errno != EWOULDBLOCK ) &&
				( errno != EINPROGRESS ))
				return -errno;

			nSent = 0;
		}
		nToWrite -= nSent;
		pPos     += nSent;
	}		
	return 0;		
}

//FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   ReadLine
//
// Reads a line of data, until a CRLF or LF occur. Terminating characters are
// not returned in the data.
//
// ARGUMENTS:
//  [IN]        uint8_t*    Bufffer to receive data into
//  [IN]        uint32_t*   Length of buffer [in]
//                          Actual bytes written [out]
//  [IN]        uint32_t    Timeout for write.
//  [IN]        char*       Terminator list ( 0 = \r\n )
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int Rs232Socket::ReadLine( 
                    uint8_t *pData, 
                    uint32_t &nDataLen, 
                    uint32_t nTimeout, 
                    char *pTerminators 
                    )
{
    uint32_t nRead = 0;
    bool     fFilterCR = true;

    if (( pTerminators ) && ( strchr( pTerminators, '\r' )))
        fFilterCR = false;

    while( nRead < nDataLen )
    {
        int  nErr;
        char nC;
        uint32_t nLen;

        nLen = 1;
        nErr = Read( (uint8_t*) &nC, nLen, nTimeout );
        if ( nErr != 0 )
        {
            nDataLen = 0;
            return nErr;
        }

        if ( m_fDebug )
            printf("DEBUG:READ [%c] 0x%0X\n", nC, nC );        

        if ( fFilterCR )
        {
            if ( nC == '\r')
                continue;
        }
        if ( pTerminators == 0 )
        {

            if ( nC == '\n' )
            {
                pData[ nRead ] = 0;
                nDataLen = strlen((char*) pData );
                return 0;
            }
        }
        else
        {
            char *pTerm = pTerminators;
            while( *pTerm != 0 )
            {
                if ( nC == *pTerm )
                {
                    pData[ nRead ] = 0;
                    nDataLen = strlen((char*) pData );
                    return 0;
                }
                pTerm++;
            }
        }
        pData[ nRead ] = nC;
        nRead ++;
    }

    pData[ nDataLen - 1 ] = 0;
    nDataLen = strlen((char*) pData );
    return 0;
}

//FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   Close
//
// Closes a socket
//
// ARGUMENTS:
//  [IN/OUT]    none
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void Rs232Socket::Close( )
{
	if ( m_nSocket != -1 )
	{
		tcflush( m_nSocket, TCIOFLUSH );
		close( m_nSocket );
		m_nSocket = -1;
	}
}

//FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   WritePacket
//
// Sends a bridge command packet
//
// ARGUMENTS:
//  [IN]        uint8_t     Command ID
//  [IN]        uint16_t    Lenght of associated data
//  [IN]        void*       Associated buffer
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int Rs232Socket::WritePacket( uint8_t nCmd, uint16_t nLen, void *pData )
{

    struct bridge_packet hPckt;
    uint32_t             nSize;
    int                  nErr;

    hPckt.nHeader  = PCKT_HEADER;
    hPckt.nCmd     = nCmd;
    hPckt.nDataLen = nLen;

    nSize = sizeof( struct bridge_packet );
    nErr = Write(( uint8_t* ) &hPckt, nSize, 2000 );

    if ( nErr != 0 )
        return nErr;

    if ( nLen )
    {
        nSize = nLen;
        nErr = Write(( uint8_t *) pData, nSize, 2000 );
    }
    return nErr;
}

//FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   WriteError
//
// Sends an error bridge command packet
//
// ARGUMENTS:
//  [IN]        int         Error code
//
// RETURNS:
//              int         0 on success, else error code
//
///////////////////////////////////////////////////////////////////////////////
int Rs232Socket::WriteError( int nErr )
{
    return WritePacket( CMD_ERR, (uint16_t) sizeof( int ), &nErr );
}


//////////////////////////////////// EOF //////////////////////////////////////

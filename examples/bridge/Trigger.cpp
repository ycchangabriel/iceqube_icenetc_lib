// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   example.c
//
// PROJECT:     icehub
//
// PLATFORM:    crisp linux/arm/gcc
//
// Trigger class implementation
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2018-2019 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd.
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>

#include "Trigger.h"
#include "types.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

//
// IOCTLs to match driver
//
#define TRIGGER_MAGIC 'G'                                                   

#define TRIGGER_IOCTL_ENABLE    	    _IOR( TRIGGER_MAGIC,  0, uint8_t )
#define TRIGGER_IOCTL_GET_MODE    	    _IOR( TRIGGER_MAGIC, 10, uint8_t )
#define TRIGGER_IOCTL_SET_MODE    	    _IOW( TRIGGER_MAGIC, 20, uint8_t )
#define TRIGGER_IOCTL_GET_DELAY    	    _IOR( TRIGGER_MAGIC, 30, uint16_t )
#define TRIGGER_IOCTL_SET_DELAY    	    _IOW( TRIGGER_MAGIC, 40, uint16_t )
#define TRIGGER_IOCTL_GET_VERSION 	    _IOR( TRIGGER_MAGIC, 50, uint8_t )
#define TRIGGER_IOCTL_GET_POWER    	    _IOR( TRIGGER_MAGIC, 60, uint8_t )
#define TRIGGER_IOCTL_SET_POWER    	    _IOW( TRIGGER_MAGIC, 70, uint8_t )

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

/////////////////////////////////////////////
// PUBLIC DATA
//

/////////////////////////////////////////////
// PRIVATE DATA
//

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   Trigger         (Constructor)
//
// Initializes members
//
// ARGUMENTS:
//  (IN/OUT)    none
//
///////////////////////////////////////////////////////////////////////////////
Trigger::Trigger( )
{
    m_nFd = -1;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   ~Trigger        (Destructor)
//
// Frees any allocated resources
//
// ARGUMENTS:
//  (IN/OUT)    none
//
///////////////////////////////////////////////////////////////////////////////
Trigger::~Trigger( )
{
    Close( );
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   Open
//
// Opens trigger device (normally /dev/crisp_trigger_n).
//
// ARGUMENTS:
//  (IN)        char*       Path to device
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
bool Trigger::Open( char *szPath )
{
    m_nFd = open( szPath, O_RDWR | O_NOCTTY | O_SYNC );

    if ( m_nFd >= 0 )
	{
        int nErr;
        nErr = Enable( false );
        if ( nErr != 0 )
        {
            Close( );
            return false;
        }

        if ( ioctl( m_nFd, TRIGGER_IOCTL_GET_VERSION, (uint8_t*) &m_nVersion ) != 0 )
        {
            Close( );
            return false;
        }
        return true;
    }
    return false;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   Close
//
// Closes device handle
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              none
//
///////////////////////////////////////////////////////////////////////////////
void Trigger::Close( )
{
    if ( m_nFd != -1 )
        close( m_nFd );
    m_nFd = -1;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   IsAvailable
//
// Returns true if device exists and is addressable/usable
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              bool    true if device is available and usable, else false
//
///////////////////////////////////////////////////////////////////////////////
bool Trigger::IsAvailable( )
{
    if ( m_nFd == -1 )
        return false;
    return true;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   Enable
//
// Enables/disables trigger.
//
// ARGUMENTS:
//  (IN)        bool        true to enable, false to disable
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int  Trigger::Enable( bool fEn )
{
    if ( IsAvailable( ))
    {
        uint8_t nEn = fEn ? 1 : 0;
        return ioctl( m_nFd, TRIGGER_IOCTL_ENABLE, (uint8_t*) &nEn );
    }
    return -ENODEV;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   GetVersion
//
// Returns current trigger board firmware version
//
// ARGUMENTS:
//  (OUT)        uint8_t&       Version (high nibble is major version, low 
//                              nibble is minor version).
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int  Trigger::GetVersion( uint8_t &nVersion)
{
    if ( IsAvailable( ))
    {
        nVersion = m_nVersion;
        return 0;
    }
    return -ENODEV;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   SetConfig
//
// Configures a trigger board
//
// ARGUMENTS:
//  (IN)        uint8_t     Modulation/trigger mode 
//                           (1=2-pulse, 2=2-long pulse, 3=tripple pulse & 
//                             4=field triggering).
//  (IN)        uint16_t    Intra pulse delay (32ms-1500ms)
//  (IN)        uint8_t     Output power (1-12). Only supported on latest

//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int  Trigger::SetConfig( uint8_t nMode, uint16_t nDelay, uint8_t nPower )
{
    int nErr = 0;
    bool fEn;

    if ( IsAvailable( ) == false )
        nErr = -ENODEV;

    fEn = m_fEn;

    if (( nErr == 0 ) && ( fEn ))
        nErr = Enable( false );
    if ( nErr == 0 )
        nErr = ioctl( m_nFd, TRIGGER_IOCTL_SET_MODE, (uint8_t*) &nMode );    
    if ( nErr == 0 )
        nErr = ioctl( m_nFd, TRIGGER_IOCTL_SET_DELAY, (uint16_t*) &nDelay );
    if ( nErr == 0 )
        nErr = ioctl( m_nFd, TRIGGER_IOCTL_SET_POWER, (uint8_t*) &nPower );
    if (( nErr == 0 ) && ( fEn ))
        nErr = Enable( true );
    return nErr;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   GetConfig
//
// Returns current trigger config
//
// ARGUMENTS:
//  (OUT)       uint8_t&    Trigger mode (1-4)
//  (OUT)       uint16_t&   Intra pulse delay (32-1500)
//  (OUT)       uint8_t&    Power (only supported by latest f/w)
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int  Trigger::GetConfig( uint8_t &nMode, uint16_t &nDelay, uint8_t &nPower )
{
    int nErr = 0;
    if ( IsAvailable( ) == false )
        nErr = -ENODEV;
    if ( nErr == 0 )
        nErr = ioctl( m_nFd, TRIGGER_IOCTL_GET_MODE, (uint8_t*) &nMode );
    if ( nErr == 0 )
        nErr = ioctl( m_nFd, TRIGGER_IOCTL_GET_DELAY, (uint16_t*) &nDelay );
    if ( nErr == 0 )
        nErr = ioctl( m_nFd, TRIGGER_IOCTL_GET_POWER, (uint8_t*) &nPower );
    return nErr;

}

//////////////////////////////////// EOF //////////////////////////////////////

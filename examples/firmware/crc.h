//FILE HEADER//////////////////////////////////////////////////////////////////
// 
// FILE NAME:   crc.h
//
// PROJECT:    Ice Qube (Next Gen)
//
// PLATFORM:    Texas Instruments CC1310 (ARM Cortex-M3)
//
// Interface to CRC functions for radio
// 
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis    
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Tehnology Limited or its suppliers
// All rights reserved.
// 
// This software is protected by national and international copyright and
// other laws. Unauthorised use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
// 
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Tehnology Limited.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRC_H__
#define __CRC_H__

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>

/////////////////////////////////////////////
// DEFINES
//

/////////////////////////////////////////////
// DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PROTOTYPES
//

//
// 16-bit crc operations
//
void crc16_init( void );
void crc16_reset( void );
void crc16_add( const uint8_t *szMessage, int32_t nBytes );
uint16_t crc16_get_crc( void );

/////////////////////////////////////////////
// PUBLIC DATA
//

#endif
//////////////////////////////////// EOF //////////////////////////////////////


// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   example.c
//
// PROJECT:     iceqube_icenetc_lib example
//
// PLATFORM:    crisp linux/arm/gcc
//
// Activate example
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "host_comms.h"
#include "types.h"
#include "crc.h"

/////////////////////////////////////////////
// PRIVATE DEFINES
//

#define MAX_RETRY                       3

//
// Non user-settable states
//
#define DEVICE_STATE_LOW_POWER_SLEEP    0x10
#define DEVICE_STATE_RADIO_TX_POWER     0x11
#define DEVICE_STATE_RADIO_TX_CW_POWER  0x12
#define DEVICE_STATE_RADIO_RX_POWER     0x13
#define DEVICE_STATE_POST_FAIL          0xFF

/////////////////////////////////////////////
// PRIVATE DATA TYPES AND CLASSES
//

/////////////////////////////////////////////
// PRIVATE PROTOTYPES
//

const char *state_to_string( uint8_t nState );

int host_example_load_firmware( char *szPath );

extern bool parse_int_param( 
                const char* szParam, 
                int *pVal, 
                int nArgv, 
                char **pArgc 
                );

extern bool parse_string_param( 
                const char* szParam, 
                char  *szBuffer,
                int    nMaxLen,
                int    nArgv, 
                char **pArgc            
                );

/////////////////////////////////////////////
// PUBLIC DATA
//

/////////////////////////////////////////////
// PRIVATE DATA
//

static uint8_t  sg_nFirmwareFile[ 0x1A000 ];
static uint32_t sg_nFirmwareSize = 0;
static uint16_t sg_nFirmwareVersionMin = 0;
static uint16_t sg_nFirmwareVersionMaj = 0;
static uint16_t sg_nFirmwareCrc = 0;
static bool     sg_fIsBootloader = false;

static bool     sg_fForce = 0;
static bool     sg_fDisableTimed = 0;

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_run
//
// Called by main.c to update the firmware of a connected device
//
// ARGUMENTS:
//  (IN)        uint32_t    nConnectedPeer
//
// RETURNS:
//              int         Error code
//
///////////////////////////////////////////////////////////////////////////////
int host_example_run( uint32_t nConnectedPeer )
{
    struct iceqube_device_state      hInfo;
    struct iceqube_firmware_start    hFileInfo;
    struct iceqube_firmware_state    hState;
    int                              nErr;
    uint16_t                         nVerMaj, nVerMin;
    uint32_t                         nRemaining;
    uint16_t                         nFwVersion;
    uint8_t                          q;
    uint32_t                         nPercent = 0;
    uint8_t                          nChunkRetry;
    bool                             fUptoDate = false;

    if ( sg_nFirmwareSize == 0 )
    {
        fprintf( stdout, "  ERROR: No firmware file specified or loader\n");
        return -1;
    }

    fprintf( stdout, "\n" );
    fprintf( stdout, "DEVICE ID:               %d\n", nConnectedPeer );

    //
    // Get device info
    //
    nErr = host_comms_get_device_info( &hInfo );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query device info %d\n", nErr );
        return nErr;
    }

    //
    // Display device info
    //
    fprintf( stdout, "  Device state:          %s\n", state_to_string( hInfo.nDeviceState ));
    fprintf( stdout, "  Current firmware:      %d.%03d\n", ( hInfo.nFwVersion >> 8 ), ( hInfo.nFwVersion & 0xFF ));   
    
    if (( hInfo.nDeviceState != DEVICE_STATE_IDLE ) &&
        ( hInfo.nDeviceState != DEVICE_STATE_FIRMWARE ))
    {
        fprintf( 
            stdout, 
            "  ERROR: Device cannot be updated if not idle. Current state is %s\n", 
            state_to_string( hInfo.nDeviceState ) 
            );
        return 0;
    }

    //
    // Check bootloader version
    //
    nErr = host_comms_get_bootloader_version( &nVerMaj, &nVerMin );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to query bootloader version %d\n", nErr );
        return nErr;
    }
    fprintf( stdout, "  Bootloader version:    %d.%03d\n", nVerMaj, nVerMin );

    nFwVersion = sg_nFirmwareVersionMaj & 0xF777;
    nFwVersion <<= 8;
    nFwVersion |= ( sg_nFirmwareVersionMin & 0xFF );

    if ( sg_fIsBootloader == false )
    {
        fprintf( stdout, "  New firmware:          %d.%03d\n", sg_nFirmwareVersionMaj, sg_nFirmwareVersionMin );  
        
        if ( hInfo.nFwVersion >= nFwVersion )
        {
            fUptoDate = true;
        }
    }
    else
    {
        uint16_t nBlVersion;

        fprintf( stdout, "  New bootloader:        %d.%03d\n", sg_nFirmwareVersionMaj & 0x7FFF, sg_nFirmwareVersionMin );

        nBlVersion = nVerMaj;
        nBlVersion <<= 8;
        nBlVersion |= nVerMin;

        if ( nBlVersion >= nFwVersion )
        {
            fUptoDate = true;
        }
    }

    //
    // Verify uploading firmware is not older
    //
       
    if ( fUptoDate )
    {
        if ( sg_fForce == false )
        {
            struct trigger_config       hTriggerConfig;

            if ( sg_fIsBootloader == false )
            {
                fprintf(
                    stdout,
                    "  ERROR: Device is up-to-date (version %d.%03d)\n",
                    (( hInfo.nFwVersion >> 8 ) & 0xFF ),
                    ( hInfo.nFwVersion & 0xFF )
                    );
            }
            else
            {
                fprintf(
                    stdout,
                    "  ERROR: Bootloader is up-to-date (version %d.%03d)\n",
                    nVerMaj,
                    nVerMin
                    );
            }

            //
            // Check if we need to disable timed triggering in idle
            // option -m=1
            //
            if ( sg_fDisableTimed )
            {
                //
                // Get current trigger settings
                //
                nErr = host_comms_get_trigger_config( &hTriggerConfig );
                if ( nErr != 0 )
                {
                    fprintf(
                        stdout,
                        "  ERROR: Failed to query trigger configuration %d\n",
                        nErr
                        );

                    return nErr;
                }

                //
                // Disable timed triggering in idle, if it's set
                //
                if ( hTriggerConfig.nTimedFlags & 1 )
                {
                    fprintf( stdout, "  Disabling timed trigger in idle\n" );
                    hTriggerConfig.nTimedFlags &= ~0x01;
                    hTriggerConfig.nStandoffIndex[ 0 ] = 0xFF;
                    hTriggerConfig.nStandoffIndex[ 1 ] = 0xFF;
                    hTriggerConfig.nStandoffIndex[ 2 ] = 0xFF;
                    hTriggerConfig.nStandoffIndex[ 3 ] = 0xFF;

                    nErr = host_comms_set_trigger_config( &hTriggerConfig );

                    if ( nErr != 0 )
                    {
                        fprintf( 
                            stdout,
                            "  ERROR: Failed to update trigger configuration %d\n", 
                            nErr 
                            );
                        return nErr;
                    }

                    fprintf( stdout, "  Timed triggering in IDLE deactivated\n" );
                }
            }
            return -1;
        }
        else
        {
            fprintf(
                stdout, 
                "  WARNING: Device firmware is newer (version %d.%03d)\n", 
                (( hInfo.nFwVersion >> 8 ) & 0xFF ),
                ( hInfo.nFwVersion & 0xFF )
                );

            if ( sg_fDisableTimed )
            {
                fprintf(
                    stdout,
                    "  WARNING: Unabled to disable timed triggering if -r is set\n"
                    );

            }
        }

    }

    if ( sg_fIsBootloader == false )
        fprintf( stdout, "  Starting firmware upload " );
    else
        fprintf( stdout, "  Starting bootloader upload " );

    //
    // Start firmware upload
    //
    hFileInfo.nStructVersion = STRUCT_ICEQUBE_FIRMWARE_START_VERSION;
    hFileInfo.nVersionMajor  = sg_nFirmwareVersionMaj; // BL upload identified by high bit set to 1
    hFileInfo.nVersionMinor  = sg_nFirmwareVersionMin;
    hFileInfo.nAppSize       = sg_nFirmwareSize;
    hFileInfo.nAppCrc        = sg_nFirmwareCrc;

    nErr = host_comms_start_firmware_upload( &hFileInfo, &hState );
    if ( nErr != 0 )
    {
        fprintf( stdout, "  ERROR: Failed to start upload %d\n", nErr );
        return nErr;
    }

    //
    // Print out initial status bar
    //
    fprintf( stdout, "\n\r  [" );
    for( q = 0; q < 20; q++ )
        fprintf( stdout, " " );
    fprintf( stdout, "] (0%%)     " );

    //
    // Upload the new firmware
    //
    nChunkRetry = host_comms_get_internal_retries( );
    while( true )
    {
        uint8_t nBlockProg;

        //
        // Get current position in code
        //
        nErr = host_comms_get_firmware_state( &hState );
        if ( nErr != 0 )
        {
            fprintf( stdout, "\n  ERROR: Failed to start upload %d\n", nErr );
            return nErr;
        }

        //
        // Loop the firmware file
        //
        nRemaining = sg_nFirmwareSize;
        nRemaining -= hState.nNextExpectedOffset;

        //
        // Update percentage display
        //
        nPercent = 100 - (( 100 * nRemaining ) / sg_nFirmwareSize );
        nBlockProg = nPercent / 5;
        fprintf( stdout, "\r  [" );
        for( q = 0; q < nBlockProg; q++ )
            fprintf( stdout, "#" );
        for( ; q < 20; q++ )
            fprintf( stdout, " " );
        fprintf( stdout, "] (%u%%)     ", nPercent );

        usleep( 1000 );

        while( nRemaining )
        {
            struct iceqube_firmware_block hBlock;
            hBlock.nStructVersion = STRUCT_ICEQUBE_FIRMWARE_BLOCK_VERSION;
	        hBlock.nBlockOffset   = hState.nNextExpectedOffset;
            hBlock.nBlockLen      = ( nRemaining > 512 ) ? 512 : nRemaining;
	        
            memcpy( 
                hBlock.hData, 
                &sg_nFirmwareFile[ hBlock.nBlockOffset ],
                hBlock.nBlockLen
                );

            //
            // Update display
            //
            nPercent = 100 - (( 100 * nRemaining ) / sg_nFirmwareSize );
            nBlockProg = nPercent / 5;
            fprintf( stdout, "\r  [" );
            for( q = 0; q < nBlockProg; q++ )
                fprintf( stdout, "#" );
            for( ; q < 20; q++ )
                fprintf( stdout, " " );
            fprintf( stdout, "] (%u%%)     ", nPercent );

            //
            // Upload f/w chunk
            //
            nErr = host_comms_upload_firmware_chunk( &hBlock );
            if ( nErr != 0 )
            {
                nChunkRetry --;
                if ( nChunkRetry == 0 )// Sequence error
                {

                    fprintf( 
                        stdout, 
                        "\n  ERROR: Failed to upload block at offset %d, error %d\n", 
                        hBlock.nBlockOffset, 
                        nErr 
                        );
                    return nErr;
                }
                break;
            }            

            nRemaining -= hBlock.nBlockLen;
            hState.nNextExpectedOffset += hBlock.nBlockLen;

            nChunkRetry = host_comms_get_internal_retries( );
        }

        //
        // Done?
        // 
        if ( nRemaining == 0 )
            break;
    }
    fprintf( stdout, "\r  [####################] (100%%)     \n" );
    fprintf( stdout, "  SUCCESS: Device updated.\n" );
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   state_to_string
//
// Utility function to convert a device state into a string
//
// ARGUMENTS:
//  (IN)        uint8_t         Device state
//
// RETURNS:
//              const char*     Device state string
//
///////////////////////////////////////////////////////////////////////////////
const char *state_to_string( uint8_t nState )
{
    switch( nState )
    {
    case DEVICE_STATE_IDLE:
        return "IDLE";
    case DEVICE_STATE_ACTIVE:
        return "ACTIVE";
    case DEVICE_STATE_FIRMWARE:
        return "FIRMWARE";
    case DEVICE_STATE_LOW_POWER_SLEEP:
    case DEVICE_STATE_RADIO_TX_POWER:
    case DEVICE_STATE_RADIO_TX_CW_POWER:
    case DEVICE_STATE_RADIO_RX_POWER:
        return "TEST STATE";
    case DEVICE_STATE_POST_FAIL:
        return "POST FAILURE STATE";
    default:
        break;
    }
    return "UNKNOWN";
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_get_param_count
//
// Returns number of command line parameters supported by example
//
// ARGUMENTS:
//  (IN/OUT)    none
//
// RETURNS:
//              int     Parameter count
//
///////////////////////////////////////////////////////////////////////////////
int host_example_get_param_count( void )
{
    return 3;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_parse_params
//
// Passes a command line parameter for parsing. 'p', 'i', 's', 'r' & 'l' are
// reserved by the application.
//
// ARGUMENTS:
//  (IN)        char*   Command line
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_parse_params( char *pParam )
{
    char szStr[ 255 ];
    int  n;

    if ( parse_string_param( "-f=", szStr, 255, 1, &pParam ))
    {
        if ( host_example_load_firmware( szStr ) != 0 )
        {
            sg_nFirmwareSize = 0;                
            return -1;
        }            
		return 0;
    }

    if ( parse_int_param( "-F=", &n, 1, &pParam ))
    {
        if ( n == 0 )
            sg_fForce = 0;
        else
            sg_fForce = 1;
		return 0;
    }

    if ( parse_int_param( "-m=", &n, 1, &pParam ) )
    {
        if ( n == 0 )
            sg_fDisableTimed = 0;
        else
            sg_fDisableTimed = 1;
        return 0;
    }

    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_print_param_help
//
// Prints help string for parameters
//
// ARGUMENTS:
//  (IN)        FILE*   Stream to write help message to
//  (IN)        int     Index of parameter to print help message for
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_print_param_help( FILE *pStream, int nIndex )
{
    switch( nIndex )
    {
    case 0:
        fprintf( pStream, "\t-f=<path>       Path to loadable firmware file\n" );
        break;
    case 1:
        fprintf( pStream, "\t-F=[0-1]        Force update, even if device fw is newer (default 0)\n" );
        break;
    case 2:
        fprintf( pStream, "\t-m=[0-1]        Disable timed trigger in idle, if fw is up-to-date (default 0)\n" );
        break;

    default:
        break;
    }
    return 0;
}

// FUNCTION HEADER//////////////////////////////////////////////////////////////
//
// FUNCTION NAME:   host_example_load_firmware
//
// Loads in a firmware file
//
// ARGUMENTS:
//  (IN)        char*   Path of firmware file
//
// RETURNS:
//              int     0 on success, else error
//
///////////////////////////////////////////////////////////////////////////////
int host_example_load_firmware( char *szPath )
{
	FILE       *pFile;
	uint8_t    *pCurBuffer;
	int         nToRead, i;
	bool        fFound;
	struct stat hSt;

    //
    // Reset content
    //
    sg_nFirmwareSize = 0;
    sg_nFirmwareVersionMin = 0;
    sg_nFirmwareVersionMaj = 0;
    sg_nFirmwareCrc = 0;

	//
	// Get the file size
	//
	if ( stat( szPath, &hSt ) != 0)
    {
        fprintf( stdout, "Failed to find file %s\n", szPath );
		return -1;
    }

	//
	// Verify file is not too large
	//
	if ( hSt.st_size > 0x1A000 )
	{
        fprintf( stdout, "Firmware file %s too big\n", szPath );
		return -1;
	}

	//
	// Open the file
	//
	pFile = fopen( szPath, "rb" );
	if ( pFile == NULL )
	{
		fprintf( stdout, "Failed to open file %s\n", szPath );
		return -1;
	}

	//
	// Read the file in 2048 byte chunks
	//
	nToRead    = hSt.st_size;
	pCurBuffer = sg_nFirmwareFile;

    sg_nFirmwareSize = hSt.st_size;

	while ( nToRead )
	{
		size_t nChunk = ( nToRead > 2048 ) ? 2048 : nToRead;

		if ( fread( pCurBuffer, 1, nChunk, pFile ) != nChunk)
		{
            sg_nFirmwareSize = 0;
			fprintf( stdout, "Failed to read firmware file %s\n", szPath );
			fclose(pFile);
			return -1;
		}
		pCurBuffer += nChunk;
		nToRead    -= nChunk;
	}

	fclose( pFile );

	//
	// Find the ID strings
	//
	fFound	   = 0;
	pCurBuffer = sg_nFirmwareFile;
	nToRead    = hSt.st_size - 28; 

    //
	// Loop through the whole file and look for version strings
    //
	for (i = 0; i < hSt.st_size; i ++, pCurBuffer++)
	{
		if ((pCurBuffer[0 ] == '[') &&
		    (pCurBuffer[1 ] == 'I') &&
            (pCurBuffer[2 ] == 'C') &&
            (pCurBuffer[3 ] == 'E') &&
            (pCurBuffer[4 ] == 'Q') &&
            (pCurBuffer[5 ] == 'U') &&
            (pCurBuffer[6 ] == 'B') &&
            (pCurBuffer[7 ] == 'E') &&
            (pCurBuffer[8 ] == ']') &&
            (pCurBuffer[9 ] == ' ') &&
            (pCurBuffer[10] == '[') &&
            (pCurBuffer[11] == 'b') &&
            (pCurBuffer[12] == 'u') &&
            (pCurBuffer[13] == 'i') &&
            (pCurBuffer[14] == 'l') &&
            (pCurBuffer[15] == 'd') &&
            (pCurBuffer[16] == ' '))
        {
            char szVersionStr[10];            
            char *pMaj;
            char *pMin;
            uint8_t i;

            memcpy( szVersionStr, &pCurBuffer[17], 10 );
            pMaj = szVersionStr;
            for( i = 0; i < 9; i ++ )
            {
                if ( szVersionStr[i] == '.' )
                {
                    pMin = &szVersionStr[i + 1];
                    szVersionStr[i] = 0;

                    fFound = 1;
                    sg_nFirmwareVersionMin = atoi( pMin );
                    sg_nFirmwareVersionMaj = atoi( pMaj );
                    sg_fIsBootloader = false;
                    break;
                }
            }

            if ( fFound )
    			break;
		}
        else if (( pCurBuffer[ 0 ] == '[' ) &&
            ( pCurBuffer[ 1 ] == 'B' ) &&
            ( pCurBuffer[ 2 ] == 'O' ) &&
            ( pCurBuffer[ 3 ] == 'O' ) &&
            ( pCurBuffer[ 4 ] == 'T' ) &&
            ( pCurBuffer[ 5 ] == 'L' ) &&
            ( pCurBuffer[ 6 ] == 'O' ) &&
            ( pCurBuffer[ 7 ] == 'A' ) &&
            ( pCurBuffer[ 8 ] == 'D' ) &&
            ( pCurBuffer[ 9 ] == 'E' ) &&
            ( pCurBuffer[ 10 ] == 'R' ) &&
            ( pCurBuffer[ 11 ] == ']' ) &&
            ( pCurBuffer[ 12 ] == ' ' ) &&
            ( pCurBuffer[ 13 ] == '[' ) &&
            ( pCurBuffer[ 14 ] == 'b' ) &&
            ( pCurBuffer[ 15 ] == 'u' ) &&
            ( pCurBuffer[ 16 ] == 'i' ) &&
            ( pCurBuffer[ 17 ] == 'l' ) &&
            ( pCurBuffer[ 18 ] == 'd' ) &&
            ( pCurBuffer[ 19 ] == ' ' ) )
        {
            char szVersionStr[ 10 ];
            char *pMaj;
            char *pMin;
            uint8_t i;

            memcpy( szVersionStr, &pCurBuffer[ 20 ], 10 );
            pMaj = szVersionStr;
            for ( i = 0; i < 9; i++ )
            {
                if ( szVersionStr[ i ] == '.' )
                {
                    pMin = &szVersionStr[ i + 1 ];
                    szVersionStr[ i ] = 0;

                    fFound = 1;
                    // Add 0x8000 onto the major version to tell the
                    // application that it's a bootloader
                    sg_nFirmwareVersionMin = atoi( pMin );
                    sg_nFirmwareVersionMaj = atoi( pMaj ) | 0x8000;
                    sg_fIsBootloader = true;
                    if ( hSt.st_size > 0x5000 )
                    {
                        fprintf( stdout, "Bootloader file %s too big\n", szPath );
                        return -1;
                    }
                    break;
                }
            }

            if ( fFound )
                break;
        }
	}

	//
	// Invalid firmware file
	//
	if (fFound == 0)
	{
        sg_nFirmwareSize = 0;
        fprintf( stdout, "Error parsing file. No valid device version info found.\r\n");
		return -1;
	}

    crc16_init(  );
    crc16_add( sg_nFirmwareFile, sg_nFirmwareSize );
    sg_nFirmwareCrc = crc16_get_crc( );

	return 0;
}

//////////////////////////////////// EOF //////////////////////////////////////

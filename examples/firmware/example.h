// FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   example.h
//
// PROJECT:     iceqube_icenetc_lib example
//
// PLATFORM:    crisp linux/arm/gcc
//
// Defines example interface
//
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//      OL      Oliver Lewis
//
/////////////////////////////////////////////
//
// Copyright (c) 2016-2017 Peacock Technology Ltd or its suppliers. All
// rights reserved.
//
// This software is protected by national and international copyright and
// other laws. Unauthorized use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
//
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Peacock Technology Ltd.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __EXAMPLE_H__
#define __EXAMPLE_H__

/////////////////////////////////////////////
// INCLUDE FILES
//

#include <stdint.h>

/////////////////////////////////////////////
// DEFINES
//

//
// Example description
//
#define EXAMPLE_FN                      "FIRMWARE UPLOAD"

/////////////////////////////////////////////
// DATA TYPES
//

/////////////////////////////////////////////
// DATA DECLARATIONS
//

/////////////////////////////////////////////
// PROTOTYPES
//

//
// Example interface
//
int host_example_run( uint32_t nConnectedPeer );

//
// Additional Command Line parameters
//
int host_example_get_param_count( void );
int host_example_parse_params( char *pParam );
int host_example_print_param_help( FILE *pStream, int nIndex );

/////////////////////////////////////////////
// CLASSES
//

#endif // !__HOST_PORT_H__

//////////////////////////////////// EOF //////////////////////////////////////
